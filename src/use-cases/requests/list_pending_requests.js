module.exports = function listPendingGatePass(
  { gatePassDb },
  { encryption },
  { departmentsDB },
  { teamsDB },
  { admin_db },
  { requestTypesDB },
  { employees_db }
) {
  return async function makeListPendingGatePass(
    roleParams,
    employee_id,
    dateRange,
    moment
  ) {

    const { request_date_from, request_date_to } = dateRange;
    if (request_date_from && request_date_to) {
      if (!isNaN(request_date_from)) {
        throw new Error('Invalid request date from');
      }

      if (!isNaN(request_date_to)) {
        throw new Error('Invalid request date to');
      }
      if (!moment(request_date_from).isValid()) {
        throw new Error('Invalid request date from');
      }
      if (!moment(request_date_to).isValid()) {
        throw new Error('Invalid request date to');
      }
    }

    let listGatePass;
    // get request types
    let listRequestTypes = await requestTypesDB.listRequestTypes();

    //get departments
    let listDepartments = await departmentsDB.getDepartments();


    let isDepartmentHead = [];
    let isTeamHead;
    let DepartmentID = [];
    let teamId = [];

    //get gate passess depending on employee role (pg/admin/team head/dept head)
    if (roleParams === 'pg' || roleParams === 'admin' || roleParams === 'emp') {
      listGatePass = await gatePassDb.listPendingGatePass(
        employee_id,
        dateRange
      );
    } else if (roleParams === 'dh' || roleParams === 'pdh') {
      //check if employee is DH then get department data

      isDepartmentHead = await departmentsDB.getDepartmentsbyEmployeeId(employee_id);

      if (isDepartmentHead.length === 0) {
        throw new Error('Employee is not department head');
      }

      for (let index = 0; index < isDepartmentHead.length; index++) {
        DepartmentID.push(isDepartmentHead[index].department_id)
      }


      listGatePass = await gatePassDb.listPendingGatePassByDepartment(
        employee_id,
        dateRange
      );

    } else if (roleParams === 'th' || roleParams === 'pa') {
      //check if employee is TH then get team data
      isTeamHead = await admin_db.getTeamid(employee_id);
      if (!isTeamHead) {
        throw new Error('Employee is not team head');
      }

      for (let index = 0; index < isTeamHead.length; index++) {
        teamId.push(isTeamHead[index].team_id)
      }

      listGatePass = await gatePassDb.listPendingGatePassNotNullTeamId(
        employee_id,
        dateRange
      );
    }


    if (!listRequestTypes.length) {
      listRequestTypes = 'No gate pass request types found';
    }

    if (!listDepartments.length) {
      listDepartments = 'No departments found';
    }


    if (!listGatePass.length) {
      listGatePass = 'No data found';
    } else {
      if (roleParams == 'pdh') {
        //remove passes with null postion/team/department
        listGatePass = listGatePass.filter(
          employee =>
            employee.position !== null &&
            employee.position.team !== null &&
            employee.position.team.department !== null
        );

        let coApprovers = []
        for (ids of DepartmentID) {
          let data = await departmentsDB.getCoApprovers(ids)
          for (d of data) {

            coApprovers.push(d.employee_id)
          }
        }

        //filter data by department id
        listGatePass = listGatePass.filter(
          employee => DepartmentID.includes(employee.position.team.department.id) &&
            !coApprovers.includes(employee.id)
        );

      } else if (isDepartmentHead.length > 0) {
        //remove passes with null postion/team/department
        listGatePass = listGatePass.filter(
          employee =>
            employee.position !== null &&
            employee.position.team !== null &&
            employee.position.team.department !== null
        );

        let coApprovers = []
        for (ids of DepartmentID) {
          let data = await departmentsDB.getCoApprovers(ids)
          for (d of data) {
            const empData = await employees_db.simplefindById(d.employee_id)
            if (empData.role.name !== 'mng-Proxy Approver') {
              coApprovers.push(d.employee_id)
            }
          }
        }

        isTeamHead = await admin_db.getTeamid(employee_id);
        for (let index = 0; index < isTeamHead.length; index++) {
          teamId.push(isTeamHead[index].team_id)
        }

        //filter data by department id
        listGatePass = listGatePass.filter(
          employee => DepartmentID.includes(employee.position.team.department.id) &&
            !coApprovers.includes(employee.id) ||
            teamId.includes(employee.position.team.id)
        );
      } else if (isTeamHead) {
        //remove data with null postion/team

        listGatePass = listGatePass.filter(
          employee =>
            employee.position !== null && employee.position.team !== null
        );

        // let coApprovers = []
        // for(ids of teamId){
        //   let data = await teamsDB.getCoApprovers(ids)
        //   for(d of data){
        //    coApprovers.push(d.employee_id)
        //   }
        // }

        //filter data by team id
        listGatePass = listGatePass.filter(
          employee => teamId.includes(employee.position.team_id) &&
            (employee.role.name.split('-')[0] === 'emp' || employee.role.name.split('-')[0] === 'th')
            && employee.payroll_group === 'office_payroll'
        );


      }
      //Decrypt employees data
      listGatePass.map(employee => {
        employee.first_name = encryption.decrypt(employee.first_name);
        employee.last_name = encryption.decrypt(employee.last_name);
        if (employee.middle_name) {
          employee.middle_name = encryption.decrypt(employee.middle_name);
        }

      });


      if (roleParams === 'pg' || roleParams === 'admin') {

        //Decrypt gate pass logs if exists
        listGatePass.forEach(employee => {

          if (employee.gate_passes) {

            employee.gate_passes.forEach(gatepass =>

              gatepass.gate_pass_status_logs.forEach(log => {
                {
                  if (log.employee) {
                    log.employee.first_name = encryption.decrypt(
                      log.employee.first_name
                    );
                    log.employee.last_name = encryption.decrypt(
                      log.employee.last_name
                    );
                    if (log.employee.middle_name) {
                      log.employee.middle_name = encryption.decrypt(
                        log.employee.middle_name
                      );
                    }
                  }
                }

              }

              )
            )
          }
        })


        let withDeductions = [];
        let withoutDeductions = [];
        listGatePass.map(employee => {


          employee.gate_passes.map(gatePass => {


            if (gatePass.request_type.name.toLowerCase() === 'lunch out' && employee.payroll_group === 'manager_payroll') {

              withDeductions.push({ ...employee.get(), gate_passes: [{ ...gatePass.get() }] })
            }
            else if (gatePass.request_type.name.toLowerCase() === 'personal out') {
              withDeductions.push({ ...employee.get(), gate_passes: [{ ...gatePass.get() }] })
            } else {
              withoutDeductions.push({ ...employee.get(), gate_passes: [{ ...gatePass.get() }] })
            }

          })


        })

        listGatePass = { withDeductions, withoutDeductions }
      }





    }


    return { listGatePass, listRequestTypes, listDepartments };
  };
};
