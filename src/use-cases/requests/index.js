const makeApprovedGatePassList = require('./list_approved_requests');
const makePendingGatePass = require('./list_pending_requests');
const makeDisapprovedGatePass = require('./list_disapproved_requests');

//Data-access
const gatePassDb = require('../../data-access/gate_pass/');
const requestTypesDB = require('../../data-access/request_types/');
const departmentsDB = require('../../data-access/departments/');
const teamsDB = require('../../data-access/teams');
const admin_db = require('../../data-access/admin');
const employees_db = require('../../data-access/employees')

//Encryption
const encryption = require('../../encryption/encrypting');

const listApprovedGatePass = makeApprovedGatePassList(
  { gatePassDb },
  { encryption },
  { departmentsDB },
  { teamsDB },
  { admin_db },
  { requestTypesDB },
  { employees_db }
);

const listPendingGatePass = makePendingGatePass(
  { gatePassDb },
  { encryption },
  { departmentsDB },
  { teamsDB },
  { admin_db },
  { requestTypesDB },
  { employees_db }
);

const listDisapprovedGatePass = makeDisapprovedGatePass(
  { gatePassDb },
  { encryption },
  { departmentsDB },
  { teamsDB },
  { admin_db },
  { requestTypesDB },
  { employees_db }
);

const requestService = Object.freeze({
  listApprovedGatePass,
  listPendingGatePass,
  listDisapprovedGatePass
});

module.exports = requestService;
module.exports = {
  listApprovedGatePass,
  listPendingGatePass,
  listDisapprovedGatePass
};
