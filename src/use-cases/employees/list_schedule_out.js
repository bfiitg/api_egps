module.exports = function listScheduleOutReport({ gatePassDb },{ encryption }) {
  return async function makeListScheduleOutReport(employee_id) {
    const scheduleOut = await gatePassDb.listScheduleOut(employee_id);

    scheduleOut.map(employee => {
      
      if(employee.updater){
      employee.updater.first_name = encryption.decrypt(employee.updater.first_name);
      employee.updater.last_name = encryption.decrypt(employee.updater.last_name);
      if (employee.updater.middle_name) {
        employee.updater.first_namemiddle_name = encryption.decrypt(employee.updater.middle_name);
      }
    }

    });

    if (!scheduleOut.length) {
      return 'No schedule out found';
    } else {
      //delete employee data
      // scheduleOut.map(el => delete el.dataValues.employee);

      return scheduleOut;
    }
  };
};
