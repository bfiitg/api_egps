module.exports = function listPersonalGatePass(
  { gatePassDb },
  { departmentsDB },
  { requestTypesDB },
  { employeesDb },
  { encryption }
) {
  return async function makeListPersonalGatePass(
    employee_id,
    dateRange,
    moment
  ) {
    const { request_date_from, request_date_to } = dateRange;
    if (request_date_from && request_date_to) {
      if (!isNaN(request_date_from)) {
        throw new Error('Invalid request date from');
      }

      if (!isNaN(request_date_to)) {
        throw new Error('Invalid request date to');
      }
      if (!moment(request_date_from).isValid()) {
        throw new Error('Invalid request date from');
      }
      if (!moment(request_date_to).isValid()) {
        throw new Error('Invalid request date to');
      }
    }

    let employee = await employeesDb.findById(employee_id);
    //Get gate passes
    let listGatePass = await gatePassDb.getPersonalGatePass(
      employee_id,
      dateRange
    );

    //Get request types
    let listRequestTypes = await requestTypesDB.listRequestTypes();
    //Get departments
    let listDepartments = await departmentsDB.getDepartments();

    if (!listRequestTypes.length) {
      listRequestTypes = 'No gate pass request types found';
    }

    if (!listDepartments.length) {
      listDepartments = 'No departments found';
    }

    if (!listGatePass.length) {
      listGatePass = 'No data found';
    } else {
      //Get gate passes elapsed in hour
      listGatePass.map(gate_pass => {

        if (gate_pass.request_type.name === 'Personal Out' ||
          employee.is_offsite ||
          (gate_pass.request_type.name === 'Lunch Out'
            && employee.payroll_group === 'manager_payroll')) {

          let elapsed_time =
            (moment(
              new Date(`${gate_pass.actual_time_in} ${gate_pass.request_date}`)
            ) -
              moment(
                new Date(
                  `${gate_pass.approval_time} ${gate_pass.request_date}`
                )
              )) /
            3600000;

          elapsed_time = elapsed_time.toString();
          if (elapsed_time.indexOf('.') > 0) {
            elapsed_time = elapsed_time.slice(0, elapsed_time.indexOf('.') + 3);
          }
          gate_pass.get().elapsed_time = elapsed_time;

        } else {


          let elapsed_time =
            (moment(
              new Date(`${gate_pass.actual_time_in} ${gate_pass.request_date}`)
            ) -
              moment(
                new Date(
                  `${gate_pass.actual_time_out} ${gate_pass.request_date}`
                )
              )) /
            3600000;

          elapsed_time = elapsed_time.toString();
          if (elapsed_time.indexOf('.') > 0) {
            elapsed_time = elapsed_time.slice(0, elapsed_time.indexOf('.') + 3);
          }
          gate_pass.get().elapsed_time = elapsed_time;

        }


      });

      //Decrypt approved/disapproved by if exists
      listGatePass.map(gatepass => {
        gatepass.gate_pass_status_logs.forEach(log => {
          {
            if (log.employee) {
              log.employee.first_name = encryption.decrypt(
                log.employee.first_name
              );
              log.employee.last_name = encryption.decrypt(
                log.employee.last_name
              );
              if (log.employee.middle_name) {
                log.employee.middle_name = encryption.decrypt(
                  log.employee.middle_name
                );
              }
            }
          }
        });
      });
    }


    return { listGatePass, listDepartments, listRequestTypes };
  };
};
