module.exports = function updatePassword({ employeesDb }, { encryption }) {
  return async function makeUpdatePassword({ id, password } = {}) {
    if (!id) {
      throw new Error('Employee id must be provided');
    }
    if (isNaN(id)) {
      throw new Error('Employee id must be a number');
    }
    if (!password) {
      throw new Error('Password must be provided');
    }

    const result = await employeesDb.updateProfile(
      id,
      encryption.encrypt(password)
    );

    if (result[0] > 0) {
      return 'Successfully updated';
    } else {
      throw new Error('Update failed');
    }
  };
};
