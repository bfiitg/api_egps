//Encryption
const encryption = require('../../encryption/encrypting');

const makeScheduleOutReport = require('./list_schedule_out');
const makePersonalReport = require('./list_employee_report');
const makeEmployeeProfile = require('./view_employee_profile');
const makeUpdateProfile = require('./update_employee_password');
const makeViewEmployeeByDept = require('./view_employeesbydept');

//Data access
const gatePassDb = require('../../data-access/gate_pass');
const departmentsDB = require('../../data-access/departments');
const requestTypesDB = require('../../data-access/request_types');
const employeesDb = require('../../data-access/employees');
const admin_db = require('../../data-access/admin')

const listScheduleOut = makeScheduleOutReport({ gatePassDb }, { encryption });
const listPersonalReport = makePersonalReport(
  { gatePassDb },
  { departmentsDB },
  { requestTypesDB },
  { employeesDb },
  { encryption }
);
const getEmployeeProfile = makeEmployeeProfile({ employeesDb }, { encryption });
const updateProfile = makeUpdateProfile({ employeesDb }, { encryption });
const viewEmployeeByDept = makeViewEmployeeByDept({ employeesDb }, { encryption }, { admin_db });

const employeeService = Object.freeze({
  listScheduleOut,
  listPersonalReport,
  getEmployeeProfile,
  updateProfile,
  viewEmployeeByDept
});

module.exports = employeeService;
module.exports = {
  listScheduleOut,
  listPersonalReport,
  getEmployeeProfile,
  updateProfile,
  viewEmployeeByDept
};
