module.exports = function viewEmployeeProfile({ employeesDb }, { encryption }) {
  return async function makeViewEmployeeProfile(id) {
    if (!id) {
      throw new Error('Employee id must be provided');
    }
    if (isNaN(id)) {
      throw new Error('Employee id must be a number');
    }

    const result = await employeesDb.viewEmployeeProfile(id);
    if (result) {
      //Decrypt employee data
      result.first_name = encryption.decrypt(result.first_name);
      if (result.middle_name) {
        result.middle_name = encryption.decrypt(result.middle_name);
      }
      result.last_name = encryption.decrypt(result.last_name);
      if (result.email) result.email = encryption.decrypt(result.email);

      return result;
    } else {
      return 'Employee does not exists';
    }
  };
};
