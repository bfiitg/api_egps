module.exports = function viewEmployeeByDept({ employeesDb }, { encryption }, { admin_db }) {
    return async function makeViewEmployeeByDept(request_info) {
        let positions = await admin_db.listAllPositions();
        let roles = await admin_db.listAllRoles();
        let departments = await admin_db.listAllDepartments();

        role_th = await admin_db.findRoleByName("th-Team Head");
        role_mngpa = await admin_db.findRoleByName("mng-Proxy Approver");

        if (!role_th || !role_mngpa) {
            throw new Error("Cant find role.")
        }
        role_id_th = role_th.id;
        role_id_mngpa = role_mngpa.id;

        if (!role_id_th || !role_id_mngpa) {
            throw new Error("Role ID of TH is null.")
        }

        if (request_info.roleParam == "th" || request_info.roleParam == "pdh") {
            // Get team_id of user
            const teamsOnhead = await admin_db.getTeamid(request_info.id)
            let empList = [];
            const empToPush = [];
            const empToPush2 = [];

            for (let index = 0; index < teamsOnhead.length; index++) {
                // Get team_id of each team head
                const team_id = teamsOnhead[index].team_id;
                // Get employees by team_id
                const employees = await employeesDb.getEmployeesByTeam(team_id, request_info.id);

                for (let index = 0; index < employees.length; index++) {
                    // Check if employee is already a department head/ team head
                    const isDepthead = await admin_db.getDepartmentid(employees[index].id);

                    // If false, add to employee list
                    if (isDepthead.length == 0 && employees[index].role_id != role_id_th) {
                        employees[index].first_name = encryption.decrypt(employees[index].first_name);
                        if (employees[index].middle_name) {
                            employees[index].middle_name = encryption.decrypt(employees[index].middle_name);
                        }
                        employees[index].last_name = encryption.decrypt(employees[index].last_name);
                        if (employees[index].email) employees[index].email = encryption.decrypt(employees[index].email);

                        const isApprover = await admin_db.findIfHeadExists(team_id, employees[index].id);
                        employees[index].dataValues.isHead = true;

                        // Check if employee is already a proxy approver
                        if (isApprover.length == 0) {
                            employees[index].dataValues.approver = false;
                        } else {
                            employees[index].dataValues.approver = true;
                        }
                        // Push to employee list
                        empToPush.push(employees[index])

                    }
                }

            }
            // Final list of employees
            empList.push(empToPush);
            if (empList) {
                //Decrypted employee data
                return { employees: empList, positions, roles, departments };
            } else {
                return 'Employee does not exists';
            }
        } else if (request_info.roleParam == "dh") {
            // Get department_id of user
            const DeptsOnhead = await admin_db.getDepartmentid(request_info.id);
            let empList = [];
            const empToPush = [];
            const empToPush2 = [];

            for (let index = 0; index < DeptsOnhead.length; index++) {
                // Get department ID of each departments which employee is a head
                const department_id = DeptsOnhead[index].department_id;
                const employees = await employeesDb.getEmployeesByDepartment(department_id, request_info.id);

                for (let index = 0; index < employees.length; index++) {
                    // Check if employee is a teamhead
                    const isTeamhead = await admin_db.getTeamid(employees[index].id);

                    // If employee is a teamhead, add to list
                    // if (isTeamhead.length != 0 && (employees[index].role_id == role_id_th || employees[index].role_id == role_id_mngpa)) {
                    employees[index].first_name = encryption.decrypt(employees[index].first_name);
                    if (employees[index].middle_name) {
                        employees[index].middle_name = encryption.decrypt(employees[index].middle_name);
                    }
                    employees[index].last_name = encryption.decrypt(employees[index].last_name);
                    if (employees[index].email) employees[index].email = encryption.decrypt(employees[index].email);
                    const isApprover = await admin_db.findIfDHeadExists(department_id, employees[index].id);

                    // Check if employee is an approver
                    if (isApprover.length == 0) {
                        employees[index].dataValues.approver = false;
                    } else {
                        employees[index].dataValues.approver = true;
                    }

                    // Check if employee is a team_head
                    if (isTeamhead.length != 0 && (employees[index].role_id == role_id_th || employees[index].role_id == role_id_mngpa)) {
                        employees[index].dataValues.isHead = true;
                        empToPush.push(employees[index])
                    } else {
                        employees[index].dataValues.isHead = false;
                        empToPush2.push(employees[index])

                    }
                    // Push to list of employees
                    // }
                }

            }
            // Final list of employees
            empList.push(empToPush.concat(empToPush2));

            if (empList) {
                //Decrypt employee data
                return { employees: empList, positions, roles, departments };
            } else {
                return 'Employee does not exists';
            }
        }


    };
};
