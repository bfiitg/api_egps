const makeApproveRequest = require('./approve_request');
const makeDisapproveRequest = require('./disapprove_request');
const makeVerifyGatePass = require('./verify_personal_out');
const makeUnverifyGatePass = require('./unverify_personal_out');
const makePostRequest = require('./file_request');
const makeListNotifications = require('./list_notification');
const makeReadNotifications = require('./update_notifcation');
const makeCancelRequest = require('./cancel_request');
const makeCorrectRequest = require('./correct_gate_pass');
const makeImportGatePass = require('./import_gate_pass');
const makeProcessGatePass = require('./process_verified');

//Data Access
const gatePassDb = require('../../data-access/gate_pass/');
const requestTypeDb = require('../../data-access/request_types');
const gatePassStatusLogsDb = require('../../data-access/gate_pass_status_logs');
const employeesDb = require('../../data-access/employees');

//Moment
const moment = require('moment');

const requestGatepass = makePostRequest(
  { gatePassDb },
  { requestTypeDb },
  { moment }
);
const approve_request = makeApproveRequest(
  { gatePassDb },
  { gatePassStatusLogsDb },
  { employeesDb }
);
const disapprove_request = makeDisapproveRequest(
  { gatePassDb },
  { gatePassStatusLogsDb }
);
const verify_gate_pass = makeVerifyGatePass(
  { gatePassDb },
  { gatePassStatusLogsDb }
);

const process_gate_pass = makeProcessGatePass(
  { gatePassDb },
  { gatePassStatusLogsDb }
);

const unverify_gate_pass = makeUnverifyGatePass(
  { gatePassDb },
  { gatePassStatusLogsDb }
);
const cancel_gate_pass = makeCancelRequest(
  { gatePassDb },
  { gatePassStatusLogsDb }
);

const correct_gate_pass = makeCorrectRequest(
  { gatePassDb },
  { gatePassStatusLogsDb },
  { moment },
  { requestTypeDb }
);
const import_gate_pass = makeImportGatePass(
  { gatePassDb },
  { requestTypeDb },
  { gatePassStatusLogsDb },
  { moment }
);



//Notification
const listNotifications = makeListNotifications({ gatePassDb });
const readNotifications = makeReadNotifications({ gatePassDb });

const gatePassService = Object.freeze({
  requestGatepass,
  approve_request,
  disapprove_request,
  verify_gate_pass,
  unverify_gate_pass,
  cancel_gate_pass,
  listNotifications,
  readNotifications,
  correct_gate_pass,
  import_gate_pass,
  process_gate_pass
});

module.exports = gatePassService;
module.exports = {
  requestGatepass,
  approve_request,
  approve_request,
  disapprove_request,
  verify_gate_pass,
  unverify_gate_pass,
  cancel_gate_pass,
  listNotifications,
  readNotifications,
  correct_gate_pass,
  import_gate_pass,
  process_gate_pass
};
