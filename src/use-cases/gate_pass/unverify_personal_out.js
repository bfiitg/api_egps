module.exports = function makeUnerifyGatePass(
  { gatePassDb },
  { gatePassStatusLogsDb }
) {
  return async function unVerifiyGatePass(id, status, employee_id) {
    if (!id) {
      throw new Error('Gate pass id must have a value');
    }
    if (isNaN(id)) {
      throw new Error('Gate pass id must ba a number');
    }

    if (!status) {
      throw new Error('Status must have a value');
    }
    if (!isNaN(status)) {
      throw new Error('Status must be a string');
    }

    if (status !== 'Time In') {
      throw new Error('Invalid status');
    }

   const gatePass = await gatePassDb.findGatePassById(id);
   if(gatePass.status === 'Processed'){
    throw new Error('Cannot unverify processed gate pass');

   }

    const result = await gatePassDb.updateRequest(id, status, employee_id);
    if (result.length > 0) {
      const createLog = await gatePassStatusLogsDb.insertLog({
        gate_pass_id: id,
        status,
        employee_id
      });
      return createLog;
    } else {
      throw new Error('Update failed');
    }
  };
};
