module.exports = function readNotification({ gatePassDb }) {
  return async function makeReadNotifcation(employee_id) {
    const readNotifications = await gatePassDb.updateNotifications(employee_id);
    if (readNotifications[0] > 0) {
      return 'Success';
    } else {
      throw new Error('Update failed');
    }
  };
};
