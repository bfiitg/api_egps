module.exports = function makeProcessGatePass(
    { gatePassDb },
    { gatePassStatusLogsDb }
  ) {
    return async function processGatePass(id, status, employee_id) {
      if (!id) {
        throw new Error('Gate pass id must have a value');
      }
      if (isNaN(id)) {
        throw new Error('Gate pass id must ba a number');
      }
  
      if (!status) {
        throw new Error('Status must have a value');
      }
      if (!isNaN(status)) {
        throw new Error('Status must be a string');
      }
  
      if (status !== 'Processed') {
        throw new Error('Invalid status');
      }
  
      const result = await gatePassDb.updateRequest(id, status, employee_id);
      if (result.length > 0) {
        const createLog = await gatePassStatusLogsDb.insertLog({
          gate_pass_id: id,
          status,
          employee_id
        });
      
        return createLog;
      } else {
        throw new Error('Update failed');
      }
    };
  };
  