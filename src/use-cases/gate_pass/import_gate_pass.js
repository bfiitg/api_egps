module.exports = function postMultipleRequests(
  { gatePassDb },
  { requestTypeDb },
  { gatePassStatusLogsDb },
  { moment }
) {
  return async function makePostMultipleRequests(requestData, employee_id) {
    await requestData.forEach(async request => {
      const requestType = await requestTypeDb.findOneRequestTypeByName(
        request.request_type
      );

      if (!requestType) {
        throw new Error('Request type does not exists');
      }

      request = {
        ...request,
        request_type_id: requestType.id,
        updated_by: employee_id,
        is_read: true
      };

      if (!request.employee_id) {
        throw new Error('Employee id must be provided');
      }
      if (isNaN(request.employee_id) === true) {
        throw new Error('Employee id must be a number');
      }
      if (!request.actual_time_out) {
        throw new Error('Actual time out must be provided');
      }
      if (!isNaN(request.actual_time_out)) {
        throw new Error('Invalid request time out');
      }

      if (!request.approval_time) {
        throw new Error('Gate pass approval time required');
      }


      const getRequestType = await requestTypeDb.findOneRequestType(
        request.request_type_id
      );
      if (getRequestType.has_time_in) {
        if (!request.actual_time_in) {
          throw new Error('Actual time in must be provided');
        }
        if (!isNaN(request.actual_time_in)) {
          throw new Error('Invalid request time in');
        }

        //compare time in and time out, if time in less than time out throw error
        const isTimeInGreaterThanTimeOut =
          request.actual_time_in <= request.actual_time_out;
        if (isTimeInGreaterThanTimeOut)
          throw new Error('Actual time in must be above request time out');

        if (request.approval_time > request.actual_time_out) {
          throw new Error('Actual time out must be above approval time');
        }

        if (request.approval_time >= request.actual_time_in) {
          throw new Error('Actual time in must be above approval time');
        }

      }




      if (!getRequestType.has_time_in) {
        if (request.actual_time_in) {
          throw new Error('Actual type does not have a time in');
        } else {
          request.actual_time_in = null;
        }
      }

      if (!request.request_date) {
        throw new Error('Request date must be provided');
      }
      if (!isNaN(request.request_date)) {
        throw new Error('Invalid request date');
      }
      request = {
        ...request,
        request_date: moment(request.request_date).format('YYYY-MM-DD')
      };

      if (!moment(request.request_date).isValid()) {
        throw new Error('Invalid request date');
      }

      //check if request type is lunch out
      if (requestType.name.toLowerCase() === 'lunch out') {
        //check db if employee already requested for lunch out
        const existingLunchOut = await gatePassDb.checkExistingLunchOutGatePass(
          request.employee_id,
          requestType.id,
          request.request_date
        );
        if (existingLunchOut) {
          throw new Error('You have already requested for lunch out');
        }
      }

      //check if employee already out of the premises by checking all passes with request type of false has_time_in attribute
      const conflict = await gatePassDb.checkConflict(request);
      if (conflict) {
        throw new Error('Cannot request for a gate pass');
      }

      //check gatepasses if there are active gate pass
      const existing = await gatePassDb.checkExistingGatePass(
        request.employee_id,
        request.request_date
      );
      if (existing) {
        throw new Error(`You still have an active request gate pass`);
      }

      const result = await gatePassDb.postRequest(request);


      //Insert log for approved by (approved status)
      await gatePassStatusLogsDb.insertLog({
        gate_pass_id: result.id,
        status: 'Approved',
        employee_id
      });
    });

    return 'Successfully added';
  };
};
