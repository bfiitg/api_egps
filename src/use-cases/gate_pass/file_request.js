const makeGatepass = require('../../entities/gate_pass');

module.exports = function makePostRequest(
  { gatePassDb },
  { requestTypeDb },
  { moment }
) {
  return async function makeRequest(request_info) {
    const request = await makeGatepass(request_info, moment, requestTypeDb);

    //check if request type is lunch out
    if (request.requestType().name.toLowerCase() === 'lunch out') {
      //check db if employee already requested for lunch out
      const existingLunchOut = await gatePassDb.checkExistingLunchOutGatePass(
        request.getEmployeeId(),
        request.getRequestTypeId(),
        request.getRequestDate()
      );
      if (existingLunchOut) {
        throw new Error('You have already requested for lunch out');
      }
    }

    //check if employee already out of the premises by checking all passes with request type of false has_time_in attribute
    const conflict = await gatePassDb.checkConflict(request_info);
    if (conflict) {
      throw new Error('Cannot request for a gate pass');
    }

    //check gatepasses if there are active gate pass
    const existing = await gatePassDb.checkExistingGatePass(
      request.getEmployeeId(),
      request.getRequestDate()
    );
    if (existing) {
      throw new Error(`You still have an active request gate pass`);
    }

    const create_request = await gatePassDb.postRequest(request_info);

    if (!create_request) {
      throw new Error('Insert failed');
    }
    return create_request;
  };
};
