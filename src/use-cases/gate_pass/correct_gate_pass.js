module.exports = function makeCorrectGatePass(
  { gatePassDb },
  { gatePassStatusLogsDb },
  { moment },
  { requestTypeDb }
) {
  return async function correctGatePass({
    id,
    actual_time_out,
    actual_time_in,
    request_type_id,
    request_date,
    status,
    employee_id,
    approval_time
  } = {}) {
    if (!id) {
      throw new Error('Gate pass id must have a value');
    }
    if (isNaN(id)) {
      throw new Error('Gate pass id must ba a number');
    }

    if (!actual_time_out) {
      throw new Error('Gate pass actual time out must be provided');
    }

    if (!approval_time) {
      throw new Error('Gate pass approval time required');
    }

    if (!isNaN(actual_time_out)) {
      throw new Error('Invalid actual time out');
    }

    if (!request_type_id) {
      throw new Error('Request type id must be provided');
    }
    if (isNaN(request_type_id)) {
      throw new Error('Request type id must a number');
    }

    if (status === 'Pending') {
      throw new Error('Cannot edit pending gate pass');
    }

    if (status === 'Processed') {
      throw new Error('Cannot edit processed gate pass');
    }

    //get request type
    const getRequestType = await requestTypeDb.findOneRequestType(
      request_type_id
    );
  

    if (getRequestType.has_time_in) {

      if (!actual_time_in) {
        throw new Error('Request time in must be provided');
      }
      if (!isNaN(actual_time_in)) {
        throw new Error('Invalid request time in');
      }


      //comapare if actual time in less than actual time out throw error if true
      const isTimeInGreaterThanTimeOut = actual_time_in <= actual_time_out;
      if (isTimeInGreaterThanTimeOut)
        throw new Error('Request time in must be above request time out');

       
          if(approval_time > actual_time_out){
            throw new Error('Actual time out must be above approval time');
          }

          if(approval_time >= actual_time_in){
            throw new Error('Actual time in must be above approval time');
          }

        

    } else {
      if (actual_time_in) {
        throw new Error('Request type does not have a time in');
      }
    }

    if (!request_date) {
      throw new Error('Request date must be provided');
    }
    if (!isNaN(request_date)) {
      throw new Error('Invalid request date');
    }

    moment(request_date).format('YYYY-MM-DD');
    if (!moment(request_date).isValid()) {
      throw new Error('Invalid request date');
    }

    if (!status) {
      throw new Error('Status must be provided');
    }
    if (!isNaN(status)) {
      throw new Error('Status must be a character');
    }
 


    

    const result = await gatePassDb.updateGatePassTime(
      id,
      actual_time_out,
      actual_time_in,
      employee_id,
      request_type_id,
      getRequestType.has_time_in ? 'Time In' : 'Time Out',
      approval_time
    );

    if (result.length > 0) {
      const createLog = await gatePassStatusLogsDb.insertLog({
        gate_pass_id: id,
        status: 'Updated',
        employee_id
      });



      //get elapsed time by subtracting actual time in to actual time out then divide by hour (3600000 milisec)
      

      // let elapsed_time =
      //   (moment(new Date(`${actual_time_in} ${request_date}`)) -
      //     moment(new Date(`${actual_time_out} ${request_date}`))) /
      //   3600000;

      // //truncate to 2 decimal places without rounding off
      // elapsed_time = elapsed_time.toString();
      // if (elapsed_time.indexOf('.') > 0) {
      //   elapsed_time = elapsed_time.slice(0, elapsed_time.indexOf('.') + 3);
      // }



      // createLog.get().elapsed_time = elapsed_time;

      return createLog;
    } else {
      throw new Error('Update failed');
    }
  };
};
