module.exports = function makeApproveRequest({
  gatePassDb
}, {
  gatePassStatusLogsDb
}, {
  employeesDb
}) {
  return async function approveRequest(id, employee_id, note, roleParam, moment) {

    let status;
    let approval_time;

    if (!id) {
      throw new Error('Gate pass id must have a value');
    }
    if (isNaN(id)) {
      throw new Error('Gate pass id must ba a number');
    }
    // if (!status) {
    //   throw new Error('Status must have a value');
    // }
    // if (!isNaN(status)) {
    //   throw new Error('Status must be a string');
    // }

    // if (status !== 'Approved') {
    //   throw new Error('Invalid status');
    // }
    if (note) {
      if (!isNaN(note)) {
        throw new Error('Note must be a character');
      }
    }


    const passInfo = await gatePassDb.findGatePassById(id);
    const employeeInfo = await employeesDb.findById(passInfo.employee_id);

 
    approval_time = moment()
    .tz('Asia/Manila')
    .format('H:mm');

    if (roleParam === 'pg' || roleParam === 'admin') {
      status = 'Approved'
      // for now change to moment later
 
      // approval_time = moment()
      //     .tz('Asia/Manila')
      //     .format('H:mm');
      // if (passInfo.request_type.name === 'Personal Out') {
      //   approval_time = moment()
      //     .tz('Asia/Manila')
      //     .format('H:mm');
      // } else if (passInfo.request_type.name === 'Lunch Out' && employeeInfo.payroll_group === 'manager_payroll') {
      //   approval_time = moment()
      //     .tz('Asia/Manila')
      //     .format('H:mm');
      // }


    } else {

      if (passInfo.request_type.name === 'Official Business' || (passInfo.request_type.name === 'Lunch Out' &&
          employeeInfo.payroll_group === 'office_payroll')) {

        status = 'Approved'
       
      } else {
        status = 'Pre-Approved'
        // approval_time = null

      }

    }

    const result = await gatePassDb.updateRequest(
      id,
      status,
      employee_id,
      note,
      approval_time
    );


    if (result.length > 0) {
      const createLog = await gatePassStatusLogsDb.insertLog({
        gate_pass_id: id,
        status,
        employee_id
      });
      return createLog;
    } else {
      throw new Error('Update failed');
    }
  };
};