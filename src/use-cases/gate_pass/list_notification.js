module.exports = function listNotifications({ gatePassDb }) {
  return async function makeListNotification(employee_id) {
    const listNotifications = await gatePassDb.getNotifications(employee_id);
    if (listNotifications.length > 0) {
      return listNotifications;
    } else {
      return 'No notifications';
    }
  };
};
