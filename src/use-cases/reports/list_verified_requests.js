module.exports = function listVerifiedGatePass(
  { gatePassDb },
  { encryption },
  { requestTypesDB },
  { departmentsDB }
) {
  return async function makeListVerifiedGatePass(dateRange, moment) {
    const { request_date_from, request_date_to } = dateRange;
    if (request_date_from && request_date_to) {
      if (!isNaN(request_date_from)) {
        throw new Error("Invalid request date from");
      }

      if (!isNaN(request_date_to)) {
        throw new Error("Invalid request date to");
      }
      if (!moment(request_date_from).isValid()) {
        throw new Error("Invalid request date from");
      }
      if (!moment(request_date_to).isValid()) {
        throw new Error("Invalid request date to");
      }
    }

    //get verified gate passes
    let listGatePass = await gatePassDb.getEmployeesVerifiedGatePass(dateRange);
    // get rquest types
    let listRequestTypes = await requestTypesDB.listRequestTypes();
    //get departments
    let listDepartments = await departmentsDB.getDepartments();

    let lunchOutRequestType;
    if (!listRequestTypes.length) {
      listRequestTypes = "No gate pass request types found";
    } else {
      //get lunch out request type
      lunchOutRequestType = listRequestTypes.find(
        requestType => requestType.name.toLowerCase() === "lunch out"
      );
    }

    if (!listDepartments.length) {
      listDepartments = "No departments found";
    }

    if (!listGatePass.length) {
      listGatePass = "No gate passes found";
    } else {
      //Get gate passes elapsed in hour
      listGatePass.map(employee =>
        employee.gate_passes.map(gate_pass => {
          //subtract actual time in with actual time out then divide by hour (3600000 milisec)
          let elapsed_time =
            (moment(
              new Date(`${gate_pass.actual_time_in} ${gate_pass.request_date}`)
            ) -
              moment(
                new Date(`${gate_pass.approval_time} ${gate_pass.request_date}`)
              )) /
            3600000;

          //Minus 1 hour if request type is lunch out
          if (
            lunchOutRequestType &&
            lunchOutRequestType.id === gate_pass.request_type_id
          ) {
            if (gate_pass.request_date >= "2020-03-23") {
              elapsed_time = elapsed_time - 0.5;
            } else {
              elapsed_time = elapsed_time - 1;
            }
          }

          //truncate to 2 decimal places without rounding off
          elapsed_time = elapsed_time.toString();
          if (elapsed_time.indexOf(".") > 0) {
            elapsed_time = elapsed_time.slice(0, elapsed_time.indexOf(".") + 3);
          }
          gate_pass.get().elapsed_time = elapsed_time;
        })
      );

      //Get gate passes elapsed
      listGatePass.map(employee =>
        employee.gate_passes.map(gate_pass => {
          console.log(gate_pass.get().elapsed_time);
          if (
            gate_pass.request_type.name.toLowerCase() === "lunch out" &&
            gate_pass.request_date >= "2020-03-23" &&
            gate_pass.get().elapsed_time > 30
          ) {
            gate_pass.get().elapsed_time = gate_pass.get().elapsed_time - 30;
          } else if (
            gate_pass.request_type.name.toLowerCase() === "lunch out" &&
            gate_pass.get().elapsed_time > 60
          ) {
            gate_pass.get().elapsed_time = gate_pass.get().elapsed_time - 60;
          }
        })
      );

      //Decrypt Employee data
      await listGatePass.map(employee => {
        employee.first_name = encryption.decrypt(employee.first_name);
        employee.last_name = encryption.decrypt(employee.last_name);
        if (employee.middle_name) {
          employee.middle_name = encryption.decrypt(employee.middle_name);
        }
      });

      //Decrypt gate pass logs
      listGatePass.forEach(employee =>
        employee.gate_passes.forEach(gatepass =>
          gatepass.gate_pass_status_logs.forEach(log => {
            {
              if (log.employee) {
                log.employee.first_name = encryption.decrypt(
                  log.employee.first_name
                );
                log.employee.last_name = encryption.decrypt(
                  log.employee.last_name
                );
                if (log.employee.middle_name) {
                  log.employee.middle_name = encryption.decrypt(
                    log.employee.middle_name
                  );
                }
              }
            }
          })
        )
      );
    }
    return { listGatePass, listRequestTypes, listDepartments };
  };
};
