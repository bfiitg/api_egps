module.exports = function listPersonalOutGatePass(
  { gatePassDb },
  { encryption },
  { requestTypesDB },
  { departmentsDB }
) {
  return async function makeListPersonalOutGatePass(dateRange, moment) {
    const { request_date_from, request_date_to } = dateRange;

    const actual_time = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");
    const actual_date = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    if (request_date_from && request_date_to) {
      if (!isNaN(request_date_from)) {
        throw new Error("Invalid request date from");
      }

      if (!isNaN(request_date_to)) {
        throw new Error("Invalid request date to");
      }
      if (!moment(request_date_from).isValid()) {
        throw new Error("Invalid request date from");
      }
      if (!moment(request_date_to).isValid()) {
        throw new Error("Invalid request date to");
      }
    }

    //get request types
    const requestTypes = await requestTypesDB.listRequestTypes();

    //Get Personal out Request type id by name
    if (!requestTypes) {
      throw new Error("No request types");
    }

    //Get personal out request type
    const personalOutRequestType = requestTypes.find(
      requestType => requestType.name.toLowerCase() === "personal out"
    );
    //Get lunch out request type
    const lunchOutRequestType = requestTypes.find(
      requestType => requestType.name.toLowerCase() === "lunch out"
    );

    // get all excess lunch out of offsite employees
    let offsiteExcessLunchOutRequests = await gatePassDb.listLunchOutGatePassOffsite(
      dateRange,
      lunchOutRequestType.id
    )

    //get all employee lunch out with mng prefix
    let listUnfiltertedLunchOutRequests = await gatePassDb.listLunchOutGatePass(
      dateRange,
      lunchOutRequestType.id
    );
    let listLunchOutRequests;

    //list all personal out gate passes
    let listGatePass = await gatePassDb.listPersonalOutGatePass(
      dateRange,
      personalOutRequestType.id
    );

    //get request types
    let listRequestTypes = await requestTypesDB.listRequestTypes();
    //get departments
    let listDepartments = await departmentsDB.getDepartments();

    if (!listRequestTypes.length) {
      listRequestTypes = "No gate pass request types found";
    }

    if (!listDepartments.length) {
      listDepartments = "No departments found";
    }


    if (offsiteExcessLunchOutRequests.length) {
      if (!listUnfiltertedLunchOutRequests) {
        listUnfiltertedLunchOutRequests = []
      }
      for (let i = 0; i < offsiteExcessLunchOutRequests.length; i++) {
        listUnfiltertedLunchOutRequests.push(offsiteExcessLunchOutRequests[i])
      }
    }

    if (!listUnfiltertedLunchOutRequests.length) {
      listLunchOutRequests = "No lunch out requests found";
    } else {
      //Get gate passes elapsed
      listUnfiltertedLunchOutRequests.map(employee =>
        employee.gate_passes.map(gate_pass => {
          if (gate_pass.actual_time_in) {
            //subract actual time in with actual time out then divide by hour (3600000 milisec)
            let elapsed_time =
              (moment(`${gate_pass.request_date} ${gate_pass.actual_time_in}`) -
                moment(
                  `${gate_pass.request_date} ${gate_pass.approval_time}`
                )) /
              3600000;
            gate_pass.get().elapsed_time = elapsed_time;
          } else if (gate_pass.actual_time_out) {
            let elapsed_time;
            if (
              moment().format("YYYY-MM-DD") ==
              moment(gate_pass.request_date).format("YYYY-MM-DD")
            ) {
              elapsed_time =
                (moment(`${actual_date} ${actual_time}`) -
                  moment(
                    `${gate_pass.request_date} ${gate_pass.actual_time_out}`
                  )) /
                3600000;
            } else {
              elapsed_time =
                (moment(`${gate_pass.request_date} 23:59:00 `) -
                  moment(
                    `${gate_pass.request_date} ${gate_pass.actual_time_out}`
                  )) /
                3600000;
            }
            gate_pass.get().elapsed_time = elapsed_time;
          } else {
            let elapsed_time;
            if (
              moment().format("YYYY-MM-DD") ==
              moment(gate_pass.request_date).format("YYYY-MM-DD")
            ) {
              elapsed_time =
                (moment(`${actual_time} ${gate_pass.request_date}`) -
                  moment(
                    `${gate_pass.approval_time} ${gate_pass.request_date}`
                  )) /
                3600000;
            } else {
              elapsed_time =
                (moment(`${gate_pass.request_date} 23:59:00`) -
                  moment(
                    `${gate_pass.request_date} ${gate_pass.approval_time}`
                  )) /
                3600000;
            }
            gate_pass.get().elapsed_time = elapsed_time;
          }
        })
      );

      //Decrypt employees data
      listUnfiltertedLunchOutRequests.map(employee => {
        employee.first_name = encryption.decrypt(employee.first_name);
        employee.last_name = encryption.decrypt(employee.last_name);
        if (employee.middle_name) {
          employee.middle_name = encryption.decrypt(employee.middle_name);
        }
      });

      //Return Lunch outs where elapsed time > 1 hour
      listLunchOutRequests = listUnfiltertedLunchOutRequests.map(
        employee =>
          (employee = {
            ...employee.get(),
            gate_passes: employee.gate_passes.filter(gatepass => {
              if (gatepass.request_date >= "2020-03-23")
                return gatepass.get().elapsed_time > 0.5;
              else return gatepass.get().elapsed_time > 1;
            })
          })
      );
      //Calculate Exceeded time (elapsed time - 1)
      listLunchOutRequests.map(employee => {
        if (employee.gate_passes.length > 0) {
          employee.gate_passes.map(gatepass => {
            if (gatepass.request_date >= "2020-03-23")
              gatepass.get().elapsed_time = gatepass.get().elapsed_time - 0.5;
            else gatepass.get().elapsed_time = gatepass.get().elapsed_time - 1;
          });
        }
        employee.gate_passes.map(gatepass => {
          //truncate to 2 decimal places without rounding off
          let elapsed_time = gatepass.get().elapsed_time.toString();
          if (elapsed_time.indexOf(".") > 0) {
            elapsed_time = elapsed_time.slice(0, elapsed_time.indexOf(".") + 3);
          }
          gatepass.get().elapsed_time = elapsed_time;
        });
      });
    }
    if (!listGatePass.length) {
      listGatePass = "No personal out requests found";
    } else {
      //Get gate passes elapsed in hour

      listGatePass.map(employee =>
        employee.gate_passes.map(gate_pass => {
          //subract actual time in with actual time out then divide by hour (3600000 milisec)
          if (gate_pass.actual_time_in) {
            let elapsed_time =
              (moment(`${gate_pass.actual_time_in} ${gate_pass.request_date}`) -
                moment(
                  `${gate_pass.approval_time} ${gate_pass.request_date}`
                )) /
              3600000;

            //truncate to 2 decimal places without rounding off
            elapsed_time = elapsed_time.toString();
            if (elapsed_time.indexOf(".") > 0) {
              elapsed_time = elapsed_time.slice(
                0,
                elapsed_time.indexOf(".") + 3
              );
            }
            gate_pass.get().elapsed_time = elapsed_time;
          } else {
            let elapsed_time =
              (moment(`${actual_time} ${gate_pass.request_date}`) -
                moment(
                  `${gate_pass.approval_time} ${gate_pass.request_date}`
                )) /
              3600000;

            //truncate to 2 decimal places without rounding off
            elapsed_time = elapsed_time.toString();
            if (elapsed_time.indexOf(".") > 0) {
              elapsed_time = elapsed_time.slice(
                0,
                elapsed_time.indexOf(".") + 3
              );
            }
            gate_pass.get().elapsed_time = elapsed_time;
          }
        })
      );

      //Decrypt employees data
      listGatePass.map(employee => {
        employee.first_name = encryption.decrypt(employee.first_name);
        employee.last_name = encryption.decrypt(employee.last_name);
        if (employee.middle_name) {
          employee.middle_name = encryption.decrypt(employee.middle_name);
        }
      });
    }

    return {
      listGatePass,
      listLunchOutRequests,
      listRequestTypes,
      listDepartments
    };
  };
};
