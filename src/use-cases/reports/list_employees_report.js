module.exports = function listEmployeesReport(
  { gatePassDb },
  { encryption },
  { requestTypesDB },
  { departmentsDB },
  { teamsDB },
  { admin_db },
  { employees_db }
) {
  return async function makeListEmployeesReport(
    roleParam,
    employee_id,
    dateRange,
    moment
  ) {
    const { request_date_from, request_date_to } = dateRange;
    if (request_date_from && request_date_to) {
      if (!isNaN(request_date_from)) {
        throw new Error('Invalid request date from');
      }

      if (!isNaN(request_date_to)) {
        throw new Error('Invalid request date to');
      }
      if (!moment(request_date_from).isValid()) {
        throw new Error('Invalid request date from');
      }
      if (!moment(request_date_to).isValid()) {
        throw new Error('Invalid request date to');
      }
    }

    let listGatePass;
    //get request types
    let listRequestTypes = await requestTypesDB.listRequestTypes();
    //get departments
    let listDepartments = await departmentsDB.getDepartments();

    let isDepartmentHead = [];
    let DepartmentID = [];
    let isTeamHead = [];
    let TeamID = [];

    //get gate passes depending on employee role (PG/ADMIN/TEAM HEAD/DEPT HEAD)
    if (roleParam === 'pg' || roleParam === 'admin' || roleParam === 'emp') {

      listGatePass = await gatePassDb.getAllEmployeesGatePass(
        employee_id,
        dateRange
      );
    } else if (roleParam === 'dh') {

      isDepartmentHead = await departmentsDB.getDepartmentsbyEmployeeId(employee_id);

      if (isDepartmentHead.length === 0) {
        throw new Error('Employee is not department head');
      }

      for (let index = 0; index < isDepartmentHead.length; index++) {
        DepartmentID.push(isDepartmentHead[index].department_id)
      }


      listGatePass = await gatePassDb.getEmployeesGatePassByDepartment(
        employee_id,
        DepartmentID,
        dateRange
      )


    } else if (roleParam === 'th' || roleParam === 'pdh') {


      isTeamHead = await admin_db.getTeamid(employee_id);
      if (!isTeamHead) {
        throw new Error('Employee is not team head');
      }

      for (let index = 0; index < isTeamHead.length; index++) {
        TeamID.push(isTeamHead[index].team_id)
      }

      listGatePass = await gatePassDb.getEmployeesGatePassByTeam(
        employee_id,
        TeamID,
        dateRange
      );

    }

    if (!listRequestTypes.length) {
      listRequestTypes = 'No gate pass request types found';
    }

    if (!listDepartments.length) {
      listDepartments = 'No departments found';
    }

    if (!listGatePass.length) {
      listGatePass = 'No data found';
    } else {

      if (isDepartmentHead && isDepartmentHead.length > 0) {
        //remove passes with null postion/team/department
        listGatePass = listGatePass.filter(
          employee =>
            employee.position !== null &&
            employee.position.team !== null &&
            employee.position.team.department !== null
        );

        //get co-approvers
        let coApprovers = []
        for (ids of DepartmentID) {
          let data = await departmentsDB.getCoApprovers(ids)
          for (d of data) {
            const empData = await employees_db.simplefindById(d.employee_id)
            if (empData.role.name !== 'mng-Proxy Approver') {
              coApprovers.push(d.employee_id)
            }
          }
        }

        isTeamHead = await admin_db.getTeamid(employee_id);
        for (let index = 0; index < isTeamHead.length; index++) {
          TeamID.push(isTeamHead[index].team_id)
        }

        //filter data by department id
        // listGatePass = listGatePass.filter(
        //   employee => DepartmentID.includes(employee.position.team.department.id) ||
        //     TeamID.includes(employee.position.team.id) &&
        //     !coApprovers.includes(employee.id)
        // );

        listGatePass = listGatePass.filter(
          employee => DepartmentID.includes(employee.position.team.department.id) &&
            !coApprovers.includes(employee.id) ||
            TeamID.includes(employee.position.team.id)
        );


        // //get teams by departments with gatepasses of team heads
        // let teams = await teamsDB.getTeamHeadByDepartment(
        //   DepartmentID,
        //   dateRange
        // );
        // //Get team heads
        // let teamHeads = [];
        // teams.forEach(team => teamHeads.push(team.employee));
        // teamHeads = teamHeads.filter(employee => employee);
        // //Insert teamheads to list of gatepasses
        // teamHeads.forEach(employee => {
        //   if (
        //     listGatePass.find(
        //       employeeReport => employee.id === employeeReport.id
        //     )
        //   ) {
        //   } else {
        //     listGatePass.push(employee);
        //   }
        // })
      } else if (isTeamHead && isTeamHead.length > 0) {
        //remove data with null postion/team


        listGatePass = listGatePass.filter(
          employee =>
            employee.position !== null && employee.position.team !== null
        );

        //filter data by team id

        listGatePass = listGatePass.filter(
          employee => {
            return TeamID.includes(employee.position.team.id) &&
              (employee.role.name.split('-')[0] === 'emp' || employee.role.name.split('-')[0] === 'th')
              && employee.payroll_group === 'office_payroll'
          }
        );

      }
      //Decrypt employee data
      listGatePass.map(employee => {
        employee.first_name = encryption.decrypt(employee.first_name);
        employee.last_name = encryption.decrypt(employee.last_name);
        if (employee.middle_name) {
          employee.middle_name = encryption.decrypt(employee.middle_name);
        }
      });
      //Decrypt gate pass logs if exists
      listGatePass.forEach(employee => {
        if (employee.gate_passes) {
          employee.gate_passes.forEach(gatepass =>
            gatepass.gate_pass_status_logs.forEach(log => {
              {
                if (log.employee) {
                  log.employee.first_name = encryption.decrypt(
                    log.employee.first_name
                  );
                  log.employee.last_name = encryption.decrypt(
                    log.employee.last_name
                  );
                  if (log.employee.middle_name) {
                    log.employee.middle_name = encryption.decrypt(
                      log.employee.middle_name
                    );
                  }
                }
              }
            })
          );
        }
      });


      //Get gate passes elapsed in hour
      listGatePass.map(employee =>

        employee.gate_passes.map(gate_pass => {
          //subtract actual time in and time out then divide by hour (3600000 milisec)

          if (gate_pass.request_type.name === 'Personal Out' ||
            employee.is_offsite ||
            (gate_pass.request_type.name === 'Lunch Out'
              && employee.payroll_group === 'manager_payroll')) {

            let elapsed_time =
              (moment(
                new Date(`${gate_pass.actual_time_in} ${gate_pass.request_date}`)
              ) -

                moment(
                  new Date(
                    `${gate_pass.approval_time} ${gate_pass.request_date}`
                  )
                )) /
              3600000;

            elapsed_time = elapsed_time.toString();
            if (elapsed_time.indexOf('.') > 0) {
              elapsed_time = elapsed_time.slice(0, elapsed_time.indexOf('.') + 3);
            }
            gate_pass.get().elapsed_time = elapsed_time;

          } else {

            let elapsed_time =
              (moment(
                new Date(`${gate_pass.actual_time_in} ${gate_pass.request_date}`)
              ) -
                moment(
                  new Date(
                    `${gate_pass.actual_time_out} ${gate_pass.request_date}`
                  )
                )) /
              3600000;

            elapsed_time = elapsed_time.toString();
            if (elapsed_time.indexOf('.') > 0) {
              elapsed_time = elapsed_time.slice(0, elapsed_time.indexOf('.') + 3);
            }
            gate_pass.get().elapsed_time = elapsed_time;

          }

          //truncate to 2 decimal places without rounding off

        })
      );
    }


    return { listGatePass, listRequestTypes, listDepartments };
  };
};
