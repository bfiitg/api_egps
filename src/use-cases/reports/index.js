const makeEmployeeReportList = require('./list_employees_report');
const makePersonalOutRequests = require('./list_personal_out_requests');
const makeVerifiedRequests = require('./list_verified_requests');

//Data-access
const gatePassDb = require('../../data-access/gate_pass/');
const requestTypesDB = require('../../data-access/request_types/');
const departmentsDB = require('../../data-access/departments/');
const teamsDB = require('../../data-access/teams');
const admin_db = require('../../data-access/admin');
const employees_db = require('../../data-access/employees')

//Encryption
const encryption = require('../../encryption/encrypting');

const listEmployeeReport = makeEmployeeReportList(
  { gatePassDb },
  { encryption },
  { requestTypesDB },
  { departmentsDB },
  { teamsDB },
  { admin_db },
  { employees_db } 
);
const listPersonalOutRequests = makePersonalOutRequests(
  { gatePassDb },
  { encryption },
  { requestTypesDB },
  { departmentsDB }
);
const listVerifiedRequests = makeVerifiedRequests(
  { gatePassDb },
  { encryption },
  { requestTypesDB },
  { departmentsDB }
);

const reportService = Object.freeze({
  listEmployeeReport,
  listPersonalOutRequests,
  listVerifiedRequests
});
module.exports = reportService;
module.exports = {
  listEmployeeReport,
  listPersonalOutRequests,
  listVerifiedRequests
};
