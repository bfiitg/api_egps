const makeGetRequest = require('./list_request_types');
const requestTypesDb = require('../../data-access/request_types');

//Encryption
const encryption = require('../../encryption/encrypting');

const listRequestTypes = makeGetRequest({ requestTypesDb });

const requestTypesService = Object.freeze(listRequestTypes);

module.exports = requestTypesService;
module.exports = listRequestTypes;
