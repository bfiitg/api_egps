module.exports = function makeListRequestTypes({ requestTypesDb }) {
  return async function makeList() {
    const requestTypes = await requestTypesDb.listRequestTypes();
    if (!requestTypes) {
      throw new Error('No data found');
    }
    return requestTypes;
  };
};
