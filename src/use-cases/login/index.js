const makePostLogin = require('./login');
const employeeDb = require('../../data-access/employees');
const teamsDB = require('../../data-access/teams');
const departmentsDB = require('../../data-access/departments');
const admin_db = require('../../data-access/admin');
const authenticate = require('../../token/');
const encryption = require('../../encryption/encrypting');
const login = makePostLogin(
  { employeeDb },
  { authenticate },
  { encryption },
  { teamsDB },
  { departmentsDB },
  { admin_db }
);

const loginService = Object.freeze(login);

module.exports = loginService;
module.exports = login;
