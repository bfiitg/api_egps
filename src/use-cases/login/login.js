const makeLogin = require('../../entities/login');

module.exports = function makeLoginEmployee(
  { employeeDb },
  { authenticate },
  { encryption },
  { teamsDB },
  { departmentsDB },
  { admin_db }
) {
  return async function login(loginInfo) {
    console.log(loginInfo);

    // if(parseInt(browser.version.substring(0, 2), 10) <  56){
    //   throw new Error(`Browser outdated. Update your browser to latest version`)
    // }


    const employee = makeLogin(loginInfo);

    let approver, approverName = [], approverrows, empInfo

    //check if id exists in db
    const existing = await employeeDb.findById(employee.getEmployeeId());

    if (!existing) throw new Error('Employee does not exist');

    //Decrypt employee data
    existing.first_name = encryption.decrypt(existing.first_name);
    existing.last_name = encryption.decrypt(existing.last_name);
    if (existing.middle_name) {
      existing.middle_name = encryption.decrypt(existing.middle_name);
    }
    if (employee.getPassword() !== encryption.decrypt(existing.password))
      throw new Error('Incorrect password');

    if (existing.email) existing.email = encryption.decrypt(existing.email);

    //check status if active
    if (existing.status !== 'Active') {
      throw new Error(
        'Your account is currently inactive. Contact system administrator'
      );
    }

    //check postion/team/department if active
    if (existing.position) {
      if (existing.position.status !== 'Active') {
        throw new Error(
          'Your position is currently inactive. Contact system administrator'
        );
      }
      if (existing.position.team) {
        if (existing.position.team.status !== 'Active') {
          throw new Error(
            'Your team is currently inactive. Contact system administrator'
          );
        }
        if (existing.position.team.department) {
          if (existing.position.team.department.status !== 'Active') {
            throw new Error(
              'Your department is currently inactive. Contact system administrator'
            );
          }
        }
      }
    }
    //create new token
    const token = await authenticate.makeToken(existing.id, existing.role.name.split('-')[0]);

    const updateToken = await employeeDb.updateToken(token, existing.id);

    if (updateToken[0] > 0) {
      //Remove Duplicates in Modules
      function getUnique(arr, comp) {
        const unique = arr
          .map(e => e[comp])

          // store the keys of the unique objects
          .map((e, i, final) => final.indexOf(e) === i && i)

          // eliminate the dead keys & store unique objects
          .filter(e => arr[e])
          .map(e => arr[e]);

        return unique;
      }

      //Get employee role

      //Check if employee is department head
      const isDepartmentHead = await departmentsDB.getDepartmentsbyEmployeeId(
        loginInfo.id
      );



      //check if employee is team head
      const isTeamHead = await admin_db.getTeamid(loginInfo.id);
      const employeeRole = existing.role.name.split('-');
      let roleParam;

      //set employee role to be passed as paramter
      if (employeeRole[0] === 'pg') {
        roleParam = 'pg';
      } else if (employeeRole[0] === 'admin') {
        roleParam = 'admin';
      } else if (employeeRole[0] === 'emp') {
        roleParam = 'emp';
        // approver = await employeeDb.simplefindById(existing.position.team.employee_id);
        approver = await admin_db.findByTeamID(existing.position.team.id);
        // console.log(approver);
      } else if (employeeRole[0] === 'actng') {
        roleParam = 'actng';
        approver = await employeeDb.simplefindById(existing.position.team.employee_id);
      } else if (employeeRole[0] === 'sec') {
        roleParam = 'sec';
      } else if (existing.role.name == 'mng-Proxy Approver') {
        roleParam = 'pdh';
      } else if (isDepartmentHead.length > 0) {
        roleParam = 'dh';
      } else if (isTeamHead || isTeamHead.length != 0) {
        roleParam = 'th';
        approver = await departmentsDB.getEmployeesbyDepartmentId(existing.position.team.department.id);
      }

      if (existing.payroll_group === 'manager_payroll') {
        approver = null
      }

      if (approver) {
        for (let index = 0; index < approver.length; index++) {
          empInfo = await employeeDb.findApprover(approver[index].employee_id);
          empInfo.first_name = encryption.decrypt(empInfo.first_name);
          empInfo.middle_name = empInfo.middle_name ? encryption.decrypt(empInfo.middle_name) : '';
          empInfo.last_name = encryption.decrypt(empInfo.last_name);

          // approverName[index] = `${encryption.decrypt(empInfo.first_name)} ${empInfo.middle_name ? encryption.decrypt(empInfo.middle_name) : ''} ${encryption.decrypt(empInfo.last_name)}`
          approverName[index] = empInfo;
        }
        // approverName = `${encryption.decrypt(approver.first_name)} ${approver.middle_name ? encryption.decrypt(approver.middle_name) : ''} ${encryption.decrypt(approver.last_name)}`
      } else {
        approverName = [{ first_name: "PG", middle_name: "", last_name: "", position: { name: "People Group", team: { name: "", department: { name: "" } } } }]
      }

      //get modules
      let getModules = existing.role.access_rights.map(el =>
        el.action.get('module')
      );

      //get modules
      const actions = existing.role.access_rights
        .map(el => el.get('action'))
        .map(
          (el, index) =>
            (el = {
              id: el.id,
              description: el.description,
              module_id: el.module_id,
              status: existing.role.access_rights[index].status
            })
        );

      //Remove duplicates
      const modules = getUnique(getModules, 'description');
      modules.map(
        el =>
          (el.get().actions = actions.filter(
            action => action.module_id === el.id
          ))
      );


      return {
        id: existing.id,
        first_name: existing.first_name,
        last_name: existing.last_name,
        middle_name: existing.middle_name,
        email: existing.email,
        image: existing.img_profile_path,
        position: existing.position,
        roleParam,
        role: {
          id: existing.role.id,
          name: existing.role.name,
          status: existing.role.status
        },
        modules,
        token,
        approverName
      };
    } else {
      throw new Error('Token update failed...');
    }
  };
};
