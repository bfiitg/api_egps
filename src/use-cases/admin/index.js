//Data access
const admin_db = require('../../data-access/admin/');
const rolesDb = require('../../data-access/roles');
const employeesDb = require('../../data-access/employees');
const departmentsDB = require('../../data-access/departments')

//Encryption
const encryption = require('../../encryption/encrypting');

//Email Function
const emailFunction = require('../../email/');

//Employees
const makeEmployeeList = require('./employees/list_employees');
const makeEmployee = require('./employees/add_employee');
const makeUpdateEmployee = require('./employees/update_employee');
const makeImportEmployees = require('./employees/import_employees');
const makeEditEmployeeImport = require('./employees/update_employees_import');
const makeResetEmployeePasswrod = require('./employees/reset_employee_password');

const listEmployees = makeEmployeeList({ admin_db }, { encryption });
const addEmployee = makeEmployee(
  { admin_db },
  { encryption },
  { emailFunction },
  { rolesDb }
);
const updateEmployee = makeUpdateEmployee(
  { admin_db },
  { encryption },
  { rolesDb }
);
const addMultipleEmployee = makeImportEmployees({ admin_db }, { encryption });
const editMultipleEmployee = makeEditEmployeeImport(
  { admin_db },
  { encryption }
);
const editEmployeePassword = makeResetEmployeePasswrod(
  { employeesDb },
  { encryption }
);

//Roles
const makeRoleList = require('./roles/list_roles');
const makeRole = require('./roles/add_role');
const makeUpdateRole = require('./roles/update_role');

const listRoles = makeRoleList({ admin_db });
const addRole = makeRole({ admin_db });
const updateRole = makeUpdateRole({ admin_db });

//Positions
const makePositionList = require('./positions/list_positions');
const makePosition = require('./positions/add_position');
const makeUpdatePosition = require('./positions/update_position');

const listPositions = makePositionList({ admin_db });
const addPosition = makePosition({ admin_db });
const updatePosition = makeUpdatePosition({ admin_db });

//Teams
const makeTeamList = require('./teams/list_teams');
const makeTeam = require('./teams/add_team');
const makeUpdateTeam = require('./teams/update_team');

const listTeams = makeTeamList({ admin_db }, { encryption });
const addTeam = makeTeam({ admin_db }, { encryption });
const updateTeam = makeUpdateTeam({ admin_db });

// Teamheads
const makeDeleteTeamhead = require('./team_heads/delete_teamhead');
const makeCreateTeamhead = require('./team_heads/add_team_head');
const makeCreateProxy = require('./proxy/add_proxy');
const makeDeleteProxy = require('./proxy/delete_proxy')

const deleteTeamhead = makeDeleteTeamhead({ admin_db });
const addTeamhead = makeCreateTeamhead({ admin_db }, { encryption });
const deleteProxy = makeDeleteProxy({ admin_db });
const addProxy = makeCreateProxy({ admin_db }, { encryption });

//Departments
const makeDepartmentList = require('./departments/list_departments');
const makeDepartment = require('./departments/add_department');
const makeUpdateDepartment = require('./departments/update_department');

const listDepartments = makeDepartmentList({ admin_db }, { encryption });
const addDepartment = makeDepartment({ admin_db }, { encryption });
const updateDepartment = makeUpdateDepartment({ admin_db }, { departmentsDB }, { encryption });

//Request types
const makeRequestTypesList = require('./request_types/list_request_types.js');
const makeRequestType = require('./request_types/add_request_type');
const makeUpdateRequestType = require('./request_types/update_request_type');

const listRequestTypes = makeRequestTypesList({ admin_db });
const addRequestType = makeRequestType({ admin_db });
const updateRequestType = makeUpdateRequestType({ admin_db });

//Modules
const makeModuleList = require('./modules/list_modules');
const makeModule = require('./modules/add_module');
const makeUpdateModule = require('./modules/update_module');

const listModules = makeModuleList({ admin_db });
const addModule = makeModule({ admin_db });
const updateModule = makeUpdateModule({ admin_db });

//Action
const makeActionList = require('./actions/list_actions');
const makeAction = require('./actions/add_action');
const makeUpdateAction = require('./actions/update_action');

const listActions = makeActionList({ admin_db });
const addAction = makeAction({ admin_db });
const updateAction = makeUpdateAction({ admin_db });

//Access rights
const makeAccessRightList = require('./access_rights/list_access_rights');
const makeAccessRight = require('./access_rights/add_access_rights');
const makeUpdateAccessRight = require('./access_rights/update_action');
const makeListAccessRightByRole = require('./access_rights/get_access_rights_by_role');

const listAccessRights = makeAccessRightList({ admin_db });
const addAccessRight = makeAccessRight({ admin_db });
const updateAccessRight = makeUpdateAccessRight({ admin_db });
const listAccessRightsbyRole = makeListAccessRightByRole({ admin_db });

//Activity Logs
const makeActivityLogList = require('./activity_logs/list_activity_logs');

const listActivityLogs = makeActivityLogList({ admin_db }, { encryption });

const adminService = Object.freeze({
  listEmployees,
  addEmployee,
  updateEmployee,
  addMultipleEmployee,
  editMultipleEmployee,
  editEmployeePassword,

  listRoles,
  addRole,
  updateRole,

  listPositions,
  addPosition,
  updatePosition,

  listTeams,
  addTeam,
  updateTeam,

  deleteTeamhead,
  addTeamhead,

  addProxy,
  deleteProxy,

  listDepartments,
  addDepartment,
  updateDepartment,

  listRequestTypes,
  addRequestType,
  updateRequestType,

  listModules,
  addModule,
  updateModule,

  listActions,
  addAction,
  updateAction,

  listAccessRights,
  addAccessRight,
  updateAccessRight,
  listAccessRightsbyRole,

  listActivityLogs
});

module.exports = adminService;
module.exports = {
  listEmployees,
  addEmployee,
  updateEmployee,
  addMultipleEmployee,
  editMultipleEmployee,
  editEmployeePassword,

  listRoles,
  addRole,
  updateRole,

  listPositions,
  addPosition,
  updatePosition,

  listTeams,
  addTeam,
  updateTeam,

  deleteTeamhead,
  addTeamhead,

  addProxy,
  deleteProxy,

  listDepartments,
  addDepartment,
  updateDepartment,

  listRequestTypes,
  addRequestType,
  updateRequestType,

  listModules,
  addModule,
  updateModule,

  listActions,
  addAction,
  updateAction,

  listAccessRights,
  addAccessRight,
  updateAccessRight,
  listAccessRightsbyRole,

  listActivityLogs
};
