const { makeTeam } = require('../../../entities/teams');


module.exports = function postTeam({ admin_db }) {
  return async function makePostTeam(request_info) {
    await makeTeam(request_info, { admin_db });

    if (request_info.employee_id) {
      const employeeExists = await admin_db.findOneEmployeeById(
        request_info.employee_id
      );

      if (!employeeExists) {
        throw new Error('Employee does exists');
      }

      // const teamHead = await admin_db.findTeamHead(employeeExists.id);
      // if (teamHead) {
      //   throw new Error('Employee is already a team head');
      // }

      // const departmentHead = await admin_db.findDepartmentHead(
      //   employeeExists.id
      // );
      // if (departmentHead) {
      //   throw new Error('Employee is already a department head');
      // }

      const position = await admin_db.findPositionById(employeeExists.id);
      if ([!position]) {
        throw new Error('Employee does not have a position');
      }


      const teamHead = await admin_db.findIfHeadExists(position.team_id, employeeExists.id);
      if (teamHead) {
        throw new Error('Employee is already a team head');
      }

      const teams = await admin_db.findTeamById(position.team_id);
      if (!department) {
        throw new Error('Employee does not belong to any team.');
      }

      const departmentHead = await departmentheadDB.findIfHeadExists(
        teams.department_id, employeeExists.id
      );
      if (departmentHead) {
        throw new Error('Employee is already a department head');
      }

      const updateEmployee = { id: employeeExists.id, position_id: null };
      const assign = await admin_db.updateEmployee(updateEmployee);
      if (assign < 1) {
        throw new Error('Update employee failed');
      }
    }

    const result = await admin_db.addTeam(request_info);
    if (!result) {
      throw new Error('Insert failed');
    }

    return result;
  };
};
