module.exports = function listTeams({ admin_db }, { encryption }) {
  return async function makeListTeams() {
    let teams = await admin_db.listAllTeams();
    let departments = await admin_db.listAllDepartments();
    let employees = await admin_db.listAllEmployees();

    if (departments) {
      //Decrypt department head
      departments.map(department => {
        if (department.employee) {
          department.employee.first_name = encryption.decrypt(
            department.employee.first_name
          );
          department.employee.last_name = encryption.decrypt(
            department.employee.last_name
          );
          if (department.employee.middle_name) {
            department.employee.middle_name = encryption.decrypt(
              department.employee.middle_name
            );
          }
        }
      });
    } else {
      departments = 'No department data found.';
    }

    if (employees) {
      //Decrypt employees data
      employees.map(employee => {
        employee.first_name = encryption.decrypt(employee.first_name);
        employee.last_name = encryption.decrypt(employee.last_name);
        if (employee.middle_name) {
          employee.middle_name = encryption.decrypt(employee.middle_name);
        }
      });
    } else {
      employees = 'No employees data found.';
    }

    if (!teams.length) {
      teams = 'No teams data found.';
      return { teams, departments, employees };
    } else {
      //Decrypt team head data
      teams.map(team => {
        if (team.employees) {
          for (let index = 0; index < team.employees.length; index++) {
            if (team.employees[index].dataValues.first_name) {
              team.employees[index].dataValues.first_name = encryption.decrypt(
                team.employees[index].dataValues.first_name
              );
              team.employees[index].dataValues.last_name = encryption.decrypt(team.employees[index].dataValues.last_name);
              if (team.employees[index].dataValues.middle_name) {
                team.employees[index].dataValues.middle_name = encryption.decrypt(
                  team.employees[index].dataValues.middle_name
                );
              }
            }
          }
        }
      }
      );

      return { teams, departments, employees };
    }
  };
};
