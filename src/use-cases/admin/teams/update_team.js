const { makeUpdateTeam } = require('../../../entities/teams');

module.exports = function putTeam({ admin_db }) {
  return async function makeEditTeam(request_info) {
    await makeUpdateTeam(request_info, { admin_db });

    const result = await admin_db.updateTeam({
      id: request_info.id,
      ...request_info
    });

    const theads = request_info.team_heads;

    // update_info.employee_id is Team ID. bad naming
    const employeesOnTeam = await admin_db.findByTeamID(request_info.employee_id);
    if (!employeesOnTeam) {
      throw new Error("Cant find employees.");
    }

    // Get role information based on role name
    const role_th = await admin_db.findRoleByName("th-Team Head");
    const role_emp = await admin_db.findRoleByName("emp-Employee");
    const role_mng = await admin_db.findRoleByName("mng-Department Head")

    // Check if roles exist
    if (!role_th || !role_emp || !role_mng) {
      throw new Error("Cant find role.")
    }

    // Get role ID
    const th_role_id = role_th.id;
    const emp_role_id = role_emp.id;
    const mng_role_id = role_mng.id;

    // Check if role id is not null
    if (!th_role_id || !emp_role_id || !mng_role_id) {
      throw new Error("Cant get team ID");
    }

    let addTeamheadResult;
    let removeTeamheadResult;

    // Delete all teamheads on team
    for (let index = 0; index < employeesOnTeam.length; index++) {
      removeTeamheadResult = await admin_db.deleteTeamHead(employeesOnTeam[index].get().team_id, employeesOnTeam[index].get().employee_id);
      const check = await admin_db.getTeamid(employeesOnTeam[index].get().employee_id);
      const check2 = await admin_db.getDepartmentid(employeesOnTeam[index].get().employee_id);
      if (check2.length != 0) {
        await admin_db.updateEmpRole(employeesOnTeam[index].get().employee_id, mng_role_id);
      } else if (check.length == 0) {
        await admin_db.updateEmpRole(employeesOnTeam[index].get().employee_id, emp_role_id);
      }
    }

    // Add new teamheads
    for (let index = 0; index < theads.length; index++) {
      addTeamheadResult = await admin_db.addTeamHead(theads[index].team_id, theads[index].employee_id);
      const check2 = await admin_db.getDepartmentid(theads[index].employee_id);
      if (check2.length != 0) {
        await admin_db.updateEmpRole(theads[index].employee_id, mng_role_id);
      } else {
        await admin_db.updateEmpRole(theads[index].employee_id, th_role_id);
      }
    }

    if (result[0] > 0 || addTeamheadResult || removeTeamheadResult) {
      delete request_info.role;
      delete request_info.modules;
    } else {
      throw new Error('Update team failed');
    }

    return { ...request_info };
  }
}
