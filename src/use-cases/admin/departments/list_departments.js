module.exports = function listDepartments({ admin_db }, { encryption }) {
  return async function makeListDepartments() {
 
    let departments = await admin_db.listAllDepartments();
    let employees = await admin_db.listAllEmployees();
    if (employees) {
      //Decrypt employees data
      
      employees.map(employee => {
       
        employee.first_name = encryption.decrypt(employee.first_name);
        employee.last_name = encryption.decrypt(employee.last_name);
        if (employee.middle_name) {
          employee.middle_name = encryption.decrypt(employee.middle_name);
        }
      })
    } else {
      employees = 'No employees data found.';
    }

    if (!departments.length) {
      departments = 'No departments found';
    } else {
      //Decrypt department head
      departments.map(department => {

        department.employees.map( employee => {

        if (employee) {
          
          employee.first_name = encryption.decrypt(
           employee.first_name
          );

          if( employee.middle_name){
          employee.middle_name = encryption.decrypt(
            employee.middle_name
          )}
          
          employee.last_name = encryption.decrypt(
            employee.last_name
          );
        }

      })

      });
    }

    return { departments, employees };
  };
};
