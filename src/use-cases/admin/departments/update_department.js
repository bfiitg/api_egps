const {
  makeUpdateDepartment
} = require('../../../entities/departments');

module.exports = function putDepartment({
  admin_db

}, { departmentsDB }) {
  return async function makeEditDepartment(request_info) {

    await makeUpdateDepartment(request_info, {
      admin_db
    });


    const existingEmp = await departmentsDB.getEmployeesbyDepartmentId(request_info.id)

    if (request_info.employees.length > 0) {

      for (employees of request_info.employees) {

        const employeeExists = await admin_db.findOneEmployeeById(
          employees
        )
        if (!employeeExists) {
          throw new Error('Employee does not exists');
        }

      }
    }


    const departmentDetailsUpdated = await admin_db.updateDepartment({
      id: request_info.id,
      ...request_info
    })

    if (departmentDetailsUpdated) {

      const department = await admin_db.findDepartmentById(request_info.id)
      const departmentHeadsUpdated = await admin_db.updateDepartmentHead(department, request_info.employees)


      const role_dh = await admin_db.findRoleByName("mng-Department Head");
      const role_emp = await admin_db.findRoleByName("emp-Employee");
      const role_th = await admin_db.findRoleByName("th-Team Head");

      //change to mng
      for (employees of request_info.employees) {

        let updateEmployee = {
          id: employees,
          role_id: role_dh.id
        }


        const assign = await admin_db.updateEmployee(updateEmployee);
      }

      //cleanup
      for (employees of existingEmp) {

        let isDepartmentHead = await departmentsDB.getDepartmentsbyEmployeeId(
          employees.employee_id
        );

        let isTeamHead = await admin_db.getTeamid(employees.employee_id);

        if (isDepartmentHead.length === 0 && isTeamHead.length === 0) {

          updateEmployee = {
            id: employees.employee_id,
            role_id: role_emp.id
          }

          await admin_db.updateEmployee(updateEmployee);

        } else if (isDepartmentHead.length === 0 && isTeamHead.length !== 0) {

          updateEmployee = {
            id: employees.employee_id,
            role_id: role_th.id
          }
          await admin_db.updateEmployee(updateEmployee);
        }

      }

    } else {
      throw new Error('Update failed');
    }




    return true;
  };
};