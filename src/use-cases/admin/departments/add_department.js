const { makeDepartment } = require('../../../entities/departments');

module.exports = function postDepartment({ admin_db }) {
  return async function makePostDepartment(request_info) {
    await makeDepartment(request_info, { admin_db });

    // if (request_info.employee_id) {
    //   const employeeExists = await admin_db.findOneEmployeeById(
    //     request_info.employee_id
    //   );
    //   if (!employeeExists) {
    //     throw new Error('Employee does exists');
    //   }

    //   const teamHead = await admin_db.findTeamHead(employeeExists.id);
    //   if (teamHead) {
    //     throw new Error('Employee is already a team head');
    //   }

    //   const departmentHead = await admin_db.findDepartmentHead(
    //     employeeExists.id
    //   );
    //   if (departmentHead) {
    //     throw new Error('Employee is already a department head');
    //   }

    //   const updateEmployee = { id: employeeExists.id, position_id: null };
    //   const assign = await admin_db.updateEmployee(updateEmployee);
    //   if (assign < 1) {
    //     throw new Error('Update employee failed');
    //   }
    // }

    const result = await admin_db.addDepartment(request_info);
    if (!result) {
      throw new Error('Insert failed');
    }
    return result;
  };
};
