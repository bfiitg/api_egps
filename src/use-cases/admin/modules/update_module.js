const { makeUpdateModule } = require('../../../entities/modules');

module.exports = function putModule({ admin_db }) {
  return async function makeEditModule(request_info) {
    await makeUpdateModule(request_info, { admin_db });

    const result = await admin_db.updateModule(request_info);
    if (result[0] > 0) {
      delete request_info.role;
      delete request_info.modules;
    } else {
      throw new Error('Update failed');
    }

    return request_info;
  };
};
