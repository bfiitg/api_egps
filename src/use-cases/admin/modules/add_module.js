const { makeModule } = require('../../../entities/modules');

module.exports = function postModule({ admin_db }) {
  return async function makePostModule(request_info) {
    await makeModule(request_info, { admin_db });

    const result = await admin_db.addModule(request_info);
    if (!result) {
      throw new Error('Insert failed');
    }

    return result;
  };
};
