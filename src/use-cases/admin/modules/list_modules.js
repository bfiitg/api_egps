module.exports = function listModules({ admin_db }) {
  return async function makeListModules() {
    const result = await admin_db.listAllModules();
    if (!result.length) {
      return 'No data found';
    } else {
      return result;
    }
  };
};
