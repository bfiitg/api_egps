const { makeEmployee } = require('../../../entities/employees');

module.exports = function postEmployee(
  { admin_db },
  { encryption },
  { emailFunction },
  { rolesDb }
) {
  return async function makePostEmployee(request_info) {
    await makeEmployee(request_info, { admin_db }, { encryption }, { rolesDb });

    //Encrypting employee data
    request_info.first_name = encryption.encrypt(request_info.first_name);
    request_info.last_name = encryption.encrypt(request_info.last_name);
    if (request_info.middle_name) {
      request_info.middle_name = encryption.encrypt(request_info.middle_name);
    }
    request_info.password = encryption.encrypt(request_info.id);
    if (request_info.email) {
      request_info.email = encryption.encrypt(request_info.email);
    }
    const result = await admin_db.addEmployee(request_info);
    if (!result) {
      throw new Error('Insert failed');
    }

    //Decrypting
    result.first_name = encryption.decrypt(result.first_name);
    result.last_name = encryption.decrypt(result.last_name);
    if (result.middle_name) {
      result.middle_name = encryption.decrypt(result.middle_name);
    }
    if (result.email) {
      result.email = encryption.decrypt(result.email);
    }

    //Email to Admin
    const emailData = {
      recipient: request_info.adminEmail,
      subject: 'EGPS New Employee Account',
      text: `Good Day, Admin,\n\nBelow is the login details of new created account of ${result.first_name} ${result.middle_name} ${result.last_name}.\nEmployee Id: ${result.id}\nPassword: ${result.password}\n\nRegards,\n\nBiotech Farms Inc.`
    };

    await emailFunction.sendEmail(emailData);

    return result;
  };
};
