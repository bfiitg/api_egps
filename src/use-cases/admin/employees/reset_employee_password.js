module.exports = function resetPassword({ employeesDb }, { encryption }) {
  return async function makeResetPassword({ id } = {}) {
    if (!id) {
      throw new Error('Employee id must be provided');
    }
    if (isNaN(id)) {
      throw new Error('Employee id must be a number');
    }

    const result = await employeesDb.updateProfile(
      id,
      encryption.encrypt(id.toString())
    );

    if (result[0] > 0) {
      //Decrypt employee data
      result[1][0].first_name = encryption.decrypt(result[1][0].first_name);
      if (result[1][0].middle_name) {
        result[1][0].middle_name = encryption.decrypt(result[1][0].middle_name);
      }
      result[1][0].last_name = encryption.decrypt(result[1][0].last_name);
      return result[1][0];
    } else {
      throw new Error('Update failed');
    }
  };
};
