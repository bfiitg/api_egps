module.exports = function postMultipleEmployee({ admin_db }, { encryption }) {
  return async function makePostMultipleEmployee(employeeData) {
    

    let array = []


    for (employee of employeeData) {

      employee.first_name = encryption.encrypt(employee.first_name);
      employee.last_name = encryption.encrypt(employee.last_name);
      if (employee.middle_name) {
        employee.middle_name = encryption.encrypt(employee.middle_name);
      }
      employee.password = encryption.encrypt(employee.id.toString());
      if (employee.email) {
        employee.email = encryption.encrypt(employee.email);
      }


      const team = await admin_db.findTeamByNameOnly(employee.team)

      if(!team){
        throw new Error(`Error in  finding team emp id: ${employee.id}`)
      }

      const position = await admin_db.findPositionByName(team.id, employee.position)

      if(!position){
        throw new Error(`Error in finding position emp id: ${employee.id}`)
      }


      employee.position_id = position.id
      
      delete employee.position
      delete employee.team

     
      const exist = await admin_db.findOneEmployeeById(employee.id)

      if(!exist){
      array.push(employee)
      }

    }

    

    let result = await admin_db.bulkaddEmployee(array)

    if(!result){
      throw new Error(`Import failed`)
    }

    return 'Import successful'
  };
};
