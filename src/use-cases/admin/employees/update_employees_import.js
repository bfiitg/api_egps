

module.exports = function putMultipleEmployee({ admin_db }, { encryption }) {
  return async function makePutMultipleEmployee(employeeData) {
    await employeeData.forEach(async employee => {
      //Encrypting employee data
        employee.password = encryption.encrypt(`${employee.id}`);

      await admin_db.updateEmployee({
        id: employee.id,
        password: employee.password
      });
    });

    return 'Successfully edited';
  };
};
