module.exports = function listEmployees({ admin_db }, { encryption }) {
  return async function makeListEmployees() {
    let positions = await admin_db.listAllPositions();
    let roles = await admin_db.listAllRoles();
    let departments = await admin_db.listAllDepartments();
    if (!departments.length) {
      departments = 'No departments found';
    }
    if (!positions.length) {
      positions = 'No positions found';
    }
    if (!roles.length) {
      roles = 'No roles found';
    }
    let employees = await admin_db.listAllEmployees();
    //Decrypt Employee data
    if (!employees.length) {
      employees = 'No employees found';
    } else {
      employees.map(employee => {
        employee.first_name = encryption.decrypt(employee.first_name);
        employee.last_name = encryption.decrypt(employee.last_name);
        if (employee.middle_name) {
          employee.middle_name = encryption.decrypt(employee.middle_name);
        }
        if (employee.email) {
          employee.email = encryption.decrypt(employee.email);
        }
      });
    }
    return { employees, positions, roles, departments };
  };
};
