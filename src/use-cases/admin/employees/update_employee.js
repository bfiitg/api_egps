const { makeUpdateEmployee } = require('../../../entities/employees');

module.exports = function putEmployee(
  { admin_db },
  { encryption },
  { rolesDb }
) {
  return async function makePutEmployee(request_info) {
    await makeUpdateEmployee(request_info, { admin_db }, { rolesDb });

    //Encryption
    request_info.first_name = encryption.encrypt(request_info.first_name);
    request_info.last_name = encryption.encrypt(request_info.last_name);
    if (request_info.middle_name) {
      request_info.middle_name = encryption.encrypt(request_info.middle_name);
    }
    if (request_info.password) {
      request_info.password = encryption.encrypt(request_info.password);
    }
    if (request_info.email) {
      request_info.email = encryption.encrypt(request_info.email);
    }
    const result = await admin_db.updateEmployee({
      id: request_info.id,
      ...request_info
    });
    if (result[0] > 0) {
      delete request_info.role;
      delete request_info.modules;
      //Decrypt
      request_info.first_name = encryption.decrypt(request_info.first_name);
      request_info.last_name = encryption.decrypt(request_info.last_name);
      if (request_info.middle_name) {
        request_info.middle_name = encryption.decrypt(request_info.middle_name);
      }
      if (request_info.email) {
        request_info.email = encryption.decrypt(request_info.email);
      }

      return { ...request_info };
    } else {
      throw new Error('Update failed');
    }
  };
};
