module.exports = function listActivityLogs({ admin_db }, { encryption }) {
  return async function makeListActivityLogs(dateRange) {
    const result = await admin_db.listAllActivityLogs(dateRange);
    if (!result) {
      return 'No data found';
    }
    //Parse JSON.String
    result.forEach(
      data =>
        (data.new_values = JSON.parse(data.new_values)) &&
        (data.prev_values = JSON.parse(data.prev_values))
    );

    //Decrypt values with employee data
    result.forEach(activityLog => {
      //Decrypt previous values
      if (activityLog.prev_values) {
        if (
          activityLog.prev_values.first_name &&
          activityLog.prev_values.last_name &&
          activityLog.prev_values.password
        ) {
          activityLog.prev_values.first_name = encryption.decrypt(
            activityLog.prev_values.first_name
          );
          activityLog.prev_values.last_name = encryption.decrypt(
            activityLog.prev_values.last_name
          );
          if (activityLog.prev_values.middle_name) {
            activityLog.prev_values.middle_name = encryption.decrypt(
              activityLog.prev_values.middle_name
            );
          }
          delete activityLog.prev_values.password 

          if (activityLog.prev_values.email) {
            activityLog.prev_values.email = encryption.decrypt(
              activityLog.prev_values.email
            );
          }
        }
      }

      //Decrypt new values
      if (
        activityLog.new_values.first_name &&
        activityLog.new_values.last_name &&
        activityLog.new_values.password
      ) {
        activityLog.new_values.first_name = encryption.decrypt(
          activityLog.new_values.first_name
        );
        activityLog.new_values.last_name = encryption.decrypt(
          activityLog.new_values.last_name
        );
        if (activityLog.new_values.middle_name) {
          activityLog.new_values.middle_name = encryption.decrypt(
            activityLog.new_values.middle_name
          );
        }
        
        delete activityLog.new_values.password 

        if (activityLog.new_values.email) {
          activityLog.new_values.email = encryption.decrypt(
            activityLog.new_values.email
          );
        }
      }

      // //Decrypt created/updated by employee data
      if (activityLog.employee) {
        activityLog.employee.first_name = encryption.decrypt(
          activityLog.employee.first_name
        );
        activityLog.employee.last_name = encryption.decrypt(
          activityLog.employee.last_name
        );
        if (activityLog.employee.middle_name) {
          activityLog.employee.middle_name = encryption.decrypt(
            activityLog.employee.middle_name
          );
        }

        if (activityLog.employee.email) {
          activityLog.employee.email = encryption.decrypt(
            activityLog.employee.email
          );
        }
      }
    });

    

    return result;
  };
};
