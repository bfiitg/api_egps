const { makeUpdateAccessRight } = require('../../../entities/access_rights');

module.exports = function putAccessRight({ admin_db }) {
  return async function makeEditAccessRight(request_info) {
    await makeUpdateAccessRight(request_info, { admin_db });

    const result = await admin_db.updateAccessRights({
      id: request_info.id,
      ...request_info
    });

    if (result[0] > 0) {
      return request_info;
    } else {
      throw new Error('Update failed');
    }
  };
};
