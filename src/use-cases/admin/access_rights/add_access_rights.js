const { makeAccessRight } = require('../../../entities/access_rights');

module.exports = function postAccessRight({ admin_db }) {
  return async function makePostAccessRight(request_info) {
    await makeAccessRight(request_info, { admin_db });

    const result = await admin_db.addAccessRights(request_info);
    if (!result) {
      throw new Error('Insert failed');
    }
    return result;
  };
};
