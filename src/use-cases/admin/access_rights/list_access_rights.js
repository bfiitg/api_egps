module.exports = function listAccessRights({ admin_db }) {
  return async function mkaeListAccessRights() {
    let access_rights = await admin_db.listAllAccessRights();
    let roles = await admin_db.listAllRoles();
    let actions = await admin_db.listAllActions();

    if (!roles.length) {
      roles = 'No roles found.';
    }
    if (!actions.length) {
      actions = 'No actions found.';
    }

    if (!access_rights.length) {
      access_rights = 'No access rights data found.';
    }
    return { access_rights, roles, actions };
  };
};
