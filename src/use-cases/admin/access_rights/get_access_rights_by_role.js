module.exports = function listAccessRightsbyRole({ admin_db }) {

    return async function mkaeListAccessRightsbyRole(roleId) {
        
      let modules = await admin_db.listAllActionsActiveOnly();

      let actionsByRole = await admin_db.listAllAccessRightsbyRole(roleId);

  
      if (!modules.length) {
        actions = 'No actions found.';
      }

      if (!actionsByRole.length) {
        actionsByRole = 'No actions found.';
      }
  
    
      return { actionsByRole, modules }
    };
  };
  