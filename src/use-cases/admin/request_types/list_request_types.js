module.exports = function listRequestTypes({ admin_db }) {
  return async function makeListRequestTypes() {
    const result = await admin_db.listAllRequestTypes();
    if (!result.length) {
      return 'No data found';
    } else {
      return result;
    }
  };
};
