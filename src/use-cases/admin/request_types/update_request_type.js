const { makeUpdateRequestType } = require('../../../entities/request_types');

module.exports = function putRequestType({ admin_db }) {
  return async function makeEditRequestType(request_info) {
    await makeUpdateRequestType(request_info, { admin_db });

    const result = await admin_db.updateRequestType({
      id: request_info.id,
      ...request_info
    });

    if (result[0] > 0) {
      delete request_info.role;
      delete request_info.modules;
    } else {
      throw new Error('Update failed');
    }

    return request_info;
  };
};
