const { makeRequestType } = require('../../../entities/request_types');

module.exports = function postRequestType({ admin_db }) {
  return async function makePostRequestType(request_info) {
    await makeRequestType(request_info, { admin_db });

    const result = await admin_db.addRequestType(request_info);
    if (!result) {
      throw new Error('Insert failed');
    }
    return result;
  };
};
