module.exports = function listRoles({ admin_db }) {
  return async function makeListRoles() {
    const result = await admin_db.listAllRoles();
    if (!result.length) {
      return 'No data found';
    }
    return result;
  };
};
