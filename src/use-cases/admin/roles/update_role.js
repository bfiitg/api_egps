const { makeUpdateRole } = require('../../../entities/roles');

module.exports = function putRole({ admin_db }) {
  return async function makeEditRole(request_info) {

    await makeUpdateRole(request_info, { admin_db });

    let array = []

    const result = await admin_db.updateRole({
      id: request_info.id,
      ...request_info
    });

    if (result) {
      delete request_info.role;
      delete request_info.modules;

      if (request_info.actions) {
        for (let index = 0; index < request_info.actions.length; index++) {
          array.push({ role_id: request_info.id, action_id: request_info.actions[index] })
        }


      }
      const teardown = await admin_db.removeActionsfromRole(request_info.id)
      const build = await admin_db.addActionstoRole(array)



    } else {
      throw new Error('Update failed');
    }

    return { ...request_info, array };
  };
};
