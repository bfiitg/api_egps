const { makeRole } = require('../../../entities/roles');

module.exports = function postRole({ admin_db }) {
  return async function makePostRole(request_info) {
    await makeRole(request_info, { admin_db });

    const result = await admin_db.addRole(request_info);
    if (!result) {
      throw new Error('Insert failed');
    }
    return result;
  };
};
