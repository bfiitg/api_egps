const { makeDeleteProxy } = require('../../../entities/proxy');

module.exports = function deleteProxy({ admin_db }) {
    return async function makedeleteProxy(request_info) {
        await makeDeleteProxy(request_info, { admin_db });

        let role_pa, result;

        // Check if user is TH or DH
        if (request_info.roleParam == 'th' || request_info.roleParam == 'pdh') {
            const checkIfHeadExist = await admin_db.findIfHeadExists(request_info.team_id, request_info.employee_id)

            // Check if employee is alread a proxy approver            
            if (checkIfHeadExist.length == 0) {
                throw new Error("Team proxy approver does not exist.")
            }
            // Delete as proxy approver
            result = await admin_db.deleteTeamHead(request_info.team_id, request_info.employee_id);
            // Get role information of emp-Employee
            role_pa = await admin_db.findRoleByName("emp-Employee");
        } else if (request_info.roleParam == 'dh') {
            const checkIfHeadExist = await admin_db.findIfDHeadExists(request_info.department_id, request_info.employee_id)

            // Check if employee is alread a proxy approver
            if (checkIfHeadExist.length == 0) {
                throw new Error("Department proxy approver does not exist.")
            }

            // Delete as proxy approver
            result = await admin_db.deleteDepartmentHead(request_info.department_id, request_info.employee_id);
            // Get role information of th-Team Head
            role_pa = await admin_db.findRoleByName("th-Team Head");
        } else {
            throw new Error("User cant add proxy")
        }

        if (!role_pa) {
            throw new Error("Cant find role.")
        }

        const pa_role_id = role_pa.id;

        if (!pa_role_id) {
            throw new Error("Role ID of PA is null.")
        }

        if (!result) {
            throw new Error("Add proxy failed")
        } else {
            // Update proxy employee_role
            await admin_db.updateEmpRole(request_info.employee_id, pa_role_id);
        }



        return result;
    }

}