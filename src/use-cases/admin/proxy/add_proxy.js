const { makeCreateProxy } = require('../../../entities/proxy');

module.exports = function createProxy({ admin_db }, { encryption }) {
    return async function makecreateProxy(request_info) {
        await makeCreateProxy(request_info, { admin_db });

        let role_pa, result;

        // Check if user is TH or DH
        if (request_info.roleParam == 'th' || request_info.roleParam == "pdh") {
            const checkIfHeadExist = await admin_db.findIfHeadExists(request_info.position.team.id, request_info.employee_id)

            // Check if employee is already a proxy approver
            if (checkIfHeadExist.lenght > 0) {
                throw new Error("Team proxy approver already exist.")
            }
            // Get employee_info
            const employee_info = await admin_db.findOneEmployeeById(request_info.employee_id);
            const employee_team_id = employee_info.position.team.id;

            // Add as proxy approver
            result = await admin_db.addTeamHead(employee_team_id, request_info.employee_id);
            // Get role information of th-Proxy Approver
            role_pa = await admin_db.findRoleByName("th-Proxy Approver");
        } else if (request_info.roleParam == 'dh') {
            const checkIfHeadExist = await admin_db.findIfDHeadExists(request_info.position.team.department.id, request_info.employee_id)

            // Check if employee is already a proxy approver
            if (checkIfHeadExist.lenght > 0) {
                throw new Error("Department proxy approver already exist.")
            }
            // Get employee_info
            const employee_info = await admin_db.findOneEmployeeById(request_info.employee_id);
            const employee_department_id = employee_info.position.team.department.id;
            // Add as proxy approver
            result = await admin_db.addDepartmentHead(employee_department_id, request_info.employee_id);
            // Get role information of mng-Proxy Approver
            role_pa = await admin_db.findRoleByName("mng-Proxy Approver");
        } else {
            throw new Error("User cant add proxy")
        }

        if (!role_pa) {
            throw new Error("Cant find role.")
        }

        const pa_role_id = role_pa.id;

        if (!pa_role_id) {
            throw new Error("Role ID of PA is null.")
        }

        if (!result) {
            throw new Error("Add proxy failed")
        } else {
            // Update proxy employee_role
            await admin_db.updateEmpRole(request_info.employee_id, pa_role_id);
        }

        return result;
    }

}