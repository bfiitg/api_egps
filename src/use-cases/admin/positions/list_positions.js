module.exports = function listPositions({ admin_db }) {
  return async function makeListPositions() {
    let positions = await admin_db.listAllPositions();
    let teams = await admin_db.listAllTeams();
    let departments = await admin_db.listAllDepartments();
    if (!positions.length) {
      positions = 'No positions found';
    }
    if (!departments.length) {
      departments = 'No departments found';
    }
    if (!teams.length) {
      teams = 'No teams data found';
    }
    return { positions, teams, departments };
  };
};
