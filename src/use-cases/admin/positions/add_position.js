const { makePosition } = require('../../../entities/positions');

module.exports = function postPosition({ admin_db }) {
  return async function makePostPosition(request_info) {
    await makePosition(request_info, { admin_db });

    const result = await admin_db.addPosition(request_info);
    if (!result) {
      throw new Error('Insert failed');
    }

    return result;
  };
};
