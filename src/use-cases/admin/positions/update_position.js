const { makeUpdatePosition } = require('../../../entities/positions');

module.exports = function putRole({ admin_db }) {
  return async function makeEditPostion(request_info) {
    await makeUpdatePosition(request_info, { admin_db });

    const result = await admin_db.updatePosition({
      id: request_info.id,
      ...request_info
    });
    if (result) {
      delete request_info.role;
      delete request_info.modules;
    } else {
      throw new Error('Update failed');
    }

    return { ...request_info };
  };
};
