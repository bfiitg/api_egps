const { makeAction } = require('../../../entities/actions');

module.exports = function postAction({ admin_db }) {
  return async function makePostAction(request_info) {
    await makeAction(request_info, { admin_db });

    const result = await admin_db.addAction(request_info);
    if (!result) {
      throw new Error('Insert failed');
    }

    return result;
  };
};
