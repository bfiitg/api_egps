module.exports = function listActions({ admin_db }) {
  return async function mkaeListActions() {
    let actions = await admin_db.listAllActions();
    let modules = await admin_db.listAllModules();
    if (!actions.length) {
      actions = 'No actions found';
    }
    if (!modules) {
      modules = 'No modules data found.';
    }

    return { actions, modules };
  };
};
