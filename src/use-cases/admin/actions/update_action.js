const { makeUpdateAction } = require('../../../entities/actions');

module.exports = function putAction({ admin_db }) {
  return async function makeEditAction(request_info) {
    await makeUpdateAction(request_info, { admin_db });

    const result = await admin_db.updateAction({
      id: request_info.id,
      ...request_info
    });
    if (result[0] > 0) {
      delete request_info.role;
      delete request_info.modules;
    } else {
      throw new Error('Updated failed');
    }

    return request_info;
  };
};
