const { makeDeleteTeamhead } = require('../../../entities/team_heads');

module.exports = function deleteTeamhead({ admin_db }) {
    return async function makedeleteTeamhead(request_info) {
        await makeDeleteTeamhead(request_info, { admin_db });

        const result = await admin_db.deleteTeamHead(request_info.team_id, request_info.employee_id);
        if (!result) {
            throw new Error("Remove teamhead failed")
        }

        return result;
    }

}