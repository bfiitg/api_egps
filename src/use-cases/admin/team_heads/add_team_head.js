const { makeCreateTeamhead } = require('../../../entities/team_heads');

module.exports = function createTeamhead({ admin_db }, { encryption }) {
    return async function makecreateTeamhead(request_info) {
        await makeCreateTeamhead(request_info, { admin_db });

        const result = await admin_db.addTeamHead(request_info.team_id, request_info.employee_id);

        if (!result) {
            throw new Error("Add teamhead failed")
        }

        result.emp.dataValues.first_name = encryption.decrypt(result.emp.dataValues.first_name);
        result.emp.dataValues.middle_name = encryption.decrypt(result.emp.dataValues.middle_name);
        result.emp.dataValues.last_name = encryption.decrypt(result.emp.dataValues.last_name);

        return result;
    }

}