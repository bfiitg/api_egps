module.exports = function listTimeInGatePass({ gatePassDb }, { encryption }) {
  return async function makeListTimeInGatePass() {
    const listGatePass = await gatePassDb.listTimeinGatePass();
    if (!listGatePass.length) {
      return 'No data found';
    } else {
      //Decrypt employee data
      listGatePass.map(gatepass => {
        gatepass.employee.first_name = encryption.decrypt(
          gatepass.employee.first_name
        );
        gatepass.employee.last_name = encryption.decrypt(
          gatepass.employee.last_name
        );
        if (gatepass.employee.middle_name) {
          gatepass.employee.middle_name = encryption.decrypt(
            gatepass.employee.middle_name
          );
        }
      });
      return listGatePass;
    }
  };
};
