module.exports = function putTimeRequest({
  gatePassDb
}, {
  gatePassStatusLogsDB
}, {
  employeesDb
}, {
  encryption
}) {
  return async function makeTimeRequest(
    id,
    employee_id,
    moment
  ) {
    let status;
    const actual_time = moment().tz('Asia/Manila')
      .format('HH:mm');

    let updateGatePass;


    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD')



    if (!id) {
      throw new Error('Gate pass not found');
    }
    if (isNaN(id)) {
      throw new Error('Gate pass not found');
    }


    if (id > 2147483647) {
      throw new Error('Invalid employee ID');
    }



    const exists = await employeesDb.findEmployeeandGatePass(id, dateToday)

    const existTimein = await employeesDb.findEmployeeandGatePassTimeIn(id, dateToday)


    console.log(dateToday);
    if (!exists) {
      if (!existTimein) {
        throw new Error('You have no gate pass request. Please file to proceed');
      }

      throw new Error(`You have no pending gate pass request. Your last time in is ${
        existTimein.gate_passes[0].actual_time_in
        }`)

    }



    if (exists.gate_passes[0].status === 'Approved') {
      status = 'Time Out'

      updateGatePass = await gatePassDb.outGatePass(
        exists.gate_passes[0].id,
        status,
        actual_time,
        employee_id
      );

    } else if (exists.gate_passes[0].status === 'Time Out') {


      if (exists.gate_passes[0].request_type.has_time_in === false) {
        throw new Error(`Gate pass does not have time in`);
      }

      const interval = moment(exists.gate_passes[0].updatedAt).tz('Asia/Manila').add(1, 'minutes').format('HH:mm:ss')

      if (interval >= moment().tz('Asia/Manila').format('HH:mm:ss')) {
        throw new Error(`Duplicated Scan`);
      }

      status = 'Time In'

      updateGatePass = await gatePassDb.inGatePass(
        exists.gate_passes[0].id,
        status,
        actual_time,
        employee_id
      );
    } else {


      if (exists.gate_passes[0].status === 'Pending') {
        throw new Error(`Please proceed to your immediate superior.`)
      } else if (exists.gate_passes[0].status === 'Pre-Approved') {
        throw new Error(`Please proceed to PG immediately.`)
      }

    }


    if (updateGatePass[0] > 0) {
      const log = await gatePassStatusLogsDB.logGatePassRequest(
        exists.gate_passes[0].id,
        status,
        employee_id
      )

      //  const employeeDetails = await gatePassDb.findGatePassWithEmployeeInformation(exists.gate_passes[0].id) 

      //decrypt
      exists.first_name = encryption.decrypt(exists.first_name);
      exists.last_name = encryption.decrypt(exists.last_name);
      if (exists.middle_name) {
        exists.middle_name = encryption.decrypt(exists.middle_name);
      }
      exists.gate_passes[0].status = status


      return exists;

    } else {
      throw new Error('Update failed');
    }
  };
};