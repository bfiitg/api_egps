const makeListApprovedGatePass = require('./list_approved_requests');
const makeListTimeInGatePass = require('./list_time_in_requests');
const makeListTimeOutGatePass = require('./list_time_out_requests');

const makeMarkTimeRequest = require('./put_time')

//Data-access
const gatePassDb = require('../../data-access/gate_pass/');
const gatePassStatusLogsDB = require('../../data-access/gate_pass_status_logs/');
const employeesDb = require('../../data-access/employees');

//Encryption
const encryption = require('../../encryption/encrypting');

const listApprovedGatePass = makeListApprovedGatePass(
  { gatePassDb },
  { encryption }
);
const listTimeInGatePass = makeListTimeInGatePass(
  { gatePassDb },
  { encryption }
);
const listTimeOutGatePass = makeListTimeOutGatePass(
  { gatePassDb },
  { encryption }
);

const markTimeGatePass = makeMarkTimeRequest(
  { gatePassDb },
  { gatePassStatusLogsDB },
  { employeesDb },
  { encryption }
)

const securityGuardService = Object.freeze({
  listApprovedGatePass,
  listTimeInGatePass,
  listTimeOutGatePass,

  markTimeGatePass
});

module.exports = securityGuardService;
module.exports = {
  listApprovedGatePass,
  listTimeInGatePass,
  listTimeOutGatePass,
  markTimeGatePass
};
