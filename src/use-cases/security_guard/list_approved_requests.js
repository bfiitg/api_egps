module.exports = function listApprovedGatePass({ gatePassDb }, { encryption }) {
  return async function makeListApprovedGatePass(employee_id) {
    const listGatePass = await gatePassDb.listApprovedGatePass(employee_id);
    if (!listGatePass.length) {
      return 'No data found';
    }
    //Decrypt employee data
    listGatePass.map(gatepass => {
      gatepass.first_name = encryption.decrypt(gatepass.first_name);
      gatepass.last_name = encryption.decrypt(gatepass.last_name);
      if (gatepass.middle_name) {
        gatepass.middle_name = encryption.decrypt(gatepass.middle_name);
      }
    });
    return listGatePass;
  };
};
