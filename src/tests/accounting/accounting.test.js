const accounting = require('./accounting');
let loginData;
let unauthorized;
const moment = require('moment');
const dateToday = moment().format('YYYY-MM-DD');

beforeAll(async () => {
  loginData = await accounting.login(154151106, 'emp').then(data => data);

  unauthorized = await accounting
    .login(154151104, 'emp')
    .then(data => data);
});

test('Login - Role must not be accounting', async () => {
  expect(unauthorized.role.name).not.toBe('actng-Accounting');
});

test('Login - fetch data without access rights', async () => {
  return await accounting
    .fetchEmployessReport(
      unauthorized.id,
      dateToday,
      dateToday,
      unauthorized.modules,
      unauthorized.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('Login - fetch data with access rights', async () => {
  let actions = [];
  await loginData.modules.map(modules =>
    modules.actions.map(action => actions.push(action))
  );
  expect(actions).toEqual(
    expect.arrayContaining([
      expect.objectContaining({ description: 'View verified gate pass' })
    ])
  );
});

test('Login - Role must  be accounting', async () => {
  const roles = loginData.role;
  expect(roles.name).toBe('actng-Accounting');
});

test('Accounting - fetch data personal gate pass without token', async () => {
  return await accounting
    .fetchEmployessReport(loginData.id, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Accounting - fetch data personal gate pass without modules', async () => {
  return await accounting
    .fetchEmployessReport(loginData.id, null, null, null, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Accounting - fetch data personal gate pass(NULL-NULL)', async () => {
  const request_date_from = null;
  const request_date_to = null;

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Accounting - fetch data personal gate pass(NULL-STRING))', async () => {
  const request_date_from = null;
  const request_date_to = 'hello date';

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Accounting - fetch data personal gate pass(NULL-SPECIAL CHARACTER)', async () => {
  const request_date_from = null;
  const request_date_to = '*&!^@*&#^@!';

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Accounting - fetch data personal gate pass(NULL-DATE)', async () => {
  const request_date_from = null;
  const request_date_to = '2019-06-13';
  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Accounting - fetch data personal gate pass(STRING-NULL)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = null;

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Accounting - fetch data personal gate pass(STRING-STRING)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = 'hellodate';

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Accounting - fetch data personal gate pass(STRING-SPECIAL CHARACTER)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = '^*#@^*$&^#$';

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Accounting - fetch data personal gate pass(STRING-DATE)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = '2019-06-13';

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Accounting - fetch data personal gate pass(SPECIAL CHARACTER-NULL)', async () => {
  const request_date_from = '$&^(*^@#*&$^#$';
  const request_date_to = null;

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Accounting - fetch data personal gate pass(SPECIAL CHARACTER-STRING)', async () => {
  const request_date_from = '$&^(*^@#*&$^#$';
  const request_date_to = 'hellodate';

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Accounting - fetch data personal gate pass(SPECIAL CHARACTER-SPECIAL CHARACTER)', async () => {
  const request_date_from = '$&^(*^@#*&$^#$';
  const request_date_to = '&^@#^@!##$$#';

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Accounting - fetch data personal gate pass(SPECIAL CHARACTER-DATE)', async () => {
  const request_date_from = '$&^(*^@#*&$^#$';
  const request_date_to = '2019-06-13';

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Accounting - fetch data personal gate pass(DATE-NULL)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = null;

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Accounting - fetch data personal gate pass(DATE-STRING)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = 'hellodate';

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Accounting - fetch data personal gate pass(DATE-SPECIAL CHARACTER)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = '$^@#*^$*&@#*$';
  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Accounting - fetch data personal gate pass(DATE-DATE)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = '2019-06-13';

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data).not.toBe('Error');
    });
});

test('Accounting - fetch data personal gate pass(NULL-INTEGER)', async () => {
  const request_date_from = null;
  const request_date_to = 2049;

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data).not.toBe('Error');
    });
});

test('Accounting - fetch data personal gate pass(STRING-INTEGER)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = 2049;

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data).not.toBe('Error');
    });
});

test('Accounting - fetch data personal gate pass(SPECIAL CHARACTER-INTEGER)', async () => {
  const request_date_from = '@*#^@%&^@#^%!@';
  const request_date_to = 2049;

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data).not.toBe('Error');
    });
});

test('Accounting - fetch data personal gate pass(DATE-INTEGER)', async () => {
  const request_date_from = '2019-07-17';
  const request_date_to = 2049;

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data).not.toBe('Error');
    });
});

test('Accounting - fetch data personal gate pass(INTEGER-INTEGER)', async () => {
  const request_date_from = 2049;
  const request_date_to = 2049;

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data).not.toBe('Error');
    });
});

test('Accounting - fetch data personal gate pass(INTEGER-NULL)', async () => {
  const request_date_from = 2049;
  const request_date_to = null;

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data).not.toBe('Error');
    });
});

test('Accounting - fetch data personal gate pass(INTEGER-NULL)', async () => {
  const request_date_from = 2049;
  const request_date_to = 'hellodate';

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data).not.toBe('Error');
    });
});

test('Accounting - fetch data personal gate pass(INTEGER-SPECIAL CHARACTER)', async () => {
  const request_date_from = 2049;
  const request_date_to = '@^%#&%@^&#';

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data).not.toBe('Error');
    });
});

test('Accounting - fetch data personal gate pass(INTEGER-DATE)', async () => {
  const request_date_from = 2049;
  const request_date_to = '2019-07-17';

  return await accounting
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data).not.toBe('Error');
    });
});
