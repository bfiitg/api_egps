const axios = require('axios');

const accounting = {
  login: (id, password) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/login',
      data: {
        id,
        password
      }
    })
      .then(res => res.data)
      .catch(err => err.response),

  fetchEmployessReport: (
    id,
    request_date_from,
    request_date_to,
    modules,
    token
  ) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/reports/view_verified_personal_out',
      data: {
        employee_id: id,
        request_date_from: request_date_from,
        request_date_to: request_date_to,
        modules
      },
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err)
};
module.exports = accounting;
