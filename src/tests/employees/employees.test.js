const employees = require('./employees');

let loginData;

beforeAll(async () => {
  loginData = await employees.login(154151104, 'emp');
});

// //-------------------------SCHEDULE OUT -----------------------------

test('Employees - fetch data view gate pass without access rights', async () => {
  const employee = loginData;
  const modules = [
    {
      id: 2,
      description: 'Gate Pass',
      actions: [
        {
          id: 19,
          description: 'Request gate pass',
          module_id: 2
        }
      ]
    }
  ];
  return await employees
    .fetchScheduleOut(employee.id, modules, employee.token)
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('Employees - fetch data view gate pass with access rights', async () => {
  const employee = loginData;

  let actions = [];
  employee.modules.map(modules =>
    modules.actions.map(action => actions.push(action))
  );

  expect(actions).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        description: 'View schedule out'
      })
    ])
  );
});
test('Employees - fetch data schedule out without token', async () => {
  return await employees
    .fetchScheduleOut(loginData.id, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Employees - fetch data schedule out without modules', async () => {
  return await employees
    .fetchScheduleOut(loginData.id, undefined, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Employees - fetch data schedule out', async () => {
  return await employees
    .fetchScheduleOut(loginData.id, loginData.modules, loginData.token)
    .then(data => {
      expect(data.status).not.toBe(400 && 401 && 403 && 400);
    });
});

test('Employees - fetch data schedule out(null id)', async () => {
  return await employees.fetchScheduleOut(null, null, null).then(data => {
    expect(data.response.status).toBe(403);
  });
});

test('Employees - fetch data schedule out(string id)', async () => {
  const id = loginData.id.toString();

  return await employees
    .fetchScheduleOut(id, loginData.modules, loginData.token)
    .then(data => {
      expect(data.status).not.toBe(400 && 401 && 403 && 400);
    });
});

test('Employees - fetch data schedule out(special character id)', async () => {
  const id = '!@#$%^&*()';

  return await employees
    .fetchScheduleOut(id, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});
// //-------------------------EMPLOYEE REPORT -----------------------------------------------------
test('Employees - fetch data view gate pass without access rights', async () => {
  const employee = loginData;
  const modules = [
    {
      id: 2,
      description: 'Gate Pass',
      actions: [
        {
          id: 19,
          description: 'Request gate pass',
          module_id: 2
        }
      ]
    }
  ];
  return await employees
    .fetchEmployeeReport(employee.id, modules, employee.token)
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('Employees - fetch data employee report with access rights', async () => {
  const employee = loginData;

  let actions = [];
  employee.modules.map(modules =>
    modules.actions.map(action => actions.push(action))
  );

  expect(actions).toEqual(
    expect.arrayContaining([
      expect.objectContaining({ description: 'View gate pass report' })
    ])
  );
});

test('Employees - fetch data employee report without token', async () => {
  return await employees
    .fetchEmployeeReport(loginData.id, loginData.module_id)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Employees - fetch data employee report without modules', async () => {
  return await employees
    .fetchEmployeeReport(loginData.id, undefined, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Employees - fetch data employee report', async () => {
  return await employees
    .fetchEmployeeReport(loginData.id, loginData.module_id, loginData.token)
    .then(data => {
      expect(data.status).not.toBe(400 && 401 && 403 && 400);
    });
});

test('Employees - fetch data employee report(null id)', async () => {
  const id = null;

  return await employees
    .fetchEmployeeReport(id, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Employees - fetch data employee report(string id)', async () => {
  const id = loginData.id.toString();

  return await employees.fetchEmployeeReport(id).then(data => {
    expect(data.status).not.toBe(400 && 401 && 403 && 400);
  });
});

test('Employees - fetch data employee report(special character id)', async () => {
  const id = '!@#$%^&*()';

  return await employees
    .fetchEmployeeReport(id, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});
