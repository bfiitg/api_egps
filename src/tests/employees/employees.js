const axios = require('axios');

const employees = {
  login: (id, password) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/login',
      data: {
        id,
        password
      }
    })
      .then(res => res.data)
      .catch(err => err.response),

  fetchScheduleOut: (id, modules, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/employees/schedule_out',
      data: {
        employee_id: id,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  fetchEmployeeReport: (id, modules, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/employees/employee_report',
      data: {
        employee_id: id,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err)
};

module.exports = employees;
