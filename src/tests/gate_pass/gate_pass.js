const axios = require('axios');

const gate_pass = {
  login: (id, password) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/login',
      data: {
        id,
        password
      }
    })
      .then(res => res.data)
      .catch(err => err.response),

  postGatePass: (
    employee_id,
    request_type_id,
    request_time_out,
    request_time_in,
    request_date,
    reason,
    modules,
    token
  ) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/gatepass/file_request',
      data: {
        employee_id: employee_id,
        request_type_id: request_type_id,
        request_time_out: request_time_out,
        request_time_in: request_time_in,
        request_date: request_date,
        reason: reason,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  approveRequest: (id, status, updatedBy, modules, token) =>
    axios({
      method: 'PUT',
      url: 'http://localhost:3000/gatepass/requests/validate_request/approve',
      data: {
        id: id,
        status: status,
        employee_id: updatedBy,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  cancelRequest: (id, status, updatedBy, modules, token) =>
    axios({
      method: 'PUT',
      url: 'http://localhost:3000/gatepass/cancel_request',
      data: {
        id: id,
        status: status,
        employee_id: updatedBy,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  disapproveRequest: (id, status, updatedBy, modules, token) =>
    axios({
      method: 'PUT',
      url: 'http://localhost:3000/gatepass/requests/validate_request/disapprove',
      data: {
        id: id,
        status: status,
        employee_id: updatedBy,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  verifyPersonalOut: (id, status, updatedBy, modules, token) =>
    axios({
      method: 'PUT',
      url:
        'http://localhost:3000/gatepass/requests/validate_request/verify_personal_out',
      data: {
        id: id,
        status: status,
        employee_id: updatedBy,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  unverifyPersonalOut: (id, status, updatedBy, modules, token) =>
    axios({
      method: 'PUT',
      url:
        'http://localhost:3000/gatepass/requests/validate_request/unverify_personal_out',
      data: {
        id: id,
        status: status,
        employee_id: updatedBy,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err)
};

module.exports = gate_pass;
