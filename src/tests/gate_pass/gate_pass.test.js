const gate_pass = require("./gate_pass");
const moment = require("moment");
const dateToday = moment().format("YYYY-MM-DD");

let loginData;

beforeAll(async () => {
  loginData = await gate_pass.login(154151100, "admin");
});

test("Gata Pass - file request without token", async () => {
  const request_type_id = 1;
  const request_time_out = "11:30:00";
  const request_time_in = "12:30:00";
  const request_date = dateToday;
  const reason = "Lunch out";

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test("Gata Pass - file request without module", async () => {
  const request_type_id = 1;
  const request_time_out = "11:30:00";
  const request_time_in = "12:30:00";
  const request_date = dateToday;
  const reason = "Lunch out";
  const modules = [
    {
      id: 2,
      description: "Gate Pass",
      actions: [
        {
          id: 20,
          description: "View schedule out",
          module_id: 2
        }
      ]
    }
  ];
  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test("Gate Pass - File request(null all fields)", async () => {
  const employee_id = null;
  const request_type_id = null;
  const request_time_out = null;
  const request_time_in = null;
  const request_date = null;
  const reason = null;

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(int all fields)", async () => {
  const employee_id = 123456;
  const request_type_id = 123456;
  const request_time_out = 123456;
  const request_time_in = 123456;
  const request_date = 123456;
  const reason = 123456;

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test("Gate Pass - File request(string all fields)", async () => {
  const employee_id = "employee_id";
  const request_type_id = "employee_id";
  const request_time_out = "request_time_out";
  const request_time_in = "request_time_in";
  const request_date = "request_date";
  const reason = "reason";

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(special characters all fields)", async () => {
  const employee_id = "!@#$%^&*()";
  const request_type_id = "!@#$%^&*()";
  const request_time_out = "!@#$%^&*()";
  const request_time_in = "!@#$%^&*()";
  const request_date = "!@#$%^&*()";
  const reason = "!@#$%^&*()";

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given id - null rest of fields)", async () => {
  const request_type_id = null;
  const request_time_out = null;
  const request_time_in = null;
  const request_date = null;
  const reason = null;
  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given request_type_id- null rest of fields)", async () => {
  const employee_id = null;
  const request_type_id = 2;
  const request_time_out = null;
  const request_time_in = null;
  const request_date = null;
  const reason = null;

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given request_time_out - null rest of fields)", async () => {
  const employee_id = null;
  const request_type_id = null;
  const request_time_out = "09:58:00";
  const request_time_in = null;
  const request_date = null;
  const reason = null;

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given request_time_in - null rest of fields)", async () => {
  const employee_id = null;
  const request_type_id = null;
  const request_time_out = null;
  const request_time_in = "09:58:00";
  const request_date = null;
  const reason = null;

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given request_date - null rest of fields)", async () => {
  const employee_id = null;
  const request_type_id = null;
  const request_time_out = null;
  const request_time_in = null;
  const request_date = "2019-07-13";
  const reason = null;

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given reason - null rest of fields)", async () => {
  const employee_id = null;
  const request_type_id = null;
  const request_time_out = null;
  const request_time_in = null;
  const request_date = null;
  const reason = "Lunch Out";

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});
//string
test("Gate Pass - File request(given employee id - string rest of fields)", async () => {
  const request_type_id = "employee_id";
  const request_time_out = "request_time_out";
  const request_time_in = "request_time_in";
  const request_date = "request_date";
  const reason = "reason";

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given request_type_id - string rest of fields)", async () => {
  const employee_id = "employee_id";
  const request_type_id = 2;
  const request_time_out = "request_time_out";
  const request_time_in = "request_time_in";
  const request_date = "request_date";
  const reason = "reason";

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given request_time_out  - string rest of fields)", async () => {
  const employee_id = "employee_id";
  const request_type_id = "request_type_id";
  const request_time_out = "09:58:00";
  const request_time_in = "request_time_in";
  const request_date = "request_date";
  const reason = "reason";

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given request_time_in  - string rest of fields)", async () => {
  const employee_id = "employee_id";
  const request_type_id = "request_type_id";
  const request_time_out = "request_time_out";
  const request_time_in = "09:58:00";
  const request_date = "request_date";
  const reason = "reason";

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given request date - string rest of fields)", async () => {
  const employee_id = "employee_id";
  const request_type_id = "request_type_id";
  const request_time_out = "request_time_out";
  const request_time_in = "request_time_in";
  const request_date = "2019-07-13";
  const reason = "reason";

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given request date - string rest of fields)", async () => {
  const employee_id = "employee_id";
  const request_type_id = "request_type_id";
  const request_time_out = "request_time_out";
  const request_time_in = "request_time_in";
  const request_date = "2019-07-13";
  const reason = "reason";

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

// //special character
test("Gate Pass - File request(given employee id - special character rest of fields)", async () => {
  const employee_id = 154151101;
  const request_type_id = "!@#$%^&*()";
  const request_time_out = "!@#$%^&*()";
  const request_time_in = "!@#$%^&*()";
  const request_date = "!@#$%^&*()";
  const reason = "!@#$%^&*()";

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test("Gate Pass - File request(given request_type_id - special character rest of fields)", async () => {
  const employee_id = "!@#$%^&*()";
  const request_type_id = 2;
  const request_time_out = "!@#$%^&*()";
  const request_time_in = "!@#$%^&*()";
  const request_date = "!@#$%^&*()";
  const reason = "!@#$%^&*()";

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given request_time_out - special character rest of fields)", async () => {
  const employee_id = "!@#$%^&*()";
  const request_type_id = "!@#$%^&*()";
  const request_time_out = "09:58:00";
  const request_time_in = "!@#$%^&*()";
  const request_date = "!@#$%^&*()";
  const reason = "!@#$%^&*()";

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given request_time_in - special character rest of fields)", async () => {
  const employee_id = "!@#$%^&*()";
  const request_type_id = "!@#$%^&*()";
  const request_time_out = "!@#$%^&*()";
  const request_time_in = "09:58:00";
  const request_date = "!@#$%^&*()";
  const reason = "!@#$%^&*()";

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given request date  - special character rest of fields)", async () => {
  const employee_id = "!@#$%^&*()";
  const request_type_id = "!@#$%^&*()";
  const request_time_out = "!@#$%^&*()";
  const request_time_in = "!@#$%^&*()";
  const request_date = "2019-07-13";
  const reason = "!@#$%^&*()";

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given request date  - special character rest of fields)", async () => {
  const employee_id = "!@#$%^&*()";
  const request_type_id = "!@#$%^&*()";
  const request_time_out = "!@#$%^&*()";
  const request_time_in = "!@#$%^&*()";
  const request_date = "!@#$%^&*()-07-13";
  const reason = "Lunch Out";

  return await gate_pass
    .postGatePass(
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});
// //2 given data
test("Gate Pass - File request(given id and request_type_id - null rest of fields)", async () => {
  const request_type_id = 2;
  const request_time_out = null;
  const request_time_in = null;
  const request_date = null;
  const reason = null;

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given id and request_type_id - string rest of fields)", async () => {
  const request_type_id = 2;
  const request_time_out = "null";
  const request_time_in = "null";
  const request_date = "null";
  const reason = "null";

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given id and request_type_id - special character rest of fields)", async () => {
  const employee_id = 154151102;
  const request_type_id = 2;
  const request_time_out = "!@#$%^&*()";
  const request_time_in = "!@#$%^&*()";
  const request_date = "!@#$%^&*()";
  const reason = "!@#$%^&*()";

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given id and request_type_id - int rest of fields)", async () => {
  const request_type_id = 2;
  const request_time_out = 321654;
  const request_time_in = 31654;
  const request_date = 321654;
  const reason = 31654;

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});
// //3 given fields
test("Gate Pass - File request(given id, request_type_id and request_time_out - null rest of fields)", async () => {
  const request_type_id = 2;
  const request_time_out = "08:33:00";
  const request_time_in = null;
  const request_date = null;
  const reason = null;

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given id, request_type_id and request_time_out - string rest of fields)", async () => {
  const request_type_id = 2;
  const request_time_out = "08:33:00";
  const request_time_in = "null";
  const request_date = "null";
  const reason = "null";

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given id, request_type_id and request_time_out - special character rest of fields)", async () => {
  const request_type_id = 2;
  const request_time_out = "08:33:00";
  const request_time_in = "!@#$%^&*()";
  const request_date = "!@#$%^&*()";
  const reason = "!@#$%^&*()";

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given id, request_type_id and request_time_out - int rest of fields)", async () => {
  const employee_id = 154151102;
  const request_type_id = 2;
  const request_time_out = "08:33:00";
  const request_time_in = 123456987;
  const request_date = 12345;
  const reason = 123456;

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

// //4 given data
test("Gate Pass - File request(given id, request_type_id, request_time_out and request_time_in - null rest of fields)", async () => {
  const request_type_id = 2;
  const request_time_out = "08:33:00";
  const request_time_in = "09:33:00";
  const request_date = null;
  const reason = null;

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given id, request_type_id, request_time_out and request_time_in - string rest of fields)", async () => {
  const request_type_id = 2;
  const request_time_out = "08:33:00";
  const request_time_in = "09:33:00";
  const request_date = "null";
  const reason = "null";

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given id, request_type_id, request_time_out and request_time_in - int rest of fields)", async () => {
  const request_type_id = 2;
  const request_time_out = "08:33:00";
  const request_time_in = "09:33:00";
  const request_date = 3169745789;
  const reason = 32165;

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given id, request_type_id, request_time_out and request_time_in - special character rest of fields)", async () => {
  const request_type_id = 2;
  const request_time_out = "08:33:00";
  const request_time_in = "09:33:00";
  const request_date = "!@#$%^&*()";
  const reason = "!@#$%^&*()";

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

// //5 given
test("Gate Pass - File request(given id, request_type_id, request_time_out, request_time_in and request_date - null rest of fields)", async () => {
  const employee_id = 154151102;
  const request_type_id = 2;
  const request_time_out = "08:33:00";
  const request_time_in = "09:33:00";
  const request_date = "2019-09-16";
  const reason = null;

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given id, request_type_id, request_time_out, request_time_in and request_date - empty string rest of fields)", async () => {
  const request_type_id = 2;
  const request_time_out = "08:33:00";
  const request_time_in = "09:33:00";
  const request_date = "2019-08-16";
  const reason = "";

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given id, request_type_id, request_time_out, request_time_in and request_date - int rest of fields)", async () => {
  const employee_id = 154151102;
  const request_type_id = 2;
  const request_time_out = "08:33:00";
  const request_time_in = "09:33:00";
  const request_date = "2019-08-16";
  const reason = 32145;

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request(given id, request_type_id, request_time_out, request_time_in and request_date - special character rest of fields)", async () => {
  const request_type_id = 2;
  const request_time_out = "2019-07-16 08:33:00";
  const request_time_in = "2019-07-16 09:33:00";
  const request_date = "2019-07-16";
  const reason = "!@#$%^&*()";

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request (Perfect scenario Lunch out)", async () => {
  const request_type_id = 1;
  const request_time_out = "2019-08-16 08:33:00";
  const request_time_in = "2019-08-16 09:33:00";
  const request_date = "2019-08-16";
  const reason = "Lunch out";
  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(201);
    });
});

test("Gate Pass - File request (Perfect scenario Personal out)", async () => {
  const request_type_id = 2;
  const request_time_out = "2019-08-16 09:33:00";
  const request_time_in = "2019-08-16 10:33:00";
  const request_date = "2019-08-16";
  const reason = "Emergency";
  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(201);
    });
});

test("Gate Pass - File request (Perfect scenario Official Business)", async () => {
  const request_type_id = 3;
  const request_time_out = "2019-08-16 14:33:00";
  const request_time_in = "2019-08-16 16:33:00";
  const request_date = "2019-08-16";
  const reason = "Meeting with the Department Heads";
  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(201);
    });
});

test("Gate Pass - File request (Perfect scenario Half day)", async () => {
  const request_type_id = 4;
  const request_time_out = "2019-08-16 08:33:00";
  const request_time_in = "2019-08-16 12:33:00";
  const request_date = "2019-08-16";
  const reason = "Nausea";
  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(201);
    });
});

test("Gate Pass - File request (Perfect scenario Undertime)", async () => {
  const request_type_id = 5;
  const request_time_out = "2019-08-16 16:33:00";
  const request_time_in = "2019-08-16 17:33:00";
  const request_date = "2019-08-16";
  const reason = "Nausea";
  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(201);
    });
});

test("Gate Pass - File request (Request for Half day with Undertime Pass already within this day)", async () => {
  const request_type_id = 4;
  const request_time_out = "2019-08-13 08:33:00";
  const request_time_in = "2019-08-13 09:33:00";
  const request_date = "2019-08-13";
  const reason = "Nausea";
  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - File request  (Lunch out in same day)", async () => {
  const request_type_id = 1;
  const request_time_out = "2019-08-13 08:33:00";
  const request_time_in = "2019-08-13 09:33:00";
  const request_date = "2019-08-13";
  const reason = "Lunch out";

  return await gate_pass
    .postGatePass(
      loginData.id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

//********************** Approve request
test("Gate Pass - approve Request without token", async () => {
  return await gate_pass
    .approveRequest(1, "Approved", loginData.id, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test("Gate Pass - approve Request without module", async () => {
  return await gate_pass
    .approveRequest(1, "Approved", loginData.id, undefined, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - approve Request without access rights", async () => {
  const modules = [
    {
      id: 2,
      description: "Gate Pass",
      actions: [
        {
          id: 19,
          description: "Request gate pass",
          module_id: 2
        },
        {
          id: 20,
          description: "View schedule out",
          module_id: 2
        }
      ]
    }
  ];
  return await gate_pass
    .approveRequest(1, "Approved", loginData.id, modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - aprrove Request(null all values)", async () => {
  const id = null;
  const status = null;
  const updatedBy = null;

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - approve Request (NULL(string updatedBy))", async () => {
  const id = null;
  const status = null;
  const updatedBy = "154151102";

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(403);
  });
});

test("Gate Pass - approve Request(NULL(special character updatedBy)", async () => {
  const id = null;
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - approve Request(NULL(special character updatedBy)", async () => {
  const id = null;
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - approve Request(NULL(string status)", async () => {
  const id = null;
  const status = "Approved";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - approve Request(NULL(string status and string updatedby)", async () => {
  const id = null;
  const status = "Approved";
  const updatedBy = "154151102";

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(403);
  });
});

test("Gate Pass - approve Request(NULL(string status and special character updatedby)", async () => {
  const id = null;
  const status = "Approved";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - approve Request(NULL(special character status and null updatedby)", async () => {
  const id = null;
  const status = "!@#$%^&*(";
  const updatedBy = null;

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - approve Request(NULL(special character status and special character updatedby)", async () => {
  const id = null;
  const status = "!@#$%^&*(";
  const updatedBy = "!@#$%^&*(";

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

// //string
test("Gate Pass - approve Request(STRING(string status and string updatedby)", async () => {
  const id = "2";
  const status = "Approved";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .approveRequest(id, status, updatedBy, loginData.modules, loginData.token)
    .then(data => {
      expect(data.status).toBe(201);
    });
});

test("Gate Pass - approve Request(STRING(string status and null updatedby)", async () => {
  const id = "2";
  const status = "Approved";
  const updatedBy = null;

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - approve Request(STRING(string status and special character updatedby)", async () => {
  const id = "2";
  const status = "Approved";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - approve Request(STRING(null status and special character updatedby)", async () => {
  const id = "2";
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - approve Request(STRING(null status and null updatedby)", async () => {
  const id = "2";
  const status = null;
  const updatedBy = null;

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Approve Request(STRING(special character status and string updatedby)", async () => {
  const id = "2";
  const status = "!@#$%^&*()";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .approveRequest(id, status, updatedBy, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Approve Request(STRING(special character status and null updatedby)", async () => {
  const id = "2";
  const status = "!@#$%^&*()";
  const updatedBy = null;

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Approve Request(STRING(special character status and special character updatedby)", async () => {
  const id = "2";
  const status = "!@#$%^&*()";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Approve Request(Special Character(special character status and special character updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "!@#$%^&*()";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Approve Request(Special Character(special character status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "!@#$%^&*()";
  const updatedBy = null;

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Approve Request(Special Character(special character status and string updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "!@#$%^&*()";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .approveRequest(id, status, updatedBy, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Approve Request(Special Character(null status and special character updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Approve Request(Special Character(null status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = null;
  const updatedBy = null;

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Approve Request(Special Character(null status and string updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = null;
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .approveRequest(id, status, updatedBy, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Approve Request(Special Character(string status and special character updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "Approved";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Approve Request(Special Character(string status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "Approved";
  const updatedBy = null;

  return await gate_pass.approveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Approve Request(Special Character(string status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "Approved";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .approveRequest(id, status, updatedBy, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Approve Request (Perfect scenario)", async () => {
  const id = 1;
  const status = "Approved";
  const updatedBy = loginData.id;

  return await gate_pass
    .approveRequest(id, status, updatedBy, loginData.modules, loginData.token)
    .then(data => {
      expect(data.status).toBe(201);
    });
});

test("Gate Pass - Approve Request (Invalid status", async () => {
  const id = 1;
  const status = "Disapprove";
  const updatedBy = loginData.id;

  return await gate_pass
    .approveRequest(id, status, updatedBy, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

//********************** Disapprove request

test("Gate Pass - Dispprove Request without token", async () => {
  return await gate_pass
    .disapproveRequest(1, "Disapproved", loginData.id, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test("Gate Pass - Dispprove Request without module", async () => {
  return await gate_pass
    .disapproveRequest(
      1,
      "Disapproved",
      loginData.id,
      undefined,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Dispprove Request without access rights", async () => {
  const modules = [
    {
      id: 2,
      description: "Gate Pass",
      actions: [
        {
          id: 19,
          description: "Request gate pass",
          module_id: 2
        },
        {
          id: 20,
          description: "View schedule out",
          module_id: 2
        }
      ]
    }
  ];
  return await gate_pass
    .disapproveRequest(1, "Disapproved", loginData.id, modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - aprrove Request(null all values)", async () => {
  const id = null;
  const status = null;
  const updatedBy = null;

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request (NULL(string updatedBy))", async () => {
  const id = null;
  const status = null;
  const updatedBy = "154151102";

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(403);
  });
});

test("Gate Pass - Dispprove Request(NULL(special character updatedBy)", async () => {
  const id = null;
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(NULL(special character updatedBy)", async () => {
  const id = null;
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(NULL(string status)", async () => {
  const id = null;
  const status = "Disapproved";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(NULL(string status and string updatedby)", async () => {
  const id = null;
  const status = "Disapproved";
  const updatedBy = "154151102";

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(403);
  });
});

test("Gate Pass - Dispprove Request(NULL(string status and special character updatedby)", async () => {
  const id = null;
  const status = "Disapproved";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(NULL(special character status and null updatedby)", async () => {
  const id = null;
  const status = "!@#$%^&*(";
  const updatedBy = null;

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(NULL(special character status and special character updatedby)", async () => {
  const id = null;
  const status = "!@#$%^&*(";
  const updatedBy = "!@#$%^&*(";

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

// //string
test("Gate Pass - Dispprove Request(STRING(string status and string updatedby)", async () => {
  const id = "2";
  const status = "Disapproved";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .disapproveRequest(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(201);
    });
});

test("Gate Pass - Dispprove Request(STRING(string status and null updatedby)", async () => {
  const id = "2";
  const status = "Disapproved";
  const updatedBy = null;

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(STRING(string status and special character updatedby)", async () => {
  const id = "2";
  const status = "Disapproved";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(STRING(null status and special character updatedby)", async () => {
  const id = "2";
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(STRING(null status and null updatedby)", async () => {
  const id = "2";
  const status = null;
  const updatedBy = null;

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(STRING(special character status and string updatedby)", async () => {
  const id = "2";
  const status = "!@#$%^&*()";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .disapproveRequest(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Dispprove Request(STRING(special character status and null updatedby)", async () => {
  const id = "2";
  const status = "!@#$%^&*()";
  const updatedBy = null;

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(STRING(special character status and special character updatedby)", async () => {
  const id = "2";
  const status = "!@#$%^&*()";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(Special Character(special character status and special character updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "!@#$%^&*()";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(Special Character(special character status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "!@#$%^&*()";
  const updatedBy = null;

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(Special Character(special character status and string updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "!@#$%^&*()";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .disapproveRequest(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Dispprove Request(Special Character(null status and special character updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(Special Character(null status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = null;
  const updatedBy = null;

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(Special Character(null status and string updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = null;
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .disapproveRequest(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Dispprove Request(Special Character(string status and special character updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "Disapproved";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(Special Character(string status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "Disapproved";
  const updatedBy = null;

  return await gate_pass.disapproveRequest(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Dispprove Request(Special Character(string status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "Disapproved";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .disapproveRequest(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Dispprove Request (Perfect scenario)", async () => {
  const id = 1;
  const status = "Disapproved";
  const updatedBy = loginData.id;

  return await gate_pass
    .disapproveRequest(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(201);
    });
});

test("Gate Pass - Dispprove Request (Invalid status", async () => {
  const id = 1;
  const status = "Disapprove";
  const updatedBy = loginData.id;

  return await gate_pass
    .disapproveRequest(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

// **********************  Unverify Personal out
test("Gate Pass - Unverify personal out without token", async () => {
  return await gate_pass
    .unverifyPersonalOut(1, "Time In", loginData.id, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test("Gate Pass - Unverify personal out without module", async () => {
  return await gate_pass
    .unverifyPersonalOut(1, "Time In", loginData.id, undefined, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out without access rights", async () => {
  const modules = [
    {
      id: 2,
      description: "Gate Pass",
      actions: [
        {
          id: 19,
          description: "Request gate pass",
          module_id: 2
        },
        {
          id: 20,
          description: "View schedule out",
          module_id: 2
        }
      ]
    }
  ];
  return await gate_pass
    .unverifyPersonalOut(1, "Time In", loginData.id, modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify Request(null all values)", async () => {
  const id = null;
  const status = null;
  const updatedBy = null;

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out (NULL(string updatedBy))", async () => {
  const id = null;
  const status = null;
  const updatedBy = "154151102";

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test("Gate Pass - Unverify personal out(NULL(special character updatedBy)", async () => {
  const id = null;
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(NULL(special character updatedBy)", async () => {
  const id = null;
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(NULL(string status)", async () => {
  const id = null;
  const status = "Time In";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(NULL(string status and string updatedby)", async () => {
  const id = null;
  const status = "Time In";
  const updatedBy = "154151102";

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test("Gate Pass - Unverify personal out(NULL(string status and special character updatedby)", async () => {
  const id = null;
  const status = "Time In";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(NULL(special character status and null updatedby)", async () => {
  const id = null;
  const status = "!@#$%^&*(";
  const updatedBy = null;

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(NULL(special character status and special character updatedby)", async () => {
  const id = null;
  const status = "!@#$%^&*(";
  const updatedBy = "!@#$%^&*(";

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

// //string
test("Gate Pass - Unverify personal out(STRING(string status and string updatedby)", async () => {
  const id = "2";
  const status = "Time In";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .unverifyPersonalOut(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(201);
    });
});

test("Gate Pass - Unverify personal out(STRING(string status and null updatedby)", async () => {
  const id = "2";
  const status = "Time In";
  const updatedBy = null;

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(STRING(string status and special character updatedby)", async () => {
  const id = "2";
  const status = "Time In";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(STRING(null status and special character updatedby)", async () => {
  const id = "2";
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(STRING(null status and null updatedby)", async () => {
  const id = "2";
  const status = null;
  const updatedBy = null;

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(STRING(special character status and string updatedby)", async () => {
  const id = "2";
  const status = "!@#$%^&*()";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .unverifyPersonalOut(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(STRING(special character status and null updatedby)", async () => {
  const id = "2";
  const status = "!@#$%^&*()";
  const updatedBy = null;

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(STRING(special character status and special character updatedby)", async () => {
  const id = "2";
  const status = "!@#$%^&*()";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(Special Character(special character status and special character updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "!@#$%^&*()";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(Special Character(special character status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "!@#$%^&*()";
  const updatedBy = null;

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(Special Character(special character status and string updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "!@#$%^&*()";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .unverifyPersonalOut(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(Special Character(null status and special character updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(Special Character(null status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = null;
  const updatedBy = null;

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(Special Character(null status and string updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = null;
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .unverifyPersonalOut(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(Special Character(string status and special character updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "Time In";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(Special Character(string status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "Time In";
  const updatedBy = null;

  return await gate_pass
    .unverifyPersonalOut(id, status, updatedBy)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out(Special Character(string status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "Time In";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .unverifyPersonalOut(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Unverify personal out (Perfect scenario)", async () => {
  const id = 1;
  const status = "Time In";
  const updatedBy = loginData.id;

  return await gate_pass
    .unverifyPersonalOut(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(201);
    });
});

test("Gate Pass - Unverify personal out (Invalid status)", async () => {
  const id = 1;
  const status = "Disapprove";
  const updatedBy = loginData.id;

  return await gate_pass
    .unverifyPersonalOut(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

//Cancel Request
test("Gate Pass - Cancel request without token", async () => {
  const id = 2;
  const status = "Cancelled";
  const updatedBy = loginData.id;

  return await gate_pass
    .cancelRequest(id, status, updatedBy, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test("Gate Pass - Cancel request without module", async () => {
  const id = 2;
  const status = "Cancelled";
  const updatedBy = loginData.id;

  return await gate_pass
    .cancelRequest(id, status, updatedBy, undefined, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Cancel request without token", async () => {
  const id = 2;
  const status = "Cancelled";
  const updatedBy = loginData.id;
  const modules = [
    {
      id: 2,
      description: "Gate Pass",
      actions: [
        {
          id: 19,
          description: "Request gate pass",
          module_id: 2
        },
        {
          id: 20,
          description: "View schedule out",
          module_id: 2
        }
      ]
    }
  ];

  return await gate_pass
    .cancelRequest(id, status, updatedBy, modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Cancel request null id", async () => {
  const id = 2;
  const status = "Cancelled";
  const updatedBy = loginData.id;

  return await gate_pass
    .cancelRequest(null, status, updatedBy, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Cancel request null status", async () => {
  const id = 2;
  const status = "Cancelled";
  const updatedBy = loginData.id;

  return await gate_pass
    .cancelRequest(id, null, updatedBy, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Cancel request null updated by", async () => {
  const id = 2;
  const status = "Cancelled";
  const updatedBy = loginData.id;

  return await gate_pass
    .cancelRequest(id, status, null, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Cancel request invalid status", async () => {
  const id = 2;
  const status = "Cancel";
  const updatedBy = loginData.id;

  return await gate_pass
    .cancelRequest(id, status, updatedBy, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Cancel request string id", async () => {
  const id = 2;
  const status = "Cancel";
  const updatedBy = loginData.id;

  return await gate_pass
    .cancelRequest("id", status, updatedBy, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Cancel request (Perfect scenario)", async () => {
  const id = 1;
  const status = "Cancelled";
  const updatedBy = loginData.id;

  return await gate_pass
    .cancelRequest(id, status, updatedBy, loginData.modules, loginData.token)
    .then(data => {
      expect(data.status).toBe(201);
    });
});

// // **********************  Verify Personal out
test("Gate Pass - Verify personal out without token", async () => {
  return await gate_pass
    .verifyPersonalOut(1, "Verified", loginData.id, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test("Gate Pass - Verify personal out without module", async () => {
  return await gate_pass
    .verifyPersonalOut(1, "Verified", loginData.id, undefined, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Verify personal out without access rights", async () => {
  const modules = [
    {
      id: 2,
      description: "Gate Pass",
      actions: [
        {
          id: 19,
          description: "Request gate pass",
          module_id: 2
        },
        {
          id: 20,
          description: "View schedule out",
          module_id: 2
        }
      ]
    }
  ];
  return await gate_pass
    .verifyPersonalOut(1, "Verified", loginData.id, modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Verify Request(null all values)", async () => {
  const id = null;
  const status = null;
  const updatedBy = null;

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out (NULL(string updatedBy))", async () => {
  const id = null;
  const status = null;
  const updatedBy = "154151102";

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(403);
  });
});

test("Gate Pass - Verify personal out(NULL(special character updatedBy)", async () => {
  const id = null;
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(NULL(special character updatedBy)", async () => {
  const id = null;
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(NULL(string status)", async () => {
  const id = null;
  const status = "Verified";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(NULL(string status and string updatedby)", async () => {
  const id = null;
  const status = "Verified";
  const updatedBy = "154151102";

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(403);
  });
});

test("Gate Pass - Verify personal out(NULL(string status and special character updatedby)", async () => {
  const id = null;
  const status = "Verified";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(NULL(special character status and null updatedby)", async () => {
  const id = null;
  const status = "!@#$%^&*(";
  const updatedBy = null;

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(NULL(special character status and special character updatedby)", async () => {
  const id = null;
  const status = "!@#$%^&*(";
  const updatedBy = "!@#$%^&*(";

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

// //string
test("Gate Pass - Verify personal out(STRING(string status and string updatedby)", async () => {
  const id = "2";
  const status = "Verified";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .verifyPersonalOut(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(201);
    });
});

test("Gate Pass - Verify personal out(STRING(string status and null updatedby)", async () => {
  const id = "2";
  const status = "Verified";
  const updatedBy = null;

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(STRING(string status and special character updatedby)", async () => {
  const id = "2";
  const status = "Verified";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(STRING(null status and special character updatedby)", async () => {
  const id = "2";
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(STRING(null status and null updatedby)", async () => {
  const id = "2";
  const status = null;
  const updatedBy = null;

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(STRING(special character status and string updatedby)", async () => {
  const id = "2";
  const status = "!@#$%^&*()";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .verifyPersonalOut(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Verify personal out(STRING(special character status and null updatedby)", async () => {
  const id = "2";
  const status = "!@#$%^&*()";
  const updatedBy = null;

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(STRING(special character status and special character updatedby)", async () => {
  const id = "2";
  const status = "!@#$%^&*()";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(Special Character(special character status and special character updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "!@#$%^&*()";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(Special Character(special character status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "!@#$%^&*()";
  const updatedBy = null;

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(Special Character(special character status and string updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "!@#$%^&*()";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .verifyPersonalOut(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Verify personal out(Special Character(null status and special character updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = null;
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(Special Character(null status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = null;
  const updatedBy = null;

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(Special Character(null status and string updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = null;
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .verifyPersonalOut(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Verify personal out(Special Character(string status and special character updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "Verified";
  const updatedBy = "!@#$%^&*()";

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(Special Character(string status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "Verified";
  const updatedBy = null;

  return await gate_pass.verifyPersonalOut(id, status, updatedBy).then(data => {
    expect(data.response.status).toBe(400);
  });
});

test("Gate Pass - Verify personal out(Special Character(string status and null updatedby)", async () => {
  const id = "!@#$%^&*()";
  const status = "Verified";
  const updatedBy = loginData.id.toString();

  return await gate_pass
    .verifyPersonalOut(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test("Gate Pass - Verify personal out (Perfect scenario)", async () => {
  const id = 1;
  const status = "Verified";
  const updatedBy = loginData.id;

  return await gate_pass
    .verifyPersonalOut(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(201);
    });
});

test("Gate Pass - Verify personal out (Invalid status", async () => {
  const id = 1;
  const status = "Disapprove";
  const updatedBy = loginData.id;

  return await gate_pass
    .verifyPersonalOut(
      id,
      status,
      updatedBy,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});
