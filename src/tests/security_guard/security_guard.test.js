const security_guard = require('./security_guard');
const moment = require('moment');
const dateToday = moment().format('YYYY-MM-DD');
let loginData;
let unauthorizedData;

beforeAll(async () => {
  loginData = await security_guard.login(154151105, 'emp');
  unauthorizedData = await security_guard.login(
    154151104,
    'emp'
  );
});

test('Security guard - fetch data without token', async () => {
  return await security_guard
    .fetchSGApproved(loginData.id, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Security guard - fetch data without modules', async () => {
  return await security_guard
    .fetchSGApproved(loginData.id, undefined, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Security guard - fetch data without access rights', async () => {
  return await await security_guard
    .fetchSGApproved(
      unauthorizedData.id,
      unauthorizedData.modules,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('Security Guard - Fetch data viewApproved route', async () => {
  return await security_guard
    .fetchSGApproved(loginData.id, loginData.modules, loginData.token)
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Security Guard - Fetch data viewTimeout without token', async () => {
  return await security_guard
    .fetchSGTimeout(loginData.id, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Security Guard - Fetch data viewTimeout without modules', async () => {
  return await security_guard
    .fetchSGTimeout(loginData.id, undefined, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Security Guard - Fetch data viewTimeout without access rights', async () => {
  return await security_guard
    .fetchSGTimeout(
      unauthorizedData.id,
      unauthorizedData.modules,
      unauthorizedData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('Security Guard - Fetch data viewTimeout route', async () => {
  return await security_guard
    .fetchSGTimeout(loginData.id, loginData.modules, loginData.token)
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Security Guard - time out without token', async () => {
  const id = 1;
  const status = 'Time Out';

  return await security_guard
    .SGTimeOut(id, loginData.id, status, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Security Guard - time out without modules', async () => {
  const id = 1;
  const status = 'Time Out';

  return await security_guard
    .SGTimeOut(id, loginData.id, status, undefined, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Security Guard - time out without access rights', async () => {
  const id = 1;
  const status = 'Time Out';

  return await security_guard
    .SGTimeOut(
      id,
      unauthorizedData.id,
      status,
      unauthorizedData.modules,
      unauthorizedData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('Security Guard - time out null status', async () => {
  const id = 1;
  const status = null;

  return await security_guard
    .SGTimeOut(id, loginData.id, status, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Security Guard - time out invalid status', async () => {
  const id = 1;
  const status = 'status';

  return await security_guard
    .SGTimeOut(id, loginData.id, status, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Security Guard - time out null id', async () => {
  const id = null;
  const status = 'Time Out';

  return await security_guard
    .SGTimeOut(id, loginData.id, status, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Security Guard - time out string id', async () => {
  const id = 'gatepass';
  const status = 'Time Out';

  return await security_guard
    .SGTimeOut(id, loginData.id, status, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Security Guard - correct data given in time out', async () => {
  const id = 1;
  const status = 'Time Out';

  return await security_guard
    .SGTimeOut(id, loginData.id, status, loginData.modules, loginData.token)
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Security Guard - time in without token', async () => {
  const id = 1;
  const status = 'Time In';

  return await security_guard
    .SGTimein(id, loginData.id, status, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Security Guard - time in without modules', async () => {
  const id = 1;
  const status = 'Time In';

  return await security_guard
    .SGTimein(id, loginData.id, status, undefined, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Security Guard - time in without access rights', async () => {
  const id = 1;
  const status = 'Time In';

  return await security_guard
    .SGTimein(
      id,
      unauthorizedData.id,
      status,
      unauthorizedData.modules,
      unauthorizedData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('Security Guard - time in null status', async () => {
  const id = 1;
  const status = null;

  return await security_guard
    .SGTimein(id, loginData.id, status, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Security Guard - time in invalid status', async () => {
  const id = 1;
  const status = 'status';

  return await security_guard
    .SGTimein(id, loginData.id, status, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Security Guard - time in null id', async () => {
  const id = null;
  const status = 'Time In';

  return await security_guard
    .SGTimein(id, loginData.id, status, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Security Guard - time in string id', async () => {
  const id = 'gatepass';
  const status = 'Time In';

  return await security_guard
    .SGTimein(id, loginData.id, status, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Security Guard - time in perfect scenario', async () => {
  const id = 1;
  const status = 'Time In';

  return await security_guard
    .SGTimein(id, loginData.id, status, loginData.modules, loginData.token)
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Security Guard - time in gatepass that does not have time in', async () => {
  const id = 2;
  const status = 'Time In';

  return await security_guard
    .SGTimein(id, loginData.id, status, loginData.modules, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});
