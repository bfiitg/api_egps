const axios = require('axios');

const security_guard = {
  login: (id, password) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/login',
      data: {
        id,
        password
      }
    })
      .then(res => res.data)
      .catch(err => err.response),

  fetchSGApproved: (id, modules, token, roleParams) =>
    axios({
      method: 'POST',
      url: `http://localhost:3000/securityguard/viewApproved`,
      data: { employee_id: id, modules },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  fetchSGTimein: (id, modules, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/securityguard/viewTimein',
      data: { employee_id: id, modules },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  fetchSGTimeout: (id, modules, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/securityguard/viewTimeout',
      data: { employee_id: id, modules },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  // fetchSGSearch: (employee_id, search_by, modules, token) =>
  //   axios({
  //     method: 'POST',
  //     url: `http://localhost:3000/securityguard/search`,
  //     data: {
  //       employee_id: employee_id,
  //       search_by,
  //       modules
  //     },
  //     headers: { Authorization: 'Bearer ' + token }
  //   })
  //     .then(res => res)
  //     .catch(err => err),

  SGTimeOut: (id, employee_id, status, modules, token) =>
    axios({
      method: 'PUT',
      url: `http://localhost:3000/securityguard/gatepass/time_out`,
      data: {
        id,
        employee_id,
        status,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  SGTimein: (id, employee_id, status, modules, token) =>
    axios({
      method: 'PUT',
      url: `http://localhost:3000/securityguard/gatepass/time_in`,
      data: {
        id,
        employee_id,
        status: status,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err)
};

module.exports = security_guard;
