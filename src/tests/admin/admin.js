const axios = require('axios');

const admin = {
  login: (id, password) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/login',
      data: {
        id,
        password
      }
    })
      .then(res => res.data)
      .catch(err => err.response),

  listEmployees: (id, role, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/employees',
      data: {
        employee_id: id,
        role
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  addEmployee: (
    id,
    role_id,
    position_id,
    first_name,
    last_name,
    middle_name,
    created_by,
    role,
    token,
    adminEmail
  ) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/employees/add_employee',
      data: {
        id,
        role_id,
        position_id,
        first_name,
        middle_name,
        last_name,
        created_by,
        role,
        adminEmail
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  updateEmployee: (
    id,
    role_id,
    position_id,
    first_name,
    middle_name,
    last_name,
    password,
    status,
    updated_by,
    role,
    token
  ) =>
    axios({
      method: 'PUT',
      url: 'http://localhost:3000/admin/employees/update_employee',
      data: {
        id,
        role_id,
        position_id,
        first_name,
        last_name,
        middle_name,
        password,
        status,
        updated_by,
        role
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  listRoles: (role, employee_id, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/roles/',
      data: {
        role,
        employee_id
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  addRole: (name, created_by, role, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/roles/add_role',
      data: {
        name,
        created_by,
        role
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  updateRole: (id, name, status, updated_by, role, token) =>
    axios({
      method: 'PUT',
      url: 'http://localhost:3000/admin/roles/update_role',
      data: {
        id,
        name,
        status,
        updated_by,
        role
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  listPositions: (role, employee_id, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/positions',
      data: {
        role,
        employee_id
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  addPositions: (name, team_id, created_by, role, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/positions/add_position',
      data: {
        name,
        team_id,
        created_by,
        role
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  updatePositions: (id, name, team_id, status, updated_by, role, token) =>
    axios({
      method: 'PUT',
      url: 'http://localhost:3000/admin/positions/update_position',
      data: {
        id,
        name,
        team_id,
        status,
        updated_by,
        role
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  listTeams: (role, employee_id, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/teams',
      data: {
        role,
        employee_id
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  addTeam: (name, department_id, employee_id, created_by, role, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/teams/add_team',
      data: {
        name,
        department_id,
        employee_id,
        created_by,
        role
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  updateTeam: (
    id,
    name,
    department_id,
    employee_id,
    status,
    updated_by,
    role,
    token
  ) =>
    axios({
      method: 'PUT',
      url: 'http://localhost:3000/admin/teams/update_team',
      data: { id, name, department_id, employee_id, status, updated_by, role },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  listDepartments: (role, employee_id, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/departments',
      data: {
        role,
        employee_id
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  addDepartment: (name, employee_id, created_by, role, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/departments/add_department',
      data: {
        name,
        employee_id,
        created_by,
        role
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  updateDepartment: (id, name, employee_id, status, updated_by, role, token) =>
    axios({
      method: 'PUT',
      url: 'http://localhost:3000/admin/departments/update_department',
      data: { id, name, employee_id, status, updated_by, role },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  listRequestTypes: (role, employee_id, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/request_types',
      data: {
        role,
        employee_id
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  addRequestType: (name, has_time_in, created_by, role, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/request_types/add_request_type',
      data: {
        name,
        has_time_in,
        created_by,
        role
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  updateRequestType: (id, name, has_time_in, status, updated_by, role, token) =>
    axios({
      method: 'PUT',
      url: 'http://localhost:3000/admin/request_types/update_request_type',
      data: {
        id,
        name,
        has_time_in,
        status,
        updated_by,
        role
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  listAccessRights: (role, employee_id, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/access_rights',
      data: {
        role,
        employee_id
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  addAccessRight: (action_id, role_id, created_by, role, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/access_rights/add_access_right',
      data: {
        action_id,
        role_id,
        created_by,
        role
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  updateAccessRight: (
    id,
    action_id,
    role_id,
    status,
    updated_by,
    role,
    token
  ) =>
    axios({
      method: 'PUT',
      url: 'http://localhost:3000/admin/access_rights/update_access_right',
      data: { id, action_id, role_id, status, updated_by, role },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  listModules: (role, employee_id, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/modules',
      data: {
        role,
        employee_id
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  addModule: (description, created_by, role, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/modules/add_module',
      data: {
        description,
        created_by,
        role
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  updateModule: (id, description, status, updated_by, role, token) =>
    axios({
      method: 'PUT',
      url: 'http://localhost:3000/admin/modules/update_module',
      data: { id, description, status, updated_by, role },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  listActions: (role, employee_id, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/actions',
      data: {
        role,
        employee_id
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  addAction: (module_id, description, created_by, role, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/actions/add_action',
      data: {
        module_id,
        description,
        created_by,
        role
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  updateAction: (id, module_id, description, status, updated_by, role, token) =>
    axios({
      method: 'PUT',
      url: 'http://localhost:3000/admin/actions/update_action',
      data: {
        id,
        module_id,
        description,
        status,
        updated_by,
        role
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  listActivityLogs: (role, employee_id, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/admin/activity_logs',
      data: {
        role,
        employee_id
      },

      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err)
};

module.exports = admin;
