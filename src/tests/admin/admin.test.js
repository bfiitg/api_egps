const admin = require('./admin');
let loginData;
let unauthorizedData;
const id = Math.floor(Math.random() * 4000000);

beforeAll(async () => {
  loginData = await admin.login(154151100, 'admin');

  unauthorizedData = await admin.login(154151103, 'emp');
});
//Employees
test('View employees - without token', async () => {
  return await admin.listEmployees(loginData.id, loginData.role).then(data => {
    expect(data.response.status).toBe(403);
  });
});

test('View employees - without role', async () => {
  return await admin
    .listEmployees(loginData.id, null, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('View employees - with not authorized role', async () => {
  return await admin
    .listEmployees(
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('View employees - without id', async () => {
  return await admin
    .listEmployees(null, loginData.role, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('View employees - Perfect Scenario', async () => {
  return await admin
    .listEmployees(loginData.id, loginData.role, loginData.token)
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Add Employee - without token', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';

  return await admin
    .addEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      loginData.id,
      loginData.role
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Add Employee - without role', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';

  return await admin
    .addEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      loginData.id,
      undefined,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Employee - with unauthorized role', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';

  return await admin
    .addEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('Add Employee - null id', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';

  return await admin
    .addEmployee(
      null,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Employee - null role id', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';

  return await admin
    .addEmployee(
      id,
      null,
      position_id,
      first_name,
      middle_name,
      last_name,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Employee - null position id', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';

  return await admin
    .addEmployee(
      id,
      role_id,
      null,
      first_name,
      middle_name,
      last_name,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Employee - null first name', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';

  return await admin
    .addEmployee(
      id,
      role_id,
      position_id,
      null,
      middle_name,
      last_name,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Employee - null middle name', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';

  return await admin
    .addEmployee(
      id,
      role_id,
      position_id,
      first_name,
      null,
      last_name,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Employee - null last name', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';

  return await admin
    .addEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      null,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Employee - string  id', async () => {
  const role_id = 'role id';
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';

  return await admin
    .addEmployee(
      'employee',
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Employee - string role id', async () => {
  const role_id = 'role id';
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';

  return await admin
    .addEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Employee - string position id', async () => {
  const role_id = 2;
  const position_id = 'position';
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';

  return await admin
    .addEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Employee - non existing role id', async () => {
  const role_id = 100;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';

  return await admin
    .addEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Employee - non existing position id', async () => {
  const role_id = 2;
  const position_id = 1000;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';

  return await admin
    .addEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Employee - Perfect Scenario', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const adminEmail = "ahmadbaulo2001@gmail.com"

  return await admin
    .addEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      loginData.id,
      loginData.role,
      loginData.token,
      adminEmail
    )
    .then(data => {
      expect(data.status).toBe(201);
    });
});

test('Add Employee - with existing ID', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';

  return await admin
    .addEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - without token', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      password,
      status,
      loginData.id,
      loginData.role
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Update Employee - without role', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      password,
      status,
      loginData.id,
      undefined,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - unauthorized role', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      password,
      status,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('Update Employee - null id', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      null,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - null role id', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      null,
      position_id,
      first_name,
      middle_name,
      last_name,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - null position id', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      role_id,
      null,
      first_name,
      middle_name,
      last_name,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - null first name', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      role_id,
      position_id,
      null,
      middle_name,
      last_name,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - null middle name', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      role_id,
      position_id,
      first_name,
      null,
      last_name,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - null last name', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      null,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - string id', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      'employee',
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - string role id', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      'role_id',
      position_id,
      first_name,
      middle_name,
      last_name,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - string position id', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      role_id,
      'position_id',
      first_name,
      middle_name,
      last_name,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - non existing id', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      1000,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - non existing role id', async () => {
  const role_id = 1000;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(
        400
      );
    });
});

test('Update Employee - non existing position id', async () => {
  const role_id = 2;
  const position_id = 100000;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(
        400
      );
    });
});

test('Update Employee - integer first name', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      role_id,
      position_id,
      1234,
      middle_name,
      last_name,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - integer middle name', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      role_id,
      position_id,
      first_name,
      1234,
      last_name,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - integer last name', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      1234,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - invalid status', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      password,
      'status',
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Employee - Perfect scenario', async () => {
  const role_id = 2;
  const position_id = 1;
  const first_name = 'Elizabeth';
  const middle_name = 'Lily';
  const last_name = 'Leung';
  const password = 'employee';
  const status = 'Active';

  return await admin
    .updateEmployee(
      id,
      role_id,
      position_id,
      first_name,
      middle_name,
      last_name,
      password,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

//Roles

test('Roles - list roles without token', async () => {
  return await admin.listRoles(loginData.role, loginData.id).then(data => {
    expect(data.response.status).toBe(403);
  });
});

test('Roles - list roles without role', async () => {
  return await admin
    .listRoles(undefined, loginData.id, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Roles - list roles unauthorized role', async () => {
  return await admin
    .listRoles(
      unauthorizedData.role,
      unauthorizedData.id,
      unauthorizedData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('Roles - list roles', async () => {
  return await admin
    .listRoles(loginData.role, loginData.id, loginData.token)
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Add Roles - without token', async () => {
  const name = 'security guard';
  return await admin.addRole(name, loginData.id, loginData.role).then(data => {
    expect(data.response.status).toBe(403);
  });
});

test('Add Roles - without role', async () => {
  const name = 'security guard';
  return await admin
    .addRole(name, loginData.id, undefined, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Roles - unauthorized role', async () => {
  const name = 'security guard';
  return await admin
    .addRole(
      name,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('Add Roles - integer name', async () => {
  const name = 1234;
  return await admin
    .addRole(name, loginData.id, loginData.role, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Roles - Perfect scenario', async () => {
  const name = 'security guard';
  return await admin
    .addRole(name, loginData.id, loginData.role, loginData.token)
    .then(data => {
      expect(data.status).toBe(201);
    });
});

test('Update Role - without token', async () => {
  const id = 8;
  const name = 'security guard';
  const status = 'Active';
  return await admin
    .updateRole(id, name, status, loginData.id, loginData.role)
    .then(data => expect(data.response.status).toBe(403));
});

test('Update Role - without role', async () => {
  const id = 8;
  const name = 'security guard';
  const status = 'Active';
  return await admin
    .updateRole(id, name, status, loginData.id, undefined, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Role - unauthorized role', async () => {
  const id = 8;
  const name = 'security guard';
  const status = 'Active';
  return await admin
    .updateRole(
      id,
      name,
      status,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('Update Role - string id', async () => {
  const id = 'role id';
  const name = 'security guard';
  const status = 'Active';
  return await admin
    .updateRole(id, name, status, loginData.id, loginData.role, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Role - invalid status', async () => {
  const id = 8;
  const name = 'security guard';
  const status = 'status';
  return await admin
    .updateRole(id, name, status, loginData.id, loginData.role, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Role - integer status', async () => {
  const id = 8;
  const name = 'security guard';
  const status = 4123;
  return await admin
    .updateRole(id, name, status, loginData.id, loginData.role, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Role - non existing id', async () => {
  const id = 10000;
  const name = 'security guard';
  const status = 'Active';
  return await admin
    .updateRole(id, name, status, loginData.id, loginData.role, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Role - perfect scenario', async () => {
  const id = 8;
  const name = 'security guard';
  const status = 'Active';
  return await admin
    .updateRole(id, name, status, loginData.id, loginData.role, loginData.token)
    .then(data => expect(data.status).toBe(200));
});

//Positions

test('Position - without token', async () => {
  return await admin
    .listPositions(loginData.role, loginData.id)
    .then(data => expect(data.response.status).toBe(403));
});

test('Position - without role', async () => {
  return await admin
    .listPositions(undefined, loginData.id, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('Position - unauthorized role', async () => {
  return await admin
    .listPositions(
      unauthorizedData.role,
      unauthorizedData.id,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('Position - null id', async () => {
  return await admin
    .listPositions(loginData.role, null, loginData.token)
    .then(data => expect(data.response.status).toBe(403));
});

test('Position - string id', async () => {
  return await admin
    .listPositions(loginData.role, 'employee', loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('Position - view', async () => {
  return await admin
    .listPositions(loginData.role, loginData.id, loginData.token)
    .then(data => expect(data.status).toBe(200));
});

test('Add Postion - without token', async () => {
  const name = 'Data Analyst';
  const team_id = 4;

  return await admin
    .addPositions(name, team_id, loginData.id, loginData.role)
    .then(data => expect(data.response.status).toBe(403));
});

test('Add Postion - without role', async () => {
  const name = 'Data Analyst';
  const team_id = 4;

  return await admin
    .addPositions(name, team_id, loginData.id, null, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Postion - unauthorized role', async () => {
  const name = 'Data Analyst';
  const team_id = 4;

  return await admin
    .addPositions(
      name,
      team_id,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('Add Postion - integer name', async () => {
  const name = 123;
  const team_id = 4;

  return await admin
    .addPositions(name, team_id, loginData.id, loginData.role, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Postion - non existing team id', async () => {
  const name = 'Data analyst';
  const team_id = 400;

  return await admin
    .addPositions(name, team_id, loginData.id, loginData.role, loginData.token)
    .then(data =>
      expect(data.response.status).toBe(
        400
      )
    );
});

test('Add Postion - Perfect Scenario', async () => {
  const name = 'Data Analyst';
  const team_id = 4;

  return await admin
    .addPositions(name, team_id, loginData.id, loginData.role, loginData.token)
    .then(data => {
      expect(data.status).toBe(201);
    });
});

test('Update Position - without token', async () => {
  const id = 11;
  const name = 'Data Analyst';
  const team_id = 4;
  const status = 'Active';
  return await admin
    .updatePositions(id, name, team_id, status, loginData.id, loginData.role)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Update Position - without role', async () => {
  const id = 11;
  const name = 'Data Analyst';
  const team_id = 4;
  const status = 'Active';
  return await admin
    .updatePositions(
      id,
      name,
      team_id,
      status,
      loginData.id,
      undefined,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Position - unauthorized role', async () => {
  const id = 11;
  const name = 'Data Analyst';
  const team_id = 4;
  const status = 'Active';
  return await admin
    .updatePositions(
      id,
      name,
      team_id,
      status,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('Update Position - null id', async () => {
  const id = 11;
  const name = 'Data Analyst';
  const team_id = 4;
  const status = 'Active';
  return await admin
    .updatePositions(
      null,
      name,
      team_id,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Position - null name', async () => {
  const id = 11;
  const name = 'Data Analyst';
  const team_id = 4;
  const status = 'Active';
  return await admin
    .updatePositions(
      id,
      null,
      team_id,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Position - null team id', async () => {
  const id = 11;
  const name = 'Data Analyst';
  const team_id = 4;
  const status = 'Active';
  return await admin
    .updatePositions(
      id,
      name,
      null,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Position - null updated by', async () => {
  const id = 11;
  const name = 'Data Analyst';
  const team_id = 4;
  const status = 'Active';
  return await admin
    .updatePositions(
      id,
      name,
      team_id,
      status,
      null,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Update Position - string id', async () => {
  const id = 'position id';
  const name = 'Data Analyst';
  const team_id = 4;
  const status = 'Active';
  return await admin
    .updatePositions(
      id,
      name,
      team_id,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Position - string team id', async () => {
  const id = 11;
  const name = 'Data Analyst';
  const team_id = 'team';
  const status = 'Active';
  return await admin
    .updatePositions(
      id,
      name,
      team_id,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Position - invalid status', async () => {
  const id = 11;
  const name = 'Data Analyst';
  const team_id = 4;
  const status = 'status';
  return await admin
    .updatePositions(
      id,
      name,
      team_id,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Position - integer status', async () => {
  const id = 11;
  const name = 'Data Analyst';
  const team_id = 4;
  const status = 1234;
  return await admin
    .updatePositions(
      id,
      name,
      team_id,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Position - Perfect Scenario', async () => {
  const id = 12;
  const name = 'Data Analyst';
  const team_id = 4;
  const status = 'Active';
  return await admin
    .updatePositions(
      id,
      name,
      team_id,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

//Teams

test('Teams - view without token', async () => {
  return await admin.listTeams(loginData.role, loginData.id).then(data => {
    expect(data.response.status).toBe(403);
  });
});

test('Teams - view without role', async () => {
  return await admin
    .listTeams(undefined, loginData.id, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Teams - view unauthorized role', async () => {
  return await admin
    .listTeams(
      unauthorizedData.role,
      unauthorizedData.id,
      unauthorizedData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('Teams - view null id', async () => {
  return await admin
    .listTeams(loginData.role, null, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Teams - view', async () => {
  return await admin
    .listTeams(loginData.role, loginData.id, loginData.token)
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Add Team - without token', async () => {
  const name = 'IT Ops';
  const department_id = 1;

  return await admin
    .addTeam(name, department_id, null, loginData.id, loginData.role)
    .then(data => expect(data.response.status).toBe(403));
});

test('Add Team - without role', async () => {
  const name = 'IT Ops';
  const department_id = 1;

  return await admin
    .addTeam(
      name,
      department_id,
      null,
      loginData.id,
      undefined,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Team - unauthorized role', async () => {
  const name = 'IT Ops';
  const department_id = 1;

  return await admin
    .addTeam(
      name,
      department_id,
      null,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('Add Team - null created by ', async () => {
  const name = 'IT Ops';
  const department_id = 1;

  return await admin
    .addTeam(name, department_id, null, null, loginData.role, loginData.token)
    .then(data => expect(data.response.status).toBe(403));
});

test('Add Team - null name', async () => {
  const name = 'IT Ops';
  const department_id = 1;

  return await admin
    .addTeam(
      null,
      department_id,
      null,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Team - string department id', async () => {
  const name = 'IT Ops';
  const department_id = 1;

  return await admin
    .addTeam(
      name,
      'department_id',
      null,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Team - Perfect Scenario with team head', async () => {
  const name = 'IT Ops';
  const department_id = 1;

  return await admin
    .addTeam(
      name,
      department_id,
      154151108,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Team -  employee is already a team head', async () => {
  const name = 'IT Ops';
  const department_id = 1;

  return await admin
    .addTeam(
      name,
      department_id,
      154151107,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Team - Perfect Scenario ', async () => {
  const name = 'IT Ops';
  const department_id = 1;

  return await admin
    .addTeam(
      name,
      department_id,
      null,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.status).toBe(201));
});

test('Update Team - without token ', async () => {
  const id = 6;
  const name = 'IT Ops';
  const department_id = 1;
  const status = 'Active';

  return await admin
    .updateTeam(
      id,
      name,
      department_id,
      null,
      status,
      loginData.id,
      loginData.role
    )
    .then(data => expect(data.response.status).toBe(403));
});

test('Update Team - without role ', async () => {
  const id = 6;
  const name = 'IT Ops';
  const department_id = 1;
  const status = 'Active';

  return await admin
    .updateTeam(
      id,
      name,
      department_id,
      null,
      status,
      loginData.id,
      undefined,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Team - unauthorized role ', async () => {
  const id = 6;
  const name = 'IT Ops';
  const department_id = 1;
  const status = 'Active';

  return await admin
    .updateTeam(
      id,
      name,
      department_id,
      null,
      status,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('Update Team - null id ', async () => {
  const id = 6;
  const name = 'IT Ops';
  const department_id = 1;
  const status = 'Active';

  return await admin
    .updateTeam(
      null,
      name,
      department_id,
      null,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Team - null name ', async () => {
  const id = 6;
  const name = 'IT Ops';
  const department_id = 1;
  const status = 'Active';

  return await admin
    .updateTeam(
      id,
      null,
      department_id,
      null,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Team - null department id ', async () => {
  const id = 6;
  const name = 'IT Ops';
  const department_id = 1;
  const status = 'Active';

  return await admin
    .updateTeam(
      id,
      name,
      null,
      null,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Team - null status ', async () => {
  const id = 6;
  const name = 'IT Ops';
  const department_id = 1;
  const status = 'Active';

  return await admin
    .updateTeam(
      id,
      name,
      department_id,
      null,
      null,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Team - null updated by ', async () => {
  const id = 6;
  const name = 'IT Ops';
  const department_id = 1;
  const status = 'Active';

  return await admin
    .updateTeam(
      id,
      name,
      department_id,
      null,
      status,
      null,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(403));
});

test('Update Team - string id ', async () => {
  const id = 6;
  const name = 'IT Ops';
  const department_id = 1;
  const status = 'Active';

  return await admin
    .updateTeam(
      'id',
      name,
      department_id,
      null,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Team - string department_id ', async () => {
  const id = 6;
  const name = 'IT Ops';
  const department_id = 1;
  const status = 'Active';

  return await admin
    .updateTeam(
      id,
      name,
      'department_id',
      null,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Team - invalid status ', async () => {
  const id = 6;
  const name = 'IT Ops';
  const department_id = 1;
  const status = 'Active';

  return await admin
    .updateTeam(
      id,
      name,
      department_id,
      null,
      'status',
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Team - employee is already a team/department head ', async () => {
  const id = 6;
  const name = 'IT Ops';
  const department_id = 1;
  const status = 'Active';

  return await admin
    .updateTeam(
      id,
      name,
      department_id,
      154151107,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Team - Perfect Scenario ', async () => {
  const id = 6;
  const name = 'IT Ops';
  const department_id = 1;
  const status = 'Active';

  return await admin
    .updateTeam(
      id,
      name,
      department_id,
      null,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.status).toBe(200));
});

//Departments

test('View Department - without token', async () => {
  return await admin
    .listDepartments(loginData.role, loginData.id)
    .then(data => expect(data.response.status).toBe(403));
});

test('View Department - without role', async () => {
  return await admin
    .listDepartments(undefined, loginData.id, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('View Department - unauthorized role', async () => {
  return await admin
    .listDepartments(
      unauthorizedData.role,
      unauthorizedData.id,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('View Department - null employee id', async () => {
  return await admin
    .listDepartments(loginData.role, null, loginData.token)
    .then(data => expect(data.response.status).toBe(403));
});

test('View Department', async () => {
  return await admin
    .listDepartments(loginData.role, loginData.id, loginData.token)
    .then(data => expect(data.status).toBe(200));
});

test('Add Department - null name', async () => {
  const name = 'Network';

  return await admin
    .addDepartment(null, null, loginData.id, loginData.role, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Department - null created by', async () => {
  const name = 'Network';

  return await admin
    .addDepartment(name, null, null, loginData.role, loginData.token)
    .then(data => expect(data.response.status).toBe(403));
});

test('Add Department - string created by', async () => {
  const name = 'Network';

  return await admin
    .addDepartment(name, null, 'created', loginData.role, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Department - employee already a team/department head', async () => {
  const name = 'Network';

  return await admin
    .addDepartment(
      name,
      154151107,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Department - Perfect Scenario', async () => {
  const name = 'Network';

  return await admin
    .addDepartment(name, null, loginData.id, loginData.role, loginData.token)
    .then(data => expect(data.status).toBe(201));
});

test('Update Department - without token', async () => {
  const id = 7;
  const name = 'Network';
  const status = 'Active';

  return await admin
    .updateDepartment(id, name, null, status, loginData.id, loginData.role)
    .then(data => expect(data.response.status).toBe(403));
});

test('Update Department - without role', async () => {
  const id = 7;
  const name = 'Network';
  const status = 'Active';

  return await admin
    .updateDepartment(
      id,
      name,
      null,
      status,
      loginData.id,
      null,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Department - unauthorized role', async () => {
  const id = 7;
  const name = 'Network';
  const status = 'Active';

  return await admin
    .updateDepartment(
      id,
      name,
      null,
      status,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('Update Department - null id', async () => {
  const id = 7;
  const name = 'Network';
  const status = 'Active';

  return await admin
    .updateDepartment(
      null,
      name,
      null,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Department - null name', async () => {
  const id = 7;
  const name = 'Network';
  const status = 'Active';

  return await admin
    .updateDepartment(
      id,
      null,
      null,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Department - null status', async () => {
  const id = 7;
  const name = 'Network';
  const status = 'Active';

  return await admin
    .updateDepartment(
      id,
      name,
      null,
      null,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Department - null created by', async () => {
  const id = 7;
  const name = 'Network';
  const status = 'Active';

  return await admin
    .updateDepartment(
      id,
      null,
      name,
      status,
      null,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(403));
});

test('Update Department - string id', async () => {
  const id = 7;
  const name = 'Network';
  const status = 'Active';

  return await admin
    .updateDepartment(
      'id',
      name,
      null,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Department -  invalid status', async () => {
  const id = 7;
  const name = 'Network';
  const status = 'Active';

  return await admin
    .updateDepartment(
      id,
      name,
      null,
      'status',
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Department - employee already team/department head', async () => {
  const id = 7;
  const name = 'Network';
  const status = 'Active';

  return await admin
    .updateDepartment(
      id,
      name,
      154151107,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Department - Perfect Scenario', async () => {
  const id = 7;
  const name = 'Network';
  const status = 'Active';

  return await admin
    .updateDepartment(
      id,
      name,
      null,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.status).toBe(200));
});

//Request Types
test('View Request Types - without token', async () => {
  return await admin
    .listRequestTypes(loginData.role, loginData.id)
    .then(data => expect(data.response.status).toBe(403));
});

test('View Request Types - without role', async () => {
  return await admin
    .listRequestTypes(undefined, loginData.id, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('View Request Types - unauthorized role', async () => {
  return await admin
    .listRequestTypes(
      unauthorizedData.role,
      unauthorizedData.id,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('View Request Types - null employee id', async () => {
  return await admin
    .listRequestTypes(loginData.role, null, loginData.token)
    .then(data => expect(data.response.status).toBe(403));
});

test('View Request Types', async () => {
  return await admin
    .listRequestTypes(loginData.role, loginData.id, loginData.token)
    .then(data => expect(data.status).toBe(200));
});

test('Add Request Type - without token', async () => {
  const name = 'Item Pass';
  const has_time_in = true;

  return await admin
    .addRequestType(name, has_time_in, loginData.id, loginData.role)
    .then(data => expect(data.response.status).toBe(403));
});

test('Add Request Type - without role', async () => {
  const name = 'Item Pass';
  const has_time_in = true;

  return await admin
    .addRequestType(name, has_time_in, loginData.id, undefined, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Request Type - unauthorized role', async () => {
  const name = 'Item Pass';
  const has_time_in = true;

  return await admin
    .addRequestType(
      name,
      has_time_in,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('Add Request Type - null name', async () => {
  const name = 'Item Pass';
  const has_time_in = true;

  return await admin
    .addRequestType(
      null,
      has_time_in,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Request Type - without has time in', async () => {
  const name = 'Item Pass';
  const has_time_in = true;

  return await admin
    .addRequestType(name, null, loginData.id, loginData.role, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Request Type - without created by', async () => {
  const name = 'Item Pass';
  const has_time_in = true;

  return await admin
    .addRequestType(name, has_time_in, null, loginData.role, loginData.token)
    .then(data => expect(data.response.status).toBe(403));
});

test('Add Request Type - string has_time_in', async () => {
  const name = 'Item Pass';
  const has_time_in = true;

  return await admin
    .addRequestType(
      name,
      'has_time_in',
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Request Type - integer has time in', async () => {
  const name = 'Item Pass';
  const has_time_in = true;

  return await admin
    .addRequestType(name, 1234, loginData.id, loginData.role, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Request Type - Perfect Scenario', async () => {
  const name = 'Item Pass';
  const has_time_in = true;

  return await admin
    .addRequestType(
      name,
      has_time_in,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.status).toBe(201));
});

test('Update Request Type - without token', async () => {
  const id = 6;
  const name = 'Item Pass';
  const has_time_in = true;
  const status = 'Active';

  return await admin
    .updateRequestType(
      id,
      name,
      has_time_in,
      status,
      loginData.id,
      loginData.role
    )
    .then(data => expect(data.response.status).toBe(403));
});

test('Update Request Type - without role', async () => {
  const id = 6;
  const name = 'Item Pass';
  const has_time_in = true;
  const status = 'Active';

  return await admin
    .updateRequestType(
      id,
      name,
      has_time_in,
      status,
      loginData.id,
      undefined,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Request Type - unauthorized role', async () => {
  const id = 6;
  const name = 'Item Pass';
  const has_time_in = true;
  const status = 'Active';

  return await admin
    .updateRequestType(
      id,
      name,
      has_time_in,
      status,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('Update Request Type - null id', async () => {
  const id = 6;
  const name = 'Item Pass';
  const has_time_in = true;
  const status = 'Active';

  return await admin
    .updateRequestType(
      null,
      name,
      has_time_in,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Request Type - null name', async () => {
  const id = 6;
  const name = 'Item Pass';
  const has_time_in = true;
  const status = 'Active';

  return await admin
    .updateRequestType(
      id,
      null,
      has_time_in,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Request Type - null has time in ', async () => {
  const id = 6;
  const name = 'Item Pass';
  const has_time_in = true;
  const status = 'Active';

  return await admin
    .updateRequestType(
      id,
      name,
      null,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Request Type - null created by', async () => {
  const id = 6;
  const name = 'Item Pass';
  const has_time_in = true;
  const status = 'Active';

  return await admin
    .updateRequestType(
      id,
      name,
      has_time_in,
      status,
      null,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(403));
});

test('Update Request Type - string ID', async () => {
  const id = 6;
  const name = 'Item Pass';
  const has_time_in = true;
  const status = 'Active';

  return await admin
    .updateRequestType(
      'id',
      name,
      has_time_in,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Request Type - string has time in', async () => {
  const id = 6;
  const name = 'Item Pass';
  const has_time_in = true;
  const status = 'Active';

  return await admin
    .updateRequestType(
      id,
      name,
      'has_time_in',
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Request Type - integer has time in', async () => {
  const id = 6;
  const name = 'Item Pass';
  const has_time_in = true;
  const status = 'Active';

  return await admin
    .updateRequestType(
      id,
      name,
      1234,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Request Type - invalid status', async () => {
  const id = 6;
  const name = 'Item Pass';
  const has_time_in = true;
  const status = 'Active';

  return await admin
    .updateRequestType(
      id,
      name,
      has_time_in,
      'status',
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Request Type - integer status', async () => {
  const id = 6;
  const name = 'Item Pass';
  const has_time_in = true;
  const status = 'Active';

  return await admin
    .updateRequestType(
      id,
      name,
      has_time_in,
      2134,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Request Type = Perfect Scenario', async () => {
  const id = 6;
  const name = 'Item Pass';
  const has_time_in = true;
  const status = 'Active';

  return await admin
    .updateRequestType(
      id,
      name,
      has_time_in,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.status).toBe(200));
});

//Modules

test('View Modules - without token', async () => {
  return await admin
    .listModules(loginData.role, loginData.id)
    .then(data => expect(data.response.status).toBe(403));
});

test('View Modules - without role', async () => {
  return await admin
    .listModules(undefined, loginData.id, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('View Modules - unauthorized role', async () => {
  return await admin
    .listModules(
      unauthorizedData.role,
      unauthorizedData.id,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('View Modules - null employee id', async () => {
  return await admin
    .listModules(loginData.role, null, loginData.token)
    .then(data => expect(data.response.status).toBe(403));
});

test('View Modules', async () => {
  return await admin
    .listModules(loginData.role, loginData.id, loginData.token)
    .then(data => expect(data.status).toBe(200));
});

test('Add Module - without token', async () => {
  const description = 'Accounting';

  return await admin
    .addModule(description, loginData.id, loginData.role)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Add Module - without role', async () => {
  const description = 'Accounting';

  return await admin
    .addModule(description, loginData.id, undefined, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Module - unauthorized role', async () => {
  const description = 'Accounting';

  return await admin
    .addModule(
      description,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('Add Module - null description', async () => {
  const description = 'Accounting';

  return await admin
    .addModule(null, loginData.id, loginData.role, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Module - null created by', async () => {
  const description = 'Accounting';

  return await admin
    .addModule(description, null, loginData.role, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Add Module - Perfect Scenario', async () => {
  const description = 'Accounting';

  return await admin
    .addModule(description, loginData.id, loginData.role, loginData.token)
    .then(data => {
      expect(data.status).toBe(201);
    });
});

test('Update Modules - without token', async () => {
  const id = 5;
  const description = 'Accounting';
  const status = 'Active';
  return await admin
    .updateModule(id, description, status, loginData.id, loginData.role)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Update Modules - without role', async () => {
  const id = 5;
  const description = 'Accounting';
  const status = 'Active';
  return await admin
    .updateModule(
      id,
      description,
      status,
      loginData.id,
      undefined,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Modules - unauthorized role', async () => {
  const id = 5;
  const description = 'Accounting';
  const status = 'Active';
  return await admin
    .updateModule(
      id,
      description,
      status,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('Update Modules - null id', async () => {
  const id = 5;
  const description = 'Accounting';
  const status = 'Active';
  return await admin
    .updateModule(
      null,
      description,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Modules - null description', async () => {
  const id = 5;
  const description = 'Accounting';
  const status = 'Active';
  return await admin
    .updateModule(
      id,
      null,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Modules - null status', async () => {
  const id = 5;
  const description = 'Accounting';
  const status = 'Active';
  return await admin
    .updateModule(
      id,
      description,
      null,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Modules - null updated by', async () => {
  const id = 5;
  const description = 'Accounting';
  const status = 'Active';
  return await admin
    .updateModule(
      id,
      description,
      status,
      null,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Update Modules - string id', async () => {
  const id = 5;
  const description = 'Accounting';
  const status = 'Active';
  return await admin
    .updateModule(
      'id',
      description,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Modules - invalid status', async () => {
  const id = 5;
  const description = 'Accounting';
  const status = 'Active';
  return await admin
    .updateModule(
      id,
      description,
      'status',
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Modules - integer status', async () => {
  const id = 5;
  const description = 'Accounting';
  const status = 'Active';
  return await admin
    .updateModule(
      id,
      description,
      1234,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Update Modules - Perfect Scenario', async () => {
  const id = 5;
  const description = 'Accounting';
  const status = 'Active';
  return await admin
    .updateModule(
      id,
      description,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //Action
test('View Action - without token', async () => {
  return await admin.listActions(loginData.role, loginData.id).then(data => {
    expect(data.response.status).toBe(403);
  });
});

test('View Action - without role', async () => {
  return await admin
    .listActions(undefined, loginData.id, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('View Action - unauthorized role', async () => {
  return await admin
    .listActions(
      unauthorizedData.role,
      unauthorizedData.id,
      unauthorizedData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('View Action - null id', async () => {
  return await admin
    .listActions(loginData.role, null, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('View Action', async () => {
  return await admin
    .listActions(loginData.role, loginData.id, loginData.token)
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Add Action - without token', async () => {
  const module_id = 5;
  const description = 'View verified gate pass';

  return await admin
    .addAction(module_id, description, loginData.id, loginData.role)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Add Action - without role', async () => {
  const module_id = 5;
  const description = 'View verified gate pass';

  return await admin
    .addAction(module_id, description, loginData.id, undefined, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Action - unauthorized role', async () => {
  const module_id = 5;
  const description = 'View verified gate pass';

  return await admin
    .addAction(
      module_id,
      description,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('Add Action - null module id', async () => {
  const module_id = 5;
  const description = 'View verified gate pass';

  return await admin
    .addAction(null, description, loginData.id, loginData.role, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Action - null description', async () => {
  const module_id = 5;
  const description = 'View verified gate pass';

  return await admin
    .addAction(module_id, null, loginData.id, loginData.role, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Action - null created by', async () => {
  const module_id = 5;
  const description = 'View verified gate pass';

  return await admin
    .addAction(module_id, description, null, loginData.role, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Add Action - string module id', async () => {
  const module_id = 5;
  const description = 'View verified gate pass';

  return await admin
    .addAction(
      'module_id',
      description,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Action - integer created by', async () => {
  const module_id = 5;
  const description = 'View verified gate pass';

  return await admin
    .addAction(
      module_id,
      description,
      'created by',
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Add Action - Perfect Scenario', async () => {
  const module_id = 5;
  const description = 'View verified gate pass';

  return await admin
    .addAction(
      module_id,
      description,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(201);
    });
});

test('Update Action - without token', async () => {
  const id = 37;
  const module_id = 5;
  const description = 'View verified gate pass';
  const status = 'Active';

  return await admin
    .updateAction(
      id,
      module_id,
      description,
      status,
      loginData.id,
      loginData.role
    )
    .then(data => expect(data.response.status).toBe(403));
});

test('Update Action - without role', async () => {
  const id = 37;
  const module_id = 5;
  const description = 'View verified gate pass';
  const status = 'Active';

  return await admin
    .updateAction(
      id,
      module_id,
      description,
      status,
      loginData.id,
      undefined,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Action - unauthorized role', async () => {
  const id = 37;
  const module_id = 5;
  const description = 'View verified gate pass';
  const status = 'Active';

  return await admin
    .updateAction(
      id,
      module_id,
      description,
      status,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('Update Action - null id', async () => {
  const id = 37;
  const module_id = 5;
  const description = 'View verified gate pass';
  const status = 'Active';

  return await admin
    .updateAction(
      null,
      module_id,
      description,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Action - null module id', async () => {
  const id = 37;
  const module_id = 5;
  const description = 'View verified gate pass';
  const status = 'Active';

  return await admin
    .updateAction(
      id,
      null,
      description,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Action - null description', async () => {
  const id = 37;
  const module_id = 5;
  const description = 'View verified gate pass';
  const status = 'Active';

  return await admin
    .updateAction(
      id,
      module_id,
      null,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Action - null status', async () => {
  const id = 37;
  const module_id = 5;
  const description = 'View verified gate pass';
  const status = 'Active';

  return await admin
    .updateAction(
      id,
      module_id,
      description,
      null,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Action - null updated by', async () => {
  const id = 37;
  const module_id = 5;
  const description = 'View verified gate pass';
  const status = 'Active';

  return await admin
    .updateAction(
      id,
      module_id,
      description,
      status,
      null,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(403));
});

test('Update Action - string id', async () => {
  const id = 37;
  const module_id = 5;
  const description = 'View verified gate pass';
  const status = 'Active';

  return await admin
    .updateAction(
      'id',
      module_id,
      description,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Action - string module_id', async () => {
  const id = 37;
  const module_id = 5;
  const description = 'View verified gate pass';
  const status = 'Active';

  return await admin
    .updateAction(
      id,
      'module_id',
      description,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Action - invalid status', async () => {
  const id = 37;
  const module_id = 5;
  const description = 'View verified gate pass';
  const status = 'Active';

  return await admin
    .updateAction(
      id,
      module_id,
      description,
      'status',
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Action - integer status', async () => {
  const id = 37;
  const module_id = 5;
  const description = 'View verified gate pass';
  const status = 'Active';

  return await admin
    .updateAction(
      id,
      module_id,
      description,
      1234,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Action - non existing module id', async () => {
  const id = 37;
  const module_id = 40000;
  const description = 'View verified gate pass';
  const status = 'Active';

  return await admin
    .updateAction(
      id,
      module_id,
      description,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data =>
      expect(data.response.status).toBe(
        400
      )
    );
});

test('Update Action - Perfect Scenario', async () => {
  const id = 37;
  const module_id = 5;
  const description = 'View verified gate pass';
  const status = 'Active';

  return await admin
    .updateAction(
      id,
      module_id,
      description,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.status).toBe(200));
});

// //Access rights

test('View Access Rights - without token', async () => {
  return await admin
    .listAccessRights(loginData.role, loginData.id)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('View Access Rights - without role', async () => {
  return await admin
    .listAccessRights(undefined, loginData.id, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('View Access Rights - unauthorized role', async () => {
  return await admin
    .listAccessRights(
      unauthorizedData.role,
      unauthorizedData.id,
      unauthorizedData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('View Access Rights - null id', async () => {
  return await admin
    .listAccessRights(loginData.role, null, loginData.token)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('View Access Rights', async () => {
  return await admin
    .listAccessRights(loginData.role, loginData.id, loginData.token)
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Add Access Right - without token', async () => {
  const role_id = 8;
  const action_id = 37;

  return await admin
    .addAccessRight(action_id, role_id, loginData.id, loginData.role)
    .then(data => expect(data.response.status).toBe(403));
});

test('Add Access Right - without role', async () => {
  const role_id = 8;
  const action_id = 37;

  return await admin
    .addAccessRight(
      action_id,
      role_id,
      loginData.id,
      undefined,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Access Right - unauthorized role', async () => {
  const role_id = 8;
  const action_id = 37;

  return await admin
    .addAccessRight(
      action_id,
      role_id,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('Add Access Right - without created by', async () => {
  const role_id = 8;
  const action_id = 37;

  return await admin
    .addAccessRight(action_id, role_id, null, loginData.role, loginData.token)
    .then(data => expect(data.response.status).toBe(403));
});

test('Add Access Right - null action id', async () => {
  const role_id = 8;
  const action_id = 37;

  return await admin
    .addAccessRight(
      null,
      role_id,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Access Right - null role id', async () => {
  const role_id = 8;
  const action_id = 37;

  return await admin
    .addAccessRight(
      action_id,
      null,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Access Right - string action id', async () => {
  const role_id = 8;
  const action_id = 37;

  return await admin
    .addAccessRight(
      'action_id',
      role_id,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Access Right - string role id', async () => {
  const role_id = 8;
  const action_id = 37;

  return await admin
    .addAccessRight(
      action_id,
      'role_id',
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Add Access Right - non existing action id', async () => {
  const role_id = 8;
  const action_id = 3700;

  return await admin
    .addAccessRight(
      action_id,
      role_id,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data =>
      expect(data.response.status).toBe(
        400
      )
    );
});

test('Add Access Right - non existing action id', async () => {
  const role_id = 800;
  const action_id = 37;

  return await admin
    .addAccessRight(
      action_id,
      role_id,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data =>
      expect(data.response.status).toBe(
        400
      )
    );
});

test('Add Access Right - Perfect Scenario', async () => {
  const role_id = 8;
  const action_id = 37;

  return await admin
    .addAccessRight(
      action_id,
      role_id,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.status).toBe(201));
});

test('Update Access Right - without token', async () => {
  const id = 83;
  const role_id = 8;
  const action_id = 37;
  const status = 'Active';

  return await admin
    .updateAccessRight(
      id,
      action_id,
      role_id,
      status,
      loginData.id,
      loginData.role
    )
    .then(data => expect(data.response.status).toBe(403));
});

test('Update Access Right - without role', async () => {
  const id = 83;
  const role_id = 8;
  const action_id = 37;
  const status = 'Active';

  return await admin
    .updateAccessRight(
      id,
      action_id,
      role_id,
      status,
      loginData.id,
      undefined,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Access Right - unauthorized role', async () => {
  const id = 83;
  const role_id = 8;
  const action_id = 37;
  const status = 'Active';

  return await admin
    .updateAccessRight(
      id,
      action_id,
      role_id,
      status,
      unauthorizedData.id,
      unauthorizedData.role,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('Update Access Right - null id', async () => {
  const id = 83;
  const role_id = 8;
  const action_id = 37;
  const status = 'Active';

  return await admin
    .updateAccessRight(
      null,
      action_id,
      role_id,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Access Right - null action id', async () => {
  const id = 83;
  const role_id = 8;
  const action_id = 37;
  const status = 'Active';

  return await admin
    .updateAccessRight(
      id,
      null,
      role_id,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Access Right - null role id', async () => {
  const id = 83;
  const role_id = 8;
  const action_id = 37;
  const status = 'Active';

  return await admin
    .updateAccessRight(
      id,
      action_id,
      null,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Access Right - null status', async () => {
  const id = 83;
  const role_id = 8;
  const action_id = 37;
  const status = 'Active';

  return await admin
    .updateAccessRight(
      id,
      action_id,
      role_id,
      null,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Access Right - null updated by', async () => {
  const id = 83;
  const role_id = 8;
  const action_id = 37;
  const status = 'Active';

  return await admin
    .updateAccessRight(
      id,
      action_id,
      role_id,
      status,
      null,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(403));
});

test('Update Access Right - string id', async () => {
  const id = 83;
  const role_id = 8;
  const action_id = 37;
  const status = 'Active';

  return await admin
    .updateAccessRight(
      'id',
      action_id,
      role_id,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Access Right - string action id', async () => {
  const id = 83;
  const role_id = 8;
  const action_id = 37;
  const status = 'Active';

  return await admin
    .updateAccessRight(
      id,
      'action_id',
      role_id,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Access Right - string role id', async () => {
  const id = 83;
  const role_id = 8;
  const action_id = 37;
  const status = 'Active';

  return await admin
    .updateAccessRight(
      id,
      action_id,
      'role_id',
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Access Right - invalid status', async () => {
  const id = 83;
  const role_id = 8;
  const action_id = 37;
  const status = 'Active';

  return await admin
    .updateAccessRight(
      id,
      action_id,
      role_id,
      'status',
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Access Right - integer status', async () => {
  const id = 83;
  const role_id = 8;
  const action_id = 37;
  const status = 'Active';

  return await admin
    .updateAccessRight(
      id,
      action_id,
      role_id,
      1234,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test('Update Access Right - Perfect Scenario', async () => {
  const id = 83;
  const role_id = 3;
  const action_id = 37;
  const status = 'Inactive';

  return await admin
    .updateAccessRight(
      id,
      action_id,
      role_id,
      status,
      loginData.id,
      loginData.role,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(200)
    });
});

//Database activity logs
test('View Activity Logs - without token', async () => {
  return await admin
    .listActivityLogs(loginData.role, loginData.id)
    .then(data => expect(data.response.status).toBe(403));
});

test('View Activity Logs - without role', async () => {
  return await admin
    .listActivityLogs(undefined, loginData.id, loginData.token)
    .then(data => expect(data.response.status).toBe(400));
});

test('View Activity Logs - unauthorized role', async () => {
  return await admin
    .listActivityLogs(
      unauthorizedData.role,
      unauthorizedData.id,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('View Activity Logs - null employee id', async () => {
  return await admin
    .listActivityLogs(loginData.role, null, loginData.token)
    .then(data => expect(data.response.status).toBe(403));
});

test('View Activity Logs', async () => {
  return await admin
    .listActivityLogs(loginData.role, loginData.id, loginData.token)
    .then(data => {
      expect(data.status).toBe(200)});
});
