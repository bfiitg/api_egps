const axios = require('axios');

const team = {
  login: (id, password) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/login',
      data: {
        id,
        password
      }
    })
      .then(res => res.data)
      .catch(err => err.response),

  fetchTeamDisapproved: (
    id,
    request_date_from,
    request_date_to,
    modules,
    token,
    roleParam
  ) =>
    axios({
      method: 'POST',
      url: `http://localhost:3000/requests/viewDisapproved/${roleParam}`,
      data: {
        employee_id: id,
        request_date_from: request_date_from,
        request_date_to: request_date_to,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  fetchTeamApproved: (id, request_date_from, request_date_to, modules, token,roleParam) =>
    axios({
      method: 'POST',
      url: `http://localhost:3000/requests/viewApproved/${roleParam}`,
      data: {
        employee_id: id,
        request_date_from: request_date_from,
        request_date_to: request_date_to,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  fetchTeamPending: (id, request_date_from, request_date_to, modules, token,roleParam) =>
    axios({
      method: 'POST',
      url:  `http://localhost:3000/requests/viewPending/${roleParam}`,
      data: {
        employee_id: id,
        request_date_from: request_date_from,
        request_date_to: request_date_to,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  fetchTeamEmployeeGatePass: (
    id,
    request_date_from,
    request_date_to,
    modules,
    token,
    roleParam
  ) =>
    axios({
      method: 'POST',
      url: `http://localhost:3000/reports/employees_report/${roleParam}`,
      data: {
        employee_id: id,
        request_date_from: request_date_from,
        request_date_to: request_date_to,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err)
};

module.exports = team;
