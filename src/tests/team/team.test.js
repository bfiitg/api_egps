const team = require("./team");
const moment = require("moment");
const dateToday = moment().format("YYYY-MM-DD");

let loginData;
let unauthorizedData;

beforeAll(async () => {
  loginData = await team.login(154151107, "emp");
  unauthorizedData = await team.login(154151104, "emp");

});
// //***************************/viewDisapproved****************************************
test("Team - fetch data viewDisapproved gate pass without token ", async () => {
  return await team
    .fetchTeamDisapproved(loginData.id, dateToday, dateToday, loginData.modules)
    .then(data => expect(data.response.status).toBe(403));
});

test("Team - fetch data viewDisapproved gate pass without modules ", async () => {
  return await team
    .fetchTeamDisapproved(
      loginData.id,
      dateToday,
      dateToday,
      undefined,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data viewDisapproved gate pass without access rights ", async () => {
  return await team
    .fetchTeamDisapproved(
      unauthorizedData.id,
      dateToday,
      dateToday,
      unauthorizedData.modules,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test("Team - fetch data viewDisapproved gate pass (string id)", async () => {
  const id = "employee id";
  return await team
    .fetchTeamDisapproved(id, dateToday, dateToday)
    .then(data => expect(data.response.status).toBe(403));
});

test("Team - fetch data viewDisapproved gate pass (null dates)", async () => {
  return await team
    .fetchTeamDisapproved(
      loginData.id,
      null,
      null,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => expect(data.status).toBe(200));
});

test("Team - fetch data viewDisapproved gate pass (null date from)", async () => {
  return await team
    .fetchTeamDisapproved(
      loginData.id,
      null,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => expect(data.status).toBe(200));
});

test("Team - fetch data viewDisapproved gate pass (null date to)", async () => {
  return await team
    .fetchTeamDisapproved(
      loginData.id,
      dateToday,
      null,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => expect(data.status).toBe(200));
});

test("Team - fetch data viewDisapproved gate pass (string date to)", async () => {
  return await team
    .fetchTeamDisapproved(
      loginData.id,
      dateToday,
      "date",
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data viewDisapproved gate pass (string date from)", async () => {
  return await team
    .fetchTeamDisapproved(
      loginData.id,
      "date",
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data viewDisapproved gate pass (integer date from)", async () => {
  return await team
    .fetchTeamDisapproved(
      loginData.id,
      54345,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data viewDisapproved gate pass (integer date to)", async () => {
  return await team
    .fetchTeamDisapproved(
      loginData.id,
      dateToday,
      2512,
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data viewDisapproved gate pass", async () => {
  return await team
    .fetchTeamDisapproved(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => expect(data.status).toBe(200));
});

// //Approved Gate passes
test("Team - fetch data Approved gate pass without token ", async () => {
  return await team
    .fetchTeamApproved(loginData.id, dateToday, dateToday, loginData.modules)
    .then(data => expect(data.response.status).toBe(403));
});

test("Team - fetch data viewApproved gate pass without modules ", async () => {
  return await team
    .fetchTeamApproved(
      loginData.id,
      dateToday,
      dateToday,
      undefined,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data Approved gate pass without access rights ", async () => {
  return await team
    .fetchTeamApproved(
      unauthorizedData.id,
      dateToday,
      dateToday,
      unauthorizedData.modules,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test("Team - fetch data Approved gate pass (string id)", async () => {
  const id = "employee id";
  return await team
    .fetchTeamApproved(id, dateToday, dateToday)
    .then(data => expect(data.response.status).toBe(403));
});

test("Team - fetch data Approved gate pass (null dates)", async () => {
  return await team
    .fetchTeamApproved(
      loginData.id,
      null,
      null,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => expect(data.status).toBe(200));
});

test("Team - fetch data Approved gate pass (null date from)", async () => {
  return await team
    .fetchTeamApproved(
      loginData.id,
      null,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => expect(data.status).toBe(200));
});

test("Team - fetch data Approved gate pass (null date to)", async () => {
  return await team
    .fetchTeamApproved(
      loginData.id,
      dateToday,
      null,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => expect(data.status).toBe(200));
});

test("Team - fetch data Approved gate pass (string date to)", async () => {
  return await team
    .fetchTeamApproved(
      loginData.id,
      dateToday,
      "date",
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data Approved gate pass (string date from)", async () => {
  return await team
    .fetchTeamApproved(
      loginData.id,
      "date",
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data Approved gate pass (integer date from)", async () => {
  return await team
    .fetchTeamApproved(
      loginData.id,
      12323,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data Approved gate pass (integer date t0)", async () => {
  return await team
    .fetchTeamApproved(
      loginData.id,
      dateToday,
      213542,
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data Approved gate pass", async () => {
  return await team
    .fetchTeamApproved(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => expect(data.status).toBe(200));
});

// //Pending gatepasses

test("Team - fetch data Pending gate pass without token ", async () => {
  return await team
    .fetchTeamPending(loginData.id, dateToday, dateToday, loginData.modules)
    .then(data => expect(data.response.status).toBe(403));
});

test("Team - fetch data Pending gate pass without modules ", async () => {
  return await team
    .fetchTeamPending(
      loginData.id,
      dateToday,
      dateToday,
      undefined,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data Pending gate pass without access rights ", async () => {
  return await team
    .fetchTeamPending(
      unauthorizedData.id,
      dateToday,
      dateToday,
      unauthorizedData.modules,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test("Team - fetch data Pending gate pass (string id)", async () => {
  const id = "employee id";
  return await team
    .fetchTeamPending(id, dateToday, dateToday)
    .then(data => expect(data.response.status).toBe(403));
});

test("Team - fetch data Pending gate pass (null dates)", async () => {
  return await team
    .fetchTeamPending(
      loginData.id,
      null,
      null,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => expect(data.status).toBe(200));
});

test("Team - fetch data Pending gate pass (integer date from)", async () => {
  return await team
    .fetchTeamPending(
      loginData.id,
      123123,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data Pending gate pass (integer date to)", async () => {
  return await team
    .fetchTeamPending(
      loginData.id,
      dateToday,
      12532,
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data Pending gate pass (null date from)", async () => {
  return await team
    .fetchTeamPending(
      loginData.id,
      null,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => expect(data.status).toBe(200));
});

test("Team - fetch data Pending gate pass (null date to)", async () => {
  return await team
    .fetchTeamPending(
      loginData.id,
      dateToday,
      null,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => expect(data.status).toBe(200));
});

test("Team - fetch data Pending gate pass (string date to)", async () => {
  return await team
    .fetchTeamPending(
      loginData.id,
      dateToday,
      "date",
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data Pending gate pass (string date from)", async () => {
  return await team
    .fetchTeamPending(
      loginData.id,
      "date",
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data Pending gate pass", async () => {
  return await team
    .fetchTeamPending(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => expect(data.status).toBe(200));
});

//////////////////////////////////////////////////////////////
//View employees report (gate passes)

test("Team - fetch data employee gate passes (Report) without token", async () => {
  return await team
    .fetchTeamEmployeeGatePass(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules
    )
    .then(data => expect(data.response.status).toBe(403));
});

test("Team - fetch data employee gate passes (Report) without modules", async () => {
  return await team
    .fetchTeamEmployeeGatePass(
      loginData.id,
      dateToday,
      dateToday,
      undefined,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data employee gate passes (Report) without access rights", async () => {
  return await team
    .fetchTeamEmployeeGatePass(
      unauthorizedData.id,
      dateToday,
      dateToday,
      unauthorizedData.modules,
      unauthorizedData.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test("Team - fetch data employee gate passes (string id)", async () => {
  const id = "employeeid";
  return await team
    .fetchTeamEmployeeGatePass(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(403));
});

test("Team - fetch data employee gate passes (null id)", async () => {
  return await team
    .fetchTeamEmployeeGatePass(
      null,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(403));
});

test("Team - fetch data employee gate passes (null dates)", async () => {
  return await team
    .fetchTeamEmployeeGatePass(
      loginData.id,
      null,
      null,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => expect(data.status).toBe(200));
});

test("Team - fetch data employee gate passes (null date from)", async () => {
  return await team
    .fetchTeamEmployeeGatePass(
      loginData.id,
      null,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => expect(data.status).toBe(200));
});

test("Team - fetch data employee gate passes (null date to)", async () => {
  return await team
    .fetchTeamEmployeeGatePass(
      loginData.id,
      dateToday,
      null,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200)});
});

test("Team - fetch data employee gate passes (integer date to)", async () => {
  return await team
    .fetchTeamEmployeeGatePass(
      loginData.id,
      dateToday,
      13425,
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data employee gate passes (integer date from)", async () => {
  return await team
    .fetchTeamEmployeeGatePass(
      loginData.id,
      12352,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => expect(data.response.status).toBe(400));
});

test("Team - fetch data employee gate passes (Report)", async () => {
  return await team
    .fetchTeamEmployeeGatePass(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200)});
});

test("Team - fetch data viewDisapproved gate pass(string id)",async () => {
    const id = "154151102";

    return await team.fetchTeamDisapproved(id).then(data => {
        expect(data.response.status).toBe(403);
    })
})
test("Team - fetch data viewDisapproved gate pass(special character id)",async () => {
    const id = "*@&#^$*^*&#$";

    return await team.fetchTeamDisapproved(id).then(data => {
        expect(data.response.status).toBe(403);
    })
})
test("Team - fetch data viewDisapproved gate pass(null id)",async () => {
    const id = null;

    return await team.fetchTeamDisapproved(id).then(data => {
        expect(data.response.status).toBe(403);
    })
})
//---------------ID-----------------------------------------
test("Team - fetch data viewDisapproved gate pass(string id)",async () => {
    const id = "154151102";
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewDisapproved gate pass(string letters)",async () => {
    const id = "helloid";
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewDisapproved gate pass(special characters)",async () => {
    const id = "&^%&^%@&#!@#";
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewDisapproved gate pass(null id)",async () => {
    const id = null;
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})
//-----------------REQUEST DATE FROM-------------------------------------------------------

test("Team - fetch data viewDisapproved gate pass(string letters)",async () => {
    const id = 154151102;
    const request_date_from = "hellodate";
    const request_date_to = null;

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewDisapproved gate pass(special characters)",async () => {
    const id = 154151102
    const request_date_from = "&#^%&@!%#&%!@";
    const request_date_to = null;

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewDisapproved gate pass(null date from)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

//---------------REQUEST DATE TO------------------------------------------------

test("Team - fetch data viewDisapproved gate pass(string letters)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = "hellodate";

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewDisapproved gate pass(special characters)",async () => {
    const id = 154151102
    const request_date_from = null;
    const request_date_to = "&#^%&@!%#&%!@";

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewDisapproved gate pass(null date from)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

//------------SINGLE DATE GIVEN----------------------------------------------
//-----------------REQUEST DATE FROM-------------------------------------------------------

test("Team - fetch data viewDisapproved gate pass(date_to given - string letters)",async () => {
    const id = 154151102;
    const request_date_from = "hellodate";
    const request_date_to = "2019-06-13";

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewDisapproved gate pass(date_to given - special characters)",async () => {
    const id = 154151102
    const request_date_from = "&#^%&@!%#&%!@";
    const request_date_to = "2019-06-13";

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewDisapproved gate pass(date_to given - null date from)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = "2019-06-13";

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

//---------------REQUEST DATE TO------------------------------------------------

test("Team - fetch data viewDisapproved gate pass(date_from given - string letters)",async () => {
    const id = 154151102;
    const request_date_from = "2019-06-13";
    const request_date_to = "hellodate";

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewDisapproved gate pass(date_from given - special characters)",async () => {
    const id = 154151102
    const request_date_from = "2019-06-13";
    const request_date_to = "&#^%&@!%#&%!@";

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewDisapproved gate pass(date_from given - null date from)",async () => {
    const id = 154151102;
    const request_date_from = "2019-06-13";
    const request_date_to = null;

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})
//----------------------------------------------------------------------------
test("Team - fetch data viewDisapproved gate pass",async () => {
    const id = 154151102;
    const request_date_from = "2019-06-13";
    const request_date_to = "2019-06-13";

    return await team.fetchTeamDisapproved(id,request_date_from,request_date_to).then(data => {
        expect(data).not.toBe("Error");
    })
})
//**************************/viewDisapproved*************************
//**************************/viewApproved*************************
test("Team - fetch data viewApproved gate pass",async () => {
    const id = 154151102;

    return await team.fetchTeamApproved(id).then(data => {
        expect(data).not.toBe("Error");
    })
})
test("Team - fetch data viewApproved gate pass(string id)",async () => {
    const id = "154151102";

    return await team.fetchTeamApproved(id).then(data => {
        expect(data.response.status).toBe(403);
    })
})
test("Team - fetch data viewApproved gate pass(special character id)",async () => {
    const id = "*@&#^$*^*&#$";

    return await team.fetchTeamApproved(id).then(data => {
        expect(data.response.status).toBe(403);
    })
})
test("Team - fetch data viewApproved gate pass(null id)",async () => {
    const id = null;

    return await team.fetchTeamApproved(id).then(data => {
        expect(data.response.status).toBe(403);
    })
})
//---------------ID-----------------------------------------
test("Team - fetch data viewApproved gate pass(string id)",async () => {
    const id = "154151102";
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewApproved gate pass(string letters)",async () => {
    const id = "helloid";
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewApproved gate pass(special characters)",async () => {
    const id = "&^%&^%@&#!@#";
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewApproved gate pass(null id)",async () => {
    const id = null;
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})
//-----------------REQUEST DATE FROM-------------------------------------------------------

test("Team - fetch data viewApproved gate pass(string letters)",async () => {
    const id = 154151102;
    const request_date_from = "hellodate";
    const request_date_to = null;

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewApproved gate pass(special characters)",async () => {
    const id = 154151102
    const request_date_from = "&#^%&@!%#&%!@";
    const request_date_to = null;

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewApproved gate pass(null date from)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

//---------------REQUEST DATE TO------------------------------------------------

test("Team - fetch data viewApproved gate pass(string letters)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = "hellodate";

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewApproved gate pass(special characters)",async () => {
    const id = 154151102
    const request_date_from = null;
    const request_date_to = "&#^%&@!%#&%!@";

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewApproved gate pass(null date from)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

//------------SINGLE DATE GIVEN----------------------------------------------
//-----------------REQUEST DATE FROM-------------------------------------------------------

test("Team - fetch data viewApproved gate pass(date_to given - string letters)",async () => {
    const id = 154151102;
    const request_date_from = "hellodate";
    const request_date_to = "2019-06-13";

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewApproved gate pass(date_to given - special characters)",async () => {
    const id = 154151102
    const request_date_from = "&#^%&@!%#&%!@";
    const request_date_to = "2019-06-13";

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewApproved gate pass(date_to given - null date from)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = "2019-06-13";

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

//---------------REQUEST DATE TO------------------------------------------------

test("Team - fetch data viewApproved gate pass(date_from given - string letters)",async () => {
    const id = 154151102;
    const request_date_from = "2019-06-13";
    const request_date_to = "hellodate";

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewApproved gate pass(date_from given - special characters)",async () => {
    const id = 154151102
    const request_date_from = "2019-06-13";
    const request_date_to = "&#^%&@!%#&%!@";

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewApproved gate pass(date_from given - null date from)",async () => {
    const id = 154151102;
    const request_date_from = "2019-06-13";
    const request_date_to = null;

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})
//----------------------------------------------------------------------------
test("Team - fetch data viewApproved gate pass",async () => {
    const id = 154151102;
    const request_date_from = "2019-06-13";
    const request_date_to = "2019-06-13";

    return await team.fetchTeamApproved(id,request_date_from,request_date_to).then(data => {
        expect(data).not.toBe("Error");
    })
})
//**************************/viewApproved*************************
//**************************/viewPending*************************
test("Team - fetch data viewPending gate pass",async () => {
    const id = 154151102;

    return await team.fetchTeamPending(id).then(data => {
        expect(data).not.toBe("Error");
    })
})
test("Team - fetch data viewPending gate pass(string id)",async () => {
    const id = "154151102";

    return await team.fetchTeamPending(id).then(data => {
        expect(data.response.status).toBe(403);
    })
})
test("Team - fetch data viewPending gate pass(special character id)",async () => {
    const id = "*@&#^$*^*&#$";

    return await team.fetchTeamPending(id).then(data => {
        expect(data.response.status).toBe(403);
    })
})
test("Team - fetch data viewPending gate pass(null id)",async () => {
    const id = null;

    return await team.fetchTeamPending(id).then(data => {
        expect(data.response.status).toBe(403);
    })
})
//---------------ID-----------------------------------------
test("Team - fetch data viewPending gate pass(string id)",async () => {
    const id = "154151102";
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewPending gate pass(string letters)",async () => {
    const id = "helloid";
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewPending gate pass(special characters)",async () => {
    const id = "&^%&^%@&#!@#";
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewPending gate pass(null id)",async () => {
    const id = null;
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})
//-----------------REQUEST DATE FROM-------------------------------------------------------

test("Team - fetch data viewPending gate pass(string letters)",async () => {
    const id = 154151102;
    const request_date_from = "hellodate";
    const request_date_to = null;

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewPending gate pass(special characters)",async () => {
    const id = 154151102
    const request_date_from = "&#^%&@!%#&%!@";
    const request_date_to = null;

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewPending gate pass(null date from)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

//---------------REQUEST DATE TO------------------------------------------------

test("Team - fetch data viewPending gate pass(string letters)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = "hellodate";

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewPending gate pass(special characters)",async () => {
    const id = 154151102
    const request_date_from = null;
    const request_date_to = "&#^%&@!%#&%!@";

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewPending gate pass(null date from)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

//------------SINGLE DATE GIVEN----------------------------------------------
//-----------------REQUEST DATE FROM-------------------------------------------------------

test("Team - fetch data viewPending gate pass(date_to given - string letters)",async () => {
    const id = 154151102;
    const request_date_from = "hellodate";
    const request_date_to = "2019-06-13";

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewPending gate pass(date_to given - special characters)",async () => {
    const id = 154151102
    const request_date_from = "&#^%&@!%#&%!@";
    const request_date_to = "2019-06-13";

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewPending gate pass(date_to given - null date from)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = "2019-06-13";

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

//---------------REQUEST DATE TO------------------------------------------------

test("Team - fetch data viewPending gate pass(date_from given - string letters)",async () => {
    const id = 154151102;
    const request_date_from = "2019-06-13";
    const request_date_to = "hellodate";

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewPending gate pass(date_from given - special characters)",async () => {
    const id = 154151102
    const request_date_from = "2019-06-13";
    const request_date_to = "&#^%&@!%#&%!@";

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data viewPending gate pass(date_from given - null date from)",async () => {
    const id = 154151102;
    const request_date_from = "2019-06-13";
    const request_date_to = null;

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})
//----------------------------------------------------------------------------
test("Team - fetch data viewPending gate pass",async () => {
    const id = 154151102;
    const request_date_from = "2019-06-13";
    const request_date_to = "2019-06-13";

    return await team.fetchTeamPending(id,request_date_from,request_date_to).then(data => {
        expect(data).not.toBe("Error");
    })
})
//**************************/viewPending*************************
//**************************/view_employees_gate_pass*************************
test("Team - fetch data view_employees_gate_pass gate pass",async () => {
    const id = 154151102;

    return await team.fetchTeamEmployeeGatePass(id).then(data => {
        expect(data).not.toBe("Error");
    })
})
test("Team - fetch data view_employees_gate_pass gate pass(string id)",async () => {
    const id = "154151102";

    return await team.fetchTeamEmployeeGatePass(id).then(data => {
        expect(data.response.status).toBe(403);
    })
})
test("Team - fetch data view_employees_gate_pass gate pass(special character id)",async () => {
    const id = "*@&#^$*^*&#$";

    return await team.fetchTeamEmployeeGatePass(id).then(data => {
        expect(data.response.status).toBe(403);
    })
})
test("Team - fetch data view_employees_gate_pass gate pass(null id)",async () => {
    const id = null;

    return await team.fetchTeamEmployeeGatePass(id).then(data => {
        expect(data.response.status).toBe(403);
    })
})
//---------------ID-----------------------------------------
test("Team - fetch data view_employees_gate_pass gate pass(string id)",async () => {
    const id = "154151102";
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data view_employees_gate_pass gate pass(string letters)",async () => {
    const id = "helloid";
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data view_employees_gate_pass gate pass(special characters)",async () => {
    const id = "&^%&^%@&#!@#";
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data view_employees_gate_pass gate pass(null id)",async () => {
    const id = null;
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})
//-----------------REQUEST DATE FROM-------------------------------------------------------

test("Team - fetch data view_employees_gate_pass gate pass(string letters)",async () => {
    const id = 154151102;
    const request_date_from = "hellodate";
    const request_date_to = null;

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data view_employees_gate_pass gate pass(special characters)",async () => {
    const id = 154151102
    const request_date_from = "&#^%&@!%#&%!@";
    const request_date_to = null;

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data view_employees_gate_pass gate pass(null date from)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

//---------------REQUEST DATE TO------------------------------------------------

test("Team - fetch data view_employees_gate_pass gate pass(string letters)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = "hellodate";

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data view_employees_gate_pass gate pass(special characters)",async () => {
    const id = 154151102
    const request_date_from = null;
    const request_date_to = "&#^%&@!%#&%!@";

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data view_employees_gate_pass gate pass(null date from)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = null;

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

//------------SINGLE DATE GIVEN----------------------------------------------
//-----------------REQUEST DATE FROM-------------------------------------------------------

test("Team - fetch data view_employees_gate_pass gate pass(date_to given - string letters)",async () => {
    const id = 154151102;
    const request_date_from = "hellodate";
    const request_date_to = "2019-06-13";

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data view_employees_gate_pass gate pass(date_to given - special characters)",async () => {
    const id = 154151102
    const request_date_from = "&#^%&@!%#&%!@";
    const request_date_to = "2019-06-13";

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data view_employees_gate_pass gate pass(date_to given - null date from)",async () => {
    const id = 154151102;
    const request_date_from = null;
    const request_date_to = "2019-06-13";

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

//---------------REQUEST DATE TO------------------------------------------------

test("Team - fetch data view_employees_gate_pass gate pass(date_from given - string letters)",async () => {
    const id = 154151102;
    const request_date_from = "2019-06-13";
    const request_date_to = "hellodate";

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data view_employees_gate_pass gate pass(date_from given - special characters)",async () => {
    const id = 154151102
    const request_date_from = "2019-06-13";
    const request_date_to = "&#^%&@!%#&%!@";

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})

test("Team - fetch data view_employees_gate_pass gate pass(date_from given - null date from)",async () => {
    const id = 154151102;
    const request_date_from = "2019-06-13";
    const request_date_to = null;

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data.response.status).toBe(403);
    })
})
//----------------------------------------------------------------------------
test("Team - fetch data view_employees_gate_pass gate pass",async () => {
    const id = 154151102;
    const request_date_from = "2019-06-13";
    const request_date_to = "2019-06-13";

    return await team.fetchTeamEmployeeGatePass(id,request_date_from,request_date_to).then(data => {
        expect(data).not.toBe("Error");
    })
})
//**************************/view_employees_gate_pass*************************
