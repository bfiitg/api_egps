const axios = require('axios');

const department_head = {
  login: (id, password) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/login',
      data: {
        id,
        password
      }
    })
      .then(res => res.data)
      .catch(err => err.response),

  fetchDepartmentDisapproved: (
    id,
    request_date_from,
    request_date_to,
    modules,
    token,
    roleParams
  ) =>
    axios({
      method: 'POST',
      url: `http://localhost:3000/requests/viewDisapproved/${roleParams}`,
      data: {
        employee_id: id,
        request_date_from: request_date_from,
        request_date_to: request_date_to,
        modules
      },
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  fetchDepartmentApproved: (
    id,
    request_date_from,
    request_date_to,
    modules,
    token,
    roleParams
  ) =>
    axios({
      method: 'POST',
      url: `http://localhost:3000/requests/viewApproved/${roleParams}`,
      data: {
        employee_id: id,
        request_date_from: request_date_from,
        request_date_to: request_date_to,
        modules
      },
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  fetchDepartmentPending: (
    id,
    request_date_from,
    request_date_to,
    modules,
    token,
    roleParams
  ) =>
    axios({
      method: 'POST',
      url: `http://localhost:3000/requests/viewPending/${roleParams}`,
      data: {
        employee_id: id,
        request_date_from: request_date_from,
        request_date_to: request_date_to,
        modules
      },
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => res)
      .catch(err => err),

  fetchEmployessReport: (
    id,
    request_date_from,
    request_date_to,
    modules,
    token,
    roleParams
  ) =>
    axios({
      method: 'POST',
      url: `http://localhost:3000/reports/employees_report/${roleParams}`,
      data: {
        employee_id: id,
        request_date_from: request_date_from,
        request_date_to: request_date_to,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err)
};

module.exports = department_head;
