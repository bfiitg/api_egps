const department_head = require('./department_head');
const moment = require('moment');
const dateToday = moment().format('YYYY-MM-DD');

let loginData;
let employee;

beforeAll(async () => {
  loginData = await department_head.login(154151101, 'emp');
  employee = await department_head
    .login(154151104, 'emp')
    .then(data => data);
});

test('Department_Head - fetch data view gate pass without access rights', async () => {
  return await department_head
    .fetchDepartmentPending(
      employee.id,
      dateToday,
      dateToday,
      employee.modules,
      employee.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('Department_Head - fetch data view gate pass with access rights', async () => {
  const employee = loginData;


  let actions = [];
  employee.modules.map(modules =>
    modules.actions.map(action => actions.push(action))
  );

  expect(actions).toEqual(
    expect.arrayContaining([
      expect.objectContaining({ description: 'View requests' })
    ])
  );
});

//***************************/viewDisapproved****************************************

test('Department_Head - fetch data viewDisapproved gate pass without token', async () => {
  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data viewDisapproved gate pass', async () => {
  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).not.toBe(400 && 403 && 401);
    });
});

test('Department_Head - fetch data viewDisapproved gate pass(string id)', async () => {
  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Department_Head - fetch data viewDisapproved gate pass(special character id)', async () => {
  const id = '*@&#^$*^*&#$';

  return await department_head
    .fetchDepartmentDisapproved(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data viewDisapproved gate pass(null id)', async () => {
  const id = null;

  return await department_head
    .fetchDepartmentDisapproved(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});
// //---------------ID-----------------------------------------
test('Department_Head - fetch data viewDisapproved gate pass(string id)', async () => {
  const request_date_from = null;
  const request_date_to = null;
  const id = loginData.id.toString();
  return await department_head
    .fetchDepartmentDisapproved(
      id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Department_Head - fetch data viewDisapproved gate pass(string letters)', async () => {
  const id = 'helloid';
  const request_date_from = null;
  const request_date_to = null;

  return await department_head
    .fetchDepartmentDisapproved(
      id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data viewDisapproved gate pass(special characters)', async () => {
  const id = '&^%&^%@&#!@#';
  const request_date_from = null;
  const request_date_to = null;

  return await department_head
    .fetchDepartmentDisapproved(
      id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data viewDisapproved gate pass(null id)', async () => {
  const id = null;
  const request_date_from = null;
  const request_date_to = null;

  return await department_head
    .fetchDepartmentDisapproved(
      id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});
// //-----------------REQUEST DATE FROM-------------------------------------------------------

test('Department_Head - fetch data viewDisapproved gate pass(string letters)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = null;

  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewDisapproved gate pass(special characters)', async () => {
  const request_date_from = '&#^%&@!%#&%!@';
  const request_date_to = null;
  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewDisapproved gate pass(null date from)', async () => {
  const request_date_from = null;
  const request_date_to = null;

  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //---------------REQUEST DATE TO------------------------------------------------

test('Department_Head - fetch data viewDisapproved gate pass(string letters)', async () => {
  const request_date_from = null;
  const request_date_to = 'hellodate';

  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewDisapproved gate pass(special characters)', async () => {
  const request_date_from = null;
  const request_date_to = '&#^%&@!%#&%!@';

  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewDisapproved gate pass(null date from)', async () => {
  const request_date_from = null;
  const request_date_to = null;

  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //------------SINGLE DATE GIVEN----------------------------------------------
// //-----------------REQUEST DATE FROM-------------------------------------------------------

test('Department_Head - fetch data viewDisapproved gate pass(date_to given - string letters)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = '2019-06-13';

  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewDisapproved gate pass(date_to given - special characters)', async () => {
  const request_date_from = '&#^%&@!%#&%!@';
  const request_date_to = '2019-06-13';

  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewDisapproved gate pass(date_to given - null date from)', async () => {
  const request_date_from = null;
  const request_date_to = '2019-06-13';

  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //---------------REQUEST DATE TO------------------------------------------------

test('Department_Head - fetch data viewDisapproved gate pass(date_from given - string letters)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = 'hellodate';

  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewDisapproved gate pass(date_from given - special characters)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = '&#^%&@!%#&%!@';

  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewDisapproved gate pass(date_from given - null date from)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = null;

  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

//----------------------------------------------------------------------------

test('Department_Head - fetch data viewDisapproved gate pass', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = '2019-06-13';

  return await department_head
    .fetchDepartmentDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response).not.toBe(400 && 401 && 403);
    });
});

//**************************/viewDisapproved*************************

//**************************/viewApproved*************************
test('Department_Head - fetch data viewApproved gate pass', async () => {
  return await department_head
    .fetchDepartmentApproved(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data viewApproved gate pass', async () => {
  return await department_head
    .fetchDepartmentApproved(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).not.toBe(403 && 401 && 400);
    });
});

test('Department_Head - fetch data viewApproved gate pass(string id)', async () => {
  const id = loginData.id.toString();

  return await department_head
    .fetchDepartmentApproved(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Department_Head - fetch data viewApproved gate pass(special character id)', async () => {
  const id = '*@&#^$*^*&#$';

  return await department_head
    .fetchDepartmentApproved(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data viewApproved gate pass(null id)', async () => {
  const id = null;

  return await department_head
    .fetchDepartmentApproved(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});
// //---------------ID-----------------------------------------
test('Department_Head - fetch data viewApproved gate pass(string id)', async () => {
  const id = loginData.id.toString();

  return await department_head
    .fetchDepartmentApproved(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Department_Head - fetch data viewApproved gate pass(string letters)', async () => {
  const id = 'helloid';

  return await department_head
    .fetchDepartmentApproved(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data viewApproved gate pass(special characters)', async () => {
  const id = '&^%&^%@&#!@#';

  return await department_head
    .fetchDepartmentApproved(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data viewApproved gate pass(null id)', async () => {
  const id = null;

  return await department_head
    .fetchDepartmentApproved(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

// //-----------------REQUEST DATE FROM-------------------------------------------------------

test('Department_Head - fetch data viewApproved gate pass(string letters)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = null;

  return await department_head
    .fetchDepartmentApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewApproved gate pass(special characters)', async () => {
  const request_date_from = '&#^%&@!%#&%!@';
  const request_date_to = null;

  return await department_head
    .fetchDepartmentApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewApproved gate pass(null date from)', async () => {
  const request_date_from = null;
  const request_date_to = null;

  return await department_head
    .fetchDepartmentApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //---------------REQUEST DATE TO------------------------------------------------

test('Department_Head - fetch data viewApproved gate pass(string letters)', async () => {
  const request_date_from = null;
  const request_date_to = 'hellodate';

  return await department_head
    .fetchDepartmentApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewApproved gate pass(special characters)', async () => {
  const request_date_from = null;
  const request_date_to = '&#^%&@!%#&%!@';

  return await department_head
    .fetchDepartmentApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewApproved gate pass(null date from)', async () => {
  const request_date_from = null;
  const request_date_to = null;

  return await department_head
    .fetchDepartmentApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //------------SINGLE DATE GIVEN----------------------------------------------
// //-----------------REQUEST DATE FROM-------------------------------------------------------

test('Department_Head - fetch data viewApproved gate pass(date_to given - string letters)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = '2019-06-13';

  return await department_head
    .fetchDepartmentApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewApproved gate pass(date_to given - special characters)', async () => {
  const request_date_from = '&#^%&@!%#&%!@';
  const request_date_to = '2019-06-13';

  return await department_head
    .fetchDepartmentApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewApproved gate pass(date_to given - null date from)', async () => {
  const request_date_from = null;
  const request_date_to = '2019-06-13';

  return await department_head
    .fetchDepartmentApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //---------------REQUEST DATE TO------------------------------------------------

test('Department_Head - fetch data viewApproved gate pass(date_from given - string letters)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = 'hellodate';

  return await department_head
    .fetchDepartmentApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewApproved gate pass(date_from given - special characters)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = '&#^%&@!%#&%!@';

  return await department_head
    .fetchDepartmentApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewApproved gate pass(date_from given - null date from)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = null;

  return await department_head
    .fetchDepartmentApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});
// //----------------------------------------------------------------------------
test('Department_Head - fetch data viewApproved gate pass', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = '2019-06-13';

  return await department_head
    .fetchDepartmentApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).not.toBe(400 && 403 && 401);
    });
});
// //**************************/viewApproved*************************
// //**************************/viewPending*************************

test('Department_Head - fetch data viewPending gate pass', async () => {
  return await department_head
    .fetchDepartmentPending(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).not.toBe(403 && 400 && 401);
    });
});

test('Department_Head - fetch data viewPending gate pass(string id)', async () => {
  const id = loginData.id.toString();

  return await department_head
    .fetchDepartmentPending(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Department_Head - fetch data viewPending gate pass(special character id)', async () => {
  const id = '*@&#^$*^*&#$';

  return await department_head
    .fetchDepartmentPending(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data viewPending gate pass(null id)', async () => {
  const id = null;

  return await department_head
    .fetchDepartmentPending(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

// //---------------ID-----------------------------------------

test('Department_Head - fetch data viewPending gate pass(string id)', async () => {
  const id = loginData.id.toString();

  return await department_head
    .fetchDepartmentPending(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('Department_Head - fetch data viewPending gate pass(string letters)', async () => {
  const id = 'helloid';

  return await department_head
    .fetchDepartmentPending(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data viewPending gate pass(special characters)', async () => {
  const id = '&^%&^%@&#!@#';

  return await department_head
    .fetchDepartmentPending(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data viewPending gate pass(null id)', async () => {
  const id = null;
  return await department_head
    .fetchDepartmentPending(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

// //-----------------REQUEST DATE FROM-------------------------------------------------------

test('Department_Head - fetch data viewPending gate pass(string letters)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = null;

  return await department_head
    .fetchDepartmentPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewPending gate pass(special characters)', async () => {
  const request_date_from = '&#^%&@!%#&%!@';
  const request_date_to = null;

  return await department_head
    .fetchDepartmentPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewPending gate pass(null date from)', async () => {
  const request_date_from = null;
  const request_date_to = null;

  return await department_head
    .fetchDepartmentPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //---------------REQUEST DATE TO------------------------------------------------

test('Department_Head - fetch data viewPending gate pass(string letters)', async () => {
  const request_date_from = null;
  const request_date_to = 'hellodate';

  return await department_head
    .fetchDepartmentPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewPending gate pass(special characters)', async () => {
  const request_date_from = null;
  const request_date_to = '&#^%&@!%#&%!@';

  return await department_head
    .fetchDepartmentPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewPending gate pass(null date from)', async () => {
  const request_date_from = null;
  const request_date_to = null;

  return await department_head
    .fetchDepartmentPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //------------SINGLE DATE GIVEN----------------------------------------------
// //-----------------REQUEST DATE FROM-------------------------------------------------------

test('Department_Head - fetch data viewPending gate pass(date_to given - string letters)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = '2019-06-13';

  return await department_head
    .fetchDepartmentPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewPending gate pass(date_to given - special characters)', async () => {
  const request_date_from = '&#^%&@!%#&%!@';
  const request_date_to = '2019-06-13';
  return await department_head
    .fetchDepartmentPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewPending gate pass(date_to given - null date from)', async () => {
  const request_date_from = null;
  const request_date_to = '2019-06-13';

  return await department_head
    .fetchDepartmentPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //---------------REQUEST DATE TO------------------------------------------------

test('Department_Head - fetch data viewPending gate pass(date_from given - string letters)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = 'hellodate';

  return await department_head
    .fetchDepartmentPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewPending gate pass(date_from given - special characters)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = '&#^%&@!%#&%!@';

  return await department_head
    .fetchDepartmentPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data viewPending gate pass(date_from given - null date from)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = null;

  return await department_head
    .fetchDepartmentPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});
// //----------------------------------------------------------------------------
test('Department_Head - fetch data viewPending gate pass', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = '2019-06-13';

  return await department_head
    .fetchDepartmentPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data).not.toBe('Error');
    });
});
// //**************************/viewPending*************************

// //**************************/employeees_report*************************
test('Department_Head - fetch data employeees_report gate pass without access rights', async () => {
  return await department_head
    .fetchEmployessReport(
      employee.id,
      dateToday,
      dateToday,
      employee.modules,
      employee.token
    )
    .then(data => expect(data.response.status).toBe(401));
});

test('Department_Head - fetch data employeees_report gate pass with access rights', async () => {
  const employee = loginData;

  let actions = [];
  employee.modules.map(modules =>
    modules.actions.map(action => actions.push(action))
  );

  expect(actions).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        description: 'View employees gate pass report'
      })
    ])
  );
});

test('Department_Head - fetch data employeees_report gate pass without token', async () => {
  return await department_head
    .fetchEmployessReport(loginData.id, dateToday, dateToday, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data employeees_report gate pass', async () => {
  return await department_head
    .fetchEmployessReport(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).not.toBe(401 && 400 && 403 && 400);
    });
});

test('Department_Head - fetch data employeees_report gate pass(string id)', async () => {
  const id = loginData.id.toString();
  return await department_head
    .fetchEmployessReport(
      'id',
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data employeees_report gate pass(special character id)', async () => {
  const id = '*@&#^$*^*&#$';

  return await department_head
    .fetchEmployessReport(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data employeees_report gate pass(null id)', async () => {
  const id = null;

  return await department_head
    .fetchEmployessReport(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});
// //---------------ID-----------------------------------------

test('Department_Head - fetch data employeees_report gate pass(string letters)', async () => {
  const id = 'helloid';

  return await department_head
    .fetchEmployessReport(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data employeees_report gate pass(special characters)', async () => {
  const id = '&^%&^%@&#!@#';

  return await department_head
    .fetchEmployessReport(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('Department_Head - fetch data employeees_report gate pass(null id)', async () => {
  const id = null;

  return await department_head
    .fetchEmployessReport(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});
// //-----------------REQUEST DATE FROM-------------------------------------------------------

test('Department_Head - fetch data employeees_report gate pass(string letters)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = null;
  return await department_head
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data employeees_report gate pass(special characters)', async () => {
  const request_date_from = '&#^%&@!%#&%!@';
  const request_date_to = null;
  return await department_head
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data employeees_report gate pass(null date from)', async () => {
  const request_date_from = null;
  const request_date_to = null;

  return await department_head
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //---------------REQUEST DATE TO------------------------------------------------

test('Department_Head - fetch data employeees_report gate pass(string letters)', async () => {
  const request_date_from = null;
  const request_date_to = 'hellodate';

  return await department_head
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data employeees_report gate pass(special characters)', async () => {
  const request_date_from = null;
  const request_date_to = '&#^%&@!%#&%!@';

  return await department_head
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});


// //------------SINGLE DATE GIVEN----------------------------------------------
// //-----------------REQUEST DATE FROM-------------------------------------------------------

test('Department_Head - fetch data employeees_report gate pass(date_to given - string letters)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = '2019-06-13';

  return await department_head
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data employeees_report gate pass(date_to given - special characters)', async () => {
  const request_date_from = '&#^%&@!%#&%!@';
  const request_date_to = '2019-06-13';

  return await department_head
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data employeees_report gate pass(date_to given - null date from)', async () => {
  const request_date_from = null;
  const request_date_to = '2019-06-13';

  return await department_head
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //---------------REQUEST DATE TO------------------------------------------------

test('Department_Head - fetch data employeees_report gate pass(date_from given - string letters)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = 'hellodate';

  return await department_head
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data employeees_report gate pass(date_from given - special characters)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = '&#^%&@!%#&%!@';
  return await department_head
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('Department_Head - fetch data employeees_report gate pass(date_from given - null date from)', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = null;
  return await department_head
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});
// //----------------------------------------------------------------------------
test('Department_Head - fetch data employeees_report gate pass', async () => {
  const request_date_from = '2019-06-13';
  const request_date_to = '2019-06-13';

  return await department_head
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).not.toBe(400 && 401 && 403);
    });
});
// // **************************/employeees_report*************************
