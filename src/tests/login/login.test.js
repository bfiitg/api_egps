const login = require("./login");

test("Login (null-null)", () => {
    const id = null;
    const password = null;

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})

test("Login (null-string)", () => {
    const id = null;
    const password = "hellopassword";

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})

test("Login (null-special character)", () => {
    const id = null;
    const password = "!@#$%^&*()";

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})

test("Login (null-int)", () => {
    const id = null;
    const password = 1234567890;

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})

test("Login (string-string)", () => {
    const id = "helloid";
    const password = "hellopassword";

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})

test("Login (string-null)", () => {
    const id = "helloid";
    const password = null;

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})

test("Login (string-special character)", () => {
    const id = "helloid";
    const password = "!@#$%^&*()";

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})

test("Login (string-int)", () => {
    const id = "helloid";
    const password = 1234567890;

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})

test("Login (special character-special character)", () => {
    const id = "!@#$%^&*()";
    const password = "!@#$%^&*()";

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})


test("Login (special character-null)", () => {
    const id = "!@#$%^&*()";
    const password = null;

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})

test("Login (special character-string)", () => {
    const id = "!@#$%^&*()";
    const password = "hellopassword";

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})

test("Login (special character-int)", () => {
    const id = "!@#$%^&*()";
    const password = 1234567890;

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})

test("Login (int-int)", () => {
    const id = 1234567890;
    const password = 1234567890;

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})

test("Login (int-null)", () => {
    const id = 1234567890;
    const password = null;

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})

test("Login (int-string)", () => {
    const id = 1234567890;
    const password = "hellopassword";

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})

test("Login (int-special character)", () => {
    const id = 1234567890;
    const password = "!@#$%^&*()";

    return login.fetchLogin(id,password).then(data => {
        expect(data.response.status).toBe(400);
    })
})