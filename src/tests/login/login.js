const axios = require("axios");

const login = {

    fetchLogin: (employee) => 
    axios.post("http://localhost:3000/login", {
        employee: employee
    })
    .then(res => res.data)
    .catch(err => err)

}

module.exports = login;