const people_group = require('./people_group');
//***************************/viewDisapproved****************************************
const moment = require('moment');
const dateToday = moment().format('YYYY-MM-DD');
let loginData;
beforeAll(async () => {
  loginData = await people_group.login(154151102, 'emp');
});

test('People group - fetch data view disapprove gate pass without token ', async () => {
  return await people_group
    .fetchPGDisapproved(loginData.id, dateToday, dateToday, loginData.modules, loginData.roleParam)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('People group - fetch data view disapprove gate pass without modules ', async () => {
  return await people_group
    .fetchPGDisapproved(
      loginData.id,
      dateToday,
      dateToday,
      undefined,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('People group - fetch data view disapprove gate pass without access rights ', async () => {
  const modules = [
    {
      id: 2,
      description: 'Gate Pass',
      actions: [
        {
          id: 19,
          description: 'Request gate pass',
          module_id: 2
        },
        {
          id: 20,
          description: 'View schedule out',
          module_id: 2
        }
      ]
    }
  ];
  return await people_group
    .fetchPGDisapproved(
      loginData.id,
      dateToday,
      dateToday,
      modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('People Group - fetch data viewDisapproved gate pass', async () => {
  return await people_group
    .fetchPGDisapproved(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('People Group - fetch data viewDisapproved gate pass(special character id)', async () => {
  const id = '*@&#^$*^*&#$';

  return await people_group.fetchPGDisapproved(id).then(data => {
    expect(data.response.status).toBe(403);
  });
});

test('People Group - fetch data viewDisapproved gate pass(null id)', async () => {
  const id = null;

  return await people_group.fetchPGDisapproved(id).then(data => {
    expect(data.response.status).toBe(403);
  });
});

// //---------------ID-----------------------------------------
test('People Group - fetch data viewDisapproved gate pass(null dates)', async () => {
  const request_date_from = null;
  const request_date_to = null;

  return await people_group
    .fetchPGDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('People Group - fetch data viewDisapproved gate pass(string letters)', async () => {
  const id = 'helloid';
  const request_date_from = null;
  const request_date_to = null;

  return await people_group
    .fetchPGDisapproved(id, request_date_from, request_date_to)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('People Group - fetch data viewDisapproved gate pass(null id)', async () => {
  const id = null;
  const request_date_from = null;
  const request_date_to = null;

  return await people_group
    .fetchPGDisapproved(id, request_date_from, request_date_to)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});
// //-----------------REQUEST DATE FROM-------------------------------------------------------

test('People Group - fetch data viewDisapproved gate pass(string request date from)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = null;

  return await people_group
    .fetchPGDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('People Group - fetch data viewDisapproved gate pass(string request date to)', async () => {
  const request_date_from = null;
  const request_date_to = 'hellodate';

  return await people_group
    .fetchPGDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('People Group - fetch data viewDisapproved gate pass(integer request date from)', async () => {
  const request_date_from = 231452;
  const request_date_to = null;

  return await people_group
    .fetchPGDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('People Group - fetch data viewDisapproved gate pass(integer request date to)', async () => {
  const request_date_from = null;
  const request_date_to = 123532;

  return await people_group
    .fetchPGDisapproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //**************************/viewDisapproved*************************
// //**************************/viewApproved*************************
test('People group - fetch data view approved gate pass without token ', async () => {
  return await people_group
    .fetchPGApproved(loginData.id, dateToday, dateToday, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('People group - fetch data view approved gate pass without modules ', async () => {
  return await people_group
    .fetchPGApproved(
      loginData.id,
      dateToday,
      dateToday,
      undefined,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('People group - fetch data view approved gate pass without access rights ', async () => {
  const modules = [
    {
      id: 2,
      description: 'Gate Pass',
      actions: [
        {
          id: 19,
          description: 'Request gate pass',
          module_id: 2
        },
        {
          id: 20,
          description: 'View schedule out',
          module_id: 2
        }
      ]
    }
  ];
  return await people_group
    .fetchPGApproved(
      loginData.id,
      dateToday,
      dateToday,
      modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('People Group - fetch data viewApproved gate pass', async () => {
  return await people_group
    .fetchPGApproved(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});
test('People Group - fetch data viewApproved gate pass(string id)', async () => {
  const id = '*@&#^$*^*&#$';

  return await people_group
    .fetchPGApproved(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('People Group - fetch data viewApproved gate pass(null id)', async () => {
  const id = null;

  return await people_group
    .fetchPGApproved(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});
// //---------------ID-----------------------------------------
test('People Group - fetch data viewApproved gate pass(null dates)', async () => {
  const request_date_from = null;
  const request_date_to = null;

  return await people_group
    .fetchPGApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //-----------------REQUEST DATE FROM-------------------------------------------------------

test('People Group - fetch data viewApproved gate pass(string date from)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = null;

  return await people_group
    .fetchPGApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('People Group - fetch data viewApproved gate pass(string date to)', async () => {
  const request_date_from = null;
  const request_date_to = 'hellodate';

  return await people_group
    .fetchPGApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('People Group - fetch data viewApproved gate pass(integer date from)', async () => {
  const request_date_from = 5138;
  const request_date_to = null;

  return await people_group
    .fetchPGApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('People Group - fetch data viewApproved gate pass(integer date to)', async () => {
  const request_date_from = null;
  const request_date_to = 53135;

  return await people_group
    .fetchPGApproved(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //**************************/viewApproved*************************

// //**************************/viewPending*************************
test('People group - fetch data view pending gate pass without token ', async () => {
  return await people_group
    .fetchPGPending(loginData.id, dateToday, dateToday, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('People group - fetch data view pending gate pass without modules ', async () => {
  return await people_group
    .fetchPGPending(
      loginData.id,
      dateToday,
      dateToday,
      undefined,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('People group - fetch data view pending gate pass without access rights ', async () => {
  const modules = [
    {
      id: 2,
      description: 'Gate Pass',
      actions: [
        {
          id: 19,
          description: 'Request gate pass',
          module_id: 2
        },
        {
          id: 20,
          description: 'View schedule out',
          module_id: 2
        }
      ]
    }
  ];
  return await people_group
    .fetchPGPending(
      loginData.id,
      dateToday,
      dateToday,
      modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('People Group - fetch data viewPending gate pass', async () => {
  return await people_group
    .fetchPGPending(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('People Group - fetch data viewPending gate pass(special character id)', async () => {
  const id = '*@&#^$*^*&#$';

  return await people_group
    .fetchPGPending(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});
test('People Group - fetch data viewPending gate pass(null id)', async () => {
  const id = null;

  return await people_group
    .fetchPGPending(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});
// //---------------ID-----------------------------------------
test('People Group - fetch data viewPending gate pass(null dates)', async () => {
  const request_date_from = null;
  const request_date_to = null;

  return await people_group
    .fetchPGPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //-----------------REQUEST DATE FROM-------------------------------------------------------

test('People Group - fetch data viewPending gate pass(string date from)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = null;

  return await people_group
    .fetchPGPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('People Group - fetch data viewPending gate pass(string date to)', async () => {
  const request_date_from = null;
  const request_date_to = 'hellodate';

  return await people_group
    .fetchPGPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('People Group - fetch data viewPending gate pass(integer date from)', async () => {
  const request_date_from = 185351;
  const request_date_to = null;

  return await people_group
    .fetchPGPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('People Group - fetch data viewPending gate pass(integer date to)', async () => {
  const request_date_from = null;
  const request_date_to = 15351;

  return await people_group
    .fetchPGPending(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //**************************/viewPending*************************

// //**************************/employeees_report*************************

test('People group - fetch data view pending gate pass without token ', async () => {
  return await people_group
    .fetchEmployessReport(loginData.id, dateToday, dateToday, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('People group - fetch data view pending gate pass without modules ', async () => {
  return await people_group
    .fetchEmployessReport(
      loginData.id,
      dateToday,
      dateToday,
      undefined,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('People group - fetch data view pending gate pass without access rights ', async () => {
  const modules = [
    {
      id: 2,
      description: 'Gate Pass',
      actions: [
        {
          id: 19,
          description: 'Request gate pass',
          module_id: 2
        },
        {
          id: 20,
          description: 'View schedule out',
          module_id: 2
        }
      ]
    }
  ];
  return await people_group
    .fetchEmployessReport(
      loginData.id,
      dateToday,
      dateToday,
      modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('People Group - fetch data employeees_report gate pass', async () => {
  return await people_group
    .fetchEmployessReport(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('People Group - fetch data employeees_report gate pass(special character id)', async () => {
  const id = '*@&#^$*^*&#$';

  return await people_group
    .fetchEmployessReport(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});
test('People Group - fetch data employeees_report gate pass(null id)', async () => {
  const id = null;

  return await people_group
    .fetchEmployessReport(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});
// //---------------ID-----------------------------------------
test('People Group - fetch data employeees_report gate pass(null dates)', async () => {
  const request_date_from = null;
  const request_date_to = null;

  return await people_group
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('People Group - fetch data employeees_report gate pass(string date_from)', async () => {
  const request_date_from = 'hellodate';
  const request_date_to = null;

  return await people_group
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('People Group - fetch data employeees_report gate pass(string date_to)', async () => {
  const request_date_from = null;
  const request_date_to = 'hellodate';

  return await people_group
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('People Group - fetch data employeees_report gate pass(integer date_from)', async () => {
  const request_date_from = 123123;
  const request_date_to = null;

  return await people_group
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('People Group - fetch data employeees_report gate pass(integer date_to)', async () => {
  const request_date_from = null;
  const request_date_to = 531235;

  return await people_group
    .fetchEmployessReport(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token,
      loginData.roleParam
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //**************************/employeees_report*************************
// //**************************/view_personal_out*************************
test('People group - fetch data view pending gate pass without token ', async () => {
  return await people_group
    .fetchPersonalOut(loginData.id, dateToday, dateToday, loginData.modules)
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('People group - fetch data view pending gate pass without modules ', async () => {
  return await people_group
    .fetchPersonalOut(
      loginData.id,
      dateToday,
      dateToday,
      undefined,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('People group - fetch data view pending gate pass without access rights ', async () => {
  const modules = [
    {
      id: 2,
      description: 'Gate Pass',
      actions: [
        {
          id: 19,
          description: 'Request gate pass',
          module_id: 2
        },
        {
          id: 20,
          description: 'View schedule out',
          module_id: 2
        }
      ]
    }
  ];
  return await people_group
    .fetchPersonalOut(
      loginData.id,
      dateToday,
      dateToday,
      modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(401);
    });
});

test('People Group - fetch data personal gate pass', async () => {
  return await people_group
    .fetchPersonalOut(
      loginData.id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('People Group - fetch data personal gate pass (string id)', async () => {
  const id = '$$#@@asda';
  return await people_group
    .fetchPersonalOut(
      id,
      dateToday,
      dateToday,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(403);
    });
});

test('People Group - fetch data personal gate pass(null date)', async () => {
  const request_date_from = null;
  const request_date_to = null;

  return await people_group
    .fetchPersonalOut(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('People Group - fetch data personal gate pass(string date from)', async () => {
  const request_date_from = 'date';
  const request_date_to = null;

  return await people_group
    .fetchPersonalOut(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('People Group - fetch data personal gate pass(string date to)', async () => {
  const request_date_from = null;
  const request_date_to = 'date';

  return await people_group
    .fetchPersonalOut(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.response.status).toBe(400);
    });
});

test('People Group - fetch data personal gate pass(integer date from)', async () => {
  const request_date_from = 453125;
  const request_date_to = null;

  return await people_group
    .fetchPersonalOut(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

test('People Group - fetch data personal gate pass(integer date to)', async () => {
  const request_date_from = null;
  const request_date_to = 1543851;

  return await people_group
    .fetchPersonalOut(
      loginData.id,
      request_date_from,
      request_date_to,
      loginData.modules,
      loginData.token
    )
    .then(data => {
      expect(data.status).toBe(200);
    });
});

// //**************************/view_personal_out*************************
