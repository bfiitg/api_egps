const axios = require('axios');

const people_group = {
  login: (id, password) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/login',
      data: {
        id,
        password
      }
    })
      .then(res => res.data)
      .catch(err => err.response),

  fetchPGDisapproved: (
    id,
    request_date_from,
    request_date_to,
    modules,
    token,
    roleParam
  ) =>
    axios({
      method: 'POST',
      url: `http://localhost:3000/requests/viewDisapproved/${roleParam}`,
      data: {
        employee_id: id,
        request_date_from: request_date_from,
        request_date_to: request_date_to,
        modules,
        roleParam
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  fetchPGApproved: (id, request_date_from, request_date_to, modules, token,roleParam) =>
    axios({
      method: 'POST',
      url: `http://localhost:3000/requests/viewApproved/${roleParam}`,
      data: {
        employee_id: id,
        request_date_from: request_date_from,
        request_date_to: request_date_to,
        modules,
        roleParam
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  fetchPGPending: (id, request_date_from, request_date_to, modules, token,roleParam) =>
    axios({
      method: 'POST',
      url: `http://localhost:3000/requests/viewPending/${roleParam}`,
      data: {
        employee_id: id,
        request_date_from: request_date_from,
        request_date_to: request_date_to,
        modules,
        roleParam
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  fetchEmployessReport: (
    id,
    request_date_from,
    request_date_to,
    modules,
    token,
    roleParam
  ) =>
    axios({
      method: 'POST',
      url: `http://localhost:3000/reports/employees_report/${roleParam}`,
      data: {
        employee_id: id,
        request_date_from: request_date_from,
        request_date_to: request_date_to,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err),

  fetchPersonalOut: (id, request_date_from, request_date_to, modules, token) =>
    axios({
      method: 'POST',
      url: 'http://localhost:3000/reports/view_personal_out',
      data: {
        employee_id: id,
        request_date_from: request_date_from,
        request_date_to: request_date_to,
        modules
      },
      headers: { Authorization: 'Bearer ' + token }
    })
      .then(res => res)
      .catch(err => err)
};

module.exports = people_group;
