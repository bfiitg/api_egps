const express = require('express');
const router = express.Router();

//Express-callback
const makeCallback = require('../../express-callback');

//Authorization
const getAuthorization = require('../../token/index').getAuthorization;

//Uploading
const { upload } = require('../../upload/');

//Controllers
const {
  //Employees
  getEmployees,
  postEmployee,
  putEmployee,
  postEmployeeFile,
  postImportEmployees,
  putImportEmployees,
  resetEmployeePassword,

  //Roles
  getRoles,
  postRole,
  putRole,

  //Positions
  getPositions,
  postPosition,
  putPosition,

  //Teams
  getTeams,
  postTeam,
  putTeam,

  // Teamheads
  deleteTH,
  addTH,

  addPA,
  deletePA,

  //Departments
  getDepartments,
  postDepartment,
  putDepartment,

  //Request types
  getRequestTypes,
  postRequestType,
  putRequestType,

  //Modules
  getModules,
  postModule,
  putModule,

  //Actions
  getActions,
  postAction,
  putAction,

  //Access Rights
  getAccessRights,
  postAccessRight,
  putAccessRight,
  getAccessRightsByRole,
 

  getActivityLogs
} = require('../../controllers/admin');

//Routes
//Employees
router.post('/employees', getAuthorization, makeCallback(getEmployees));
router.post(
  '/employees/add_employee',
  getAuthorization,
  makeCallback(postEmployee)
);
router.put(
  '/employees/update_employee',
  getAuthorization,
  makeCallback(putEmployee)
);
router.post(
  '/employees/upload_file',
  getAuthorization,
  upload.any(),
  makeCallback(postEmployeeFile)
);
router.post('/employees/import', makeCallback(postImportEmployees));
router.put('/employees/edit/import', makeCallback(putImportEmployees));
router.put(
  '/employees/reset_password',
  getAuthorization,
  makeCallback(resetEmployeePassword)
);

//Roles
router.post('/roles', getAuthorization, makeCallback(getRoles));
router.post('/roles/add_role', getAuthorization, makeCallback(postRole));
router.put('/roles/update_role', getAuthorization, makeCallback(putRole));

//Positions
router.post('/positions', getAuthorization, makeCallback(getPositions));
router.post(
  '/positions/add_position',
  getAuthorization,
  makeCallback(postPosition)
);
router.put(
  '/positions/update_position',
  getAuthorization,
  makeCallback(putPosition)
);

//Teams
router.post('/teams', getAuthorization, makeCallback(getTeams));
router.post('/teams/add_team', getAuthorization, makeCallback(postTeam));
router.put('/teams/update_team', getAuthorization, makeCallback(putTeam));

// Teamheads
router.delete('/teamhead/delete_teamhead', getAuthorization, makeCallback(deleteTH))
router.post('/teamhead/add_teamhead', getAuthorization, makeCallback(addTH))

// Proxy
router.delete('/proxy/delete_proxy', getAuthorization, makeCallback(deletePA))
router.post('/proxy/add_proxy', getAuthorization, makeCallback(addPA))

//Departments
router.post('/departments', getAuthorization, makeCallback(getDepartments));
router.post(
  '/departments/add_department',
  getAuthorization,
  makeCallback(postDepartment)
);
router.put(
  '/departments/update_department',
  getAuthorization,
  makeCallback(putDepartment)
);

//Request types
router.post('/request_types', getAuthorization, makeCallback(getRequestTypes));
router.post(
  '/request_types/add_request_type',
  getAuthorization,
  makeCallback(postRequestType)
);
router.put(
  '/request_types/update_request_type',
  getAuthorization,
  makeCallback(putRequestType)
);

//Modules
router.post('/modules', getAuthorization, makeCallback(getModules));
router.post('/modules/add_module', getAuthorization, makeCallback(postModule));
router.put('/modules/update_module', getAuthorization, makeCallback(putModule));

//Actions
router.post('/actions', getAuthorization, makeCallback(getActions));
router.post('/actions/add_action', getAuthorization, makeCallback(postAction));
router.put('/actions/update_action', getAuthorization, makeCallback(putAction));

//AccessRights
router.post('/access_rights', getAuthorization, makeCallback(getAccessRights));
router.post(
  '/access_rights/add_access_right',
  getAuthorization,
  makeCallback(postAccessRight)
);
router.put(
  '/access_rights/update_access_right',
  getAuthorization,
  makeCallback(putAccessRight)
);
router.post(
  '/access_rights/:roleId',
  getAuthorization,
  makeCallback(getAccessRightsByRole
    )
);



//Activity Logs
router.post('/activity_logs', getAuthorization, makeCallback(getActivityLogs));

module.exports = router;
