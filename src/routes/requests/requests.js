const express = require('express');
const router = express.Router();

//Express-callback
const makeCallback = require('../../express-callback');

//Authorization
const getAuthorization = require('../../token/index').getAuthorization;

//Controllers
const {
  getApprovedGatePass,
  getPendingGatePass,
  getDisapprovedGatePass
} = require('../../controllers/requests');

router.post(
  '/viewApproved/:roleParams',
  getAuthorization,
  makeCallback(getApprovedGatePass)
);

router.post(
  '/viewPending/:roleParams',
  getAuthorization,
  makeCallback(getPendingGatePass)
);

router.post(
  '/viewDisapproved/:roleParams',
  getAuthorization,
  makeCallback(getDisapprovedGatePass)
);

module.exports = router;
