const express = require('express');
const router = express.Router();

//Express-callback
const makeCallback = require('../../express-callback');

//Authorization
const getAuthorization = require('../../token/index').getAuthorization;

//Controllers
const {
  getEmployeesReport,
  getPersonalOutRequests,
  getVerifiedRequests
} = require('../../controllers/reports');

//Routes
router.post(
  '/employees_report/:roleParam',
  getAuthorization,
  makeCallback(getEmployeesReport)
);
router.post(
  '/view_personal_out',
  getAuthorization,
  makeCallback(getPersonalOutRequests)
);
router.post(
  '/view_verified_personal_out',
  getAuthorization,
  makeCallback(getVerifiedRequests)
);

module.exports = router;
