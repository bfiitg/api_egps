const express = require('express');
const router = express.Router();

//Express-callback
const makeCallback = require('../../express-callback');

//Authorization
const getAuthorization = require('../../token/index').getAuthorization;

//Controllers
const {
  getScheduleOut,
  getPersonalReport,
  fetchEmployeeProfile,
  updateEmployeePassword,
  fetchEmployeeByDept
} = require('../../controllers/employees');

router.post('/proxyemployees', getAuthorization, makeCallback(fetchEmployeeByDept))
router.post('/schedule_out', getAuthorization, makeCallback(getScheduleOut));
router.post(
  '/employee_report',
  getAuthorization,
  makeCallback(getPersonalReport)
);
router.post('/:id', getAuthorization, makeCallback(fetchEmployeeProfile));
router.put(
  '/update_password',
  getAuthorization,
  makeCallback(updateEmployeePassword)
);

module.exports = router;
