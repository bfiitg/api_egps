const express = require('express');
const router = express.Router();

const gatePassDb = require('../../data-access/gate_pass');

async function loadTest(req, res) {
  const request_info = req.body;

  const create_request = await gatePassDb.postRequest(request_info);

  if (!create_request) {
    res.status(400).send('Insert failed');
  } else {
    res.send(create_request);
  }
}

async function getNotifications(req, res) {
  const listNotifications = await gatePassDb.getallNotifications();
  if (listNotifications.length > 0) {
    res.send(listNotifications);
  } else {
    res.send('No notifications');
  }
}

router.post('/file_request', loadTest);
router.post('/get_notifs', getNotifications);

module.exports = router;
