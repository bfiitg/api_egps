const express = require('express');
const router = express.Router();

//Express-callback
const makeCallback = require('../../express-callback');

//Authorization
const getAuthorization = require('../../token/index').getAuthorization;

//Controllers
const {
  getApprovedGatePass,
  getTimeInGatePass,
  getTimeOutGatePass,
  getServerTime,
  putTimeGatePass
} = require('../../controllers/security_guard');

//Routes
router.post(
  '/viewApproved',
  getAuthorization,
  makeCallback(getApprovedGatePass)
);
router.post('/viewTimein', getAuthorization, makeCallback(getTimeInGatePass));
router.post('/viewTimeout', getAuthorization, makeCallback(getTimeOutGatePass));
router.put(
  '/gatepass/put_time',
  getAuthorization,
  makeCallback(putTimeGatePass)
);


router.get('/time', makeCallback(getServerTime));

module.exports = router;
