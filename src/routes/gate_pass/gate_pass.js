const express = require('express');
const router = express.Router();

//Express-callback
const makeCallback = require('../../express-callback');

//Authorization
const getAuthorization = require('../../token/index').getAuthorization;

//Uploading
const { upload } = require('../../upload/');

//Controllers
const {
  fileRequest,
  approveRequest,
  disapproveRequest,
  verifyGatePass,
  getNotifications,
  updateNotifications,
  unverifyGatePass,
  cancelGatePass,
  correctGatePass,
  importGatePass,
  postFile,
  processGatePass
} = require('../../controllers/gate_pass/');
const listRequestTypes = require('../../controllers/request_types');

//Routes
router.post('/requests/', getAuthorization, makeCallback(listRequestTypes));
router.post('/file_request', getAuthorization, makeCallback(fileRequest));

router.put(
  '/requests/validate_request/approve',
  getAuthorization,
  makeCallback(approveRequest)
);
router.put(
  '/requests/validate_request/disapprove',
  getAuthorization,
  makeCallback(disapproveRequest)
);
router.put(
  '/requests/validate_request/verify_personal_out',
  getAuthorization,
  makeCallback(verifyGatePass)
);

router.put(
  '/requests/validate_request/process',
  getAuthorization,
  makeCallback(processGatePass)
);

router.put(
  '/requests/validate_request/unverify_personal_out',
  getAuthorization,
  makeCallback(unverifyGatePass)
);
router.put(
  '/reports/update_gate_pass',
  getAuthorization,
  makeCallback(correctGatePass)
);

router.put('/cancel_request', getAuthorization, makeCallback(cancelGatePass));

router.post('/notifications', getAuthorization, makeCallback(getNotifications));

router.put(
  '/notifications/read',
  getAuthorization,
  makeCallback(updateNotifications)
);
router.post('/requests/import', getAuthorization, makeCallback(importGatePass));
router.post(
  '/upload_file',
  getAuthorization,
  upload.any(),
  makeCallback(postFile)
);

module.exports = router;
