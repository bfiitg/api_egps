const parser = new (require('simple-excel-to-json')).XlsParser();
const option = {
  isNested: true // if nested json or not; true
};

const importEmployees = async path => {
  const doc = parser.parseXls2Json(path, option);
  const data = doc[0]; // get all data in the first sheet of excel file

  return data;
};

module.exports = importEmployees;
