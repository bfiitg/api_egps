const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

// var useragent = require('express-useragent');

//middlewares
const middlewares = require('./middlewares')

const notFound = require('./controllers/not_found/not_found');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors())
// app.use(useragent.express());

//Uploading
app.use('/employee_files', middlewares.validateIP, express.static('employee_files'));

app.use('/login', middlewares.validateIP, require('./routes/login/login'));
app.use('/gatepass/', middlewares.validateIP, require('./routes/gate_pass/gate_pass'));
app.use('/admin', middlewares.validateIP, require('./routes/admin/admin'));
app.use('/securityguard', middlewares.validateIP, require('./routes/security_guard/security_guard'));
app.use('/employees', middlewares.validateIP, require('./routes/employees/employees'));
app.use('/reports', middlewares.validateIP, require('./routes/reports/reports'));
app.use('/requests', middlewares.validateIP, require('./routes/requests/requests'));



//For Load Test (request gate pass without validations)
app.use('/load_test', middlewares.validateIP, require('./routes/load_test/index'));

app.use(async (req, res) => {
  data = await notFound();
  res.status(data.status).send(data.body);
});

app.listen(3000, console.log('Server started at port 3000'));
