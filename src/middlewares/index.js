module.exports = {
    validateIP(req, res, next) {

        let clientIP = req.header('host')

        const whiteList = [
            '172.16.4.168:3000',
            '192.168.34.125:3000',
            '192.168.34.125:3001',
            '192.168.34.125:3002',
            '192.168.34.126:3000',
            'app.jltechsol.com:3002',
            'app.jltechsol.com:3003',
            'localhost:3001',
            '172.16.4.215:3001',
            '172.16.4.182:4002'
        ]

        if (!whiteList.includes(clientIP)) {
            return res.status(400).send(`Not in whitelist`)
        }

        next()

    }
}