const employees = require('./employees_db');
const models = require('../../database/models');

const employeeDb = employees({ models });

module.exports = employeeDb;
