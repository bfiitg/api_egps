module.exports = function employeesDb({ models }) {
  const {
    employees,
    positions,
    teams,
    departments,
    roles,
    access_rights,
    actions,
    modules,
    gate_pass,
    gate_pass_status_logs,
    request_types
  } = models;
  const Op = models.Sequelize.Op;

  return Object.freeze({
    findById,
    simplefindById,
    updateProfile,
    updateToken,
    checkToken,
    reportEmployeeGatePass,
    reportEmployeesGatePass,
    viewEmployeeProfile,
    findEmployeeandGatePass,
    findEmployeeandGatePassTimeIn,
    getEmployeesByDepartment,
    getEmployeesByTeam,
    findApprover
  });

  async function updateProfile(id, password) {
    return await employees.update(
      { password },
      { individualHooks: true, where: { id } }
    );
  }

  async function simplefindById(employee_id) {
    return await employees.findByPk(employee_id, {
      attributes: [
        'id',
        'first_name',
        'last_name',
        'middle_name'
      ],
      include: [{
        model: roles,
        attributes: ['id', 'name', 'status'],
        where: { status: 'Active' },
      }]
    })
  }

  async function findById(employee_id) {
    return await employees.findByPk(employee_id, {
      attributes: [
        'id',
        'first_name',
        'last_name',
        'middle_name',
        'password',
        'email',
        'status',
        'payroll_group',
        'img_profile_path',
        'is_offsite'
      ],
      include: [
        {
          model: positions,
          attributes: ['id', 'name', 'team_id', 'status'],
          status: { [Op.notLike]: 'Inactive' },
          include: [
            {
              model: teams,
              status: { [Op.notLike]: 'Inactive' },
              attributes: ['id', 'name', 'department_id', 'status', 'employee_id'],
              include: [
                {
                  model: departments,
                  status: { [Op.notLike]: 'Inactive' },
                  attributes: ['id', 'name', 'status', 'employee_id']
                }
              ]
            }
          ]
        },
        {
          model: roles,
          attributes: ['id', 'name', 'status'],
          where: { status: 'Active' },
          include: [
            {
              model: access_rights,
              attributes: ['id', 'action_id', 'role_id', 'status'],
              where: { status: 'Active' },
              include: [
                {
                  model: actions,
                  attributes: ['id', 'module_id', 'description', 'status'],
                  where: { status: 'Active' },
                  include: [
                    {
                      model: modules,
                      attributes: ['id', 'description', 'status'],
                      where: { status: 'Active' }
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    });
  }

  async function findApprover(employee_id) {
    return await employees.findByPk(employee_id, {
      attributes: [
        'id',
        'first_name',
        'last_name',
        'middle_name',
        'email',
        'img_profile_path'
      ],
      include: [
        {
          model: positions,
          attributes: ['id', 'name', 'team_id', 'status'],
          status: { [Op.notLike]: 'Inactive' },
          include: [
            {
              model: teams,
              status: { [Op.notLike]: 'Inactive' },
              attributes: ['id', 'name', 'department_id', 'status', 'employee_id'],
              include: [
                {
                  model: departments,
                  status: { [Op.notLike]: 'Inactive' },
                  attributes: ['id', 'name', 'status', 'employee_id']
                }
              ]
            }
          ]
        },
      ]
    });
  }

  async function findEmployeeandGatePass(id, dateToday) {
    return await employees.findByPk(id, {
      include: [{
        model: gate_pass,
        where: {
          status: { [Op.or]: ['Approved', 'Time Out', 'Pending', 'Pre-Approved'] },
          request_date: dateToday
        },
        include: [{
          model: request_types
        }],
      }]
    });
  }

  async function findEmployeeandGatePassTimeIn(id, dateToday) {
    return await employees.findByPk(id, {
      include: [{
        model: gate_pass,
        where: {
          status: 'Time In',
          request_date: dateToday
        },
        include: [{
          model: request_types
        }],
      }],
      order: [['gate_passes', 'updatedAt', 'desc']]
    });
  }

  async function updateToken(token, employee_id) {
    return await employees.update(
      { token, updated_by: employee_id },
      { individualHooks: true, where: { id: employee_id } }
    );
  }

  async function checkToken(employee_id, token) {
    return await employees.findOne({ where: { id: employee_id, token } });
  }

  async function viewEmployeeProfile(id) {
    return await employees.findOne({
      attributes: [
        'id',
        'first_name',
        'middle_name',
        'last_name',
        'img_profile_path'
      ],
      where: { id },
      include: [
        {
          model: positions,
          include: [{ model: teams, include: [{ model: departments }] }]
        },
        {
          model: gate_pass,
          include: [{ model: request_types }]
        }
      ]
    });
  }

  //***************** EMPLOYEES ********************* */

  //employee report(based on employee id)(included elapsed time)
  async function reportEmployeeGatePass(id) {
    return employees.findAll({
      where: { id: id },
      attributes: ['id', 'first_name', 'last_name', 'middle_name'],
      include: [
        { model: departments },
        { model: teams },
        { model: positions },
        { model: roles }
      ]
    });
  }
  //employees report(PG)(included elapsed time)
  async function reportEmployeesGatePass() {
    return employees.findAll({
      attributes: [
        'id',
        'first_name',
        'last_name',
        'middle_name',
        'img_profile_path'
      ],
      include: [
        {
          model: gate_pass,
          include: [{ model: gate_pass_status_logs }, { model: request_types }]
        },
        { model: positions, attributes: ['id', 'name'] },
        { model: teams, attributes: ['id', 'name'] },
        { model: departments, attributes: ['id', 'name'] }
      ]
    });
  }

  async function getEmployeesByDepartment(department_id, employee_id) {
    return await employees.findAll({
      include: [{
        model: positions,
        required: true,
        include: [{
          model: teams,
          required: true,
          include: [{
            model: departments,
            required: true,
            where: { id: department_id }
          }]
        }]
      }],
      where: { id: { [Op.ne]: employee_id }, status: 'Active' }

    })
  }

  async function getEmployeesByTeam(team_id, employee_id) {
    return await employees.findAll({
      include: [{
        model: positions,
        required: true,
        include: [{
          model: teams,
          required: true,
          where: { id: team_id },
          include: [{
            model: departments,
            required: true
          }]
        }]
      }],
      where: { id: { [Op.ne]: employee_id }, status: 'Active' }
    })
  }
  //***************** EMPLOYEES ********************* */
};


