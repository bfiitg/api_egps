const admin = require('./admin');
const models = require('../../database/models');

const admin_db = admin({ models });

module.exports = admin_db;
