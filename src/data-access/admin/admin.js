module.exports = function adminDB({ models }) {
  const {
    employees,
    roles,
    positions,
    teams,
    departments,
    request_types,
    access_rights,
    actions,
    modules,
    activity_logs,
    team_heads,
    department_heads
  } = models;


  const Op = models.Sequelize.Op;

  const moment = require('moment');

  return Object.freeze({
    //Employees
    listAllEmployees,
    findOneEmployeeById,
    addEmployee,
    updateEmployee,
    bulkaddEmployee,
    updateEmpRole,

    //Roles
    listAllRoles,
    findRoleById,
    findRoleByName,
    addRole,
    updateRole,

    //Positions
    listAllPositions,
    findPositionByName,
    addPosition,
    updatePosition,
    findPositionById,

    //Teams
    listAllTeams,
    findTeamByName,
    findTeamHead,
    addTeam,
    updateTeamHead,
    updateTeam,
    findTeamById,
    findTeamByNameOnly,

    // Teamheads
    findByTeamID,
    findIfHeadExists,
    findIfDHeadExists,
    addTeamHead,
    deleteTeamHead,
    getTeamid,

    addDepartmentHead,
    deleteDepartmentHead,
    getDepartmentid,

    //Departments
    listAllDepartments,
    findDeparmentByName,
    addDepartment,
    updateDepartmentHead,
    updateDepartment,
    findDepartmentHead,
    findDepartmentById,

    //Request Types
    listAllRequestTypes,
    findOneRequestTypeByName,
    addRequestType,
    updateRequestType,
    findRequestTypeById,

    //Admin
    listAllModules,
    findModuleByDescription,
    findModuleById,
    findAccessRightById,
    findAccessRightsByActionInRole,
    listAllAccessRightsbyRole,
    findActionByDescription,
    findActionById,
    listAllActions,
    listAllActionsActiveOnly,
    listAllAccessRights,
    addModule,
    addAction,
    addAccessRights,
    updateModule,
    updateAction,
    updateAccessRights,
    listAllActivityLogs,
    removeActionsfromRole,
    addActionstoRole
  });

  //Employees
  async function listAllEmployees() {
    return employees.findAll({
      include: [
        { model: roles },
        {
          model: positions,
          include: [{ model: teams, include: [{ model: departments }] }]
        }
      ]
    });
  }

  async function findOneEmployeeById(id) {
    return employees.findOne({
      attributes: [
        'id',
        'role_id',
        'position_id',
        'first_name',
        'last_name',
        'middle_name',
        'img_profile_path',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
        'status'
      ],
      where: { id },
      include: [
        { model: roles },
        {
          model: positions,
          include: [{ model: teams, include: [{ model: departments }] }]
        }
      ]
    });
  }

  async function addEmployee(employee_info) {
    return employees.create({ ...employee_info });
  }

  async function bulkaddEmployee(employee_info) {
    return employees.bulkCreate(employee_info);
  }


  async function updateEmployee({ id, ...update_info } = {}) {
    // const empInfo = await employees.findOne({
    //   attributes: ['position_id'],
    //   where: { id }
    // })

    // const position = empInfo.position_id;
    // const posInfo = await positions.findOne({
    //   attributes: ['team_id'],
    //   where: { id: position }
    // })
    // const team_id = posInfo.team_id;

    // if (update_info.role_id === 7) {
    //   await team_heads.destroy({ where: { employee_id: id, team_id } })
    //   await team_heads.create({ team_id, employee_id: id })
    // } else {
    //   await team_heads.destroy({ where: { employee_id: id, team_id } })
    // }

    return await employees.update(
      { ...update_info, token: '' },
      { individualHooks: true, where: { id } }
    );
  }

  //Roles
  async function listAllRoles() {
    return await roles.findAll({});
  }

  async function findRoleById(id) {
    return await roles.findOne({ where: { id } });
  }

  async function findRoleByName(name) {
    return await roles.findOne({ where: { name } });
  }

  async function addRole(role_info) {
    return await roles.create({ ...role_info });
  }

  async function updateRole({ id, ...update_info } = {}) {
    return await roles.update(
      {
        ...update_info
      },
      { individualHooks: true, where: { id } }
    );
  }

  async function updateEmpRole(employee_id, role_id) {
    return await employees.update({ role_id, token: '' }, { where: { id: employee_id } })
  }

  //Positions
  async function listAllPositions() {
    return await positions.findAll({
      include: [{ model: teams, include: [{ model: departments }] }]
    });
  }

  async function findPositionById(id) {
    return await positions.findOne({ where: { id } });
  }

  async function findPositionByName(team_id, name) {
    return await positions.findOne({ where: { team_id, name } });
  }

  async function addPosition(position_info) {
    return await positions.create({ ...position_info });
  }

  async function updatePosition({ id, ...update_info } = {}) {
    return await positions.update(
      { ...update_info },
      { individualHooks: true, where: { id } }
    );
  }

  // Teamheads

  async function getTeamid(employee_id) {
    return await team_heads.findAll({ where: { employee_id } })
  }

  async function getDepartmentid(employee_id) {
    return await department_heads.findAll({ where: { employee_id } })
  }

  // Get all teamheads based on team id
  async function findByTeamID(team_id) {
    return await team_heads.findAll({ where: { team_id } })
  }

  // Check if Team Head already exists
  async function findIfHeadExists(team_id, employee_id) {
    return await team_heads.findAll({ where: { team_id, employee_id } })
  }

  async function findIfDHeadExists(department_id, employee_id) {
    return await department_heads.findAll({ where: { department_id, employee_id } })
  }

  // Add team head 
  async function addTeamHead(team_id, employee_id) {
    await team_heads.create({ team_id, employee_id });

    return { message: "team approver added." }
  }

  // Add department head
  async function addDepartmentHead(department_id, employee_id) {
    await department_heads.create({ department_id, employee_id });

    return { message: "department approver added." }
  }

  // Delete teamhead/proxy approver
  async function deleteTeamHead(team_id, employee_id) {
    await team_heads.destroy({ where: { team_id, employee_id } });

    return { message: 'deleted' };
  }

  // Delete departmenthead/proxy approver
  async function deleteDepartmentHead(department_id, employee_id) {
    await department_heads.destroy({ where: { department_id, employee_id } });

    return { message: 'deleted' };
  }

  //Teams
  async function listAllTeams() {
    const teamsList = await teams.findAll({
      include: [
        { model: departments, attributes: ['id', 'name', 'status'] },
        {
          model: positions,
          attributes: ['id', 'name', 'status']
        },
        {
          model: employees,
          attributes: ['id', 'first_name', 'last_name', 'middle_name']
        }
      ]
    });

    return teamsList;
  }

  async function findTeamById(id) {
    return await teams.findOne({ where: { id } });
  }

  async function findTeamHead(employee_id) {
    return await teams.findOne({
      where: { employee_id }
    });
  }

  async function findTeamByName(department_id, name) {
    return await teams.findOne({ where: { department_id, name } });
  }

  async function findTeamByNameOnly(name) {
    return await teams.findOne({ where: { name } });
  }

  async function addTeam(team_info) {
    return await teams.create({ ...team_info });
  }

  async function updateTeamHead(team_id, employee_id) {
    return await teams.update({ employee_id }, { where: { id: team_id } });
  }

  async function updateTeam({ id, ...update_info } = {}) {
    return await teams.update(
      { ...update_info },
      { individualHooks: true, where: { id } }
    );
  }

  //Departments
  async function listAllDepartments() {
    return await departments.findAll({
      include: [
        {
          model: employees,
          attributes: ['id', 'first_name', 'last_name', 'middle_name']
        },
        { model: teams, include: { model: positions } }
      ]
    });
  }

  async function findDepartmentHead(employee_id) {
    return await departments.findOne({
      where: { employee_id }
    });
  }

  async function findDepartmentById(id) {
    return await departments.findOne({ where: { id } });
  }

  async function findDeparmentByName(name, employee_id) {
    if (!employee_id) {
      return await departments.findOne({ where: { name } });
    } else {
      return await departments.findOne({ where: { name, employee_id } });
    }
  }

  async function addDepartment(department_info) {
    return await departments.create({ ...department_info });
  }

  // async function updateDepartmentHead(department_id, employee_id) {
  //   return await departments.update(
  //     { employee_id },
  //     { where: { id: department_id } }
  //   );
  // }

  async function updateDepartmentHead(department, employees) {
    const result = await department.setEmployees(employees);
    return result

  }

  async function updateDepartment({ id, ...update_info } = {}) {
    return await departments.update(
      { ...update_info },
      { individualHooks: true, where: { id } }
    );
  }

  //Request types
  async function listAllRequestTypes() {
    return await request_types.findAll({});
  }

  async function findRequestTypeById(id) {
    return await request_types.findOne({ where: { id } });
  }

  async function findOneRequestTypeByName(name) {
    return await request_types.findOne({ where: { name } });
  }

  async function addRequestType(request_type_info) {
    return await request_types.create({ ...request_type_info });
  }

  async function updateRequestType({ id, ...update_info }) {
    return await request_types.update(
      { ...update_info },
      { individualHooks: true, where: { id } }
    );
  }

  //Modules
  async function listAllModules() {
    return await modules.findAll({});
  }

  async function findModuleByDescription(description) {
    return await modules.findOne({ where: { description } });
  }

  async function findModuleById(id) {
    return await modules.findOne({ where: { id } });
  }

  async function addModule(module_info) {
    return await modules.create({ ...module_info });
  }

  async function updateModule({ id, ...update_info }) {
    return await modules.update(
      { ...update_info },
      { individualHooks: true, where: { id } }
    );
  }

  //Actions
  async function listAllActions() {
    return await actions.findAll({
      include: [{ model: modules }]
    });
  }

  async function listAllActionsActiveOnly() {
    // return await actions.findAll({
    //   where: {
    //     status: 'Active'
    //   },
    //   include: [{
    //     model: modules,
    //     where: {
    //       status: 'Active'
    //     }
    //   }]
    // });
    return await modules.findAll({
      where: {
        status: 'Active'
      },
      include: [{
        model: actions,
        where: {
          status: 'Active'
        }
      }],
      order: [
        ['id', 'ASC']
      ]
    });
  }



  async function findActionById(id) {
    return await actions.findOne({ where: { id } });
  }



  async function findActionByDescription(module_id, description) {
    return await actions.findOne({ where: { module_id, description } });
  }

  async function addAction(action_info) {
    return await actions.create({ ...action_info });
  }

  async function updateAction({ id, ...update_info }) {
    return await actions.update(
      { ...update_info },
      { individualHooks: true, where: { id } }
    );
  }

  //Access rights
  async function listAllAccessRights() {
    return await access_rights.findAll({
      include: [{ model: roles }, { model: actions }]
    });
  }

  async function findAccessRightsByActionInRole(role_id, action_id) {
    return await access_rights.findOne({ where: { role_id, action_id } });
  }

  async function removeActionsfromRole(role_id) {
    return await access_rights.destroy({ where: { role_id } });
  }

  async function addActionstoRole(data) {
    return await access_rights.bulkCreate(data);
  }

  async function listAllAccessRightsbyRole(role_id) {
    return await access_rights.findAll({
      where: {
        role_id,
        status: 'Active'
      }
    })
  }

  async function findAccessRightById(id) {
    return await access_rights.findOne({ where: { id } });
  }

  async function addAccessRights(access_rights_info) {
    return await access_rights.create(access_rights_info);
  }

  async function updateAccessRights({ id, ...update_info }) {
    return await access_rights.update(
      { ...update_info },
      { individualHooks: true, where: { id } }
    );
  }

  //Logs
  async function listAllActivityLogs(dateRange) {
    if (dateRange.request_date_from === dateRange.request_date_to) {
      return await activity_logs.findAll({
        where: {
          created_at: {
            [Op.between]: [
              `${dateRange.request_date_from} 00:00:00`,
              `${dateRange.request_date_to} 24:00:00`
            ]
          },
        },
        include: {
          model: employees,
          attributes: ['id', 'first_name', 'last_name', 'middle_name'],

        },
        order: [
          ['created_at', 'DESC']
        ]
      })
    } else {
      return await activity_logs.findAll({
        where: {
          created_at: {
            [Op.between]: [
              `${dateRange.request_date_from} 00:00:00`,
              `${dateRange.request_date_to} 24:00:00`
            ]
          }

        },
        include: {
          model: employees,
          attributes: ['id', 'first_name', 'last_name', 'middle_name'],

        },
        order: [
          ['created_at', 'DESC']
        ]
      })
    }
  }

};
