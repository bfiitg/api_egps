module.exports = function gatepassDb({ models }) {
  const {
    gate_pass,
    request_types,
    employees,
    gate_pass_status_logs,
    positions,
    teams,
    departments,
    roles
  } = models;

  const Op = models.Sequelize.Op;
  const moment = require('moment');

  return Object.freeze({
    checkExistingLunchOutGatePass,
    postRequest,
    updateRequest,
    updateGatePassTime,
    getPersonalGatePass,
    inGatePass,
    listPendingGatePass,
    listPendingGatePassNotNullTeamId,
    listPendingGatePassByDepartment,
    listApprovedGatePassByDepartment,
    listApprovedGatePassNotNullTeamId,
    listDisapprovedGatePassByDepartment,
    listDisapprovedGatePassNotNullTeamId,
    listApprovedGatePass,
    listTimeinGatePass,
    listTimeoutGatePass,
    outGatePass,
    searchEmployeeGatePass,
    getEmployeesGatePassByDepartment,
    listScheduleOut,
    listDisapprovedGatePass,
    getEmployeesGatePassByTeam,
    listPersonalOutGatePass,
    listLunchOutGatePass,
    listLunchOutGatePassOffsite,
    getEmployeesVerifiedGatePass,
    getAllEmployeesGatePass,
    findGatePassById,
    getNotifications,
    getallNotifications,
    updateNotifications,
    checkConflict,
    checkExistingGatePassByTime,
    checkExistingGatePass,
    findGatePassWithEmployeeInformation
  });

  async function getNotifications(employee_id) {
    return gate_pass.findAll({
      include: [{ model: request_types }],
      where: {
        employee_id,
        is_read: false,
        status: {
          [Op.or]: ['Approved', 'Disapproved', 'Pre-Approved']
        }
      }
    });
  }

  async function getallNotifications() {
    return gate_pass.findAll({
      include: [{ model: request_types }],
      where: {
        is_read: false,
        status: {
          [Op.or]: ['Approved', 'Disapproved', 'Pre-Approved']
        }
      }
    });
  }

  async function updateNotifications(employee_id) {
    return gate_pass.update(
      {
        is_read: true
      },
      {
        individualHooks: true,
        where: {
          employee_id,
          is_read: false,
          status: {
            [Op.or]: ['Approved', 'Disapproved', 'Pre-Approved']
          }
        }
      }
    );
  }

  async function checkExistingLunchOutGatePass(
    employee_id,
    request_type_id,
    request_date
  ) {
    return gate_pass.findOne({
      where: {
        employee_id,
        request_type_id: request_type_id,
        request_date,
        status: {
          [Op.and]: [
            { [Op.notLike]: 'Disapproved' },
            { [Op.notLike]: 'Cancelled' }
          ]
        }
      }
    });
  }

  async function checkExistingGatePass(employee_id, request_date) {
    return gate_pass.findOne({
      where: {
        employee_id,
        request_date,
        status: {
          [Op.and]: [
            { [Op.notLike]: 'Time In' },
            { [Op.notLike]: 'Verified' },
            { [Op.notLike]: 'Disapproved' },
            { [Op.notLike]: 'Cancelled' },
            { [Op.notLike]: 'Processed' }
          ]
        }
      }
    });
  }

  async function checkConflict({
    employee_id,
    request_type_id,
    request_date
  } = {}) {
    return gate_pass.findOne({
      where: {
        employee_id,
        request_date,
        status: 'Time Out'
      },
      include: [{ model: request_types, where: { has_time_in: false } }]
    });
  }

  async function checkExistingGatePassByTime(
    employee_id,
    request_time_in,
    request_time_out,
    request_date
  ) {
    return gate_pass.findOne({
      where: {
        employee_id,
        request_date,
        request_time_out: { [Op.between]: [request_time_out, request_time_in] },
        request_time_in: { [Op.between]: [request_time_out, request_time_in] },
        status: {
          [Op.and]: [
            { [Op.notLike]: 'Time In' },
            { [Op.notLike]: 'Verified' },
            { [Op.notLike]: 'Disapproved' },
            { [Op.notLike]: 'Cancelled' },
            { [Op.notLike]: 'Processed' }
          ]
        }
      }
    });
  }

  async function postRequest(request_info) {
    request_info.created_by = request_info.employee_id;
    return gate_pass.create({ ...request_info });
  }

  async function updateRequest(id, status, updated_by, note, approval_time) {
    return gate_pass.update(
      { status, updated_by, note: note ? note : null, approval_time, is_read: false },
      { individualHooks: true, where: { id } }
    );
  }

  async function updateGatePassTime(
    id,
    actual_time_out,
    actual_time_in,
    updated_by,
    request_type_id,
    status,
    approval_time
  ) {
    return gate_pass.update(
      { actual_time_out, actual_time_in, updated_by, request_type_id, status, approval_time },
      { individualHooks: true, where: { id } }
    );
  }

  async function getPersonalGatePass(employee_id, dateRange) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    if (
      !dateRange ||
      (!dateRange.request_date_from && !dateRange.request_date_to)
    ) {
      return await gate_pass.findAll({
        where: {
          employee_id,
          request_date: dateToday,
          status: {
            [Op.or]: [
              'Time In',
              'Time Out',
              'Cancelled',
              'Verified',
              'Disapproved',
              'Processed'
            ]
          }
        },
        include: [
          { model: gate_pass_status_logs, include: { model: employees } },
          { model: request_types }
        ]
      });
    } else {
      return await gate_pass.findAll({
        where: {
          employee_id,
          request_date: {
            [Op.between]: [
              dateRange.request_date_from,
              dateRange.request_date_to
            ]
          },
          status: {
            [Op.or]: [
              'Time In',
              'Time Out',
              'Cancelled',
              'Verified',
              'Disapproved',
              'Processed'
            ]
          }
        },
        include: [
          { model: gate_pass_status_logs, include: { model: employees } },
          { model: request_types }
        ]
      });
    }
  }

  //***************** SECURITY ********************* */
  //Time in
  async function inGatePass(id, status, actual_time_in, updated_by) {
    return gate_pass.update(
      {
        actual_time_in: actual_time_in,
        status: status,
        updated_by
      },
      { individualHooks: true, where: { id: id } }
    );
  }
  //Time out
  async function outGatePass(id, status, actual_time_out, updated_by) {
    return gate_pass.update(
      {
        actual_time_out: actual_time_out,
        status: status,
        updated_by
      },
      { individualHooks: true, where: { id: id } }
    );
  }
  //Find Gate Pass by id
  async function findGatePassById(id) {
    return gate_pass.findOne({
      where: { id },
      include: [{ model: request_types }]
    });
  }

  async function findGatePassWithEmployeeInformation(id) {
    return gate_pass.findOne({
      where: { id },
      include: [
        { model: request_types },
        { model: employees }]
    });
  }

  //view approved and time in gate pass
  async function listTimeinGatePass() {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    return gate_pass.findAll({
      //where gate_pass status=Time In
      where: { status: 'Time In', request_date: dateToday },
      include: [
        {
          model: employees,
          attributes: [
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'img_profile_path'
          ],
          include: [
            {
              model: positions,
              attributes: ['id', 'name'],
              include: [
                {
                  model: teams,
                  attributes: ['id', 'name'],
                  include: [{ model: departments, attributes: ['id', 'name'] }]
                }
              ]
            }
          ]
        },
        { model: request_types }
      ]
    });
  }
  //view approved, time out and time in gate pass
  async function listTimeoutGatePass() {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    return gate_pass.findAll({
      //where gate_pass status=Time Out
      where: { status: 'Time Out', request_date: dateToday },
      include: [
        {
          model: employees,

          attributes: [
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'img_profile_path'
          ],
          include: [
            {
              model: positions,
              attributes: ['id', 'name'],
              include: [
                {
                  model: teams,
                  attributes: ['id', 'name'],
                  include: [{ model: departments, attributes: ['id', 'name'] }]
                }
              ]
            }
          ]
        },
        { model: request_types, where: { has_time_in: true } }
      ]
    });
  }
  //search employee id
  async function searchEmployeeGatePass(id) {
    return gate_pass.findAll({
      //where gate_pass status=Approved
      where: {
        [Op.or]: [{ status: 'Approved' }, [{ status: 'Time Out' }]]
      },
      include: [
        {
          model: employees,
          attributes: [
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'img_profile_path'
          ],
          where: { id: id }
        },
        { model: request_types }
      ]
    });
  }
  //***************** SECURITY ********************* */

  //***************** GATE PASS ********************* */
  //view pending gate pass
  async function listPendingGatePass(id, dateRange) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    //list all pending gate passes between date range provided
    if (!dateRange.request_date_from && !dateRange.request_date_to) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'payroll_group'
        ],
        where: { id: { [Op.not]: id } },
        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          },
          {
            model: gate_pass,
            where: {
              status: { [Op.or]: ['Pending', 'Pre-Approved'] },
              request_date: dateToday
            },
            include: [{

              model: request_types,

            }, { model: gate_pass_status_logs, include: { model: employees } }]
          }
        ]
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'payroll_group'
        ],
        where: { id: { [Op.not]: id } },
        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          },
          {
            model: gate_pass,
            where: {
              status: { [Op.or]: ['Pending', 'Pre-Approved'] },
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [{ model: request_types }, { model: gate_pass_status_logs, include: { model: employees } }]
          }
        ]
      });
    }
  }
  async function listPendingGatePassByDepartment(id, dateRange) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    //list all pending gate passes between date range provided
    if (!dateRange.request_date_from && !dateRange.request_date_to) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'position_id'
        ],
        where: { id: { [Op.not]: id } },
        include: [
          {
            model: gate_pass,
            where: {
              status: 'Pending',
              request_date: dateToday
            },
            include: [{ model: request_types }]
          },
          {
            model: positions,
            attributes: ['id', 'name', 'team_id'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [
                  {
                    model: departments,
                    attributes: ['id', 'name']
                  }
                ]
              }
            ]
          }
        ]
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'position_id'
        ],
        where: { id: { [Op.not]: id } },
        include: [
          {
            model: gate_pass,
            where: {
              status: 'Pending',
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [{ model: request_types }]
          },
          {
            model: positions,
            attributes: ['id', 'name', 'team_id'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [
                  {
                    model: departments,
                    attributes: ['id', 'name']
                  }
                ]
              }
            ]
          }
        ]
      });
    }
  }
  async function listPendingGatePassNotNullTeamId(employee_id, dateRange) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    //list all pending gate passes between date range provided
    if (!dateRange.request_date_from && !dateRange.request_date_to) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'position_id',
          'role_id',
          'payroll_group'
        ],

        include: [
          {
            model: gate_pass,
            where: {
              status: 'Pending',
              request_date: dateToday
            },
            include: [{ model: request_types }]
          },
          {
            model: positions,
            attributes: ['id', 'name', 'team_id'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          }, {

            model: roles

          }
        ],

        where: {
          position_id: { [Op.not]: null },
          id: { [Op.not]: employee_id }
        }
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'position_id',
          'role_id',
          'payroll_group'
        ],

        where: {
          position_id: { [Op.not]: null },
          id: { [Op.not]: employee_id }
        },
        include: [
          {
            model: gate_pass,
            where: {
              status: 'Pending',
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [{ model: request_types }]
          },
          {
            model: positions,
            attributes: ['id', 'name', 'team_id'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          },
          {

            model: roles

          }
        ],

      });
    }
  }

  //view approved gate pass
  async function listApprovedGatePass(id, dateRange) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    //list all pending gate passes between date range provided
    if (
      !dateRange ||
      (!dateRange.request_date_from && !dateRange.request_date_to)
    ) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path'
        ],

        where: { id: { [Op.not]: id } },

        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          },
          {
            model: gate_pass,
            where: {
              status: 'Approved',
              request_date: dateToday
            },
            include: [
              { model: request_types },
              {
                model: gate_pass_status_logs,
                include: { model: employees }
              }
            ]
          }
        ]
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path'
        ],
        where: { id: { [Op.not]: id } },
        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          },
          {
            model: gate_pass,
            where: {
              status: 'Approved',
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [
              { model: request_types },
              {
                model: gate_pass_status_logs,
                include: { model: employees }
              }
            ]
          }
        ]
      });
    }
  }
  async function listApprovedGatePassByDepartment(id, dateRange) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    if (!dateRange.request_date_from && !dateRange.request_date_to) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'position_id'
        ],
        where: { id: { [Op.not]: id } },
        include: [
          {
            model: gate_pass,
            where: {
              status: { [Op.or]: ['Approved', 'Pre-Approved'] },
              request_date: dateToday
            },
            include: [
              { model: request_types },
              {
                model: gate_pass_status_logs,
                include: { model: employees }
              }
            ]
          },
          {
            model: positions,
            attributes: ['id', 'name', 'team_id'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [
                  {
                    model: departments,
                    attributes: ['id', 'name']
                  }
                ]
              }
            ]
          }
        ]
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'position_id'
        ],
        where: { id: { [Op.not]: id } },
        include: [
          {
            model: gate_pass,
            where: {
              status: { [Op.or]: ['Approved', 'Pre-Approved'] },
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [
              { model: request_types },
              {
                model: gate_pass_status_logs,
                include: { model: employees }
              }
            ]
          },
          {
            model: positions,
            attributes: ['id', 'name', 'team_id'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [
                  {
                    model: departments,
                    attributes: ['id', 'name']
                  }
                ]
              }
            ]
          }
        ]
      });
    }
  }
  async function listApprovedGatePassNotNullTeamId(employee_id, dateRange) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    //list all pending gate passes between date range provided
    if (!dateRange.request_date_from && !dateRange.request_date_to) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'position_id',
          'role_id',
          'payroll_group'
        ],
        where: {
          position_id: { [Op.not]: null },
          id: { [Op.not]: employee_id }
        },
        include: [
          {
            model: gate_pass,
            where: {
              status: { [Op.or]: ['Approved', 'Pre-Approved'] },
              request_date: dateToday
            },
            include: [
              { model: request_types },
              {
                model: gate_pass_status_logs,
                include: { model: employees }
              }
            ]
          },
          {
            model: positions,
            attributes: ['id', 'name', 'team_id'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          },
          {
            model: roles
          }
        ]
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'position_id',
          'role_id',
          'payroll_group'
        ],
        where: {
          position_id: { [Op.not]: null },
          id: { [Op.not]: employee_id }
        },
        include: [
          {
            model: gate_pass,
            where: {
              status: { [Op.or]: ['Approved', 'Pre-Approved'] },
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [
              { model: request_types },
              {
                model: gate_pass_status_logs,
                include: { model: employees }
              }
            ]
          },
          {
            model: positions,
            attributes: ['id', 'name', 'team_id'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          },
          {
            model: roles
          }
        ]
      });
    }
  }

  //view disapproved gate pass
  async function listDisapprovedGatePass(id, dateRange) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    //list all pending gate passes between date range provided
    if (
      !dateRange ||
      (!dateRange.request_date_from && !dateRange.request_date_to)
    ) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path'
        ],
        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          },
          {
            model: gate_pass,
            where: {
              status: 'Disapproved',
              request_date: dateToday,
              employee_id: { [Op.not]: id }
            },
            include: [
              { model: request_types },
              {
                model: gate_pass_status_logs,
                include: { model: employees }
              }
            ]
          }
        ]
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path'
        ],
        where: { id: { [Op.not]: id } },
        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          },
          {
            model: gate_pass,
            where: {
              status: 'Disapproved',
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [
              { model: request_types },
              {
                model: gate_pass_status_logs,
                include: { model: employees }
              }
            ]
          }
        ]
      });
    }
  }
  async function listDisapprovedGatePassByDepartment(id, dateRange) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    if (!dateRange.request_date_from && !dateRange.request_date_to) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'position_id'
        ],
        where: { id: { [Op.not]: id } },
        include: [
          {
            model: gate_pass,
            where: {
              status: 'Disapproved',
              request_date: dateToday
            },
            include: [
              { model: request_types },
              {
                model: gate_pass_status_logs,
                include: { model: employees }
              }
            ]
          },
          {
            model: positions,
            attributes: ['id', 'name', 'team_id'],

            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [
                  {
                    model: departments,
                    attributes: ['id', 'name']
                  }
                ]
              }
            ]
          }
        ]
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'position_id'
        ],
        where: { id: { [Op.not]: id } },
        include: [
          {
            model: gate_pass,
            where: {
              status: 'Disapproved',
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [
              { model: request_types },
              {
                model: gate_pass_status_logs,
                include: { model: employees }
              }
            ]
          },
          {
            model: positions,
            attributes: ['id', 'name', 'team_id'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [
                  {
                    model: departments,
                    attributes: ['id', 'name']
                  }
                ]
              }
            ]
          }
        ]
      });
    }
  }
  async function listDisapprovedGatePassNotNullTeamId(employee_id, dateRange) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    //list all pending gate passes between date range provided
    if (!dateRange.request_date_from && !dateRange.request_date_to) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'position_id',
          'role_id',
          'payroll_group'
        ],
        where: {
          position_id: { [Op.not]: null },
          id: { [Op.not]: employee_id }
        },
        include: [
          {
            model: gate_pass,
            where: {
              status: 'Disapproved',
              request_date: dateToday
            },
            include: [
              { model: request_types },
              {
                model: gate_pass_status_logs,
                include: { model: employees }
              }
            ]
          },
          {
            model: positions,
            attributes: ['id', 'name', 'team_id'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          },
          {
            model: roles
          }
        ]
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'position_id',
          'role_id',
          'payroll_group'
        ],
        where: {
          position_id: { [Op.not]: null },
          id: { [Op.not]: employee_id }
        },
        include: [
          {
            model: gate_pass,
            where: {
              status: 'Disapproved',
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [
              { model: request_types },
              {
                model: gate_pass_status_logs,
                include: { model: employees }
              }
            ]
          },
          {
            model: positions,
            attributes: ['id', 'name', 'team_id'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          },
          {
            model: roles
          }
        ]
      });
    }
  }

  //***************** GATE PASS ********************* */

  //***************** DEPARTMENT HEAD ********************* */

  //view employee gate pass under department head supervision(Request)
  async function getEmployeesGatePassByDepartment(
    employee_id,
    DepartmentID,
    dateRange
  ) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    //list all pending gate passes between date range provided
    if (
      !dateRange ||
      (!dateRange.request_date_from && !dateRange.request_date_to)
    ) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'payroll_group',
          'is_offsite'
        ],
        where: {
          position_id: { [Op.not]: null },
          id: { [Op.not]: employee_id }
        },
        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [
                  {
                    model: departments,
                    attributes: ['id', 'name'],
                    // where: { id: DepartmentID }
                  }
                ]
              }
            ]
          },
          {
            model: gate_pass,
            where: {
              status: {
                [Op.or]: ['Approved', 'Time Out', 'Time In', 'Verified', 'Processed']
              },
              request_date: dateToday
            },
            include: [
              { model: gate_pass_status_logs, include: { model: employees } },
              { model: request_types }
            ]
          }
        ]
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'payroll_group',
          'is_offsite'
        ],
        where: {
          position_id: { [Op.not]: null },
          id: { [Op.not]: employee_id }
        },
        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [
                  {
                    model: departments,
                    attributes: ['id', 'name'],
                    // where: { id: DepartmentID }
                  }
                ]
              }
            ]
          },
          {
            model: gate_pass,
            where: {
              status: {
                [Op.or]: ['Approved', 'Time Out', 'Time In', 'Verified', 'Processed']
              },
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [
              { model: gate_pass_status_logs, include: { model: employees } },
              { model: request_types }
            ]
          }
        ]
      });
    }
  }
  //***************** DEPARTMENT HEAD ********************* */

  //***************** TEAM HEAD ********************* */

  //view employee gate pass under team head supervision(Request)
  async function getEmployeesGatePassByTeam(employee_id, TeamID, dateRange) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    //list all pending gate passes between date range provided
    if (
      !dateRange ||
      (!dateRange.request_date_from && !dateRange.request_date_to)
    ) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'payroll_group',
          'role_id',
          'is_offsite'
        ],
        where: {
          position_id: { [Op.not]: null },
          id: { [Op.not]: employee_id }
        },
        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                // where: { id: TeamID }
              }
            ]
          },
          {
            model: gate_pass,
            where: {
              status: {
                [Op.or]: ['Approved', 'Time Out', 'Time In', 'Verified', 'Processed']
              },
              request_date: dateToday
            },
            include: [
              { model: gate_pass_status_logs, include: { model: employees } },
              { model: request_types }
            ]
          },
          {
            model: roles
          }
        ]
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'payroll_group',
          'role_id',
          'is_offsite'
        ],
        where: {
          position_id: { [Op.not]: null },
          id: { [Op.not]: employee_id }
        },
        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                // where: { id: TeamID }
              }
            ]
          },
          {
            model: gate_pass,
            where: {
              status: {
                [Op.or]: ['Approved', 'Time Out', 'Time In', 'Verified', 'Processed']
              },
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [
              { model: gate_pass_status_logs, include: { model: employees } },
              { model: request_types }
            ]
          },
          {
            model: roles
          }
        ]
      });
    }
  }
  //***************** TEAM HEAD ********************* */

  //***************** SCHEDULE OUT ********************* */
  //view schedule out
  async function listScheduleOut(id) {
    return gate_pass.findAll({
      //where gate_pass status=Pending
      where: {
        status: { [Op.or]: ['Pending', 'Approved', 'Pre-Approved', 'Time Out'] }
      },
      order: [['updated_at', 'DESC']],
      include: [
        { model: employees, where: { id: id } },
        { model: employees, as: 'updater' },
        { model: request_types }
      ]
    });
  }
  //***************** SCHEDULE OUT ********************* */

  //***************** ACCOUNTING ********************* */
  async function getEmployeesVerifiedGatePass(dateRange) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    //list all pending gate passes between date range provided
    if (
      !dateRange ||
      (!dateRange.request_date_from && !dateRange.request_date_to)
    ) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path'
        ],
        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: {
                  model: departments,
                  attributes: ['id', 'name']
                }
              }
            ]
          },
          {
            model: gate_pass,
            where: {
              status: { [Op.or]: ['Verified', 'Processed'] },
              request_date: dateToday
            },
            include: [
              { model: gate_pass_status_logs, include: { model: employees } },
              { model: request_types }
            ]
          }
        ]
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path'
        ],
        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: {
                  model: departments,
                  attributes: ['id', 'name']
                }
              }
            ]
          },
          {
            model: gate_pass,
            where: {

              status: { [Op.or]: ['Verified', 'Processed'] },
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [
              { model: gate_pass_status_logs, include: { model: employees } },
              { model: request_types }
            ]
          }
        ]
      });
    }
  }
  //***************** ACCOUNTING ********************* */

  //***************** PEOPLE GROUP ********************* */
  async function listPersonalOutGatePass(dateRange, requestTypeId) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    //list all pending gate passes between date range provided

    if (
      !dateRange ||
      (!dateRange.request_date_from && !dateRange.request_date_to)
    ) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'is_offsite'
        ],
        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          },
          {
            model: gate_pass,
            where: {
              status: { [Op.or]: ['Approved', 'Time Out', 'Time In'] },

              request_type_id: requestTypeId,
              request_date: dateToday
            },
            include: [{ model: request_types }]
          }
        ]
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'is_offsite'
        ],
        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          },
          {
            model: gate_pass,
            where: {
              status: { [Op.or]: ['Approved', 'Time Out', 'Time In'] },
              request_type_id: requestTypeId,
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [{ model: request_types }]
          }
        ]
      });
    }
  }

  async function listLunchOutGatePassOffsite(dateRange, requestTypeId) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    //list all pending gate passes between date range provided

    if (
      !dateRange ||
      (!dateRange.request_date_from && !dateRange.request_date_to)
    ) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'is_offsite'
        ],
        where: { payroll_group: 'office_payroll', is_offsite: true },
        include: [
          {
            model: roles
          },
          {
            model: gate_pass,
            where: {
              status: { [Op.or]: ['Approved', 'Time Out', 'Time In'] },
              request_type_id: requestTypeId,
              request_date: dateToday
            },
            include: [{ model: request_types }]
          },
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          }
        ]
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'is_offsite'
        ],
        where: { payroll_group: 'office_payroll', is_offsite: true },
        include: [
          {
            model: roles
          },
          {
            model: gate_pass,
            where: {
              status: { [Op.or]: ['Approved', 'Time Out', 'Time In'] },
              request_type_id: requestTypeId,
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [{ model: request_types }]
          },
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          }
        ]
      });
    }
  }

  async function listLunchOutGatePass(dateRange, requestTypeId) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    //list all pending gate passes between date range provided

    if (
      !dateRange ||
      (!dateRange.request_date_from && !dateRange.request_date_to)
    ) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'is_offsite'
        ],
        where: { payroll_group: 'manager_payroll' },
        include: [
          {
            model: roles
          },
          {
            model: gate_pass,
            where: {
              status: { [Op.or]: ['Approved', 'Time Out', 'Time In'] },
              request_type_id: requestTypeId,
              request_date: dateToday
            },
            include: [{ model: request_types }]
          },
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          }
        ]
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'is_offsite'
        ],
        where: { payroll_group: 'manager_payroll' },
        include: [
          {
            model: roles
          },
          {
            model: gate_pass,
            where: {
              status: { [Op.or]: ['Approved', 'Time Out', 'Time In'] },
              request_type_id: requestTypeId,
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [{ model: request_types }]
          },
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [{ model: departments, attributes: ['id', 'name'] }]
              }
            ]
          }
        ]
      });
    }
  }

  async function getAllEmployeesGatePass(employee_id, dateRange) {
    const dateToday = moment().tz('Asia/Manila').format('YYYY-MM-DD');
    //list all pending gate passes between date range provided
    if (!dateRange.request_date_from && !dateRange.request_date_to) {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'payroll_group',
          'is_offsite'
        ],
        where: { id: { [Op.not]: employee_id } },
        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [
                  {
                    model: departments,
                    attributes: ['id', 'name']
                  }
                ]
              }
            ]
          },
          {
            model: gate_pass,
            where: {
              status: {
                [Op.or]: ['Approved', 'Time Out', 'Time In', 'Verified', 'Processed']
              },
              request_date: dateToday
            },
            include: [
              { model: gate_pass_status_logs, include: { model: employees } },
              { model: request_types }
            ]
          }
        ]
      });
    } else {
      return employees.findAll({
        attributes: [
          'id',
          'first_name',
          'last_name',
          'middle_name',
          'img_profile_path',
          'payroll_group',
          'is_offsite'
        ],
        where: { id: { [Op.not]: employee_id } },
        include: [
          {
            model: positions,
            attributes: ['id', 'name'],
            include: [
              {
                model: teams,
                attributes: ['id', 'name'],
                include: [
                  {
                    model: departments,
                    attributes: ['id', 'name']
                  }
                ]
              }
            ]
          },
          {
            model: gate_pass,
            where: {
              status: {
                [Op.or]: ['Approved', 'Time Out', 'Time In', 'Verified', 'Processed']
              },
              request_date: {
                [Op.between]: [
                  dateRange.request_date_from,
                  dateRange.request_date_to
                ]
              }
            },
            include: [
              { model: gate_pass_status_logs, include: { model: employees } },
              { model: request_types }
            ]
          }
        ]
      });
    }
  }
  //***************** PEOPLE GROUP ********************* */
};
