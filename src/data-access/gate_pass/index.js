const gatepass = require('./gate_pass');
const models = require('../../database/models');

const gatepassDb = gatepass({ models });

module.exports = gatepassDb;
