const roles = require('./roles_db');
const models = require('../../database/models');

const rolesDb = roles({ models });

module.exports = rolesDb;
