module.exports = function rolesDb({ models }) {
  const roles = models.roles;

  return Object.freeze({ findOneRoleById });

  async function findOneRoleById(id) {
    return roles.findOne({ where: { id } });
  }
};
