const teams = require('./teams');
const models = require('../../database/models');

const teamsDB = teams({ models });

module.exports = teamsDB;
