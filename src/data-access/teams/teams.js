module.exports = function teamsDB({ models }) {
  const {
    teams,
    employees,
    gate_pass,
    gate_pass_status_logs,
    request_types,
    team_heads
  } = models;
  const Op = models.Sequelize.Op;

  return Object.freeze({
    getTeamid,
    getTeams,
    getTeamHeadByDepartment,
    getTeamHeadByDepartmentPending,
    getTeamHeadByDepartmentApproved,
    getTeamHeadByDepartmentDisapproved,
    getCoApprovers
  });

  async function getTeamid(employee_id) {
    return teams.findOne({ where: { employee_id } });
  }

  async function getCoApprovers(teamId){
    return team_heads.findAll({
      where: {
        team_id: teamId
      }
    })
  }

  async function getTeams() {
    return teams.findAll();
  }

  async function getTeamHeadByDepartment(department_id, dateRange) {
    return teams.findAll({
      where: { department_id },
      include: {
        model: employees,
        include: {
          model: gate_pass,
          where: {
            status: { [Op.or]: ['Time Out', 'Time In', 'Verified'] },
            request_date: {
              [Op.between]: [
                dateRange.request_date_from,
                dateRange.request_date_to
              ]
            }
          },
          include: [
            { model: gate_pass_status_logs, include: { model: employees } },
            { model: request_types }
          ]
        }
      }
    });
  }

  async function getTeamHeadByDepartmentPending(department_id, dateRange) {
    return teams.findAll({
      where: { department_id },
      include: {
        model: employees,
        include: {
          model: gate_pass,
          where: {
            status: 'Pending',
            request_date: {
              [Op.between]: [
                dateRange.request_date_from,
                dateRange.request_date_to
              ]
            }
          },
          include: [
            { model: gate_pass_status_logs, include: { model: employees } },
            { model: request_types }
          ]
        }
      }
    });
  }
  async function getTeamHeadByDepartmentApproved(department_id, dateRange) {
    return teams.findAll({
      where: { department_id },
      include: {
        model: employees,
        include: {
          model: gate_pass,
          where: {
            status: 'Approved',
            request_date: {
              [Op.between]: [
                dateRange.request_date_from,
                dateRange.request_date_to
              ]
            }
          },
          include: [
            { model: gate_pass_status_logs, include: { model: employees } },
            { model: request_types }
          ]
        }
      }
    });
  }
  async function getTeamHeadByDepartmentDisapproved(department_id, dateRange) {
    return teams.findAll({
      where: { department_id },
      include: {
        model: employees,
        include: {
          model: gate_pass,
          where: {
            status: 'Disapproved',
            request_date: {
              [Op.between]: [
                dateRange.request_date_from,
                dateRange.request_date_to
              ]
            }
          },
          include: [
            { model: gate_pass_status_logs, include: { model: employees } },
            { model: request_types }
          ]
        }
      }
    });
  }
};
