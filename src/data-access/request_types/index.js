const requestTypes = require('./request_types');
const models = require('../../database/models');

const requestTypesDb = requestTypes({ models });

module.exports = requestTypesDb;
