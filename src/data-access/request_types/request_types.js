module.exports = function requestTypesDb({ models }) {
  const request_types = models.request_types;

  return Object.freeze({
    listRequestTypes,
    findOneRequestType,
    findOneRequestTypeByName
  });

  async function listRequestTypes() {
    return request_types.findAll();
  }

  async function findOneRequestType(id) {
    return request_types.findOne({
      attributes: ['id', 'name', 'has_time_in'],
      where: { id }
    });
  }

  async function findOneRequestTypeByName(name) {
    return request_types.findOne({
      attributes: ['id', 'name', 'has_time_in'],
      where: { name }
    });
  }
};
