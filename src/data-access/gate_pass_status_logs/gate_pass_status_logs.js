module.exports = function statusLogs({ models }) {
  const gate_pass_status_logs = models.gate_pass_status_logs;
  return Object.freeze({ insertLog, logGatePassRequest, findlogbyid});

  async function insertLog(log_info) {
    return gate_pass_status_logs.create({ ...log_info });
  }

  //Time-in/Time-out Gate Pass
  async function logGatePassRequest(gate_pass_id, status, employee_id) {
    return gate_pass_status_logs.create({
      gate_pass_id,
      status,
      employee_id,
      date_time: new Date()
    });
  }

  async function findlogbyid(gate_pass_id, status){
    return gate_pass_status_logs.findOne({
     where: {
      gate_pass_id: gate_pass_id,
      status: status
     }
    });
  }

};
