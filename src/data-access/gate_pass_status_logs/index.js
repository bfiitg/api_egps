const gatepassLogs = require('./gate_pass_status_logs');
const models = require('../../database/models');

const gatePassStatusLogsDb = gatepassLogs({ models });

module.exports = gatePassStatusLogsDb;
