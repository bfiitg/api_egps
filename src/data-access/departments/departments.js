module.exports = function departmentsDB({ models }) {
  const departments = models.departments
  const department_heads = models.department_heads

  return Object.freeze({ getDepartmentid, getDepartments, getDepartmentsbyEmployeeId, getEmployeesbyDepartmentId,
    getCoApprovers  });

  async function getDepartmentid(employee_id) {
    return departments.findOne({ where: { employee_id } });
  }

  async function getCoApprovers(departmentId){
    return department_heads.findAll({
      where: {
        department_id: departmentId
      }
    })
  }


  async function getDepartmentsbyEmployeeId(employee_id) {
    return department_heads.findAll({ where: { employee_id } })
  }

  async function getEmployeesbyDepartmentId(department_id) {
    return department_heads.findAll({ 
      where: { department_id } 
    })
  }

  async function getDepartments() {
    return departments.findAll();
  }
};
