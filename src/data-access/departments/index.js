const departments = require('./departments');
const models = require('../../database/models');

const departmentsDB = departments({ models });

module.exports = departmentsDB;
