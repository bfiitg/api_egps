require('dotenv').config();

const nodemailer = require('nodemailer');

async function sendEmail(mailData) {
  let transporter = await nodemailer.createTransport({
    host: 'smtp.gmail.com',
    secure: false,
    service: 'gmail',
    auth: {
      user: 'egpsegpsegps@gmail.com',
      pass: 'egpsegpsegps2019'
    }
  });

  let mailOptions = {
    from: 'egpsegpsegps@gmail.com',
    to: mailData.recipient,
    subject: mailData.subject,
    text: mailData.text
  };

  return await transporter.sendMail(mailOptions, function(err, data) {
    if (err) {
      console.log('Error occurs: ', err);
      return err;
    } else {
      console.log('Email sent!');
      return;
    }
  });
}

module.exports = { sendEmail };
