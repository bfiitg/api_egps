'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('teams', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      department_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'departments',
            schema: 'public'
          },
          key: 'id'
        }
      },
      name: {
        type: Sequelize.STRING
      },
      // employee_id: {
      //   type: Sequelize.INTEGER,
      //   references: {
      //     model: {
      //       tableName: 'employees',
      //       schema: 'public'
      //     },
      //     key: 'id'
      //   }
      // },
      status: {
        type: Sequelize.STRING,
        defaultValue: 'Active'
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('teams');
  }
};
