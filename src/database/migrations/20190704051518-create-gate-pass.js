'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('gate_pass', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      employee_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'employees',
            schema: 'public'
          },
          key: 'id'
        }
      },
      request_type_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'request_types',
            schema: 'public'
          },
          key: 'id'
        }
      },
      reason: {
        type: Sequelize.STRING
      },
      note: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.STRING,
        defaultValue: 'Pending'
      },
      request_time_out: {
        type: Sequelize.TIME
      },
      request_time_in: {
        type: Sequelize.TIME
      },
      actual_time_out: {
        type: Sequelize.TIME
      },
      actual_time_in: {
        type: Sequelize.TIME
      },
      request_date: {
        allowNull: false,
        type: Sequelize.DATEONLY
      },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('gate_pass');
  }
};
