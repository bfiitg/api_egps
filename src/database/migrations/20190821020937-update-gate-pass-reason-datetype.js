'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('gate_pass', 'reason', {
      type: Sequelize.STRING(1000)
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('gate_pass', 'reason', {
      type: Sequelize.STRING
    });
  }
};
