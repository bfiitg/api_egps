'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('gate_pass_status_logs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      gate_pass_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'gate_pass',
            schema: 'public'
          },
          key: 'id'
        }
      },
      status: {
        type: Sequelize.STRING
      },
      employee_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'employees',
            schema: 'public'
          },
          key: 'id'
        }
      },
      time_created: {
        type: Sequelize.TIME
      },
      date_created: {
        type: Sequelize.DATEONLY
      },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('gate_pass_status_logs');
  }
};
