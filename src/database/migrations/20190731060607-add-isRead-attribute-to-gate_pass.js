'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.addColumn('gate_pass', 'is_read', {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    
    });
  },

 
  down: (queryInterface, Sequelize) => {
 
    return queryInterface.removeColumn('gate_pass', 'is_read');
  }
};

