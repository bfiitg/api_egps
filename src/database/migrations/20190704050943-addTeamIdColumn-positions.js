'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('positions', 'team_id', {
      type: Sequelize.INTEGER,
      references: {
        model: {
          tableName: 'teams',
          schema: 'public'
        },
        key: 'id'
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('positions', 'team_id');
  }
};
