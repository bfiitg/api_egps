'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.addColumn('employees', 'email', {
      type: Sequelize.STRING
    
    });
  },

 
  down: (queryInterface, Sequelize) => {
 
    return queryInterface.removeColumn('employees', 'email');
  }
};

