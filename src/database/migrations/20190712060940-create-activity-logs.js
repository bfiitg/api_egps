'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('activity_logs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      employee_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: {
            tableName: 'employees',
            schema: 'public'
          },
          key: 'id'
        }
      },
      action_type: {
        type: Sequelize.STRING,
        allowNull: false
      },
      table: {
        type: Sequelize.STRING,
        allowNull: false
      },
      prev_values: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      new_values: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('activity_logs');
  }
};
