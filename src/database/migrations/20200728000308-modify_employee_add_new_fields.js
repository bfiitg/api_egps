module.exports = {
  up(queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'employees', // table name
        'is_offsite', // new field name
        {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false
        },
      )
    ]);
  },

  down(queryInterface, Sequelize) {
    // logic for reverting the changes
    return Promise.all([
      queryInterface.removeColumn('employees', 'is_offsite')
    ]);
  },
};