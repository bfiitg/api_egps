'use strict';
module.exports = (sequelize, DataTypes) => {
  const teams = sequelize.define(
    'teams',
    {
      department_id: DataTypes.INTEGER,
      name: DataTypes.STRING,
      //employee_id: DataTypes.INTEGER,
      status: DataTypes.STRING,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER
    },
    {
      hooks: {
        afterCreate: (instance, options) => {
          saveAuditLog('create', instance, options);
        },
        afterUpdate: (instance, options) => {
          saveAuditLog('update', instance, options);
        }
      }
    }
  );
  //Function for logs
  function saveAuditLog(action, model) {
    const activityLog = require('../models').activity_logs;
    activityLog.create({
      employee_id: !model.dataValues.updated_by
        ? model.dataValues.created_by
        : model.dataValues.updated_by,

      action_type: action,
      table: !model._modelOptions
        ? model.model.toString()
        : model._modelOptions.name.plural,
      prev_values: JSON.stringify(model._previousDataValues),
      new_values: JSON.stringify(model.dataValues)
    });
  }

  teams.associate = function (models) {
    // associations can be defined here
    teams.hasMany(models.positions);
    teams.belongsTo(models.departments);
    // teams.belongsTo(models.employees);
    // teams.hasMany(models.team_heads);

    teams.belongsToMany(models.employees, {
      through: "team_heads",
      foreignKey: "team_id"
    })
  };
  return teams;
};
