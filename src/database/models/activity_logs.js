'use strict';
module.exports = (sequelize, DataTypes) => {
  const activity_logs = sequelize.define(
    'activity_logs',
    {
      employee_id: DataTypes.INTEGER,
      action_type: DataTypes.STRING,
      table: DataTypes.STRING,
      prev_values: DataTypes.TEXT,
      new_values: DataTypes.TEXT
    },
    { updatedAt: false }
  );
  activity_logs.associate = function(models) {
    // associations can be defined here
    activity_logs.belongsTo(models.employees);
  };
  return activity_logs;
};
