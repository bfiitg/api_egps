'use strict';
module.exports = (sequelize, DataTypes) => {
  const access_rights = sequelize.define(
    'access_rights',
    {
      action_id: DataTypes.INTEGER,
      role_id: DataTypes.INTEGER,
      status: DataTypes.STRING,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER
    },
    {
      hooks: {
        afterCreate: (instance, options) => {
          saveAuditLog('create', instance, options);
        },
        afterUpdate: (instance, options) => {
          saveAuditLog('update', instance, options);
        }
      }
    }
  );

  //Function for logs
  function saveAuditLog(action, model, options) {
    const activityLog = require('../models').activity_logs;
    activityLog.create({
           employee_id: !model.dataValues.updated_by
        ? model.dataValues.created_by
        : model.dataValues.updated_by,


      action_type: action,
      table: !model._modelOptions
        ? model.model.toString()
        : model._modelOptions.name.plural,
      prev_values: JSON.stringify(model._previousDataValues),
      new_values: JSON.stringify(model.dataValues)
    });
  }

  access_rights.associate = function(models) {
    // associations can be defined here
    access_rights.belongsTo(models.actions);
    access_rights.belongsTo(models.roles);
  };
  return access_rights;
};
