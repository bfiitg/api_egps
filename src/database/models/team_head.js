'use strict';
module.exports = (sequelize, DataTypes) => {
  const team_head = sequelize.define('team_head', {
    team_id: DataTypes.INTEGER,
    employee_id: DataTypes.INTEGER
  }, {});
  team_head.associate = function(models) {
    // associations can be defined here
  };
  return team_head;
};