'use strict';
module.exports = (sequelize, DataTypes) => {
  const team_heads = sequelize.define('team_heads', {
    team_id: DataTypes.INTEGER,
    employee_id: DataTypes.INTEGER
  }, {
    hooks: {
      afterCreate: (instance, options) => {
        saveAuditLog('create', instance, options);
      },
      afterUpdate: (instance, options) => {
        saveAuditLog('update', instance, options);
      }
    }
  }
  );
  //Function for logs
  function saveAuditLog(action, model) {
    const activityLog = require('../models').activity_logs;
    activityLog.create({
      employee_id: !model.dataValues.updated_by
        ? model.dataValues.created_by
        : model.dataValues.updated_by,

      action_type: action,
      table: !model._modelOptions
        ? model.model.toString()
        : model._modelOptions.name.plural,
      prev_values: JSON.stringify(model._previousDataValues),
      new_values: JSON.stringify(model.dataValues)
    });
  }

  team_heads.associate = function (models) {
    // associations can be defined here
    team_heads.hasOne(models.employees, {
      foreignKey: 'id'
    })
  };
  return team_heads;
};