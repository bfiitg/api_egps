'use strict';
const today = new Date();

const date =
  today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
const time = today.getHours() + ':' + today.getMinutes() + ':00';
module.exports = (sequelize, DataTypes) => {
  const gate_pass = sequelize.define(
    'gate_pass',
    {
      employee_id: DataTypes.INTEGER,
      request_type_id: DataTypes.INTEGER,
      reason: DataTypes.STRING,
      note: DataTypes.STRING,
      status: DataTypes.STRING,
      request_time_out: DataTypes.TIME,
      request_time_in: DataTypes.TIME,
      actual_time_out: DataTypes.TIME,
      actual_time_in: DataTypes.TIME,
      request_date: DataTypes.DATEONLY,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      is_read: DataTypes.BOOLEAN,
      approval_time: DataTypes.TIME
    },
    {
      freezeTableName: true,
      tableName: 'gate_pass',
      hooks: {
        afterCreate: (instance, options) => {
          saveAuditLog('create', instance, options);
        },
        afterUpdate: (instance, options) => {
          saveAuditLog('update', instance, options);
        }
      }
    }
  );
  //Create activity log
  function saveAuditLog(action, model, options) {
  

 
    const activityLog = require('../models').activity_logs;
    activityLog.create({
      employee_id: !model.dataValues.updated_by
        ? model.dataValues.created_by
        : model.dataValues.updated_by,

      action_type: action,
      table: !model._modelOptions
        ? model.model.toString()
        : model._modelOptions.name.plural,
      prev_values: JSON.stringify(model._previousDataValues),
      new_values: JSON.stringify(model.dataValues)
    });
  }
  //Create log every after create
  gate_pass.afterCreate((logs, options) => {
    const gate_pass_status_logs = require('../models').gate_pass_status_logs;
    gate_pass_status_logs.create({
      gate_pass_id: logs.id,
      status: logs.status,
      employee_id: null,
      time_created: time,
      date_created: new Date(date)
    });
  });

  gate_pass.associate = function(models) {
    // associations can be defined here
    gate_pass.belongsTo(models.employees, {foreignKey: 'employee_id'});
    gate_pass.belongsTo(models.employees,  { as:'updater',foreignKey: 'updated_by'});

    gate_pass.belongsTo(models.request_types);

    gate_pass.hasMany(models.gate_pass_status_logs);


  };
  return gate_pass;
};
