'use strict';

const today = new Date();
const date =
  today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
const time = today.getHours() + ':' + today.getMinutes() + ':00';
module.exports = (sequelize, DataTypes) => {
  const gate_pass_status_logs = sequelize.define(
    'gate_pass_status_logs',
    {
      gate_pass_id: DataTypes.INTEGER,
      status: DataTypes.STRING,
      employee_id: DataTypes.INTEGER,
      time_created: DataTypes.TIME,
      date_created: DataTypes.DATEONLY
    },
    { freezeTableName: true }
  );
  //Set value for time and date every log create
  gate_pass_status_logs.beforeCreate((log, options) => {
    log.time_created = time;
    log.date_created = new Date(date);
    return;
  });

  gate_pass_status_logs.associate = function(models) {
    // associations can be defined here
    gate_pass_status_logs.belongsTo(models.gate_pass);
    gate_pass_status_logs.belongsTo(models.employees);
  };
  return gate_pass_status_logs;
};
