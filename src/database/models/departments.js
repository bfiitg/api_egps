'use strict';
module.exports = (sequelize, DataTypes) => {
  const departments = sequelize.define(
    'departments',
    {
      //employee_id: DataTypes.INTEGER,
      name: DataTypes.STRING,
      status: DataTypes.STRING,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER
    },
    {
      hooks: {
        afterCreate: (instance, options) => {
          saveAuditLog('create', instance, options);
        },
        afterUpdate: (instance, options) => {
          saveAuditLog('update', instance, options);
        }
      }
    }
  );

  //Function for logs
  function saveAuditLog(action, model, options) {
    const activityLog = require('../models').activity_logs;
    activityLog.create({
      employee_id: !model.dataValues.updated_by
        ? model.dataValues.created_by
        : model.dataValues.updated_by,


      action_type: action,
      table: !model._modelOptions
        ? model.model.toString()
        : model._modelOptions.name.plural,
      prev_values: JSON.stringify(model._previousDataValues),
      new_values: JSON.stringify(model.dataValues)
    });
  }

  departments.associate = function (models) {
    // associations can be defined here
    departments.hasMany(models.teams);

    // departments.belongsTo(models.employees);

    departments.belongsToMany(models.employees, {
      through: 'department_heads',
      foreignKey: 'department_id'
    });

  };
  return departments;
};
