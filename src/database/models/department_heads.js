'use strict';
module.exports = (sequelize, DataTypes) => {
  const department_heads = sequelize.define('department_heads', {
    department_id: DataTypes.INTEGER,
    employee_id: DataTypes.INTEGER
  }, {});
  department_heads.associate = function (models) {
    // associations can be defined here
  };
  return department_heads;
};