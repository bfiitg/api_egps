'use strict';
module.exports = (sequelize, DataTypes) => {
  const positions = sequelize.define(
    'positions',
    {
      team_id: DataTypes.INTEGER,
      name: DataTypes.STRING,
      status: DataTypes.STRING,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER
    },
    {
      hooks: {
        afterCreate: (instance, options) => {
          saveAuditLog('create', instance, options);
        },
        afterUpdate: (instance, options) => {
          saveAuditLog('update', instance, options);
        }
      }
    }
  );
  //Function for logs
  function saveAuditLog(action, model) {
    const activityLog = require('../models').activity_logs;
    activityLog.create({
           employee_id: !model.dataValues.updated_by
        ? model.dataValues.created_by
        : model.dataValues.updated_by,


      action_type: action,
      table: !model._modelOptions
        ? model.model.toString()
        : model._modelOptions.name.plural,
      prev_values: JSON.stringify(model._previousDataValues),
      new_values: JSON.stringify(model.dataValues)
    });
  }

  positions.associate = function(models) {
    // associations can be defined here
    positions.hasMany(models.employees);
    positions.belongsTo(models.teams);
  };
  return positions;
};
