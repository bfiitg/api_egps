'use strict';
module.exports = (sequelize, DataTypes) => {
  const employees = sequelize.define(
    'employees',
    {
      role_id: DataTypes.INTEGER,
      position_id: DataTypes.INTEGER,
      first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
      middle_name: DataTypes.STRING,
      password: DataTypes.STRING,
      payroll_group: DataTypes.STRING,
      status: DataTypes.STRING,
      img_profile_path: DataTypes.STRING,
      token: DataTypes.STRING,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      email: DataTypes.STRING,
      is_offsite: DataTypes.BOOLEAN
    },
    {
      hooks: {
        afterCreate: (instance, options) => {
          saveAuditLog('create', instance, options);
        },
        afterUpdate: (instance, options) => {
          saveAuditLog('update', instance, options);
        }
      }
    }
  );

  //Function for logs
  function saveAuditLog(action, model, options) {
    const activityLog = require('../models').activity_logs;
    activityLog.create({
      employee_id: !model.dataValues.updated_by
        ? model.dataValues.created_by
        : model.dataValues.updated_by,

      action_type: action,
      table: !model._modelOptions
        ? model.model.toString()
        : model._modelOptions.name.plural,
      prev_values: JSON.stringify(model._previousDataValues),
      new_values: JSON.stringify(model.dataValues)
    });
  }

  employees.associate = function (models) {
    // associations can be defined here
    employees.belongsTo(models.roles);
    employees.belongsTo(models.positions);

    //Teams/Department Heads
    employees.hasOne(models.teams);
    // employees.hasOne(models.departments);
    // employees.hasOne(models.teams);
    employees.hasOne(models.departments);

    employees.hasOne(models.gate_pass_status_logs);

    //Gate pass
    employees.hasMany(models.gate_pass);

    //Logs
    employees.hasMany(models.activity_logs);

    employees.belongsToMany(models.departments, {
      through: 'department_heads',
      foreignKey: 'employee_id'
    });
    employees.belongsToMany(models.teams, {
      through: "team_heads",
      foreignKey: "employee_id"
    })

  };
  return employees;
};
