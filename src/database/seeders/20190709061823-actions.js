'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'actions',
      [
        {
          //id: 1
          module_id: 2,
          description: 'Request gate pass',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id :2
          module_id: 2,
          description: 'View schedule out',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 3
          module_id: 2,
          description: 'View approved gate pass',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 4
          module_id: 2,
          description: 'Mark time in',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 5
          module_id: 2,
          description: 'Mark time out',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 6
          module_id: 3,
          description: 'View requests',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 7
          module_id: 3,
          description: 'Approve request',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 8
          module_id: 3,
          description: 'Disapprove request',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 9
          module_id: 4,
          description: 'View gate pass report',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 10
          module_id: 4,
          description: 'View employees gate pass report',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 11
          module_id: 4,
          description: 'View employee profile',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 12
          module_id: 4,
          description: 'View verified gate pass',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 13
          module_id: 3,
          description: 'Verify personal out gate pass',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 14
          module_id: 4,
          description: 'View personal out gate pass',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 15
          module_id: 2,
          description: 'View time out gate pass',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 16
          module_id: 3,
          description: 'Cancel gate pass',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 17
          module_id: 3,
          description: 'Unverify personal out gate pass',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 18
          module_id: 4,
          description: 'Correct gate pass report',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 19
          module_id: 1,
          description: 'View modules',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 20
          module_id: 1,
          description: 'Add module',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 21
          module_id: 1,
          description: 'Edit module',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 22
          module_id: 1,
          description: 'View actions',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 23
          module_id: 1,
          description: 'Add action',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 24
          module_id: 1,
          description: 'Edit action',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 25
          module_id: 1,
          description: 'View roles',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 26
          module_id: 1,
          description: 'Add role',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 27
          module_id: 1,
          description: 'Edit role',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 28
          module_id: 1,
          description: 'View access rights',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 29
          module_id: 1,
          description: 'Add access right',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 30
          module_id: 1,
          description: 'Edit access right',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 31
          module_id: 1,
          description: 'View departments',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 32
          module_id: 1,
          description: 'Add department',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 33
          module_id: 1,
          description: 'Edit department',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 34
          module_id: 1,
          description: 'View teams',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 35
          module_id: 1,
          description: 'Add team',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 36
          module_id: 1,
          description: 'Edit team',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 37
          module_id: 1,
          description: 'View positions',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 38
          module_id: 1,
          description: 'Add position',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 39
          module_id: 1,
          description: 'Edit position',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 40
          module_id: 1,
          description: 'View employees',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 41
          module_id: 1,
          description: 'Add employee',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 42
          module_id: 1,
          description: 'Edit employee',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 43
          module_id: 1,
          description: 'Reset employee password',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 44
          module_id: 1,
          description: 'View gate pass request types',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 45
          module_id: 1,
          description: 'Add gate pass request type',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 46
          module_id: 1,
          description: 'Edit gate pass request type',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 47
          module_id: 1,
          description: 'View activity logs',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          //id: 48
          module_id: 2,
          description: 'Import gate pass',
          created_at: new Date(),
          updated_at: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('actions', null, {});
  }
};
