'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'positions',
      [
        {
          name: 'DEV OPS',
          team_id: 1,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          name: 'UX DESIGNER',
          team_id: 1,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          name: 'SOFTWARE QUALITY ASSURANCE',
          team_id: 1,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          name: 'IT OPERATIONS ENGINEER',
          team_id: 2,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          name: 'Systems Security Engineer - CCTV',
          team_id: 2,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          name: 'PEOPLE GROUP',
          team_id: 3,
          created_at: new Date(),
          updated_at: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('positions', null, {});
  }
};
