'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'request_types',
      [
        {
          name: 'Lunch Out',
          has_time_in: true,
          color: '#d14d21',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          name: 'Personal Out',
          has_time_in: true,
          color: '#32a852',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          name: 'Official Business',
          has_time_in: true,
          color: '#63605f',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          name: 'Half Day',
          has_time_in: false,
          color: '#e33d3d',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          name: 'Undertime',
          has_time_in: false,
          color: '#3d53e3',
          created_at: new Date(),
          updated_at: new Date(),
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('request_types', null, {});
  }
};
