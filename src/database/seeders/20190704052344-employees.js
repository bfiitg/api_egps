'use strict';
const encryption = require('../../encryption/encrypting');
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'employees',
      [
        {
          id: 154151100,
          role_id: 1,
          position_id: null,
          first_name: encryption.encrypt('TREVOR'),
          last_name: encryption.encrypt('GRAND'),
          middle_name: encryption.encrypt('BRETTY'),
          password: encryption.encrypt('admin'),
          email: encryption.encrypt('egpsegpsegps@gmail.com'),
          token: null,
          created_at: new Date(),
          updated_at: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('employees', null, {});
  }
};
