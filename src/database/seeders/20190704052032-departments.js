'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'departments',
      [
        {
          name: 'BFI - MIS',
          // employee_id: null,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          name: 'PEOPLE GROUP',
          // employee_id: null,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          name: 'MIS',
          // employee_id: null,
          created_at: new Date(),
          updated_at: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('departments', null, {});
  }
};
