'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'teams',
      [
        {
          department_id: 1,
          name: 'BFI SOFTWARE',
          // employee_id: null,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          department_id: 1,
          name: 'BFI TECHNICAL',
          // employee_id: null,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          department_id: 2,
          name: 'PEOPLE GROUP',
          // employee_id: null,
          created_at: new Date(),
          updated_at: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('teams', null, {});
  }
};
