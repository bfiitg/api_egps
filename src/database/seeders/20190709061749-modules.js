'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'modules',
      [
        {
          description: 'admin',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          description: 'Gate Pass',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          description: 'Request',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          description: 'Report',
          created_at: new Date(),
          updated_at: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('modules', null, {});
  }
};
