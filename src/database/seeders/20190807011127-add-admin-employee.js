'use strict';
const encryption = require('../../encryption/encrypting');
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'employees',
      [
        {
          id: 154151000,
          role_id: 1,
          position_id: null,
          first_name: encryption.encrypt('Lily'),
          last_name: encryption.encrypt('Grand'),
          middle_name: encryption.encrypt('Doofenshmirtz'),
          password: encryption.encrypt('admin'),
          email: encryption.encrypt('egpsegpsegps@gmail.com'),
          token: null,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 154151001,
          role_id: 1,
          position_id: null,
          first_name: encryption.encrypt('Kaila'),
          last_name: encryption.encrypt('Kaith'),
          middle_name: encryption.encrypt('Mianda'),
          password: encryption.encrypt('admin'),
          email: encryption.encrypt('egpsegpsegps@gmail.com'),
          token: null,
          created_at: new Date(),
          updated_at: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('employees', null, {});
  }
};
