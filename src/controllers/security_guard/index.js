//Verify token function
const verifyToken = require('../../token/').verifyToken;

//Moment
const moment = require('moment');

//use cases
const {
  listApprovedGatePass,
  listTimeInGatePass,
  listTimeOutGatePass,

  markTimeGatePass
} = require('../../use-cases/security_guard');

//Functions
const makeApprovedGatePass = require('./get_approved_requests');
const makeTimeInGatePass = require('./get_time_in_requests');
const makeTimeOutGatePass = require('./get_time_out_requests');

const makeServerTime = require('./get_server_time');
const makeTimeGatePass = require('./put_time')

const getApprovedGatePass = makeApprovedGatePass(
  { listApprovedGatePass },
  { verifyToken }
);
const getTimeInGatePass = makeTimeInGatePass(
  { listTimeInGatePass },
  { verifyToken }
);
const getTimeOutGatePass = makeTimeOutGatePass(
  { listTimeOutGatePass },
  { verifyToken }
);



const getServerTime = makeServerTime({ moment });

const putTimeGatePass = makeTimeGatePass(
  { markTimeGatePass },
  { verifyToken },
  { moment }
)

const securityGuardController = Object.freeze({
  getApprovedGatePass,
  getTimeInGatePass,
  getTimeOutGatePass,
 
  getServerTime,
  putTimeGatePass
});

module.exports = securityGuardController;
module.exports = {
  getApprovedGatePass,
  getTimeInGatePass,
  getTimeOutGatePass,
  getServerTime,
  putTimeGatePass
};
