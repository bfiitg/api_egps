module.exports = function makeGetServerTime({ moment }) {
  return async function getServerTime(httpRequest) {
    try {
      //Get server time
      const time = moment()
        .tz('Asia/Manila')
        .format();

    
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 200,
        body: { time }
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
