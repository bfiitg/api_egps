module.exports = function makeListTimeOutGatePass(
  { listTimeOutGatePass },
  { verifyToken }
) {
  return async function getTimeOutGatePass(httpRequest) {
    try {
      const token = httpRequest.token;
      const request_info = httpRequest.body;
      const modules = httpRequest.body.modules;

      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }
      if (!request_info.employee_id) {
        throw { status: 403, message: 'Employee id must be provided' };
      }
      if (isNaN(request_info.employee_id)) {
        throw { status: 403, message: 'Employee id must be a number' };
      }

      //Verify Token
      const tokenExist = await verifyToken(
        token,
        'secret',
        request_info.employee_id
      );
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      if (!modules) {
        throw new Error('Modules must be provided');
      }

      //Check report module in modules provided
      const gatepassModule = await modules.find(
        element => element.description === 'Gate Pass'
      );
      if (!gatepassModule) {
        throw {
          status: 401,
          message: 'Access Denied. Not authorized to access report module'
        };
      }
      if (gatepassModule.status === 'Inactive') {
        throw new Error('Request module is inactive');
      }

      //Check module actions
      const viewTimeOutGatePass = await gatepassModule.actions.find(
        action => action.description === 'View time out gate pass'
      );
      if (!viewTimeOutGatePass) {
        throw {
          status: 401,
          message:
            'Access Denied. Not authorized to access view time out gate pass'
        };
      }

      if (viewTimeOutGatePass.status === 'Inactive') {
        throw new Error('View time out gate pass is inactive');
      }

      const result = await listTimeOutGatePass();
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
