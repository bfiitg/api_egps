module.exports = function makeListApprovedGatePass(
  { listApprovedGatePass },
  { verifyToken }
) {
  return async function getApprovedGatePass(httpRequest) {
    try {
      const token = httpRequest.token;
      const request_info = httpRequest.body;
      const modules = httpRequest.body.modules;

      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }
      if (!request_info.employee_id) {
        throw { status: 403, message: 'Employee id must be provided' };
      }
      if (isNaN(request_info.employee_id)) {
        throw { status: 403, message: 'Employee id must be a number' };
      }

      //Verify Token
      const tokenExist = await verifyToken(
        token,
        'secret',
        request_info.employee_id
      );
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      if (!modules) {
        throw new Error('Modules must be provided');
      }

      //Check report module in modules provided
      const gatepassModule = await modules.find(
        element => element.description === 'Gate Pass'
      );
      if (!gatepassModule) {
        throw {
          status: 401,
          message: 'Access Denied. Not authorized to access report module'
        };
      }

      if (gatepassModule.status === 'Inactive') {
        throw new Error('Gate pass module is inactive');
      }
      //Check module actions
      const viewApprovedGatePass = await gatepassModule.actions.find(
        action => action.description === 'View approved gate pass'
      );
      if (!viewApprovedGatePass) {
        throw {
          status: 401,
          message:
            'Access Denied. Not authorized to access view approved gate pass'
        };
      }
      if (viewApprovedGatePass.status === 'Inactive') {
        throw new Error('View approved gate pass is inactive');
      }

      const result = await listApprovedGatePass(request_info.employee_id);

      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
