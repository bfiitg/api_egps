const verifyToken = require('../../token/').verifyToken;
const { verifyTokenUpload } = require('../../token');

//Importing function
const importEmployees = require('../../import/import_employees');

//Use cases
const {
  listEmployees,
  addEmployee,
  updateEmployee,
  addMultipleEmployee,
  editMultipleEmployee,
  editEmployeePassword,

  listRoles,
  addRole,
  updateRole,

  listPositions,
  addPosition,
  updatePosition,

  listTeams,
  addTeam,
  updateTeam,

  deleteTeamhead,
  addTeamhead,

  addProxy,
  deleteProxy,

  listDepartments,
  addDepartment,
  updateDepartment,

  listRequestTypes,
  addRequestType,
  updateRequestType,

  listModules,
  addModule,
  updateModule,

  listActions,
  addAction,
  updateAction,

  listAccessRights,
  addAccessRight,
  updateAccessRight,
  listAccessRightsbyRole,

  listActivityLogs
} = require('../../use-cases/admin');

//Employees
const makeListEmployees = require('./employees/get_employees');
const makeAddEmployee = require('./employees/post_employee');
const makeUpdateEmployee = require('./employees/put_employee');
const makeUploadFile = require('./employees/upload_file');
const makeImportEmployees = require('./employees/import_employees');
const makeEditEmployeesImport = require('./employees/edit_employees_import');
const makeResetEmployeePasswrod = require('./employees/edit_employee_password');

const getEmployees = makeListEmployees({ listEmployees }, { verifyToken });
const postEmployee = makeAddEmployee({ addEmployee }, { verifyToken });
const putEmployee = makeUpdateEmployee({ updateEmployee }, { verifyToken });
const postEmployeeFile = makeUploadFile({ verifyTokenUpload });
const postImportEmployees = makeImportEmployees(
  { importEmployees },
  { addMultipleEmployee }
);
const putImportEmployees = makeEditEmployeesImport(
  { importEmployees },
  { editMultipleEmployee }
);
const resetEmployeePassword = makeResetEmployeePasswrod(
  { editEmployeePassword },
  { verifyToken }
);

//Roles
const makeListRoles = require('./roles/get_roles');
const makeRole = require('./roles/post_role');
const makeUpdateRole = require('./roles/put_role');

const getRoles = makeListRoles({ listRoles }, { verifyToken });
const postRole = makeRole({ addRole }, { verifyToken });
const putRole = makeUpdateRole({ updateRole }, { verifyToken });

//Positions
const makeListPositions = require('./positions/get_positions');
const makePosition = require('./positions/post_position');
const makeUpdatePosition = require('./positions/put_role');

const getPositions = makeListPositions({ listPositions }, { verifyToken });
const postPosition = makePosition({ addPosition }, { verifyToken });
const putPosition = makeUpdatePosition({ updatePosition }, { verifyToken });

//Teams
const makeListTeams = require('./teams/get_teams');
const makeTeam = require('./teams/post_team');
const makeUpdateTeam = require('./teams/put_team');

const getTeams = makeListTeams({ listTeams }, { verifyToken });
const postTeam = makeTeam({ addTeam }, { verifyToken });
const putTeam = makeUpdateTeam({ updateTeam }, { verifyToken });

// Teamheads
const deleteTeamheads = require('./team_heads/delete_teamhead');
const addTeamheads = require('./team_heads/add_teamhead');
const deleteProxys = require('./proxy/delete_proxy');
const addProxys = require('./proxy/add_proxy');

const deleteTH = deleteTeamheads({ deleteTeamhead }, { verifyToken })
const deletePA = deleteProxys({ deleteProxy }, { verifyToken })
const addTH = addTeamheads({ addTeamhead }, { verifyToken })
const addPA = addProxys({ addProxy }, { verifyToken })

//Departments
const makeListDepartments = require('./departments/get_departments');
const makeDepartment = require('./departments/post_department');
const makeUpdateDepartment = require('./departments/put_department');

const getDepartments = makeListDepartments(
  { listDepartments },
  { verifyToken }
);
const postDepartment = makeDepartment({ addDepartment }, { verifyToken });
const putDepartment = makeUpdateDepartment(
  { updateDepartment },
  { verifyToken }
);

//Request types
const makeListRequestTypes = require('./request_types/get_request_types.js');
const makeRequestType = require('./request_types/post_request_type');
const makeUpdateRequestType = require('./request_types/put_request_type');

const getRequestTypes = makeListRequestTypes(
  { listRequestTypes },
  { verifyToken }
);
const postRequestType = makeRequestType({ addRequestType }, { verifyToken });
const putRequestType = makeUpdateRequestType(
  { updateRequestType },
  { verifyToken }
);

//Modules
const makeListModules = require('./modules/get_modules');
const makeModule = require('./modules/post_module');
const makeUpdateModule = require('./modules/put_module');

const getModules = makeListModules({ listModules }, { verifyToken });
const postModule = makeModule({ addModule }, { verifyToken });
const putModule = makeUpdateModule({ updateModule }, { verifyToken });

//Actions
const makeListActions = require('./actions/get_actions');
const makeAction = require('./actions/post_action');
const makeUpdateAction = require('./actions/put_action');

const getActions = makeListActions({ listActions }, { verifyToken });
const postAction = makeAction({ addAction }, { verifyToken });
const putAction = makeUpdateAction({ updateAction }, { verifyToken });

//Access right
const makeListAccessRights = require('./access_rights/get_access_rights');
const makeAccessRight = require('./access_rights/post_access_right');
const makeUpdateAccessRight = require('./access_rights/put_access_right');
const makeListAccessRightsByRole = require('./access_rights/get_access_rights_by_role')


const getAccessRights = makeListAccessRights(
  { listAccessRights },
  { verifyToken }
);
const postAccessRight = makeAccessRight({ addAccessRight }, { verifyToken });
const putAccessRight = makeUpdateAccessRight(
  { updateAccessRight },
  { verifyToken }
);

const getAccessRightsByRole = makeListAccessRightsByRole( { listAccessRightsbyRole },
  { verifyToken })



//Activity Logs
const makeListActivityLogs = require('./activity_logs/get_activity_logs');

const getActivityLogs = makeListActivityLogs(
  { listActivityLogs },
  { verifyToken }
);

const adminController = Object.freeze({
  getEmployees,
  postEmployee,
  putEmployee,
  postEmployeeFile,
  postImportEmployees,
  putImportEmployees,
  resetEmployeePassword,

  getRoles,
  postRole,
  putRole,

  getPositions,
  postPosition,
  putPosition,

  getTeams,
  postTeam,
  putTeam,

  deleteTH,
  addTH,

  deletePA,
  addPA,

  getDepartments,
  postDepartment,
  putDepartment,

  getRequestTypes,
  postRequestType,
  putRequestType,

  getModules,
  postModule,
  putModule,

  getActions,
  postAction,
  putAction,

  getAccessRights,
  postAccessRight,
  putAccessRight,
  getAccessRightsByRole,


  getActivityLogs
});

module.exports = adminController;
module.exports = {
  getEmployees,
  postEmployee,
  putEmployee,
  postEmployeeFile,
  postImportEmployees,
  putImportEmployees,
  resetEmployeePassword,

  getRoles,
  postRole,
  putRole,

  getPositions,
  postPosition,
  putPosition,

  getTeams,
  postTeam,
  putTeam,

  deleteTH,
  addTH,

  deletePA,
  addPA,

  getDepartments,
  postDepartment,
  putDepartment,

  getRequestTypes,
  postRequestType,
  putRequestType,

  getModules,
  postModule,
  putModule,

  getActions,
  postAction,
  putAction,

  getAccessRights,
  postAccessRight,
  putAccessRight,
  getAccessRightsByRole,
 

  getActivityLogs
};
