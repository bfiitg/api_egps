module.exports = function makeResetPassword(
  { editEmployeePassword },
  { verifyToken }
) {
  return async function resetEmployeePassword(httpRequest) {
    try {
      const token = httpRequest.token;
      const request_info = httpRequest.body;
      const modules = httpRequest.body.modules;

      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }
      if (!request_info.id) {
        throw { status: 403, message: 'Employee id must be provided' };
      }
      if (isNaN(request_info.id)) {
        throw { status: 403, message: 'Employee id must be a number' };
      }

      //Verify Token
      const tokenExist = await verifyToken(
        token,
        'secret',
        request_info.employee_id
      );
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      if (!modules) {
        throw new Error('Modules must be provided');
      }

      //Check if allowed to use admin module
      const adminModule = await modules.find(
        element => element.description.toLowerCase() === 'admin'
      );

      if (!adminModule) {
        throw {
          status: 401,
          message: 'Access denied. Not authorized to access Admin module'
        };
      }
      if (adminModule.status.toLowerCase() === 'inactive') {
        throw new Error('Admin module is inactive');
      }

      //Check if resest employee password exists in actions
      const resetPassword = await adminModule.actions.find(
        action => action.description.toLowerCase() === 'reset employee password'
      );

      if (!resetPassword) {
        throw {
          status: 401,
          message: 'Access denied. Not authorized to reset employee password'
        };
      }
      if (resetPassword.status.toLowerCase() === 'inactive') {
        throw new Error('Reset employee password is inactive');
      }

      const result = await editEmployeePassword({ ...request_info });
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
