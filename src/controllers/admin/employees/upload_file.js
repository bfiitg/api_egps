module.exports = function uploadEmployeeFile({ verifyTokenUpload }) {
  return async function uploadFile(httpRequest) {
    try {
      const token = httpRequest.token;
      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }

      //Verify Token
      const tokenExist = await verifyTokenUpload(token, 'secret');
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 200,
        body: httpRequest.files[0]
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
