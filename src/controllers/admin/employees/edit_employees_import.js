module.exports = function importEditEmployeesData(
  { importEmployees },
  { editMultipleEmployee }
) {
  return async function putEmployees(httpRequest) {
    try {
      const { path } = httpRequest.body;

      //Extract data from excel
      const employeesData = await importEmployees(path);

      const result = await editMultipleEmployee(employeesData);
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
