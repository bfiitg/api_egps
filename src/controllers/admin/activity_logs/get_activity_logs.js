module.exports = function fetchActivityLogs(
  { listActivityLogs },
  { verifyToken }
) {
  return async function getActivityLogs(httpRequest) {
    try {
      const token = httpRequest.token;
      const request_info = httpRequest.body;
      const modules = httpRequest.body.modules;
      const dateRange = {
        request_date_from: httpRequest.body.request_date_from,
        request_date_to: httpRequest.body.request_date_to
      };

   
      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }
      if (!request_info.employee_id) {
        throw { status: 403, message: 'Employee id must be provided' };
      }
      if (isNaN(request_info.employee_id)) {
        throw new Error('Employee id must be a number');
      }

      //Verify Token
      const tokenExist = await verifyToken(
        token,
        'secret',
        request_info.employee_id
      );
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      if (!modules) {
        throw new Error('Modules must be provided');
      }

      //Check if allowed to use admin module
      const adminModule = await modules.find(
        element => element.description.toLowerCase() === 'admin'
      );

      if (!adminModule) {
        throw {
          status: 401,
          message: 'Access denied. Not authorized to access Admin module'
        };
      }
      if (adminModule.status.toLowerCase() === 'inactive') {
        throw new Error('Admin module is inactive');
      }

      //Check if view activity logs exists in actions
      const viewAcitivitLogs = await adminModule.actions.find(
        action => action.description === 'View activity logs'
      );

      if (!viewAcitivitLogs) {
        throw {
          status: 401,
          message: 'Access denied. Not authorized to view activity logs'
        };
      }
      if (viewAcitivitLogs.status.toLowerCase() === 'inactive') {
        throw new Error('View activity logs is inactive');
      }

      const result = await listActivityLogs(dateRange);

      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
