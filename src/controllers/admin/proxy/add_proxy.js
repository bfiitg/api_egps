module.exports = function CON_addProxy({ addProxy }, { verifyToken }) {
    return async function addprox(httpRequest) {
        try {
            const token = httpRequest.token;
            const request_info = httpRequest.body.body;
            const modules = httpRequest.body.modules;

            if (!token) {
                throw { status: 403, message: 'Token must be provided' };
            }
            if (!request_info.employee_id) {
                throw { status: 403, message: 'Employee id must be provided' };
            }
            if (isNaN(request_info.employee_id)) {
                throw new Error('Employee id must be a number');
            }

            if (!request_info.position.team.id) {
                throw { status: 403, message: 'Team id must be provided' };
            }
            if (isNaN(request_info.position.team.id)) {
                throw new Error('Team id must be a number');
            }

            //Verify Token

            const tokenExist = await verifyToken(
                token,
                'secret',
                request_info.id
            );

            if (!tokenExist) {
                throw { status: 403, message: 'Forbidden' };
            }

            if (!modules) {
                throw new Error('Modules must be provided');
            }

            //Check if allowed to use head module
            // const headModule = await modules.find(
            //     element => element.description.toLowerCase() === 'th'
            // );

            // if (!headModule) {
            //     throw {
            //         status: 401,
            //         message: 'Access denied. Not authorized to access Admin module'
            //     };
            // }
            // if (headModule.status.toLowerCase() === 'inactive') {
            //     throw new Error('Admin module is inactive');
            // }

            // //Check if add proxy exists in actions
            // const addProxyer = await headModule.actions.find(
            //     action => action.description.toLowerCase() === 'add proxy'
            // );

            // if (!addProxyer) {
            //     throw {
            //         status: 401,
            //         message: 'Access denied. Not authorized to add proxys'
            //     };
            // }
            // if (addProxyer.status.toLowerCase() === 'inactive') {
            //     throw new Error('Remove proxy is inactive');
            // }

            const result = await addProxy(request_info);
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                status: 200,
                body: result
            };

        } catch (e) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                status: e.status ? e.status : 400,
                body: e.message
            };
        }
    }
}