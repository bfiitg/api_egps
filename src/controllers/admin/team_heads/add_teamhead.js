module.exports = function CON_addTeamhead({ addTeamhead }, { verifyToken }) {
    return async function addteamh(httpRequest) {
        try {
            const token = httpRequest.token;
            const request_info = httpRequest.body;
            const modules = httpRequest.body.modules;

            if (!token) {
                throw { status: 403, message: 'Token must be provided' };
            }
            if (!request_info.employee_id) {
                throw { status: 403, message: 'Employee id must be provided' };
            }
            if (isNaN(request_info.employee_id)) {
                throw new Error('Employee id must be a number');
            }

            if (!request_info.position.team.id) {
                throw { status: 403, message: 'Team id must be provided' };
            }
            if (isNaN(request_info.position.team.id)) {
                throw new Error('Team id must be a number');
            }

            //Verify Token

            const tokenExist = await verifyToken(
                token,
                'secret',
                request_info.id
            );
            if (!tokenExist) {
                throw { status: 403, message: 'Forbidden' };
            }

            if (!modules) {
                throw new Error('Modules must be provided');
            }

            //Check if allowed to use admin module
            const adminModule = await modules.find(
                element => element.description.toLowerCase() === 'admin'
            );

            if (!adminModule) {
                throw {
                    status: 401,
                    message: 'Access denied. Not authorized to access Admin module'
                };
            }
            if (adminModule.status.toLowerCase() === 'inactive') {
                throw new Error('Admin module is inactive');
            }

            //Check if add teamhead exists in actions
            const addTeamheader = await adminModule.actions.find(
                action => action.description.toLowerCase() === 'add teamhead'
            );

            if (!addTeamheader) {
                throw {
                    status: 401,
                    message: 'Access denied. Not authorized to add teamheads'
                };
            }
            if (addTeamheader.status.toLowerCase() === 'inactive') {
                throw new Error('Remove teamhead is inactive');
            }

            const result = await addTeamhead(request_info);
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                status: 200,
                body: result
            };

        } catch (e) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                status: e.status ? e.status : 400,
                body: e.message
            };
        }
    }
}