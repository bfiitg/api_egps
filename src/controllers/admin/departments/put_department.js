module.exports = function putDepartment({ updateDepartment }, { verifyToken }) {
  return async function editDepartment(httpRequest) {
    try {
      const token = httpRequest.token;
      const request_info = httpRequest.body;
      const modules = httpRequest.body.modules;

      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }
      if (!request_info.updated_by) {
        throw { status: 403, message: 'Updated by must be provided' };
      }
      if (isNaN(request_info.updated_by)) {
        throw new Error('Updated by must be a number');
      }

      //Verify Token
      const tokenExist = await verifyToken(
        token,
        'secret',
        request_info.updated_by
      );
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      if (!modules) {
        throw new Error('Modules must be provided');
      }

      //Check if allowed to use admin module
      const adminModule = await modules.find(
        element => element.description.toLowerCase() === 'admin'
      );

      if (!adminModule) {
        throw {
          status: 401,
          message: 'Access denied. Not authorized to access Admin module'
        };
      }
      if (adminModule.status.toLowerCase() === 'inactive') {
        throw new Error('Admin module is inactive');
      }

      //Check if edit department exists in actions
      const edtDept = await adminModule.actions.find(
        action => action.description.toLowerCase() === 'edit department'
      );

      if (!edtDept) {
        throw {
          status: 401,
          message: 'Access denied. Not authorized to edit department'
        };
      }
      if (edtDept.status.toLowerCase() === 'inactive') {
        throw new Error('Edit department is inactive');
      }


      

      const result = await updateDepartment(request_info);

      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
