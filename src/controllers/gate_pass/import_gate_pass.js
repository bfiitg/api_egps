module.exports = function importGatePassRequest(
  { importRequest },
  { import_gate_pass },
  { verifyToken }
) {
  return async function postRequests(httpRequest) {
    try {
      const { path } = httpRequest.body;
      const token = httpRequest.token;
      const request_info = httpRequest.body;
      const modules = httpRequest.body.modules;

      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }
      if (!request_info.employee_id) {
        throw new Error('Employee id must be provided');
      }
      if (isNaN(request_info.employee_id)) {
        throw new Error('Employee id must be a number');
      }

      //Verify Token
      const tokenExist = await verifyToken(
        token,
        'secret',
        request_info.employee_id
      );
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      if (!modules) {
        throw new Error('Modules must be provided');
      }
      //Check if allowed to use gate pass module
      const reportModule = await modules.find(
        element => element.description.toLowerCase() === 'report'
      );

      if (!reportModule) {
        throw {
          status: 401,
          message: 'Access denied. Not authorized to access Report module'
        };
      }

      if (reportModule.status.toLowerCase() === 'inactive') {
        throw new Error('Report module is inactive');
      }

      //Check if request gate pass exists in actions
      const importGatePassExists = await reportModule.actions.find(
        action => action.description.toLowerCase() === 'import gate pass'
      );

      if (!importGatePassExists) {
        throw {
          status: 401,
          message: 'Access denied. Not authorized to access import gate pass'
        };
      }

      if (importGatePassExists.status.toLowerCase() === 'inactive') {
        throw new Error('Import gate pass is inactive');
      }

      //Extract data from excel
      const requestData = await importRequest(path);

      const result = await import_gate_pass(
        requestData,
        request_info.employee_id
      );
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
