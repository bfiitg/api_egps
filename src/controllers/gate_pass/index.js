//Verify token function
const verifyToken = require('../../token/').verifyToken;
const { verifyTokenUpload } = require('../../token');

//Import Requests function
const importRequest = require('../../import/import_employees');

const moment = require('moment');



//Use cases
const {
  requestGatepass,
  approve_request,
  disapprove_request,
  verify_gate_pass,
  unverify_gate_pass,
  cancel_gate_pass,
  correct_gate_pass,
  import_gate_pass,
  process_gate_pass,
  listNotifications,
  readNotifications
} = require('../../use-cases/gate_pass');

//Functions
const makeRequest = require('./file_request');
const makeApproveRequest = require('./approve_request');
const makeDispproveRequest = require('./disapprove_request');
const makeVerifyGatePass = require('./verify_personal_out');
const makeUnverifyGatePass = require('./unverify_personal_out');
const makeCancelGatePass = require('./cancel_request');
const makeListNotifications = require('./get_notifications');
const makeReadNotifications = require('./read_notifications');
const makeCorrectGatePass = require('./correct_gate_pass');
const makeImportRequests = require('./import_gate_pass');
const makeUploadFile = require('./upload_file');
const makeProcessVerified = require('./process_verified')

const fileRequest = makeRequest({ requestGatepass }, { verifyToken });
const approveRequest = makeApproveRequest({ approve_request }, { verifyToken }, { moment });
const disapproveRequest = makeDispproveRequest(
  { disapprove_request },
  { verifyToken }
);
const verifyGatePass = makeVerifyGatePass(
  { verify_gate_pass },
  { verifyToken }
);
const unverifyGatePass = makeUnverifyGatePass(
  { unverify_gate_pass },
  { verifyToken }
);
const cancelGatePass = makeCancelGatePass(
  { cancel_gate_pass },
  { verifyToken }
);

const correctGatePass = makeCorrectGatePass(
  { correct_gate_pass },
  { verifyToken }
);

const processGatePass = makeProcessVerified(
  { process_gate_pass },
  { verifyToken }
);


const importGatePass = makeImportRequests(
  { importRequest },
  { import_gate_pass },
  { verifyToken }
);

const getNotifications = makeListNotifications(
  { listNotifications },
  { verifyToken }
);
const updateNotifications = makeReadNotifications(
  { readNotifications },
  { verifyToken }
);


const postFile = makeUploadFile({ verifyTokenUpload });

const gatePassController = Object.freeze({
  fileRequest,
  approveRequest,
  disapproveRequest,
  verifyGatePass,
  unverifyGatePass,
  cancelGatePass,
  getNotifications,
  updateNotifications,
  correctGatePass,
  importGatePass,
  postFile,
  processGatePass
});

module.exports = gatePassController;
module.exports = {
  fileRequest,
  approveRequest,
  disapproveRequest,
  verifyGatePass,
  unverifyGatePass,
  cancelGatePass,
  getNotifications,
  updateNotifications,
  correctGatePass,
  importGatePass,
  postFile,
  processGatePass
};
