module.exports = function correctGatePass(
  { correct_gate_pass },
  { verifyToken }
) {
  return async function editGatePass(httpRequest) {
    try {
      const token = httpRequest.token;
      const request_info = httpRequest.body;
      const modules = httpRequest.body.modules;

      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }
      if (!request_info.employee_id) {
        throw new Error('Employee id must be provided');
      }
      if (isNaN(request_info.employee_id)) {
        throw new Error('Employee id must be a number');
      }

      //Verify Token
      const tokenExist = await verifyToken(
        token,
        'secret',
        request_info.employee_id
      );
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      if (!modules) {
        throw new Error('Modules must be provided');
      }

      //Check if allowed to use gate pass module
      const reportModule = await modules.find(
        element => element.description === 'Report'
      );

      if (!reportModule) {
        throw {
          status: 401,
          message: 'Access denied. Not authorized to access request module'
        };
      }

      if (reportModule.status === 'Inactive') {
        throw new Error('Report module is inactive');
      }
      //Check if approve gate pass exists in actions
      const correctGatePassExists = await reportModule.actions.find(
        action => action.description === 'Correct gate pass report'
      );

      if (!correctGatePassExists) {
        throw {
          status: 401,
          message: 'Access denied. Not authorized to correct gate pass report'
        };
      }

      if (correctGatePassExists.status === 'Inactive') {
        throw new Error('Correct gate pass report is inactive');
      }

      const result = await correct_gate_pass({
        ...request_info
      });

      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 201,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
