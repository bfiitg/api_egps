module.exports = function validateRequest(
  { approve_request },
  { verifyToken },
  { moment }
) {
  return async function putRequest(httpRequest) {
    try {
      const token = httpRequest.token;
      const request_info = httpRequest.body;
      const modules = httpRequest.body.modules;
     

      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }
      if (!request_info.employee_id) {
        throw new Error('Employee id must be provided');
      }
      if (isNaN(request_info.employee_id)) {
        throw new Error('Employee id must be a number');
      }

      //Verify Token
      const tokenExist = await verifyToken(
        token,
        'secret',
        request_info.employee_id
      );
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      if (!modules) {
        throw new Error('Modules must be provided');
      }
      //Check if allowed to use gate pass module
      const gatePassModule = await modules.find(
        element => element.description === 'Request'
      );

      if (!gatePassModule) {
        throw {
          status: 401,
          message: 'Access denied. Not authorized to access Gate Pass module'
        };
      }

      if (gatePassModule.status === 'Inactive') {
        throw new Error('Gate pass module is inactive');
      }
      //Check if approve gate pass exists in actions
      const requestGatePassExists = await gatePassModule.actions.find(
        action => action.description === 'Approve request'
      );

      if (!requestGatePassExists) {
        throw {
          status: 401,
          message: 'Access denied. Not authorized to access approve request'
        };
      }

      if (requestGatePassExists.status === 'Inactive') {
        throw new Error('Approve request is inactive');
      }

      const result = await approve_request(
        
        request_info.id,
        // request_info.status,
        request_info.employee_id,
        request_info.note,
        request_info.roleParam,
        moment
      );

      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 201,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
