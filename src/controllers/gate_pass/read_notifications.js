module.exports = function putNotification(
  { readNotifications },
  { verifyToken }
) {
  return async function editNotification(httpRequest) {
    try {
      const token = httpRequest.token;
      const { employee_id } = httpRequest.body;

      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }
      if (!employee_id) {
        throw { status: 403, message: 'Employee id must be provided' };
      }
      if (isNaN(employee_id)) {
        throw new Error('Employee id must be a number');
      }

      //Verify Token
      const tokenExist = await verifyToken(token, 'secret', employee_id);
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      const result = await readNotifications(employee_id);

      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
