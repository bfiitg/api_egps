//Verify token function
const verifyToken = require('../../token/').verifyToken;

//Moment
const moment = require('moment');

//Use cases
const {
  listEmployeeReport,
  listPersonalOutRequests,
  listVerifiedRequests
} = require('../../use-cases/reports');

//Functions
const makeEmployeeReport = require('./get_employees.report');
const makePersonalOutRequests = require('./get_personal_out_requests');
const makeVerifiedRequests = require('./get_verified_requests');

const getEmployeesReport = makeEmployeeReport(
  { listEmployeeReport },
  { verifyToken },
  { moment }
);
const getPersonalOutRequests = makePersonalOutRequests(
  { listPersonalOutRequests },
  { verifyToken },
  { moment }
);

const getVerifiedRequests = makeVerifiedRequests(
  { listVerifiedRequests },
  { verifyToken },
  { moment }
);

const reportController = Object.freeze({
  getEmployeesReport,
  getPersonalOutRequests,
  getVerifiedRequests
});
module.exports = reportController;
module.exports = {
  getEmployeesReport,
  getPersonalOutRequests,
  getVerifiedRequests
};
