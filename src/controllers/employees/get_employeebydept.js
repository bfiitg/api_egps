module.exports = function makeEmployeeByDept(
    { viewEmployeeByDept },
    { verifyToken }
) {
    return async function fetchEmployeeByDept(httpRequest) {
        try {
            const token = httpRequest.token;
            const request_info = httpRequest.body.body;
            const modules = httpRequest.body.modules;

            if (!token) {
                throw { status: 403, message: 'Token must be provided' };
            }
            if (!request_info.position.team.department.id) {
                throw { status: 403, message: 'Department id must be provided' };
            }
            if (isNaN(request_info.position.team.department.id)) {
                throw { status: 403, message: 'Department id must be a number' };
            }

            //Verify Token
            const tokenExist = await verifyToken(
                token,
                'secret',
                request_info.id
            );
            if (!tokenExist) {
                throw { status: 403, message: 'Forbidden' };
            }

            if (!modules) {
                throw new Error('Modules must be provided');
            }

            //Check if allowed to use gate pass module
            const requestModule = await modules.find(
                element => element.description === 'Request'
            );

            if (!requestModule) {
                throw {
                    status: 401,
                    message: 'Access denied. Not authorized to access Request module'
                };
            }
            if (requestModule.status === 'Inactive') {
                throw new Error('Request module is inactive');
            }

            //Check if request gate pass exists in actions
            const viewEmployeeExists = await requestModule.actions.find(
                action => action.description === 'View Proxy'
            );

            if (!viewEmployeeExists) {
                throw {
                    status: 401,
                    message: 'Access denied. Not authorized to View Proxy'
                };
            }
            if (viewEmployeeExists.status === 'Inactive') {
                throw new Error('View employee profile is inactive');
            }

            const result = await viewEmployeeByDept(request_info);
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                status: 200,
                body: result
            };
        } catch (e) {
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                status: e.status ? e.status : 400,
                body: e.message
            };
        }
    };
};
