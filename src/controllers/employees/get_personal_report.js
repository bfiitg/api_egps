module.exports = function makeListPersonalReport(
  { listPersonalReport },
  { verifyToken },
  { moment }
) {
  return async function getPersonalReport(httpRequest) {
    try {
      const token = httpRequest.token;
      const request_info = httpRequest.body;
      const modules = httpRequest.body.modules;
      const dateRange = {
        request_date_from: httpRequest.body.request_date_from,
        request_date_to: httpRequest.body.request_date_to
      };

      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }
      if (!request_info.employee_id) {
        throw { status: 403, message: 'Employee id must be provided' };
      }
      if (isNaN(request_info.employee_id)) {
        throw { status: 403, message: 'Employee id must be a number' };
      }

      //Verify Token
      const tokenExist = await verifyToken(
        token,
        'secret',
        request_info.employee_id
      );
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      //Check report module in modules provided
      const reportModule = await modules.find(
        element => element.description === 'Report'
      );

      if (!reportModule) {
        throw {
          status: 401,
          message: 'Access Denied. Not authorized to access report module'
        };
      }

      if (reportModule.status === 'Inactive') {
        throw new Error('Report module is inactive');
      }

      //Check module actions
      const viewGatePassReport = await reportModule.actions.find(
        action => action.description === 'View gate pass report'
      );

      if (!viewGatePassReport) {
        throw {
          status: 401,
          message:
            'Access Denied. Not authorized to access view gate pass report'
        };
      }

      if (viewGatePassReport.status === 'Inactive') {
        throw new Error('View gate pass report is inactive');
      }

      const result = await listPersonalReport(
        request_info.employee_id,
        dateRange,
        moment
      );
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
