module.exports = function makeUpdateProfile(
  { updateProfile },
  { verifyToken }
) {
  return async function updateEmployeeProfile(httpRequest) {
    try {
      const token = httpRequest.token;
      const request_info = httpRequest.body;
      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }
      if (!request_info.id) {
        throw { status: 403, message: 'Employee id must be provided' };
      }
      if (isNaN(request_info.id)) {
        throw { status: 403, message: 'Employee id must be a number' };
      }

      //Verify Token
      const tokenExist = await verifyToken(token, 'secret', request_info.id);
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      const result = await updateProfile({ ...request_info });
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
