module.exports = function makeListScheduleOut(
  { listScheduleOut },
  { verifyToken }
) {
  return async function getScheduleOut(httpRequest) {
    try {
      const token = httpRequest.token;
      const request_info = httpRequest.body;
      const modules = httpRequest.body.modules;

      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }
      if (!request_info.employee_id) {
        throw { status: 403, message: 'Employee id must be provided' };
      }
      if (isNaN(request_info.employee_id)) {
        throw { status: 403, message: 'Employee id must be a number' };
      }

      //Verify Token
      const tokenExist = await verifyToken(
        token,
        'secret',
        request_info.employee_id
      );
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      if (!modules) {
        throw new Error('Modules must be provided');
      }

      //Check report module in modules provided
      const gatePassModule = await modules.find(
        element => element.description === 'Gate Pass'
      );

      if (!gatePassModule) {
        throw {
          status: 401,
          message: 'Access Denied. Not authorized to access report module'
        };
      }
      if (gatePassModule.status === 'Inactive') {
        throw new Error('Gate pass module is inactive');
      }

      //Check module actions
      const viewScheduleOut = await gatePassModule.actions.find(
        action => action.description === 'View schedule out'
      );

      if (!viewScheduleOut) {
        throw {
          status: 401,
          message:
            'Access Denied. Not authorized to access view gate pass requests'
        };
      }

      if (viewScheduleOut.status === 'Inactive') {
        throw new Error('View schedule out is inactive');
      }

      const result = await listScheduleOut(request_info.employee_id);
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
