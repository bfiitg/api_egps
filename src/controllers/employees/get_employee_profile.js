module.exports = function makeEmployeeProfile(
  { getEmployeeProfile },
  { verifyToken }
) {
  return async function fetchEmployeeProfile(httpRequest) {
    try {
      const token = httpRequest.token;
      const request_info = httpRequest.body;
      const modules = httpRequest.body.modules;
      const id = httpRequest.params.id;

      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }
      if (!request_info.employee_id) {
        throw { status: 403, message: 'Employee id must be provided' };
      }
      if (isNaN(request_info.employee_id)) {
        throw { status: 403, message: 'Employee id must be a number' };
      }

      //Verify Token
      const tokenExist = await verifyToken(
        token,
        'secret',
        request_info.employee_id
      );
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      if (!modules) {
        throw new Error('Modules must be provided');
      }

      //Check if allowed to use gate pass module
      const reportModule = await modules.find(
        element => element.description === 'Report'
      );

      if (!reportModule) {
        throw {
          status: 401,
          message: 'Access denied. Not authorized to access Report module'
        };
      }
      if (reportModule.status === 'Inactive') {
        throw new Error('Report module is inactive');
      }

      //Check if request gate pass exists in actions
      const viewEmployeeExists = await reportModule.actions.find(
        action => action.description === 'View employee profile'
      );

      if (!viewEmployeeExists) {
        throw {
          status: 401,
          message: 'Access denied. Not authorized to View employee profile'
        };
      }
      if (viewEmployeeExists.status === 'Inactive') {
        throw new Error('View employee profile is inactive');
      }

      const result = await getEmployeeProfile(id);
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
