const verifyToken = require('../../token/').verifyToken;

//Moment
const moment = require('moment');

//Use cases
const {
  listScheduleOut,
  listPersonalReport,
  getEmployeeProfile,
  updateProfile,
  viewEmployeeByDept,
} = require('../../use-cases/employees');

const makeScheduleOut = require('./get_schedule_out');
const makePersonalReport = require('./get_personal_report');
const makeEmployeeProfile = require('./get_employee_profile');
const makeUpdateProfile = require('./update_password');
const makeEmployeeByDept = require('./get_employeebydept');

const getScheduleOut = makeScheduleOut({ listScheduleOut }, { verifyToken });
const getPersonalReport = makePersonalReport(
  { listPersonalReport },
  { verifyToken },
  { moment }
);
const fetchEmployeeProfile = makeEmployeeProfile(
  { getEmployeeProfile },
  { verifyToken }
);
const updateEmployeePassword = makeUpdateProfile(
  { updateProfile },
  { verifyToken }
);

const fetchEmployeeByDept = makeEmployeeByDept(
  { viewEmployeeByDept },
  { verifyToken }
)

const employeeController = Object.freeze({
  getScheduleOut,
  getPersonalReport,
  fetchEmployeeProfile,
  updateEmployeePassword,
  fetchEmployeeByDept
});

module.exports = employeeController;
module.exports = {
  getScheduleOut,
  getPersonalReport,
  fetchEmployeeProfile,
  updateEmployeePassword,
  fetchEmployeeByDept
};
