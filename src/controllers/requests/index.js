//Verify token function
const verifyToken = require('../../token/').verifyToken;

//Moment
const moment = require('moment');

//Use cases
const {
  listApprovedGatePass,
  listPendingGatePass,
  listDisapprovedGatePass
} = require('../../use-cases/requests');

//Functions
const makeApprovedGatePass = require('./get_approved_requests');
const makePendingGatePass = require('./get_pending_requests');
const makeDisapprovedGatePass = require('./get_disapproved_requests');

const getApprovedGatePass = makeApprovedGatePass(
  { listApprovedGatePass },
  { verifyToken },
  { moment }
);

const getPendingGatePass = makePendingGatePass(
  { listPendingGatePass },
  { verifyToken },
  { moment }
);

const getDisapprovedGatePass = makeDisapprovedGatePass(
  { listDisapprovedGatePass },
  { verifyToken },
  { moment }
);

const requestController = Object.freeze({
  getApprovedGatePass,
  getPendingGatePass,
  getDisapprovedGatePass
});

module.exports = requestController;
module.exports = {
  getApprovedGatePass,
  getPendingGatePass,
  getDisapprovedGatePass
};
