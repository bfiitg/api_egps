module.exports = function makeListPendingGatePass(
  { listPendingGatePass },
  { verifyToken },
  { moment }
) {
  return async function getPendingGatePass(httpRequest) {
    try {
      const token = httpRequest.token;
      const request_info = httpRequest.body;
      const modules = httpRequest.body.modules;
      const roleParams = httpRequest.params.roleParams;
      const dateRange = {
        request_date_from: httpRequest.body.request_date_from,
        request_date_to: httpRequest.body.request_date_to
      };

      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }
      if (!request_info.employee_id) {
        throw { status: 403, message: 'Employee id must be provided' };
      }
      if (isNaN(request_info.employee_id)) {
        throw { status: 403, message: 'Employee id must be a number' };
      }

      //Verify Token
      const tokenExist = await verifyToken(
        token,
        'secret',
        request_info.employee_id
      );
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      if (!modules) {
        throw new Error('Modules must be provided');
      }

      //Check report module in modules provided
      const requestModule = await modules.find(
        element => element.description === 'Request'
      );
      if (!requestModule) {
        throw {
          status: 401,
          message: 'Access Denied. Not authorized to access report module'
        };
      }
      if (requestModule.status === 'Inactive') {
        throw new Error('Request module is inactive');
      }

      //Check module actions
      const viewGatePassRequest = await requestModule.actions.find(
        action => action.description === 'View requests'
      );
      if (!viewGatePassRequest) {
        throw {
          status: 401,
          message:
            'Access Denied. Not authorized to access view gate pass requests'
        };
      }
      if (viewGatePassRequest.status === 'Inactive') {
        throw new Error('Gate pass requests is inactive');
      }

      const result = await listPendingGatePass(
        roleParams,
        request_info.employee_id,
        dateRange,
        moment
      );

      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
