module.exports = function makeRequest({ listRequestTypes }, { verifyToken }) {
  return async function postRequest(httpRequest) {
    try {
      const token = httpRequest.token;
      const request_info = httpRequest.body;
      const modules = httpRequest.body.modules;
      if (!token) {
        throw { status: 403, message: 'Token must be provided' };
      }
      if (!request_info.employee_id) {
        throw new Error('Employee id must be provided');
      }
      if (isNaN(request_info.employee_id)) {
        throw new Error('Employee id me be a number');
      }

      //Verify Token
      const tokenExist = await verifyToken(
        token,
        'secret',
        request_info.employee_id
      );
      if (!tokenExist) {
        throw { status: 403, message: 'Forbidden' };
      }

      if (!modules) {
        throw new Error('Modules must be provided');
      }
      //Check if allowed to use gate pass module
      const gatePassModule = await modules.find(
        element => element.description === 'Gate Pass'
      );
      if (!gatePassModule) {
        throw {
          status: 401,
          message: 'Access denied. Not authorized to access Gate Pass module'
        };
      }

      if (gatePassModule.status === 'Inactive') {
        throw new Error('Gate pass module is inactive');
      }

      const requestTypes = await listRequestTypes();

      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: 201,
        body: requestTypes
      };
    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
