//Verify token function
const verifyToken = require('../../token/').verifyToken;

//Function
const listRequestTypes = require('../../use-cases/request_types');

const makeListRequestTypes = require('./get_request_types');

const request_types = makeListRequestTypes(
  { listRequestTypes },
  { verifyToken }
);

const requestTypesController = Object.freeze(request_types);

module.exports = requestTypesController;
module.exports = request_types;
