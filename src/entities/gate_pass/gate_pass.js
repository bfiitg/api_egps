module.exports = function buildRequest() {
  return async function makeRequest(requestInfo, moment, request_types) {
    const {
      employee_id,
      request_type_id,
      request_time_out,
      request_time_in,
      request_date,
      reason
    } = requestInfo;

    if (!employee_id) {
      throw new Error('Employee id must be provided');
    }
    if (isNaN(employee_id) === true) {
      throw new Error('Employee id must be a number');
    }
    if (!request_type_id) {
      throw new Error('Request type id must be provided');
    }
    if (isNaN(request_type_id)) {
      throw new Error('Request type id must a number');
    }

    if (!request_time_out) {
      throw new Error('Request time out must be provided');
    }
    if (!isNaN(request_time_out)) {
      throw new Error('Invalid request time out');
    }

    const getRequestType = await request_types.findOneRequestType(
      request_type_id
    );
    if (getRequestType.has_time_in) {
      if (!request_time_in) {
        throw new Error('Request time in must be provided');
      }
      if (!isNaN(request_time_in)) {
        throw new Error('Invalid request time in');
      }

      //compare time in and time out, if time in less than time out throw error
      const isTimeInGreaterThanTimeOut = request_time_in <= request_time_out;
      if (isTimeInGreaterThanTimeOut)
        throw new Error('Request time in must be above request time out');
    }

    if (!getRequestType.has_time_in) {
      if (request_time_in) {
        throw new Error('Request type does not have a time in');
      }
    }

    if (!request_date) {
      throw new Error('Request date must be provided');
    }
    if (!isNaN(request_date)) {
      throw new Error('Invalid request date');
    }

    moment(request_date).format('YYYY-MM-DD');
    if (!moment(request_date).isValid()) {
      throw new Error('Invalid request date');
    }
    if (!reason) {
      throw new Error('Reason must have a value');
    }
    if (!isNaN(reason)) {
      throw new Error('Reason must have a value');
    }

    //compare requested time if less than time from server
    // if (
    //   moment(`${request_date} ${request_time_out}`).tz('Asia/Manila').format() <
    //   moment()
    //     .tz('Asia/Manila')
    //     .format()
    // ) {
    //   throw new Error(
    //     `Cannot request gate pass before ${moment()
    //       .tz('Asia/Manila')
    //       .format('h:mm A')}`
    //   );
    // }

    return Object.freeze({
      getEmployeeId: () => employee_id,
      getRequestTypeId: () => request_type_id,
      getRequestTimeOut: () => request_time_out,
      getRequestTimeIn: () => request_time_in,
      getRequestDate: () => request_date,
      reason: () => reason,
      requestType: () => getRequestType
    });
  };
};
