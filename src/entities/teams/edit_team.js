module.exports = function buildUpdateTeam() {
  return async function makeUpdateTeam(request_info, { admin_db }) {
    const { id, name, department_id, employee_id, status } = request_info;

    if (!id) {
      throw new Error('Team id must have a value');
    }
    if (isNaN(id)) {
      throw new Error('Team id must be a number');
    }

    const teamExist = await admin_db.findTeamById(id);
    if (!teamExist) {
      throw new Error('Team does not exist');
    }

    if (!name) {
      throw new Error('Team name must have a value');
    }
    if (!isNaN(name)) {
      throw new Error('Team name must be a string');
    }
    if (!department_id) {
      throw new Error('Department id must have a value');
    }
    if (isNaN(department_id)) {
      throw new Error('Department id must be a number');
    }
    if (!status) {
      throw new Error('Team status must have a value');
    }
    if (!isNaN(status)) {
      throw new Error('Team status must be a string');
    }
    if (status !== 'Active' && status !== 'Inactive') {
      throw new Error('Invalid status');
    }

    //check team name if already exists
    const nameExists = await admin_db.findTeamByName(department_id, name);
    //check if found team id is equal to team id given
    if (nameExists && nameExists.status === 'Active' && status === 'Active') {
      if (nameExists.employee_id !== employee_id && nameExists.id !== id) {
        if (nameExists.id !== id) {
          throw new Error('Department name already exists');
        }
      }
    }
  };
};
