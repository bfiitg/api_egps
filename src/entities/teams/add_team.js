module.exports = function buildTeam() {
  return async function makeTeam(request_info, { admin_db }) {
    const { id, name, department_id } = request_info;

    if (id) {
      throw new Error('Team id is auto increment');
    }
    if (!name) {
      throw new Error('Team name must have a value');
    }
    if (!isNaN(name)) {
      throw new Error('Team name must be a string');
    }
    if (!department_id) {
      throw new Error('Department id must have a value');
    }
    if (isNaN(department_id)) {
      throw new Error('Department id must be a number');
    }

    //check team name if already exists
    const nameExists = await admin_db.findTeamByName(department_id, name);
    if (nameExists) {
      throw new Error('Team name already exists');
    }
  };
};
