const buildTeam = require('./add_team');
const buildUpdateTeam = require('./edit_team');

const makeTeam = buildTeam();
const makeUpdateTeam = buildUpdateTeam();

module.exports = {
  makeTeam,
  makeUpdateTeam
};
