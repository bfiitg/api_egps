module.exports = function buildPosition() {
  return async function makePosition(request_info, { admin_db }) {
    const { id, name, team_id } = request_info;

    if (id) {
      throw new Error('Position id is auto increment');
    }
    if (!name) {
      throw new Error('Position name must have a value');
    }
    if (!isNaN(name)) {
      throw new Error('Position name must be a string');
    }
    if (!team_id) {
      throw new Error('Team id must have a value');
    }
    if (isNaN(team_id)) {
      throw new Error('Team id must be a number');
    }

    //Check if name exists in DB
    const nameExists = await admin_db.findPositionByName(team_id, name);
    if (nameExists) {
      throw new Error('Position name already exists');
    }
  };
};
