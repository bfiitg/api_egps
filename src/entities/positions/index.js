const buildPosition = require('./add_position');
const buildUpdatePosition = require('./edit_position');

const makePosition = buildPosition();
const makeUpdatePosition = buildUpdatePosition();

module.exports = {
  makePosition,
  makeUpdatePosition
};
