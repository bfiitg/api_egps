module.exports = function buildUpdatePosition() {
  return async function makeUpdatePosition(request_info, { admin_db }) {
    const { id, name, team_id, status } = request_info;

    if (!id) {
      throw new Error('Position id must have a value');
    }
    if (isNaN(id)) {
      throw new Error('Position id must be a number');
    }

    const postitionExist = await admin_db.findPositionById(id);
    if (!postitionExist) {
      throw new Error('Position does not exist');
    }

    if (!name) {
      throw new Error('Position name must have a value');
    }
    if (!isNaN(name)) {
      throw new Error('Position name must be a string');
    }
    if (!team_id) {
      throw new Error('Team id must have a value');
    }
    if (isNaN(team_id)) {
      throw new Error('Team id must be a number');
    }
    if (!status) {
      throw new Error('Employee status must have a value');
    }
    if (!isNaN(status)) {
      throw new Error('Employee status must be a string');
    }
    if (status !== 'Active' && status !== 'Inactive') {
      throw new Error('Invalid status');
    }

    //Check if name exists in DB
    const nameExists = await admin_db.findPositionByName(team_id, name);
    if (nameExists && nameExists.status === 'Active' && status === 'Active') {
      if (nameExists.id !== id) {
        throw new Error('Position name already exists');
      }
    }
  };
};
