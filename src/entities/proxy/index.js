const buildDeleteProxy = require('./delete_proxy');
const buildCreateProxy = require('./add_proxy');

const makeDeleteProxy = buildDeleteProxy();
const makeCreateProxy = buildCreateProxy();

module.exports = {
    makeDeleteProxy,
    makeCreateProxy
}