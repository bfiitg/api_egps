module.exports = function buildDeleteProxy() {
    return async function makeDeleteProxy(request_info, { admin_db }) {
        const { employee_id } = request_info;

        const team_id = request_info.position.team.id;


        if (!team_id) {
            throw new Error("Team id must have a value");
        }
        if (isNaN(team_id)) {
            throw new Error("Team id must be a number");
        }
        if (!employee_id) {
            throw new Error("Employee id must have a value")
        }
        if (isNaN(employee_id)) {
            throw new Error("Employee id must be a number")
        }

        // Check if teamhead exist
        const teamheadExists = await admin_db.findIfHeadExists(team_id, employee_id);
        if (!teamheadExists) {
            throw new Error("Proxy Approver does not exist");
        }
    }
}