module.exports = function buildCreateProxy() {
    return async function makeCreateProxy(request_info, { admin_db }) {
        const { employee_id } = request_info;
        const team_id = request_info.position.team.id;
        const department_id = request_info.position.team.department.id;

        if (!team_id) {
            throw new Error("Team id must have a value");
        }
        if (isNaN(team_id)) {
            throw new Error("Team id must be a number");
        }
        if (!employee_id) {
            throw new Error("Employee id must have a value")
        }
        if (isNaN(employee_id)) {
            throw new Error("Employee id must be a number")
        }

        // Check if proxy exist
        // const approverTh = await admin_db.findIfHeadExists(team_id, employee_id);
        // const approverDh = await admin_db.findIfDHeadExists(department_id, employee_id);

        // if (approverTh.length != 0 || approverDh.length != 0) {
        //     throw new Error("Employee is already an approver.");
        // }
    }

}