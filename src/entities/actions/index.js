const buildAction = require('./add_action');
const buildUpdateAction = require('./edit_action');

const makeAction = buildAction();
const makeUpdateAction = buildUpdateAction();

module.exports = {
  makeAction,
  makeUpdateAction
};
