const buildDepartment = require('./add_department');
const buildUpdateDepartment = require('./edit_department');

const makeDepartment = buildDepartment();
const makeUpdateDepartment = buildUpdateDepartment();

module.exports = {
  makeDepartment,
  makeUpdateDepartment
};
