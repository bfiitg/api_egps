module.exports = function buildUpdateDeparment() {
  return async function makeUpdateDepartment(request_info, { admin_db }) {
    const { id, name, employee_id, status } = request_info;

    if (!id) {
      res.status(500);
      throw new Error('Department id must have a value');
    }
    if (isNaN(id)) {
      res.status(500);
      throw new Error('Department id must be a number');
    }
    const departmentExist = await admin_db.findDepartmentById(id);
    if (!departmentExist) {
      throw new Error('Department does not exist');
    }
    if (!name) {
      res.status(500);
      throw new Error('Department name must have a value');
    }
    if (!isNaN(name)) {
      res.status(500);
      throw new Error('Department name must be a string');
    }

    if (!status) {
      res.status(500);
      throw new Error('Status must have a value');
    }
    if (!isNaN(status)) {
      res.status(500);
      throw new Error('Status must be a string');
    }
    if (status !== 'Active' && status !== 'Inactive') {
      res.status(500);
      throw new Error('Invalid status');
    }

    //check dept name if exists
    const nameExists = await admin_db.findDeparmentByName(name);

    if (nameExists && nameExists.status === 'Active' && status === 'Active' && nameExists.name !== name) {
      
        throw new Error('Department name already exists');

    }

  };
};
