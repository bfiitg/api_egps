module.exports = function buildDeparment() {
  return async function makeDepartment(request_info, { admin_db }) {
    const { id, name } = request_info;

    if (id) {
      throw new Error('Department id is auto increment');
    }
    if (!name) {
      throw new Error('Department name must have a value');
    }
    if (!isNaN(name)) {
      throw new Error('Department name must be a string');
    }

    //check department name if exists
    const nameExists = await admin_db.findDeparmentByName(name);

    if (nameExists) {
      throw new Error('Department name already exists');
    }
  };
};
