const buildRequestType = require('./add_request_type');
const buildUpdateRequestType = require('./edit_request_type');

const makeRequestType = buildRequestType();
const makeUpdateRequestType = buildUpdateRequestType();

module.exports = {
  makeRequestType,
  makeUpdateRequestType
};
