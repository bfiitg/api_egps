module.exports = function buildRequestType() {
  return async function makeRequestType(request_info, { admin_db }) {
    const { id, name, has_time_in, status } = request_info;

    if (!id) {
      throw new Error('Request type id must have a value');
    }
    if (isNaN(id)) {
      throw new Error('Request type id must be a number');
    }

    //check request type id if exists
    const requestTypeExist = await admin_db.findRequestTypeById(id);
    if (!requestTypeExist) {
      throw new Error('Request type does not exist');
    }

    if (!name) {
      throw new Error('Request type name must have a value');
    }
    if (!isNaN(name)) {
      throw new Error('Request type name must be a string');
    }
    if (has_time_in !== true && has_time_in !== false) {
      throw new Error('Timestamp for time in must be a boolean');
    }
    if (!status) {
      throw new Error('Status must have a value');
    }
    if (!isNaN(status)) {
      throw new Error('Status must be a string');
    }
    if (status !== 'Active' && status !== 'Inactive') {
      throw new Error('Invalid status');
    }

    //check request tpye name if exists
    const nameExists = await admin_db.findOneRequestTypeByName(name);
    if (nameExists && nameExists.status === 'Active' && status === 'Active') {
      if (nameExists.id !== id) {
        throw new Error('Request type name already exists');
      }
    }
  };
};
