module.exports = function buildRequestType() {
  return async function makeRequestType(request_info, { admin_db }) {
    const { id, name, has_time_in } = request_info;

    if (id) {
      throw new Error('Request type id is auto increment');
    }
    if (!name) {
      throw new Error('Request type name must have a value');
    }
    if (!isNaN(name)) {
      throw new Error('Request type name must be a string');
    }
    if (has_time_in !== true && has_time_in !== false) {
      throw new Error('Invalid has_time_in');
    }

    //check request tpye name if exists
    const nameExists = await admin_db.findOneRequestTypeByName(name);
    if (nameExists) {
      throw new Error('Request type name already exists');
    }
  };
};
