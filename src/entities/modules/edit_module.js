module.exports = function buildUpdateModule() {
  return async function makeUpdateModule(request_info, { admin_db }) {
    const { id, description, status } = request_info;

    if (!id) {
      res.status(500);
      throw new Error('Module id must have a value');
    }
    if (isNaN(id)) {
      res.status(500);
      throw new Error('Module id must be a number');
    }

    //check module id if exists
    const moduleExists = await admin_db.findModuleById(id);
    if (!moduleExists) {
      throw new Error('Module does not exist');
    }

    if (!description) {
      res.status(500);
      throw new Error('Module description must have a value');
    }
    if (!isNaN(description)) {
      res.status(500);
      throw new Error('Module description must be a string');
    }
    if (!status) {
      res.status(500);
      throw new Error('Status must have a value');
    }
    if (!isNaN(status)) {
      res.status(500);
      throw new Error('Status must be a string');
    }
    if (status !== 'Active' && status !== 'Inactive') {
      res.status(500);
      throw new Error('Invalid status');
    }
  };
};
