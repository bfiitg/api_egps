module.exports = function buildCreateTeamhead() {
    return async function makeCreateTeamhead(request_info, { admin_db }) {
        const { team_id, employee_id } = request_info;

        if (!team_id) {
            throw new Error("Team id must have a value");
        }
        if (isNaN(team_id)) {
            throw new Error("Team id must be a number");
        }
        if (!employee_id) {
            throw new Error("Employee id must have a value")
        }
        if (isNaN(employee_id)) {
            throw new Error("Employee id must be a number")
        }
        // Check if teamhead exist
        const teamheadExists = await admin_db.findIfHeadExists(team_id, employee_id);

        if (teamheadExists.length != 0) {
            throw new Error("Teamhead already exists");
        }
    }

}