const buildDeleteTeamhead = require('./delete_teamheads');
const buildCreateTeamhead = require('./add_teamhead');

const makeDeleteTeamhead = buildDeleteTeamhead();
const makeCreateTeamhead = buildCreateTeamhead();

module.exports = {
    makeDeleteTeamhead,
    makeCreateTeamhead
}