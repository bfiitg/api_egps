module.exports = function buildEmployee() {
  return async function makeEmployee(requestInfo, { admin_db }, { rolesDb }) {
    const {
      id,
      role_id,
      position_id,
      first_name,
      last_name,
      middle_name,
      payroll_group,
      status
    } = requestInfo;

    if (!id) {
      throw new Error('Employee id must have value');
    }
    if (isNaN(id)) {
      throw new Error('Employee id must be a number');
    }
    if (!role_id) {
      throw new Error('Role id must have value');
    }
    if (isNaN(role_id)) {
      throw new Error('Role id must be a number');
    }
    if (position_id == "") {
      requestInfo.position_id = null;
    }

    //check role if exists
    const roleExist = await rolesDb.findOneRoleById(role_id);

    if (!roleExist) {
      throw new Error('Role does not exist');
    }
    if (
      roleExist.name.split('-')[0] !== 'admin' &&
      roleExist.name.split('-')[0] !== 'sec' &&
      roleExist.name.split('-')[0] !== 'th' &&
      roleExist.name.split('-')[0] !== 'mng'
    ) {
      if (!position_id) {
        throw new Error('Position id must have value');
      }
      if (isNaN(position_id)) {
        throw new Error('Position id must be a number');
      }
    }
    if (!first_name) {
      throw new Error('Employee first name must have a value');
    }
    if (!isNaN(first_name)) {
      throw new Error('Employee first name must be a string');
    }
    if (!last_name) {
      throw new Error('Employee last name must have a value');
    }
    if (!isNaN(last_name)) {
      throw new Error('Employee last name must be a string');
    }
    if (roleExist.name.split('-')[0] !== 'admin') {

      if (!payroll_group) {
        throw new Error('Employee payroll group must be provided');
      }
      if (!isNaN(payroll_group)) {
        throw new Error('Employee payroll group must be a string');
      }
      if (
        payroll_group.toLowerCase() !== 'office_payroll' &&
        payroll_group.toLowerCase() !== 'manager_payroll'
      ) {
        throw new Error('Invalid employee payroll group');
      }
    }
    if (!status) {
      throw new Error('Employee status must have a value');
    }
    if (!isNaN(status)) {
      throw new Error('Employee status must be a string');
    }
    if (status !== 'Active' && status !== 'Inactive') {
      throw new Error('Invalid status');
    }

    //check employee if exists
    const exists = await admin_db.findOneEmployeeById(id);
    if (!exists) {
      throw new Error('Employee does not exist');
    }

    return Object.freeze({
      getEmployeeId: () => id,
      getRoleId: () => role_id,
      getPositionId: () => position_id,
      getFirstName: () => first_name,
      getLastName: () => last_name,
      getMiddleName: () => middle_name
    });
  };
};
