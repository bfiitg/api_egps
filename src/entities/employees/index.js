const buildEmployee = require('./add_employee');
const buildUpdateEmployee = require('./edit_employee');

const makeEmployee = buildEmployee();
const makeUpdateEmployee = buildUpdateEmployee();

module.exports = { makeEmployee, makeUpdateEmployee };
