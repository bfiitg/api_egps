const buildAccessRight = require('./add_access_right');
const buildUpdateAccessRight = require('./edit_access_right');

const makeAccessRight = buildAccessRight();
const makeUpdateAccessRight = buildUpdateAccessRight();

module.exports = {
  makeAccessRight,
  makeUpdateAccessRight
};
