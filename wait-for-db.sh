#!/bin/bash
# wait-for-db.sh

set -e

host="postgresql"
shift
cmd="$@"
create="npx sequelize-cli db:create"
migrate="npx sequelize-cli db:migrate"
seed="npx sequelize-cli db:seed:all"

>&2 echo "Postgres is up - executing command"
#exec $create
#exec $migrate
#exec $seed
exec $cmd
