# Employee Gate Pass System(EGPS)

Employee gate pass system is a web-based application designed to automate the gate pass processes of the KCC Corporate Office

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Guidelines
This document provides guidelines and examples for EGPS API, encouraging consistency, maintainability and best practices across
platforms. EGPS API's aim to balance a truly RESTful API interface with positive developer experience.

### Installation
Use Visual Studio Code for code editor redefined and optimized for building and debugging modern web and cloud applications.

```
*[Visual Studio Code](https://code.visualstudio.com/download)
```

Use PostgreSQL for object-relational database management system.

```
*[Postgresql](https://www.postgresql.org/download/)
```

Use Postman for complete API Development Environment

```
*[Postman](https://www.getpostman.com/downloads/)
```

To get started in this walkthrough, open the Employee Gate Pass System(EGPS) in Visual Studio Code
You'll need to open a new terminal('ctrl + ~') for node and npm command-line tools.

To install all dependencies inside 'package.json' file, you need to install node package manager(npm)

```
npm install
```

For the database, we are using sequelize.js, Sequelize.js is one JavaScript library among many that can be used to talk databases.
Sequelize.js is an ORM (Object/Relational Mapper) which provides easy access to MySQL, MariaDB, SQLite or PostgreSQL databases
by mapping database entries to objects and vice versa.

To create database using sequelize, run this command

```
npx sequelize-cli db:create
```

For migration, you need to execute 'db: migrate' command

```
npx sequelize-cli db:migrate
```

For the last step in establishing the database, you need to run this command

```
npx sequelize-cli db:seed:all
```

This will execute the seed file that will be inserted into tables

You are ready now, all you need to do is run the project by using this command

```
npm run dev src
```

## Request Examples
### API Resources
### Admin


```js
Employees
        
        POST /employees 

        Retrieve list of employees
        
        Example: http://localhost:3000/admin/employees

        Request body:

        {
            "employee_id": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            }  
        }
      
        POST /employees/add_employee

        Create new employee 

        Example: http://localhost:3000/admin/employees/add_employee

        Request body:

        {
            "id": 154151992,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "role_id": 2,
            "position_id" : 1,
            "first_name" : "Elizaaa",
            "middle_name" : "Lilyy",
            "last_name" : "Leunn",
            "adminEmail" : "egpsegpsegps@gmail.com",
            "created_by": 154151100
        }

        POST /employees/import

        Create employees based on imported excel file

        Example: http://localhost:3000/admin/employees/import

        Request body:

        {
            "path": "C:\Users\Default.DESKTOP-H07SCEJ\Downloads\data.xlsx"
        }

        PUT /employees/update_employee

        Update employee details with the given id

        Example: http://localhost:3000/admin/employees/update_employee

        Request body:

        {
            "id": 154151997,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "role_id": 2,
            "position_id" : 1,
            "first_name" : "Elizaa",
            "middle_name" : "Lily",
            "last_name" : "Leun",
            "password": "employee",
            "status": "Active",
            "updated_by": 154151100
        }

        PUT /employees/reset_password

        Reset employee password to default(id)

        Example: http://localhost:3000/admin/employees/reset_password

        Request body:

        {
                "employee_id": 154151100,
                "id": 154151101,
                "modules": [
                    {
                        "id": 1,
                        "description": "admin",
                        "status": "Active",
                        "actions": [
                            {
                                "id": 18,
                                "description": "Admin",
                                "module_id": 1,
                                "status": "Active"
                            },
                            {
                                "id": 43,
                                "description": "Reset employee password",
                                "module_id": 1,
                                "status": "Active"
                            }
                        ]
                    }
                ]
            }

Roles

        POST /roles

        Retrieve list of roles

        Example: http://localhost:3000/admin/roles

        Request body:

        {
            "employee_id": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            }
        }

        POST /roles/add_role

        Create new role

        Example: http://localhost:3000/admin/roles/add_role

        Request body:

        {
            "created_by": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "name": "bdgt-Budgets"
        }

        PUT /roles/update_role

        Update role details with the given id

        Example: http://localhost:3000/admin/roles/update_role

        Request body:

        {
            "id" : 9,
            "updated_by": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "name": "bdgt-Budget",
            "status": "Inactive"
        }

Positions

        POST /positions

        Retrieve list of positions

        Example: http://localhost:3000/admin/positions

        Request body:

        {
            "employee_id": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            }
        }

        POST /positions/add_position

        Create new position

        Example: http://localhost:3000/admin/positions/add_position

        Request body:

        {
            "created_by": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "name": "Analyst",
            "team_id" : 1
        }

        PUT /positions/update_position

        Update position details with the given id

        Example: http://localhost:3000/admin/positions/update_position

        Request body:

        {
            "id": 13,
            "updated_by": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "name": "System Analyst",
            "team_id" : 1,
            "status": "Inactive"
        }

Teams

        POST /teams

        Retrieve list of teams

        Example: http://localhost:3000/admin/teams

        Request body:

        {
            "employee_id": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            }
        }

        POST /teams/add_team

        Create new team

        Example: http://localhost:3000/admin/teams/add_team

        Request body:

        {
            "name": "Operation",
            "department_id" : 1,
            "employee_id": 154151997,
            "created_by": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            }
        }

        PUT /teams/update_team

        Update team details with the given id

        Example: http://localhost:3000/admin/teams/update_team

        Request body:

        {  
            "id": 9,
            "name": "Operations",
            "department_id" : 1,
            "employee_id": 154151998,
            "status": "Inactive",
            "updated_by": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            }
        }

Departments

        POST /departments

        Retrieve list of departments

        Example: http://localhost:3000/admin/departments

        Request body:

        { 
            "employee_id": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            }
        }

        POST /departments/add_department

        Create new department

        Example: http://localhost:3000/admin/departments/add_department

        Request body:

        { 
            "created_by": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "name": "Budget"
        }

        PUT /departments/update_department

        Update department details with the given id

        Example: http://localhost:3000/admin/departments/update_department

        Request body:

        { 
            "id": 7,
            "updated_by": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "name": "Budget",
            "status": "Inactive"
        }

Request Types

        POST /request_types

        Retrieve list of request types

        Example: http://localhost:3000/admin/request_types

        Request body:

        { 
            "employee_id": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            }
        }

        POST /request_types/add_request_type

        Create new request type

        Example: http://localhost:3000/admin/request_types/add_request_type

        Request body:

        { 
            "created_by": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "name": "Item",
            "has_time_in": true
        }

        PUT /request_types/update_request_type

        Update request type details with the given id

        Example: http://localhost:3000/admin/request_types/update_request_type

        Request body:

        { 
            "id": 6,
            "updated_by": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "name": "Item",
            "has_time_in": true,
            "status": "Active"
        }

Modules

        POST /modules

        Retrieve list of modules

        Example: http://localhost:3000/admin/modules

        Request body:

        { 
            "employee_id": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            }
        }

        POST /modules/add_module

        Create new module

        Example: http://localhost:3000/admin/modules/add_module

        Request body:

        { 
            "employee_id": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "description": "Moduletest"
        }

        PUT /modules/update_modules

        Update module details with the given id

        Example: http://localhost:3000/admin/modules/update_module

        Request body:

        { 
            "id": 5,
            "updated_by": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "description": "Moduletest",
            "status": "Inactive"
        }

Actions

        POST /actions

        Retrieve list of actions

        Example: http://localhost:3000/admin/actions

        Request body:

        {
            "employee_id": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            }
        }

        POST /actions/add_action

        Create new action

        Example: http://localhost:3000/admin/actions/add_action

        Request body:

        {
            "module_id": 5,
            "created_by": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "description": "Action Test"
        }

        PUT /actions/update_action

        Update action details with the given id

        Example: http://localhost:3000/admin/actions/update_action

        Request body:

        {
            "id": 19,
            "module_id": 5,
            "updated_by": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "description": "Action Test",
            "status" : "Inactive"
        }

Access Rights

        POST /access_rights

        Retrieve list of access rights

        Example: http://localhost:3000/admin/access_rights

        Request body:

        {
            "employee_id": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            }
        }

        POST /access_rights/add_access_rights

        Create new access right

        Example: http://localhost:3000/admin/access_rights/add_access_right

        Request body:

        {
            "created_by": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "action_id": 19,
            "role_id": 7
        }

        PUT /access_rights/update_access_rights

        Update access right details with the given id

        Example: http://localhost:3000/admin/access_rights/update_access_right

        Request body:

        {
            "id": 67,
            "updated_by": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            },
            "action_id": 19,
            "role_id": 7,
            "status" : "Inactive"
        }

Activity Logs

        POST /activity_logs

        Retrieve list of activity logs

        Example: http://localhost:3000/admin/activity_logs

        Request body: 

        {
            "employee_id": 154151100,
            "role": {
                "id": 1,
                "name": "admin"
            }
        }
``` 
      

### Employees

```js
    POST /schedule_out

    Retrieve list of employees personal pending and approved gate passes

    Example: http://localhost:3000/employees/schedule_out

    Request body:

    {
        "employee_id": 154151101,
        "modules": [
            {
                "id": 2,
                "description": "Gate Pass",
                "status": "Active",
                "actions": [
                    {
                        "id": 2,
                        "description": "View schedule out",
                        "module_id": 2,
                        "status": "Active"
                    }
                ]
            }
        ]
    }

    POST /employee_report

    Retrieve list of employees gate pass report 

    Example: http://localhost:3000/employees/employee_report

    Request body:

     {
        "employee_id": 154151101,
        "modules": [
            {
                "id": 4,
                "description": "Report",
                "status": "Active",
                "actions": [
                    {
                        "id": 9,
                        "description": "View gate pass report",
                        "module_id": 4,
                        "status": "Active"
                    }
                ]
            }
        ]
    }

    POST /:id

    Retrieve employee details with the given id

    Example: http://localhost:3000/employees/154151101

    Request body:

    {
        "employee_id": 154151101,
        "modules": [
            {
                "id": 4,
                "description": "Report",
                "status": "Active",
                "actions": [
                    {
                        "id": 11,
                        "description": "View employee profile",
                        "module_id": 4,
                        "status": "Active"
                    }
                ]
            }
        ]
    }
```

### Gate Pass

```js
    POST /file_request

    Request for a gatepass

    Example: http://localhost:3000/gatepass/file_request

    Request body:

    {
        "employee_id": 154151101,
        "modules": [
            {
                "id": 2,
                "description": "Gate Pass",
                "status": "Active",
                "actions": [
                    {
                        "id": 1,
                        "description": "Request gate pass",
                        "module_id": 2,
                        "status": "Active"
                    }
                ]
            }
        ],
        "request_type_id": 1,
        "request_time_out": "2019-08-16 15:00:00",
        "request_time_in": "2019-08-16 16:00:00",
        "request_date": "2019-08-16",
        "reason": "Lunch Out"
    }

    PUT /requests/validate_request/approve

    Approve or pre-approve pending gatepass

    Example: http://localhost:3000/gatepass/requests/validate_request/approve

    Request body:

    {
        "id": 1,
        "employee_id": 154151100,
        "modules": [
            {
                "id": 3,
                "description": "Request",
                "status": "Active",
                "actions": [
                    {
                        "id": 7,
                        "description": "Approve request",
                        "module_id": 3,
                        "status": "Active"
                    }
                ]
            }
        ]
    }	

    PUT /requests/validate_request/disapprove

    Disapprove pending or approved gatepass

    Example: http://localhost:3000/gatepass/requests/validate_request/disapprove

    Request body:

   {
        "id": 1,
        "status": "Disapproved",
        "employee_id": 154151100,
        "modules": [
            {
                "id": 3,
                "description": "Request",
                "status": "Active",
                "actions": [
                    {
                        "id": 8,
                        "description": "Disapprove request",
                        "module_id": 3,
                        "status": "Active"
                    }
                ]
            }
        ]
    }

    PUT /requests/validate_request/verify_personal_out

    Verify personal out gatepass

    Example: http://localhost:3000/gatepass/requests/validate_request/verify_personal_out

    Request body:

    {
        "id": 2,
        "status": "Verified",
        "employee_id": 154151100,
        "modules": [
            {
                "id": 3,
                "description": "Request",
                "status": "Active",
                "actions": [
                    {
                        "id": 13,
                        "description": "Verify personal out gate pass",
                        "module_id": 3,
                        "status": "Active"
                    }
                ]
            }
        ]
    }	

    PUT /requests/validate_request/unverify_personal_out

    Unverify verified gatepass

    Example: http://localhost:3000/gatepass/requests/validate_request/unverify_personal_out

    Request body:

    {
        "id": 2,
        "status": "Time In",
        "employee_id": 154151100,
        "modules": [
            {
                "id": 3,
                "description": "Request",
                "status": "Active",
                "actions": [
                    {
                        "id": 17,
                        "description": "Unverify personal out gate pass",
                        "module_id": 3,
                        "status": "Active"
                    }
                ]
            }
        ]
    }
    
    PUT /requests/validate_request/process

    Process verified gatepass

    Example: http://localhost:3000/gatepass/requests/validate_request/process

    Request body:

    {
        "id": 2,
        "status": "Processed",
        "employee_id": 154151100,
        "modules": [
            {
                "id": 3,
                "description": "Request",
                "status": "Active",
                "actions": [
                    {
                        "id": 17,
                        "description": "Unverify personal out gate pass",
                        "module_id": 3,
                        "status": "Active"
                    }
                ]
            }
        ]
    }

    PUT /reports/update_gate_pass

    Update gatepass details with the given id

    Example: http://localhost:3000/gatepass/reports/update_gate_pass

    Request body:

    {
        "id": 1,
        "request_type_id": 1,
        "actual_time_out": "2019-08-16 15:00:00",
        "actual_time_in": "2019-08-16 16:00:00",
        "approval_time": "2019-08-16 15:30:00"
        "request_date": "2019-08-16",
        "employee_id": 154151101,
        "modules": [
            {
                "id": 4,
                "description": "Report",
                "status": "Active",
                "actions": [
                    {
                        "id": 9,
                        "description": "View gate pass report",
                        "module_id": 4,
                        "status": "Active"
                    },
                    {
                        "id": 10,
                        "description": "View employees gate pass report",
                        "module_id": 4,
                        "status": "Active"
                    },
                    {
                        "id": 11,
                        "description": "View employee profile",
                        "module_id": 4,
                        "status": "Active"
                    }
                ]
            }
        ]
    }

    PUT /cancel_request

    Cancel pending gatepass request

    Example: http://localhost:3000/gatepass/cancel_request

    Request body:

    {
        "id": 1,
        "status": "Cancelled",
        "employee_id": 154151101,
        "modules": [
            {
                "id": 3,
                "description": "Request",
                "status": "Active",
                "actions": [
                    {
                        "id": 16,
                        "description": "Cancel gate pass",
                        "module_id": 3,
                        "status": "Active"
                    }
                ]
            }
        ]
    }

    POST /notifications

    Retrieve notifications

    Example: http://localhost:3000/gatepass/notifications

    Request body:
    
    {
        "employee_id": 154151101
    }	

    PUT /notifications/read

    Update gate pass as read 

    Example: http://localhost:3000/gatepass/notifications/read

    Request body:

    {
        "employee_id": 154151101
    }	

    POST /request/import

    Create gate passes based on the imported excel file

    Example: http://localhost:3000/gatepass/request/import

    Request body:

    {
        "employee_id": 154151101,
        "path": "folder/file.xlsx",
        "modules": [
        {
            "id": 2,
            "description": "Gate Pass",
            "status": "Active",
            "actions": [
                {
                    "id": 48,
                    "description": "Import gate pass",
                    "module_id": 2,
                    "status": "Active"
                }
            ]
        }
        ]
    }	

```

### Login
```js

    POST /login

    Login to the system

    Example:  http://localhost:3000/login

    Request body:

    {
        "id":154151101,
        "password":"emp"
    }
```

### Reports
```js

    POST /employees_report/:roleParam

    Retrieve list of employees gate pass reports based on user role

    Example: http://localhost:3000/reports/employees_report/dh

    Request body:

    {
        "employee_id": 154151101,
        "modules": [
            {
                "id": 4,
                "description": "Report",
                "status": "Active",
                "actions": [
                    {
                        "id": 9,
                        "description": "View gate pass report",
                        "module_id": 4,
                        "status": "Active"
                    },
                    {
                        "id": 10,
                        "description": "View employees gate pass report",
                        "module_id": 4,
                        "status": "Active"
                    },
                    {
                        "id": 11,
                        "description": "View employee profile",
                        "module_id": 4,
                        "status": "Active"
                    }
                ]
            }
        ]
    }

    POST /view_personal_out

    Retrieve list of employees personal out gate passes based on user role

    Example: http://localhost:3000/reports/view_personal_out

    Request body: 

    {
        "employee_id": 154151100,
        "modules": [
            {
                "id": 4,
                "description": "Report",
                "status": "Active",
                "actions": [
                    {
                        "id": 14,
                        "description": "View personal out gate pass",
                        "module_id": 4,
                        "status": "Active"
                    }
                ]
            }
        ]
    }	

    POST /view_verified_personal_out

    Retrieve list of employees verified gate passes based on user role

    Example: http://localhost:3000/reports/view_verified_personal_out

    Request body:

    {
        "employee_id": 154151100,
        "modules": [
            {
                "id": 4,
                "description": "Report",
                "status": "Active",
                "actions": [
                    {
                        "id": 12,
                        "description": "View verified gate pass",
                        "module_id": 4,
                        "status": "Active"
                    }
                ]
            }
        ]
    }	
```

### Requests
```js

    POST /viewApproved/:roleParams

    Retrieve list of employees approved gate passes based on user role

    Example: http://localhost:3000/requests/viewApproved/dh

    Request body:

   {
        "employee_id": 154151101,
        "modules": [
            {
                "id": 3,
                "description": "Request",
                "status": "Active",
                "actions": [
                    {
                        "id": 6,
                        "description": "View requests",
                        "module_id": 3,
                        "status": "Active"
                    }
                ]
            }
        ]
    }

    POST /viewPending/:roleParams

    Retrieve list of employees pending gate passes based on user role

    Example: http://localhost:3000/requests/viewPending/dh

    Request body:

     {
        "employee_id": 154151101,
        "modules": [
            {
                "id": 3,
                "description": "Request",
                "status": "Active",
                "actions": [
                    {
                        "id": 6,
                        "description": "View requests",
                        "module_id": 3,
                        "status": "Active"
                    }
                ]
            }
        ]
    }

    POST /viewDisapproved/:roleParams

    Retrieve list of employees disapproved gate passes based on user role

    Example: http://localhost:3000/requests/viewDisapproved/dh

    Request body: 

    {
        "employee_id": 154151101,
        "modules": [
            {
                "id": 3,
                "description": "Request",
                "status": "Active",
                "actions": [
                    {
                        "id": 6,
                        "description": "View requests",
                        "module_id": 3,
                        "status": "Active"
                    }
                ]
            }
        ]
    }	
```

### Security Guard

```js
    POST /viewApproved

    Retrieve list of all approved gate passes based on current day

    Example: http://localhost:3000/securityguard/viewApproved

    Request body:

    {
        "employee_id": 154151105,
        "modules": [
            {
                "id": 2,
                "description": "Gate Pass",
                "status": "Active",
                "actions": [
                    {
                        "id": 3,
                        "description": "View approved gate pass",
                        "module_id": 2,
                        "status": "Active"
                    }
                ]
            }
        ]
    }	

    POST /viewTimeout

    Retrieve list of all time out gate passes based on current day

    Example: http://localhost:3000/securityguard/viewTimeout

    Request body:

    {
        "employee_id": 154151105,
        "modules": [
            {
                "id": 2,
                "description": "Gate Pass",
                "status": "Active",
                "actions": [
                    {
                        "id": 15,
                        "description": "View time out gate pass",
                        "module_id": 2,
                        "status": "Active"
                    }
                ]
            }
        ]
    }


    PUT /gatepass/put_time

    Update gatepass status to time in/out 

    Example: http://localhost:3000/securityguard/gatepass/put_time

    Request body: 

    {
        "id": 1,
        "employee_id": 154151105,
        "modules": [
            {
                "id": 2,
                "description": "Gate Pass",
                "status": "Active",
                "actions": [
                    {
                        "id": 5,
                        "description": "Mark time",
                        "module_id": 2,
                        "status": "Active"
                    }
                ]
            }
        ]
    }
	
```