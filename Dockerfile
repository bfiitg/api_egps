FROM node:10-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm config set registry http://registry.npmjs.org/

RUN npm config set http-proxy http://172.16.1.6:3128

RUN npm config set https-proxy http://172.16.1.6:3128

RUN npm config set proxy http://172.16.1.6:3128

#RUN npm config rm proxy
#RUN npm config rm https-proxy

RUN npm install

RUN npm audit fix

COPY . .

RUN  chmod -R 777 src/employee_files

CMD [ "npm", "start" ]
